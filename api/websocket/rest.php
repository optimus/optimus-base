<?php
function optimus_rest()
{
	global $input, $current_user;
	
	if ($input->method == "GET" AND $input->data)
		$input->endpoint .= "?data=" . json_encode($input->data);
							
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $input->endpoint);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array("Cookie: token=" . $current_user['token']));
	if ($input->method == "POST")
		curl_setopt($curl, CURLOPT_POST, true);
	else if ($input->method == "PATCH")
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
	else if ($input->method == "DELETE")
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
	if ($input->method != "GET" AND $input->data)
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($input->data));
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
	curl_setopt($curl, CURLOPT_TIMEOUT, 5);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
	$response = curl_exec($curl);
	$http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	if (curl_errno($curl))
	{
		$message = array
		(
			"code" => ($http_status ? $http_status : 400),
			"id" => $input->id,
			"command" => "rest",
			"message" => curl_error($curl)
		);
		fwrite($current_user['socket'], nomask(json_encode($message)));
	}
	else
	{
		$response = json_decode($response);
		$message = array
		(
			"code" => $http_status,
			"message" => $response->message,
			"id" => $input->id,
			"command" => "rest",
			"data" => $response->data
		);
		fwrite($current_user['socket'], nomask(json_encode($message)));
	}
}
?>