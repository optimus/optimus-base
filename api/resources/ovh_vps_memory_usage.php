<?php
include('/srv/api/libs/ovh.php');

$get = function ()
{
	auth();
	allowed_origins_only();
	admin_only();

	$memory_usage = ovh_api_request('GET', '/vps/' . getenv('OVH_VPS') . '.vps.ovh.net/monitoring?period=lastday&type=mem%3Aused', $data = '');
	
	return array("code" => 200, "data" => $memory_usage);
};
?>
