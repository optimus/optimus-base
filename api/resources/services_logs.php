<?php
$get = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	$input->id = check('id', $input->path[2], 'module', true);

	if (!exists($connection, 'server', 'services', 'name', $input->id))
		return array("code" => 404, "message" => "Ce service n'existe pas");
	
	$fp = fsockopen('unix:///var/run/docker.sock', -1, $errno, $errstr, 30);
	if (!$fp)
		return array("code" => 500, "message" => "Error " . $errno . " : . $errstr");

	$out = "GET /v1.41/containers/" . $input->id . "/logs?&stderr=true&tail=100&timestamps=true HTTP/1.1\r\n";
	$out .= "Host: localhost\r\n";
	$out .= "Connection: Close\r\n";
	$out .= "\r\n";

	fwrite($fp, $out);
	while (!feof($fp))
		$output .= fgets($fp, 128);

	$output = explode("\r\n\r\n", $output);
	$output_header = $output[0];
	$response_code = explode(' ', $output_header)[1];
	$output_body = $output[1];

	$output_body = str_replace(chr(0),'',$output_body);
	$output_body = str_replace(chr(2),'',$output_body);
	$output_body = str_replace(chr(5),'',$output_body);
	$output_body = str_replace(chr(23),'',$output_body);
	$output_body = str_replace(chr(27),'',$output_body);
	$output_body = str_replace("[0m",'',$output_body);
	$output_body = str_replace("[32m",'',$output_body);
	$output_body = preg_replace("/\r\n[a-zA-Z0-9]{2}\r\n/",'',$output_body);
	$output_body = preg_replace("/\r\n[a-zA-Z0-9]{1}\r\n/",'',$output_body);
	$output_body = explode("\n", $output_body);
	unset($output_body[0]);
	unset($output_body[sizeof($output_body)]);
	unset($output_body[sizeof($output_body)]);

	for ($i=2; $i <= sizeof($output_body); $i++)
		if (substr($output_body[$i], 1))
			$clean_output[] = substr($output_body[$i], 1);
		else
			$clean_output[] = '';

	$output_body = $clean_output;

	fclose($fp);
	return array("code" => $response_code, "data" => $output_body);
};
?>