<?php
include('/srv/api/libs/ovh.php');

$get = function ()
{
	global $connection, $input;
	
	auth();
	allowed_origins_only();
	admin_only();

	$input->domain = check('domain', $input->path[2], 'domain', true);

	$smtp = $connection->query("SELECT value FROM `server`.`preferences` WHERE id = 'smtp'")->fetch(PDO::FETCH_OBJ);
	$smtp_password = substr(base64_encode(openssl_encrypt('ovh_smtp_' . $input->domain, 'aes-128-ecb', getenv('AES_KEY'))), 0, 24);

	
	$email_plan = ovh_api_request('GET', '/email/domain/' . $input->domain);
	$email_plan->domain = ovh_api_request('GET', '/domain/' . $input->domain . '/serviceInfos');
	$email_plan->domain->age = (strtotime(date('Y-m-d')) - strtotime($email_plan->domain->creation . ' 00:00:00'));
	$email_plan->server_ip = file_get_contents('https://ipinfo.io/ip');
	$email_plan->smtp = ($smtp ? $smtp->value : 'local');
	if ($smtp->value == 'ovh')
		$email_plan->smtp_password = $smtp_password;
	$email_plan->dkim = ovh_api_request('GET', '/email/domain/' . $input->domain . '/dkim');
	$email_plan->quota = ovh_api_request('GET', '/email/domain/' . $input->domain . '/quota');
	$email_plan->summary = ovh_api_request('GET', '/email/domain/' . $input->domain . '/summary');
	
	return array("code" => 200, "data" => $email_plan);
};
?>
