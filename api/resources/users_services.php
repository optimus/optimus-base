<?php
$get = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	$input->id = check('id', $input->path[2], 'strictly_positive_integer', true);

	$services = $connection->query("SELECT name, status, JSON_VALUE(manifest, '$.displayname') as displayname, JSON_VALUE(manifest, '$.version_display') as version_display, JSON_EXTRACT(manifest, '$.managed_resources') as managed_resources FROM `server`.`services`, `server`.`users_services` WHERE `users_services`.`service` = `services`.`name` AND `users_services`.user = '" . $input->id . "'")->fetchAll(PDO::FETCH_ASSOC);
	for($i=0; $i < sizeof($services); $i++)
		$services[$i]['managed_resources'] = json_decode($services[$i]['managed_resources']);
	return array("code" => 200, "data" => $services);
};
?>