<?php
$post = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	$input->id = check('id', $input->path[2], 'module', true);

	if (!exists($connection, 'server', 'services', 'name', $input->id))
		return array("code" => 404, "message" => "Ce service n'existe pas");
	
	include('libs/docker_socket.php');
	$result = docker_socket_request("POST /v1.41/containers/" . $input->id . "/restart");

	for ($i = 0; $i < 180; $i++)
	{
		$running = $connection->prepare("SELECT name FROM `server`.`services` WHERE name = :container_name AND status = 1");
		$running->bindParam(':container_name', $input->id, PDO::PARAM_STR);
		$running->execute();
		if ($running->rowCount() > 0 AND ($result['code'] == 204 OR $result['code'] == 304))
			return array("code" => 200, "data" => $result['body']);
		else
			sleep(1);
	}
	return array("code" => 400, "Le service n'a pas pu être redémarré");
};
?>