<?php
include('/srv/api/libs/ovh.php');

$post = function ()
{
	auth();
	allowed_origins_only();
	admin_only();

	if (!getenv('OVH_APP_KEY'))
		return array("code" => 400, "message" => "Cette fonction n'est disponible que si des identifiants OVH sont renseignés");

	$response = ovh_api_request('POST', '/vps/' . getenv('OVH_VPS') . '.vps.ovh.net/reboot');
	if ($response->state == 'todo')
		return array("code" => 200, "message" => "Redémarrage en cours");
	else
		return array("code" => intval($response->httpCode), "message" => $response->message);
};
?>
