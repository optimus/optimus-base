<?php
include_once 'libs/datatables.php';
$resource = json_decode('
{
	"id": { "type": "strictly_positive_integer", "field": "users.id", "post": ["ignored"], "patch": ["immutable"], "default": 0 },
	"type": { "type": "strictly_positive_integer", "field": "users.type", "default": 1 },
	"server": { "type": "domain", "field": "users.server", "default": null },
	"status": { "type": "boolean", "field": "users.status", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": 0 },
	"admin": { "type": "boolean", "field": "users.admin", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": 0 },
	"email": { "type": "email", "field": "users.email", "post": ["notnull", "notempty"], "patch": ["notnull", "notempty"], "default": "" },
	"firstname": { "type": "string", "field": "users.firstname", "default": 0 },
	"lastname": { "type": "string", "field": "users.lastname", "default": 0 },
	"password": { "type": "password", "field": "users.password", "post": ["notnull", "notempty"], "patch": ["notnull", "notempty"], "default": 0 },
	"displayname" : { "type" : "string", "field": "users.displayname" }
}
', null, 512, JSON_THROW_ON_ERROR);

$get = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->id = check('id', $input->path[2], 'strictly_positive_integer', false);
	

	//REQUETE SUR UN UTILISATEUR IDENTIFIÉ
	if (isset($input->id))
	{
		$input->body = json_decode('{"references": true, "filter": [{"field": "id", "type": "=", "value": ' . $input->id . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'server', 'users');
		if (sizeof($results) == 0)
			return array("code" => 404, "message" => "Cet utilisateur n'existe pas");
		else
		{
			unset($results[0]['password']);
			return array("code" => 200, "data" => sanitize($resource, $results[0]));
		}
	}
	//REQUETE SUR TOUS LES UTILISATEURS AU FORMAT DATATABLES
	else 
	{	
		$results = datatable_request($connection, $resource, 'server', 'users');
		$last_row = (int)$connection->query('SELECT FOUND_ROWS()')->fetchColumn();
		$last_page = $input->body->size > 0 ? ceil(max($last_row,1) / $input->body->size) : 1;
		for($i=0; $i < sizeof($results); $i++)
			unset($results[$i]['password']);
		return array("code" => 200, "data" => sanitize($resource, $results), "last_row" => $last_row, "last_page" => $last_page);
	}
};

$post = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();
	admin_only();

	if (isset($input->body->email))
		$input->body->email = strtolower($input->body->email);
	else
	{
		$new_id = $connection->query("SELECT auto_increment FROM INFORMATION_SCHEMA.TABLES WHERE table_schema = 'server' AND table_name = 'users'")->fetch(PDO::FETCH_OBJ);
		if (!isset($input->body->type))
			$input->body->email = 'user_' . $new_id->auto_increment . '@' . getenv('DOMAIN');
		else
			$input->body->email = 'business_' . $new_id->auto_increment . '@' . getenv('DOMAIN');
		
		if (exists($connection, 'server','users', 'email', $input->body->email))
			$input->body->email = uniqid() . '@' . getenv('DOMAIN');
	}

	if (!isset($input->body->password))
		$input->body->password = '1aA!' . uniqid();
	$input->body->password = password_hash($input->body->password, PASSWORD_BCRYPT);

	check_input_body($resource, 'post');

	if (exists($connection, 'server','users', 'email', $input->body->email))
		return array("code" => 409, "message" => "Erreur - un utilisateur avec cette adresse email existe déjà");
	
	$query = datatables_insert($connection, $resource, 'server', 'users');
	if($query->execute())
	{
		$new_id = $connection->lastInsertId();
		$connection->query('CREATE DATABASE `user_' . $new_id . '` CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci');

		if(!$connection->query("USE `user_" . $new_id . "`"))
		$errors[] = $connection->errorInfo()[2];
		$sql_files = array_diff(scandir('/srv/sql/user'), array('..', '.'));
		foreach($sql_files as $sql_file)
		{
			$sql = file_get_contents("/srv/sql/user/" . $sql_file);
			$sql = explode(';',$sql);
			foreach ($sql as $instruction)
				if($instruction!='')
					if (!$connection->query($instruction))
						$errors[] = $connection->errorInfo()[2];
		}
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $new_id . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'server', 'users');
		unset($results[0]['password']);
		return array("code" => 201, "data" => sanitize($resource, $results[0]), "message" => "Utilisateur créé avec succès");
	}
	else
		return array("code" => 400, "message" => $query->errorInfo()[2]);
};


$patch = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->id = check('id', $input->path[2], 'strictly_positive_integer', true);

	if (!is_admin($input->user->id) AND isset($input->body->firstname))
		return array("code" => 400, "message" => "Seuls les administrateurs peuvent modifier le prénom d'un utilisateur");

	if (!is_admin($input->user->id) AND isset($input->body->lastname))
		return array("code" => 400, "message" => "Seuls les administrateurs peuvent modifier le nom d'un utilisateur");

	if (!is_admin($input->user->id) AND isset($input->body->admin))
		return array("code" => 400, "message" => "Seuls les administrateurs peuvent modifier le statut admin d'un utilisateur");

	if (!is_admin($input->user->id) AND isset($input->body->status))
		return array("code" => 400, "message" => "Seuls les administrateurs peuvent modifier le statut actif d'un utilisateur");

	if ($input->id != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul un administrateur ou l'utilisateur lui même peuvent modifier un utilisateur");

	$admin_exists = $connection->query("SELECT * FROM `server`.`users` WHERE admin = 1");
	$modified_user = $connection->query("SELECT * FROM `server`.`users` WHERE id = '" . $input->id . "'")->fetch(PDO::FETCH_OBJ);
	if ($admin_exists->rowCount()==1 AND $modified_user->admin == 1 AND $input->body->admin == 'false')
		return array("code" => 400, "message" => "Cet utilisateur est le dernier administrateur actif. Il n'est pas possible de lui retirer ce statut.");
	if ($admin_exists->rowCount()==1 AND $modified_user->admin == 1 AND $input->body->status == 'false')
		return array("code" => 400, "message" => "Cet utilisateur est le dernier administrateur actif. Il n'est pas possible de le désactiver.");

	if ($input->id == 1 AND $_SERVER['SERVER_NAME'] == 'api.demoptimus5.ovh')
		return array("code" => 401, "message" => "Le mot de passe du compte demo ne peut pas être modifié");
	
	check_input_body($resource, 'patch');

	if (isset($input->body->password))
		$input->body->password = password_hash($input->body->password, PASSWORD_BCRYPT);

	if (isset($input->body->email))
	{
		$input->body->email = strtolower($input->body->email);
		$domains = $connection->query("SELECT domain FROM `server`.`domains`")->fetchAll(PDO::FETCH_COLUMN, 0);
		if (!in_array(explode('@',$input->body->email)[1], $domains))
			return array("code" => 400, "message" => "Erreur - Le nom de domaine " . explode('@',$input->body->email)[1] . " n'est pas géré par ce serveur");
	}

	if (!exists($connection, 'server','users', 'id', $input->id))
		return array("code" => 404, "message" => "Erreur - cet utilisateur n'existe pas");
	
	if (exists($connection, 'server','users', 'email', $input->body->email))
		return array("code" => 409, "message" => "Erreur - un utilisateur avec cette adresse email existe déjà");

	//TRANSMIT EMAIL CHANGE TO optimus-mail and optimus-drive (they should not rely on email but user id instead)
	if (isset($input->body->email) && $input->body->email != get_user_email($input->id))
	{
		$response = rest('https://api.' . getenv('DOMAIN') . '/optimus-mail/' . $input->id . '/service', 'PATCH', json_encode(array('email' => $input->body->email)), $_COOKIE['token']);
		$response = rest('https://api.' . getenv('DOMAIN') . '/optimus-drive/' . $input->id . '/service', 'PATCH', json_encode(array('email' => $input->body->email)), $_COOKIE['token']);
	}

	$query = datatables_update($connection, $resource, 'server', 'users', $input->id);
	if($query->execute())
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $input->id . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'server', 'users');
		unset($results[0]['password']);
		
		return array("code" => 200, "data" => sanitize($resource, $results[0]), "message" => "Utilisateur modifié avec succès");
	}
	else
		return array("code" => 400, "message" => $query->errorInfo()[2]);
};

$delete = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	$input->id = check('id', $input->path[2], 'strictly_positive_integer', false);
	$input->email = get_user_email($input->id);

	$admin_exists = $connection->query("SELECT * FROM `server`.`users` WHERE admin = 1");
	$modified_user = $connection->query("SELECT * FROM `server`.`users` WHERE id = '" . $input->id . "'")->fetch(PDO::FETCH_OBJ);
	if ($admin_exists->rowCount()==1 AND $modified_user->admin == 1)
		return array("code" => 400, "message" => "Cet utilisateur est le dernier administrateur actif. Il n'est pas possible de le supprimer.");

	if ($modified_user->id == $input->user->id)
		return array("code" => 400, "message" => "Vous ne pouvez pas vous supprimer vous même.");

	if ($input->id == 1 AND $_SERVER['SERVER_NAME'] == 'api.demoptimus5.ovh')
		return array("code" => 400, "message" => "Le compte demo ne peut pas être supprimé");

	if (is_dir('/srv/files/' . $input->email))
		exec('rm -r /srv/files/' . $input->email);
	
	if (is_dir('/srv/mailboxes/' . $input->email))
		exec('rm -r /srv/mailboxes/' . $input->email);
	
	$delete = $connection->prepare("DELETE FROM `server`.`authorizations` WHERE user = '" . $input->id . "' OR owner = '" . $input->id . "'");
	if (!$delete->execute()) return array("code" => 400, "message" => $delete->errorInfo()[2]);
	$delete = $connection->prepare("DELETE FROM `server`.`users` WHERE id = '" . $input->id . "'");
	if (!$delete->execute()) return array("code" => 400, "message" => $delete->errorInfo()[2]);
	$delete = $connection->prepare("DELETE FROM `server`.`users_services` WHERE user = '" . $input->id . "'");
	if (!$delete->execute()) return array("code" => 400, "message" => $delete->errorInfo()[2]);
	$delete = $connection->prepare("DELETE FROM `server`.`users_servers` WHERE user = '" . $input->id . "'");
	if (!$delete->execute()) return array("code" => 400, "message" => $delete->errorInfo()[2]);
	$delete = $connection->prepare("DROP DATABASE IF EXISTS `user_" . $input->id . "`");
	if (!$delete->execute()) return array("code" => 400, "message" => $delete->errorInfo()[2]);
	return array("code" => 200);
};
?>
