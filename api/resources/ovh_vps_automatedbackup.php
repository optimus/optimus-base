<?php
include('/srv/api/libs/ovh.php');

$get = function ()
{
	auth();
	allowed_origins_only();
	admin_only();

	$ovh_backup = ovh_api_request('GET', '/vps/' . getenv('OVH_VPS') . '.vps.ovh.net/automatedBackup', $data = '');
	if ($ovh_backup->state == "enabled")
	{
		$status['enabled'] = true;
		$ovh_restore_points = ovh_api_request('GET', '/vps/' . getenv('OVH_VPS') . '.vps.ovh.net/automatedBackup/restorePoints?state=available', $data = '');
		$status['restorepoints'] = $ovh_restore_points;
	}
	else
		$status['enabled'] = false;
	
	return array("code" => 200, "data" => $status);
};
?>
