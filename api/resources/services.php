<?php
use optimus\OptimusWebsocket;
$get = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	$input->id = check('id', $input->path[2], 'module', false);

	include('libs/docker_socket.php');

	if ($input->id)
	{
		$query = $connection->prepare("SELECT name, status, manifest FROM `server`.`services` WHERE name = :name");
		$query->bindParam(":name", $input->id);
	}
	else
		$query = $connection->prepare("SELECT name, status, manifest FROM `server`.`services`");
	
	$query->execute();

	while ($installed_service = $query->fetch(PDO::FETCH_OBJ))
	{
		$service = json_decode($installed_service->manifest, false);
		$service->status = (int)$installed_service->status;
			
		$container = docker_socket_request("GET /v1.41/containers/" . $installed_service->name . "/json");
		$image = $container['body']->Config->Image;
		$service->registry = explode('/', $image)[0];
		$service->group = explode('/', $image)[1];
		$service->version = explode(':', end(explode('/', $image)))[0];
		$service->branch = end(explode(':', $image));
		
		if ($_GET['check_for_updates'] == "true")
		{
			date_default_timezone_set('UTC');
			$local_version = strtotime($service->version_date);
			$remote_version = json_decode(file_get_contents($service->registry . '/tags/' . $service->branch));
			$remote_version = strtotime($remote_version->created_at);
			$service->available_update = ($remote_version > $local_version + 60) ? true : false;
		}
			$installed_services[] = $service;
	}
	if ($input->id)
		return array("code" => 200, "data" => $installed_services[0]);
	else
		return array("code" => 200, "data" => $installed_services);
};

$post = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	$registry = check('image_path', $input->body->registry, 'origin', true);
	$group = check('image_path', $input->body->group, 'module', true);
	$project = check('image_path', $input->body->project, 'module', true);
	$version = check('image_path', $input->body->version, 'module', true);
	$branch = check('image_path', $input->body->branch, 'module', true);
	$image_path = $registry . '/' . $group . '/' . $project . '/' . $version . ':' . $branch;

	include('libs/docker_socket.php');

	$websocket = new OptimusWebsocket($input->user->id, $input->user->email, is_admin($input->user->id));

	//TELECHARGE L'IMAGE
	$websocket->send(json_encode(array("command" => "service_install", "user" => "admin", "data" => array("service" => $project, "branch" => $branch, "progress" => 0))));
	$response = docker_socket_request("POST /v1.41/images/create?fromImage=" . $image_path);
	if ($response['code'] != 200)
		return array("code" => $response['code'], "message" => "Téléchargement de la nouvelle image : " . $response['body']->message);
	
	//CREE UN CONTENEUR TEMPORAIRE LE TEMPS D'EXTRAIRE LE FICHIER MANIFEST
	$websocket->send(json_encode(array("command" => "service_install", "user" => "admin", "data" => array("service" => $project, "branch" => $branch, "progress" => 10))));
	$uuid = uniqid();
	$response = docker_socket_request("POST /v1.41/containers/create?name=" . $uuid, json_encode(array("Image" => $image_path)));
	if ($response['code'] != 201)
		return array("code" => $response['code'], "message" => "Création d'un conteneur temporaire avec la nouvelle image : " . $response['body']->message);
	
	//LIT LE FICHIER MANIFEST.JSON DU CONTENEUR TEMPORAIRE
	$websocket->send(json_encode(array("command" => "service_install", "user" => "admin", "data" => array("service" => $project, "branch" => $branch, "progress" => 20))));
	$response = docker_socket_request("GET /v1.41/containers/" . $uuid . "/archive?path=/srv/manifest.json");
	if ($response['code'] != 200)
	{
		docker_socket_request("DELETE /v1.41/containers/" . $uuid);
		return array("code" => $response['code'], "message" => "Extraction du fichier manifest.json : " . $response['body']->message);
	}
	$manifest = $response['body'];

	//SUPPRIME LE CONTENEUR PROVISOIRE
	$response = docker_socket_request("DELETE /v1.41/containers/" . $uuid);
	if ($response['code'] != 404 AND $response['code'] != 204)
		return array("code" => $response['code'], "message" => "Suppression du conteneur provisoire : " . $response['body']->message);

	//VERIFICATION DU FICHIER MANIFEST
	if (!$manifest->name || !$manifest->version_date)
		return array("code" => 400, "message" => "Le fichier manifest.json de ce service est invalide");

	//VERIFICATION DES PREREQUIS
	$installed_services[] = 'optimus-databases';
	$services = $connection->query('SELECT name FROM server.services');
	while($service = $services->fetchObject())
		$installed_services[] = $service->name;
	$unmatched = array();
	foreach($manifest->prerequisites as $prerequisite)
		if (!in_array($prerequisite, $installed_services))
			$unmatched[] = $prerequisite;
	if (sizeof($unmatched) > 0)
		return array("code" => 400, "message" => "Les services suivants doivent être installés préalablement : " . implode(', ', $unmatched));

	foreach($manifest->env as &$env)
	{
		if ($env == 'MARIADB_ROOT_PASSWORD')
			$env = 'MARIADB_ROOT_PASSWORD=' . getenv('MARIADB_ROOT_PASSWORD');
		else if ($env == 'AES_KEY')
			$env = 'AES_KEY=' . getenv('AES_KEY');
		else if ($env == 'API_SHA_KEY')
			$env = 'API_SHA_KEY=' . getenv('API_SHA_KEY');
		else if ($env == 'DOMAIN')
			$env = 'DOMAIN=' . getenv('DOMAIN');
		else if ($env == 'OVH_APP_KEY')
			$env = 'OVH_APP_KEY=' . getenv('OVH_APP_KEY');
		else if ($env == 'OVH_SECRET_KEY')
			$env = 'OVH_SECRET_KEY=' . getenv('OVH_SECRET_KEY');
		else if ($env == 'OVH_CONSUMER_KEY')
			$env = 'OVH_CONSUMER_KEY=' . getenv('OVH_CONSUMER_KEY');
		else if ($env == 'OVH_VPS=$HOSTNAME')
			$env = 'OVH_VPS=' . getenv('OVH_VPS');
	}

	if ($branch == 'dev')
	{
		$manifest->volumes = array_merge($manifest->volumes, $manifest->dev->volumes);
		$manifest->env = array_merge($manifest->env, $manifest->dev->env);
	}

	$body = array(
		"Image" => $image_path,
		"HostConfig" => array(
			"RestartPolicy" => array('Name' => $manifest->restart_policy->name),
			"NetworkMode" => 'optimus',
			"Binds" => $manifest->volumes,
			"CapAdd" => $manifest->cap_add,
			"PortBindings" => $manifest->port_bindings,
			"LogConfig" => array(
				"Type" => "json-file",
				"Config" => array(
					"max-size" => "100m"
					)
			)
		),
		"Env" => $manifest->env,
		"User" => $manifest->user,
		"StopSignal" => $manifest->stop_signal
	);

	if ($manifest->name == 'optimus-base')
	{
		//SUPPRIME L'ANCIEN CONTENEUR S'IL EXISTE
		$websocket->send(json_encode(array("command" => "service_install", "user" => "admin", "data" => array("service" => $project, "branch" => $branch, "progress" => 30))));
		$response = docker_socket_request("DELETE /v1.41/containers/" . $manifest->name . '-old');
		if ($response['code'] != 404 AND $response['code'] != 204)
			return array("code" => $response['code'], "message" => "Suppression de l'ancien conteneur : " . $response['body']->message);

		//RENOMME LE CONTENEUR ACTUEL
		$response = docker_socket_request("POST /v1.41/containers/" . $manifest->name . "/rename?name=" . $manifest->name . "-old");
		if ($response['code'] != 404 AND $response['code'] != 204)
			return array("code" => $response['code'], "message" => "Renommage de l'ancien conteneur : " . $response['body']->message);

		//CREE LE NOUVEAU CONTENEUR
		$websocket->send(json_encode(array("command" => "service_install", "user" => "admin", "data" => array("service" => $project, "branch" => $branch, "progress" => 50))));
		$response = docker_socket_request("POST /v1.41/containers/create?name=" . $manifest->name, json_encode($body));
		if ($response['code'] != 201)
			return array("code" => $response['code'], "message" => "Création du nouveau conteneur : " . $response['body']->message);
	
		//DEMARRE LE NOUVEAU CONTENEUR
		$websocket->send(json_encode(array("command" => "service_install", "user" => "admin", "data" => array("service" => $project, "branch" => $branch, "progress" => 70))));
		$response = docker_socket_request("POST /v1.41/containers/" . $manifest->name . "/restart");
		if ($response['code'] != 404 AND $response['code'] != 204)
			return array("code" => $response['code'], "message" => "Démarrage du nouveau conteneur : " . $response['body']->message);

		//KILL L'ANCIEN CONTENEUR
		$websocket->send(json_encode(array("command" => "service_install", "user" => "admin", "data" => array("service" => $project, "branch" => $branch, "branch" => $branch, "progress" => 100, "manifest" => $manifest))));
		$response = docker_socket_request("POST /v1.41/containers/" . $manifest->name . '-old/kill');
		if ($response['code'] != 404 AND $response['code'] != 204)
			return array("code" => $response['code'], "message" => "Arrêt de l'ancien conteneur : " . $response['body']->message);
	}
	else if ($manifest->name == 'optimus-databases')
	{
		if (!$connection->query("UPDATE `server`.`services` SET status = 0 WHERE name = 'optimus-databases'"))
			return array("code" => 400, "message" => $connection->errorInfo()[2]);
		
		//STOP LE CONTENEUR S'IL EXISTE
		$websocket->send(json_encode(array("command" => "service_install", "user" => "admin", "data" => array("service" => $project, "branch" => $branch, "progress" => 33))));
		$response = docker_socket_request("POST /v1.41/containers/" . $manifest->name . '/stop');
		if ($response['code'] != 404 AND $response['code'] != 204 AND $response['code'] != 304)
			return array("code" => $response['code'], "message" => "Arrêt du conteneur actuel : " . $response['body']->message);

		//SUPPRIME LE CONTENEUR S'IL EXISTE
		$websocket->send(json_encode(array("command" => "service_install", "user" => "admin", "data" => array("service" => $project, "branch" => $branch, "progress" => 36))));
		$response = docker_socket_request("DELETE /v1.41/containers/" . $manifest->name);
		if ($response['code'] != 404 AND $response['code'] != 204)
			return array("code" => $response['code'], "message" => "Suppression du conteneur actuel : " . $response['body']->message);

		//CREE LE CONTENEUR
		$websocket->send(json_encode(array("command" => "service_install", "user" => "admin", "data" => array("service" => $manifest->name, "progress" => 39))));
		$response = docker_socket_request("POST /v1.41/containers/create?name=" . $manifest->name, json_encode($body));
		if ($response['code'] != 201)
			return array("code" => $response['code'], "message" => "Création du nouveau conteneur : " . $response['body']->message);
	
		//DEMARRE LE CONTENEUR
		$websocket->send(json_encode(array("command" => "service_install", "user" => "admin", "data" => array("service" => $project, "branch" => $branch, "progress" => 42))));
		$response = docker_socket_request("POST /v1.41/containers/" . $manifest->name . "/restart");
		if ($response['code'] != 204)
			return array("code" => $response['code'], "message" => "Démarrage du nouveau conteneur : " . $response['body']->message);

		sleep(3);

		$websocket->send(json_encode(array("command" => "service_install", "user" => "admin", "data" => array("service" => $project, "branch" => $branch, "progress" => 100, "manifest" => $manifest))));
		$connection = new PDO("mysql:host=optimus-databases", getenv('MARIADB_API_USER'), getenv('MARIADB_API_PASSWORD'));
		$update_service = $connection->prepare("REPLACE INTO `server`.`services` SET name = 'optimus-databases', status = 1, manifest = :manifest");
		$update_service->bindParam(':manifest', json_encode($manifest), PDO::PARAM_STR);
			if (!$update_service->execute())
				return array("code" => 400, "message" => $update_service->errorInfo()[2]);
		
		return array("code" => 201, "message" => "Le service a été installé avec succès");
	}
	else
	{
		//RECUPERE LES INFORMATIONS SUR LE CONTENEUR EN COURS D'EXECUTION
		$websocket->send(json_encode(array("command" => "service_install", "user" => "admin", "data" => array("service" => $project, "branch" => $branch, "progress" => 30))));
		$container = docker_socket_request("GET /v1.41/containers/" . $manifest->name . "/json");
		if ($response['code'] != 204)
			return array("code" => $response['code'], "message" => "Conteneur actuel : " . $response['body']->message);
		$image = $container['body']->Config->Image;
		
		//STOP LE CONTENEUR S'IL EXISTE
		$websocket->send(json_encode(array("command" => "service_install", "user" => "admin", "data" => array("service" => $project, "branch" => $branch, "progress" => 33))));
		$response = docker_socket_request("POST /v1.41/containers/" . $manifest->name . '/stop');
		if ($response['code'] != 404 AND $response['code'] != 204 AND $response['code'] != 304)
			return array("code" => $response['code'], "message" => "Arrêt du conteneur actuel : " . $response['body']->message);

		//SUPPRIME LE CONTENEUR S'IL EXISTE
		$websocket->send(json_encode(array("command" => "service_install", "user" => "admin", "data" => array("service" => $project, "branch" => $branch, "progress" => 36))));
		$response = docker_socket_request("DELETE /v1.41/containers/" . $manifest->name);
		if ($response['code'] != 404 AND $response['code'] != 204)
			return array("code" => $response['code'], "message" => "Suppression du conteneur actuel : " . $response['body']->message);

		//CREE LE CONTENEUR
		$websocket->send(json_encode(array("command" => "service_install", "user" => "admin", "data" => array("service" => $manifest->name, "progress" => 39))));
		$response = docker_socket_request("POST /v1.41/containers/create?name=" . $manifest->name, json_encode($body));
		if ($response['code'] != 201)
			return array("code" => $response['code'], "message" => "Création du nouveau conteneur : " . $response['body']->message);
	
		//DEMARRE LE CONTENEUR
		$websocket->send(json_encode(array("command" => "service_install", "user" => "admin", "data" => array("service" => $project, "branch" => $branch, "progress" => 42))));
		$response = docker_socket_request("POST /v1.41/containers/" . $manifest->name . "/restart");
		if ($response['code'] != 204)
			return array("code" => $response['code'], "message" => "Démarrage du nouveau conteneur : " . $response['body']->message);

		//SUPPRIME L'IMAGE PRECEDENTE DEVENUE INUTILE
		if ($image)
		{
			$websocket->send(json_encode(array("command" => "service_install", "user" => "admin", "data" => array("service" => $project, "branch" => $branch, "progress" => 45))));
			$response = docker_socket_request("DELETE /v1.41/images/" . $image);
			if ($response['code'] != 200 AND $response['code'] != 409)
				return array("code" => $response['code'], "message" => "Suppression de l'image du conteneur actuel : " . $response['body']->message);
		}
	
		for ($i = 0; $i < 180; $i++)
		{
			$installed = $connection->prepare("SELECT name, manifest FROM `server`.`services` WHERE name = :container_name AND status = 1");
			$installed->bindParam(':container_name', $manifest->name, PDO::PARAM_STR);
			$installed->execute();
			if ($installed->rowCount() > 0)
			{
				$installed = $installed->fetchObject();
				$websocket->send(json_encode(array("command" => "service_install", "user" => "admin", "data" => array("service" => $project, "branch" => $branch, "branch" => $branch, "progress" => 100, "manifest" => json_decode($installed->manifest)))));
				return array("code" => 201, "message" => "Le service a été installé avec succès");
			}
			else
				sleep(1);
		}

		return array("code" => 400, "message" => "L'installation a échoué");
	}
};

$delete = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	$input->id = check('id', $input->path[2], 'module', true);
	$delete_service_databases = check('delete_service_databases', $input->body->delete_service_databases, 'boolean', false);
	$delete_users_databases = check('delete_users_databases', $input->body->delete_users_databases, 'boolean', false);

	if ($input->id == 'optimus-base' || $input->id == 'optimus-databases')
		return array("code" => 400, "message" => "Le service " . $input->id . " ne peut pas être supprimé car il est indispensable au fonctionnement du système");

	$installed_services = $connection->query('SELECT * FROM server.services');
	while ($service = $installed_services->fetchObject())
	{
		$manifest = json_decode($service->manifest);
		if (in_array($input->id, $manifest->prerequisites))
			return array("code" => 400, "message" => "Ce service ne peut pas être supprimé car " . $manifest->name . " dépend de lui");
	}

	if ($_SERVER['SERVER_NAME'] == 'api.demoptimus5.ovh')
		return array("code" => 400, "message" => "Les services ne peuvent pas être supprimés sur le serveur de démo");

	$service_to_remove = $connection->prepare('SELECT * FROM server.services WHERE name = :name');
	$service_to_remove->bindParam(':name', $input->id, PDO::PARAM_STR);
		if (!$service_to_remove->execute())
			return array("code" => 400, "message" => $service_to_remove->errorInfo()[2]);
	$service_to_remove = $service_to_remove->fetchObject();
	$service_to_remove->manifest = json_decode($service_to_remove->manifest);

	include('libs/docker_socket.php');

	//SUPPRIME LES BASES DE DONNEES UTILISATEURS
	if ($delete_users_databases)
	{
		$users = $connection->query("SELECT id FROM server.users");
		while($user = $users->fetchObject())
		{
			$response = rest('https://api.' . getenv('DOMAIN') . '/' . $input->id . '/' . $user->id . '/service?remove_data=true', 'DELETE', null, $_COOKIE['token']);
			if ($response->code != 200)
				$errors[] = $response->message;
		}
	}

	//SUPPRIME LES BASES DE DONNEES DU SERVICE
	if ($delete_service_databases)
	{
		$sql = file_get_contents('https://api.' . getenv('DOMAIN') . '/' . $input->id . '/service_uninstall');
		$sql = explode(';', $sql);
		
		foreach ($sql as $instruction)
			if($instruction != '')
				if (!$connection->query($instruction))
					$errors[] = $connection->errorInfo()[2];
	}

	//DETERMINE L'IMAGE EN COURS D'EXECUTION
	$container_to_remove = docker_socket_request("GET /v1.41/containers/" . $input->id . "/json");
	$image_to_remove = $container_to_remove['body']->Config->Image;

	//STOPPE LE CONTENEUR
	$response = docker_socket_request("POST /v1.41/containers/" . $input->id . '/stop');
	if ($response['code'] != 404 AND $response['code'] != 204 AND $response['code'] != 304)
		return array("code" => $response['code'], "message" => $response['body']->message);

	//SUPPRIME LE CONTENEUR S'IL EXISTE
	$response = docker_socket_request("DELETE /v1.41/containers/" . $input->id);
	if ($response['code'] != 404 AND $response['code'] != 204)
		return array("code" => $response['code'], "message" => $response['body']->message);
	
	//SUPPRIME L'IMAGE
	if ($image_to_remove)
	{
		$response = docker_socket_request("DELETE /v1.41/images/" . $image_to_remove);
		if ($response['code'] != 200)
			return array("code" => $response['code'], "message" => $response['body']->message);
	}

	//SUPPRIME L'ENTREE DANS server.services
	$delete_service = $connection->prepare("DELETE FROM `server`.`services` WHERE name = :name");
	$delete_service->bindParam(':name', $input->id, PDO::PARAM_STR);
	if (!$delete_service->execute())
		return array("code" => 400, "message" => $delete_service->errorInfo()[2]);

	//NOTIFIE LES UTILISATEURS CONNECTES DE LA SUPPRESSION DU SERVICE
	$websocket = new OptimusWebsocket($input->user->id, $input->user->email, is_admin($input->user->id));
	$websocket->send(json_encode(array("command" => "service_uninstall", "user" => "admin", "data" => array("service" => $input->id))));

	if ($errors)
		return array("code" => 200, "errors" => $errors);
	else
		return array("code" => 200);
}
?>