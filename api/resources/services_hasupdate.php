<?php
$get = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	$input->id = check('id', $input->path[2], 'module', true);

	$query = $connection->prepare("SELECT JSON_VALUE(manifest, '$.registry') as registry, JSON_VALUE(manifest, '$.version_date') as version_date FROM `server`.`services` WHERE name = :name");
	$query->bindParam(":name", $input->id);
	$query->execute();
	
	if ($query->rowCount() == 0)
		return array("code" => 404, "message" => "Ce service n'existe pas");
	
	date_default_timezone_set('UTC');
	$service = $query->fetch(PDO::FETCH_OBJ);
	$service->version_date = strtotime($service->version_date);

	include('libs/docker_socket.php');
	$container = docker_socket_request("GET /v1.41/containers/" . $input->id . "/json");
	$image = $container['body']->Config->Image;
	$service->branch = end(explode(':', $image));
	
	$registry = json_decode(file_get_contents($service->registry . '/tags/' . $service->branch));
	$registry->version_date = strtotime($registry->created_at);

	if ($registry->version_date > $service->version_date + 60)
		return array("code" => 200, "data" => true);
	else
		return array("code" => 200, "data" => false);
};
?>