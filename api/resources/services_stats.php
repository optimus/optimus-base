<?php
$get = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	$input->id = check('id', $input->path[2], 'module', true);

	if (!exists($connection, 'server', 'services', 'name', $input->id))
		return array("code" => 404, "message" => "Ce service n'existe pas");
	
	include('libs/docker_socket.php');
	$stats = docker_socket_request("GET /v1.41/containers/" . $input->id . "/stats?stream=false");
	
	$code = $stats['code'];
	$stats = $stats['body'];

	$used_memory = $stats->memory_stats->usage - $stats->memory_stats->stats->cache;
	$available_memory = $stats->memory_stats->limit;
	$memory_usage = ($used_memory / $available_memory) * 100.0;
	
	$cpu_delta = $stats->cpu_stats->cpu_usage->total_usage - $stats->precpu_stats->cpu_usage->total_usage;
	$system_cpu_delta = $stats->cpu_stats->system_cpu_usage - $stats->precpu_stats->system_cpu_usage;
	$number_cpus = $stats->cpu_stats->cpu_usage->percpu_usage || $stats->cpu_stats->online_cpus;
	$cpu_usage = ($cpu_delta / $system_cpu_delta) * $number_cpus * 100.0;

	$simple_stats = array
	(
		"cpu_usage" => $cpu_usage,
		"memory_used" => $used_memory,
		"memory_total" => $available_memory,
		"memory_usage" => $memory_usage
	);

	return array("code" => $code, "data" => $simple_stats);
};
?>