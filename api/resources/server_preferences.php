<?php
$get = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	$input->id = check('id', $input->path[3], 'module', false);

	//REQUETE SUR UNE PREFERNCE DÉTERMINÉE
	if (isset($input->id))
	{
		$preference = $connection->prepare("SELECT value FROM `server`.`preferences` WHERE id = :id");
		$preference->bindParam("id", $input->id);
		$preference->execute();

		if ($preference->rowCount() == 0)
			return array("code" => 404, "message" => "Cette preference n'existe pas");
	
		return array("code" => 200, "data" => $preference->fetchObject()->value);
	}
	//REQUETE SUR TOUTES LES PREFERENCES
	else 
	{	
		$preferences = $connection->query("SELECT * FROM `server`.`preferences`");
		while ($preference = $preferences->fetch(PDO::FETCH_OBJ))
			$results[$preference->id] = $preference->value;
		
		if (sizeof($results) == 0)
			return array("code" => 404, "message" => "Aucune preference n'est disponible pour cet utilisateur");
	
		return array("code" => 200, "data" => $results);
	}
};


$post = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();
	
	$input->id = check('id', $input->path[3], 'module', true);
	$input->value = check('id', $input->body, 'text', false);

	$preference = $connection->prepare("REPLACE INTO `server`.`preferences` SET id=:id, value=:value");
	$preference->bindParam("id", $input->id);
	$preference->bindParam("value", $input->value);

	if (!$preference->execute()) 
		return array("code" => 400, "message" => $preference->errorInfo()[2]);
	return array("code" => 201, "data" => $input->value);
};


$delete = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	$input->id = check('id', $input->path[3], 'module', true);

	if(!exists($connection, 'server', 'preferences', 'id', $input->id))
		return array("code" => 404, "message" => "Aucune préférence portant ce nom n'a été trouvée");

	$preference = $connection->prepare("DELETE FROM `server`.`preferences` WHERE id=:id");
	$preference->bindParam("id", $input->id);

	if (!$preference->execute()) 
		return array("code" => 400, "message" => $preference->errorInfo()[2]);
	return array("code" => 200);
};
?>