<?php
use optimus\JWT;
$post = function ()
{
	global $connection, $input;
	allowed_origins_only();

	$input->body->email = check('email', $input->body->email, 'email', true);

	$exists = $connection->prepare("SELECT id, admin, type, status, email, password, firstname, lastname, displayname FROM `server`.`users` WHERE email = :email AND status = 1 LIMIT 0,1");
	$exists->bindParam(':email', $input->body->email, PDO::PARAM_STR);
	$exists->execute();
	if($exists->rowCount() != 1)
		 return array("code" => 401, "message" => "Accès refusé - Utilisateur inconnu");

	$user = $exists->fetch(PDO::FETCH_OBJ);

	if (password_verify($input->body->password, $user->password))
	{
		$jwt = new JWT(getenv('API_SHA_KEY'), 'HS512', 14400, 10);
		$token = $jwt->encode(["user" => array("id" => $user->id, "admin" => $user->admin, "email" => $user->email), "aud" => "http://" . getenv('DOMAIN'), "scopes" => ['user'], "iss" => "http://" . getenv('DOMAIN')]);
		$cookie_options = array ('expires' => time() + 14400, 'path' => '/', 'domain' => getenv('DOMAIN'), 'secure' => true, 'httponly' => true, 'samesite' => 'None');
		setcookie('token', $token, $cookie_options);
		return array("code" => 200, "data" => array("id" => $user->id, "type" => $user->type, "status" => $user->status, "admin" => $user->admin, "email" => $user->email, "firstname" => $user->firstname, "lastname" => $user->lastname, "displayname" => $user->displayname), "message" => "Authentification réussie");
	}
	else
		return array("code" => 401, "message" => "Accès refusé - Mot de passe erroné");
};
?>
