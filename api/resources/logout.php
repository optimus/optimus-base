<?php
$get = function ()
{
	$cookie_options = array ('expires' => time() - 3600, 'path' => '/', 'domain' => getenv('DOMAIN'), 'secure' => true, 'httponly' => true, 'samesite' => 'None');
	setcookie('token', '', $cookie_options);
	return array("code" => 200, "message" => "Token supprimé");
};
?>
