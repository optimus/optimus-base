<?php
ini_set('max_execution_time', '180');

$post = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	$input->id = check('id', $input->path[2], 'module', true);
	
	if (!exists($connection, 'server', 'services', 'name', $input->id))
		return array("code" => 404, "message" => "Ce service n'existe pas");

	if ($input->id == 'optimus-base' || $input->id == 'optimus-databases')
		return array("code" => 404, "message" => "Le service $input->id ne peut pas être stoppé car il est indispensable au fonctionnement du système");

	if ($_SERVER['SERVER_NAME'] == 'api.demoptimus5.ovh')
		return array("code" => 400, "message" => "Les services ne peuvent pas être stoppés sur le serveur de démo");
	
	include('libs/docker_socket.php');
	$result = docker_socket_request("POST /v1.41/containers/" . $input->id . "/stop");

	for ($i = 0; $i < 10; $i++)
	{
		$running = $connection->prepare("SELECT name FROM `server`.`services` WHERE name = :container_name AND status = 0");
		$running->bindParam(':container_name', $input->id, PDO::PARAM_STR);
		$running->execute();
		if ($running->rowCount() > 0 AND ($result['code'] == 204 OR $result['code'] == 304))
			return array("code" => 200, "data" => $result['body']);
		else
			sleep(1);
	}
	return array("code" => 400, "Le service n'a pas pu être stoppé");
};
?>