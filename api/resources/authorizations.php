<?php
use optimus\OptimusWebsocket;
include_once 'libs/datatables.php';
$resource = json_decode('
{
	"id": { "type": "strictly_positive_integer", "field": "authorizations.id", "post": ["ignored"], "patch": ["immutable"], "default": 0 },
	"owner": { "type": "positive_integer", "field": "authorizations.owner", "post": ["required", "notnull", "notempty"], "patch": ["immutable"], "default": 0 },
	"user": { "type": "positive_integer", "field": "authorizations.`user`", "post": ["required", "notnull", "notempty"], "patch": ["immutable"], "default": 0 },
	"resource": { "type": "resource", "field": "authorizations.resource", "post": ["required", "notnull", "notempty"], "patch": ["immutable"], "default": 0 },
	"resource_displayname": { "type": "string", "field": "authorizations.resource", "post": ["ignored"], "patch": ["immutable"], "default": 0 },
	"read": { "type": "boolean", "field": "authorizations.`read`", "post": ["notnull", "notempty"], "patch": ["notnull", "notempty"], "default": 0 },
	"write": { "type": "boolean", "field": "authorizations.`write`", "post": ["notnull", "notempty"], "patch": ["notnull", "notempty"], "default": 0 },
	"create": { "type": "boolean", "field": "authorizations.`create`", "post": ["notnull", "notempty"], "patch": ["notnull", "notempty"], "default": 0 },
	"delete": { "type": "boolean", "field": "authorizations.`delete`", "post": ["notnull", "notempty"], "patch": ["notnull", "notempty"], "default": 0 },
	"displayname" : { "type" : "string", "field": "authorizations.resource" },
	"user_type" : { "type": "strictly_positive_integer", "field": "user_details.type", "reference" : { "db" : "server.users AS user_details", "id" : "user_details.id", "match" : "authorizations.user" } },
	"user_firstname" : { "type": "string", "field": "user_details.firstname", "reference" : { "db" : "server.users AS user_details", "id" : "user_details.id", "match" : "authorizations.user" } },
	"user_lastname" : { "type": "string", "field": "user_details.lastname", "reference" : { "db" : "server.users AS user_details", "id" : "user_details.id", "match" : "authorizations.user" } },
	"user_email" : { "type": "string", "field": "user_details.email", "reference" : { "db" : "server.users AS user_details", "id" : "user_details.id", "match" : "authorizations.user" } },
	"user_displayname" : { "type": "string", "field": "user_details.displayname", "reference" : { "db" : "server.users AS user_details", "id" : "user_details.id", "match" : "authorizations.user" } },
	"owner_type" : { "type": "strictly_positive_integer", "field": "owner_details.type", "reference" : { "db" : "server.users AS owner_details", "id" : "owner_details.id", "match" : "authorizations.owner" } },
	"owner_firstname" : { "type": "string", "field": "owner_details.firstname", "reference" : { "db" : "server.users AS owner_details", "id" : "owner_details.id", "match" : "authorizations.owner" } },
	"owner_lastname" : { "type": "string", "field": "owner_details.lastname", "reference" : { "db" : "server.users AS owner_details", "id" : "owner_details.id", "match" : "authorizations.owner" } },
	"owner_email" : { "type": "string", "field": "owner_details.email", "reference" : { "db" : "server.users AS owner_details", "id" : "owner_details.id", "match" : "authorizations.owner" } },
	"owner_displayname" : { "type": "string", "field": "owner_details.displayname", "reference" : { "db" : "server.users AS owner_details", "id" : "owner_details.id", "match" : "authorizations.owner" } }
}
', null, 512, JSON_THROW_ON_ERROR);

$get = function ()
{
	global $connection, $resource, $input;

	auth();
	allowed_origins_only();

	$input->id = check('id', $input->path[2], 'strictly_positive_integer', false);

	if (!$input->body->filter)
		$input->body->filter = array();

	//IF USER IS NOT ADMIN, ADD FILTER TO SHOW ONLY HIS OWN AUTHORIZATIONS
	if (!is_admin($input->user->id))
	{
		$owner_filter = (object) array("field" => "owner", "type" => "=", "value" => $input->user->id);
		$user_filter = (object) array("field" => "user", "type" => "=", "value" => $input->user->id);
		array_push($input->body->filter, array($owner_filter, $user_filter));
	}

	//REQUETE SUR UNE AUTORISATION IDENTIFIÉE
	if (isset($input->id))
	{
		$input->body = json_decode('{"references": true, "filter": [{"field": "id", "type": "=", "value": ' . $input->id . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'server', 'authorizations');
		if (sizeof($results) == 0)
			return array("code" => 404, "message" => "Cette autorisation n'existe pas");
		else
			return array("code" => 200, "data" => sanitize($resource, $results));
	}
	//REQUETE SUR TOUTES LES AUTORISATIONS AU FORMAT DATATABLES
	else 
	{	
		if ($input->body->references === false)
			$resource = (object)array_filter((array)$resource, function($column){return !$column->reference;});

		if ($input->body->owner)
		{
			check('owner', $input->body->owner, 'strictly_positive_integer', true);
			$owner_filter = (object) array("field" => "owner", "type" => "=", "value" => $input->body->owner);
			array_push($input->body->filter, $owner_filter);
		}
		if ($input->body->user)
		{
			check('user', $input->body->user, 'strictly_positive_integer', true);
			$user_filter = (object) array("field" => "user", "type" => "=", "value" => $input->body->user);
			array_push($input->body->filter, $user_filter);
		}
		if ($input->body->resource)
		{
			check('resource', $input->body->resource, 'resource', true);
			$resource_specific_filter = (object) array("field" => "resource", "type" => "starts", "value" => $input->body->resource);
			$resource_all_filter = (object) array("field" => "resource", "type" => "=", "value" => "*");
			$resources_filter = array($resource_specific_filter, $resource_all_filter);
			array_push($input->body->filter, $resources_filter);
		}

		//resource_displayname CANNOT BE DETERMINED WITH A DATATABLES REQUEST
		//IF THERE'S A SEARCH OR SORT ON THE resource_displayname COLUMN, BYPASS DATATABLE FILTER, SORT AND PAGE SIZE
		$size = $input->body->size;
		$sort = $input->body->sort;
		for($i=0; $i < sizeof($input->body->filter); $i++)
			if ($input->body->filter[$i]->field == 'resource_displayname')
			{
				$input->body->search = $input->body->filter[$i]->value;
				unset($input->body->filter[$i]);
			}
		if ($input->body->sort[0]->field == 'resource_displayname')
			unset($input->body->sort[0]);
		if ($input->body->search OR $input->body->sort[0]->field == 'resource_displayname')
			$input->body->size = 0;

		//STANDARD DATATABLE REQUEST
		$results = datatable_request($connection, $resource, 'server', 'authorizations');
		$last_row = (int)$connection->query('SELECT FOUND_ROWS()')->fetchColumn();
		$last_page = $input->body->size > 0 ? ceil(max($last_row,1) / $input->body->size) : 1;

		//FETCH RESOURCES NAMES
		for($i=0; $i < sizeof($results); $i++)
		{
			$resource = explode('/', $results[$i]['resource']);
			if ($resource[1])
			{
				$resource_displayname = $connection->prepare("SELECT displayname FROM `user_" . $results[$i]['owner'] . "`.`" . $resource[0] . "` WHERE id = :id");
				$resource_displayname->bindParam(':id', $resource[1]);
				$resource_displayname->execute();
				$resource_displayname = $resource_displayname->fetch(PDO::FETCH_ASSOC);
				$results[$i]['resource_displayname'] = $resource_displayname['displayname'];
			}
			else
				$results[$i]['resource_displayname'] = $resource[0];
		}

		//IF THERE'S A SEARCH OR SORT ON THE resource_displayname COLUMN, SWITCH TO CUSTOM FILTER, ORDER AND PAGINATION FUNCTIONS
		if (sizeof($results) > 0 AND $input->body->search != '')
			$results = array_filter($results, function($item){global $input; return stristr($item['resource_displayname'], $input->body->search) != false;});
		if (sizeof($results) > 0 AND $sort[0]->field == 'resource_displayname')
			array_multisort(array_map('strtolower',array_column($results, "resource_displayname")), strtolower($sort[0]->dir) == 'asc' ? SORT_ASC : SORT_DESC, $results);
		if ($input->body->search OR $input->body->sort[0]->field == 'resource_displayname')
		{
			$last_row = sizeof($results);
			$results = array_slice($results, ($input->body->page-1) * $size, $size);
			$last_page = $size > 0 ? ceil(max($last_row,1) / $size) : 1;
		}

		return array("code" => 200, "data" => sanitize($resource, $results), "last_row" => $last_row, "last_page" => $last_page);
	}
};


$post = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$raw_resource = (object)array_filter((array)$resource, function($column){return (!$column->reference AND !$column->virtual);});
	check_input_body($raw_resource, 'post');

	if ($input->body->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Accès refusé - Seul un administrateur ou le propriétaire de la ressource peut paramétrer les autorisations");
	
	if ($input->body->owner == $input->body->user)
		return array("code" => 400, "message" => "Un utilisateur ne peut pas s'accorder des autorisations à lui même");

	$exists = $connection->prepare("SELECT id FROM `server`.`authorizations` WHERE user=:user AND owner=:owner AND resource=:resource");
	$exists->bindParam(':user', $input->body->user, PDO::PARAM_INT);
	$exists->bindParam(':owner', $input->body->owner, PDO::PARAM_INT);
	$exists->bindParam(':resource', $input->body->resource, PDO::PARAM_STR);
	$exists->execute();
	if ($exists->rowCount() > 0)
		return array("code" => 409, "message" => "Erreur - une autorisation pour cette resource et cet utilisateur existe déjà");
	
	if (!exists($connection,'server','users','id',$input->body->user))
		return array("code" => 404, "message" => "user : cet utilisateur n'existe pas");
	
	if (!exists($connection,'server','users','id',$input->body->owner))
		return array("code" => 404, "message" => "owner : cet utilisateur n'existe pas");
	
	$query = "INSERT INTO `server`.`authorizations` SET ";
	foreach($input->body as $key => $value)
		if (!in_array("ignored", $raw_resource->$key->post))
			$query .= '`' . $key . '`=:'.$key.',';
	$query = substr($query,0,-1);

	$authorization = $connection->prepare($query);
	foreach($input->body as $key => $value)
		if (!in_array("ignored", $raw_resource->$key->post))
			$authorization->bindParam(':'. $key, $input->body->$key, $input->pdotype->$key);

	if($authorization->execute())
	{
		$input->body->filter[] = (object)array("field"=> "id", "type" => "=", "value" => $connection->lastInsertId());
		$results = datatable_request($connection, $resource, 'server', 'authorizations');
		if ($input->body->read == 1)
		{
			$owner = $connection->query('SELECT displayname FROM server.users WHERE id = ' . $input->body->owner)->fetchObject();
			$resource = explode('/', $input->body->resource);
			if ($resource[1])
				$resource_name = $connection->query('SELECT displayname FROM user_' . $input->body->owner . '.' . $resource[0] . ' WHERE id = ' . $resource[1])->fetchObject();
			$websocket = new OptimusWebsocket($input->user->id, $input->user->email, is_admin($input->user->id));
			$notification = array('colorclass' => 'is-info', 'icon' => 'fas fa-share', 'title' => 'Nouveau partage', 'text' => $owner->displayname . ' vous a partagé ' . ($resource[1] ? $resource_name->displayname : $resource[0]) . ' (' . $input->body->resource . ')', 'date' => date('Y-m-d H:i:s'));
			$websocket->send(json_encode(array("command" => "notification", "user" => $input->body->user, "data" => $notification)));
		}
		return array("code" => 201, "data" => sanitize($resource, $results[0]), "message" => "Autorisation créée avec succès");
	}
	else
		return array("code" => 400, "message" => $authorization->errorInfo()[2]);
};


$patch = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->id = check('id', $input->path[2], 'strictly_positive_integer', true);

	$resource = (object)array_filter((array)$resource, function($column){return (!$column->reference AND !$column->virtual);});
	check_input_body($resource, 'patch');

	$authorization = $connection->query("SELECT `owner`, `resource` FROM `server`.`authorizations` WHERE id = " . $input->id)->fetch(PDO::FETCH_ASSOC);
	if (sizeof($authorization) == 0)
		return array("code" => 404, "message" => "Cette autorisation n'existe pas");
	else if ($authorization['owner'] != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Accès refusé - Seul un administrateur ou le propriétaire de la ressource peut modifier les autorisations");

	if (isset($input->body->user))
	{
		if (!exists($connection,'server','users','id',$input->body->user))
			return array("code" => 404, "message" => "user : cet utilisateur n'existe pas");

		$exists = $connection->prepare("SELECT id FROM `server`.`authorizations` WHERE `user`=:user AND `owner`=:owner AND `resource`=:resource AND `id`!=:id");
		$exists->bindParam(':id', $input->id, PDO::PARAM_INT);
		$exists->bindParam(':user', $input->body->user, PDO::PARAM_INT);
		$exists->bindParam(':owner', $autorisation['owner'], PDO::PARAM_INT);
		$exists->bindParam(':resource', $autorisation['resource'], PDO::PARAM_STR);
		$exists->execute();
		if ($exists->rowCount() > 0)
			return array("code" => 409, "message" => "Erreur - une autorisation pour cette resource et cet utilisateur existe déjà");
	}

	$query = "UPDATE `server`.`authorizations` SET ";
	foreach($input->body as $key => $value)
		if (!in_array("immutable", $resource->$key->patch))
			$query .= '`'.$key.'`=:'.$key.',';
	$query = substr($query,0,-1);
	$query .= " WHERE id = '" . $input->id . "'";

	$authorization = $connection->prepare($query);
	foreach($input->body as $key => $value)
		if (!in_array("ignored", $resource->$key->patch))
			$authorization->bindParam(':'. $key, $input->body->$key, $input->pdotype->$key);
		
	if($authorization->execute())
	{
		$updated_authorization = $connection->query("SELECT * FROM `server`.`authorizations` WHERE id = " . $input->id)->fetch(PDO::FETCH_ASSOC);
		return array("code" => 200, "data" =>  sanitize($resource, $updated_authorization), "message" => "Autorisation modifiée avec succès");
	}
	else
		return array("code" => 400, "message" => $authorization->errorInfo()[2]);
};


$delete = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->id = check('id', $input->path[2], 'strictly_positive_integer', true);

	$authorization = $connection->query("SELECT `owner` FROM `server`.`authorizations` WHERE id = " . $input->id)->fetch(PDO::FETCH_ASSOC);
	if (sizeof($authorization) == 0)
		return array("code" => 404, "message" => "Cette autorisation n'existe pas");
	else if ($authorization['owner'] != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Accès refusé - Seul un administrateur ou le propriétaire de la ressource peut configurer les autorisations");
	
	$delete = $connection->prepare("DELETE FROM `server`.`authorizations` WHERE id = '" . $input->id . "'");
	if($delete->execute())
		return array("code" => 200, "message" => "Autorisation supprimée avec succès");
	else
		return array("code" => 400, "message" => $authorizations->errorInfo()[2]);
};
?>