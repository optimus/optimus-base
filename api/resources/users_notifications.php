<?php
use optimus\OptimusWebsocket;
include_once 'libs/datatables.php';
$resource = json_decode('
{
	"id": { "type": "strictly_positive_integer", "field": "notifications.id", "post": ["ignored"], "patch": ["immutable"], "default": 0 },
	"colorclass": { "type": "module", "field": "notifications.colorclass", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "is-info" },
	"icon": { "type": "string", "field": "notifications.icon", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "fas fa-bell" },
	"title": { "type": "string", "field": "notifications.title", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "Nouvelle notification" },
	"text": { "type": "text", "field": "notifications.text", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"date": { "type": "datetime", "field": "notifications.date", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "' . date('Y-m-d H:i:s') . '"},
	"link": { "type": "string", "field": "notifications.link", "post": ["undefinedtonull", "emptytonull"], "patch": ["notnull", "notempty"]},
	"archived": { "type": "boolean", "field": "notifications.archived", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": 0 }
}
', null, 512, JSON_THROW_ON_ERROR);

$get = function ()
{
	global $connection, $resource, $input;
	
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[2], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[4], 'strictly_positive_integer', false);
	
	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul un administrateur et l'utilisateur concerné peuvent lire ces notifications");
	
	//REQUETE SUR UNE NOTIFICATION DÉTERMINÉE
	if (isset($input->id))
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $input->id . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'notifications');
		if (sizeof($results) == 0)
			return array("code" => 404, "message" => "Cette notification n'existe pas");
		else
			return array("code" => 200, "data" => sanitize($resource, $results[0]));
	}
	//REQUETE SUR TOUTES LES NOTIFICATIONS AU FORMAT DATATABLES
	else 
	{	
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'notifications');
		$last_row = (int)$connection->query('SELECT FOUND_ROWS()')->fetchColumn();
		$last_page = $input->body->size > 0 ? ceil(max($last_row,1) / $input->body->size) : 1;
		return array("code" => 200, "data" => sanitize($resource, $results), "last_row" => $last_row, "last_page" => $last_page);
	}
};


$post = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[2], 'strictly_positive_integer', true);

	check_input_body($resource, 'post');
	
	$query = datatables_insert($connection, $resource, 'user_' . $input->owner, 'notifications');
	if($query->execute())
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $connection->lastInsertId() . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'notifications');
		$websocket = new OptimusWebsocket($input->user->id, $input->user->email, is_admin($input->user->id));
		$websocket->send(json_encode(array("command" => "notification", "user" => $input->owner, "data" => sanitize($resource, $results[0]))));
		return array("code" => 201, "data" => sanitize($resource, $results[0]));
	}
};


$patch = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[2], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[4], 'strictly_positive_integer', true);

	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul un administrateur et l'utilisateur concerné peuvent modifier cette notification");

	check_input_body($resource, 'patch');

	if (count(array_intersect(array_keys((array)$input->body),array_keys(array_filter((array)$resource, function($item){return !in_array('immutable', (array)$item->patch);})))) == 0) 
		return array("code" => 400, "message" => "Aucune donnée à modifier n'a été transmise");

	if (!exists($connection, 'user_' . $input->owner, 'notifications', 'id', $input->id))
		return array("code" => 404, "message" => "cette notification n'existe pas");

	$query = datatables_update($connection, $resource, 'user_' . $input->owner, 'notifications', $input->id);
	if($query->execute())
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $input->id . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'notifications');
		return array("code" => 200, "data" => sanitize($resource, $results[0]));
	}
};


$delete = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	
	$input->owner = check('owner', $input->path[2], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[4], 'strictly_positive_integer', true);

	if ($input->owner != $input->user->id AND !is_admin($input->user->id))
		return array("code" => 401, "message" => "Seul un administrateur et l'utilisateur concerné peuvent supprimer cette notification");

	if (!exists($connection, 'user_' . $input->owner, 'notifications', 'id', $input->id))
		return array("code" => 404, "message" => "Cette notification n'existe pas");

	$delete = $connection->query("DELETE FROM `" . 'user_' . $input->owner . "`.notifications WHERE id = '" . $input->id . "'");
	if (!$delete->execute()) 
		return array("code" => 400, "message" => $delete->errorInfo()[2]);
	return array("code" => 200);
};
?>