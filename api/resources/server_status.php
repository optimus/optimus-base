<?php
$get = function ()
{
	auth();
	allowed_origins_only();
	admin_only();

	include('libs/docker_socket.php');
	$docker = docker_socket_request("GET /info");

	$status['system'] = array(
		"kernel_version" => $docker['body']->KernelVersion,
		"os" => ucfirst($docker['body']->OperatingSystem),
		"os_type" => ucfirst($docker['body']->OSType),
		"architecture" => $docker['body']->Architecture,
	);

	$status['cpu'] = array(
		"usage" =>  floatval(shell_exec("top -n 1 -b | awk '/^%Cpu/{print $2}'"))
	);

	$status['memory'] = array(
		"usage" => floatval(shell_exec("free | grep Mem | awk '{print $3/$2 * 100.0}'")),
		"total" => $ovh->model->memory ? $ovh->model->memory * 1024 * 1024 : $docker['body']->MemTotal
	);

	$system_disk = preg_split('/\s+/',shell_exec("df -P | grep '/dev/sda1'"));
	$status['disk'][0]['name'] = "System Disk";
	$status['disk'][0]['device'] = $system_disk[0];
	$status['disk'][0]['used'] = (int)$system_disk[2] * 1024;
	$status['disk'][0]['total'] = intval($system_disk[1]) * 1024;
	$status['disk'][0]['usage'] = intval($system_disk[4]);

	$data_disk = preg_split('/\s+/',shell_exec("df -P | grep '/dev/mapper/cryptsda2'"));
	$status['disk'][1]['name'] = "Data Disk";
	$status['disk'][1]['device'] = $data_disk[0];
	$status['disk'][1]['used'] = (int)$data_disk[2] * 1024;
	$status['disk'][1]['total'] = intval($data_disk[1]) * 1024;
	$status['disk'][1]['usage'] = intval($data_disk[4]);
	
	$status['docker'] = array(
		"version" => $docker['body']->ServerVersion,
		"containers" => array(
			"running" => $docker['body']->ContainersRunning,
			"paused" => $docker['body']->ContainersPaused,
			"stopped" => $docker['body']->ContainersStopped,
		)
	);

	$nginx_version = str_replace("\r\n", "", shell_exec("curl -I https://api." . getenv('DOMAIN')));
	$nginx_version = substr($nginx_version, strpos($nginx_version, 'nginx/') + 6);
	$nginx_version = substr($nginx_version, 0, strpos($nginx_version, 'date'));
	$status['nginx'] = array(
		"version" => $nginx_version
	);
	
	return array("code" => 200, "data" => $status);
};
?>
