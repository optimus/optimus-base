<?php
include('/srv/api/libs/ovh.php');

$get = function ()
{
	auth();
	allowed_origins_only();
	admin_only();

	$all_disks = shell_exec("df -P");
	$all_disks = explode("\n", $all_disks);
	foreach($all_disks as $disk)
	{
		$line = preg_split('/\s+/',$disk);
		if ($line[0] != 'Filesystem' AND $line[0] != 'overlay')
			$all_disks_array[$line[0]] = $line[1]; 
	}

	$ovh = ovh_api_request('GET', '/vps/'.getenv('OVH_VPS').'.vps.ovh.net', $data = '');
	$status = array
	(
		"id" => getenv('OVH_VPS').'.vps.ovh.net',
		"disk" => $ovh->model->disk * 1024 * 1024 * 1024,
		"model" => $ovh->model->name,
		"version" => $ovh->model->version,
		"displayname" => $ovh->model->name,
		"memory" => $ovh->model->memory * 1024 * 1024,
		"vcores" => $ovh->model->vcore,
		"disk_freespace" => round(($ovh->model->disk) - array_sum($all_disks_array) / 1024 / 1024) * 1024 * 1024 * 1024
	);
	
	return array("code" => 200, "data" => $status);
};
?>
