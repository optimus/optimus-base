<?php
include('/srv/api/libs/ovh.php');

$get = function ()
{
	auth();
	allowed_origins_only();
	admin_only();

	$domains = ovh_api_request('GET', '/domain', $data = '');
	
	return array("code" => 200, "data" => $domains);
};
?>
