<?php
$get = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	$domains = $connection->query("SELECT id, domain FROM server.domains ORDER BY id")->fetchAll(PDO::FETCH_ASSOC);

	foreach ($domains as &$domain)
	{
		$certificate = shell_exec("echo -n Q | openssl s_client -servername 'api." . $domain['domain'] . "' -connect 'api." . $domain['domain'] . ":443' | openssl x509 -noout -dates");
		$domain['certificate_creation'] = date('Y-m-d', strtotime(explode("\n", substr($certificate, strpos($certificate, 'notBefore') + 10))[0]));
		$domain['certificate_expiry'] = date('Y-m-d', strtotime(substr($certificate, strpos($certificate, 'notAfter') + 9)));
	}

	return array("code" => 200, "data" => $domains);
};


$post = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	$input->body->domain = check('domain', $input->body->domain, 'topdomain', true);
	
	if (exists($connection, 'server', 'domains', 'domain', $input->body->domain))
		return array("code" => 409, "message" => "Erreur - Ce domaine existe déjà");

	$next_id = $connection->query("SELECT MAX(id) FROM `server`.`domains`")->fetch(PDO::FETCH_ASSOC);

	$mailbox = $connection->prepare("INSERT INTO server.domains SET id = '" . ($next_id['MAX(id)']+1) . "', status = 1, domain = '" . $input->body->domain . "'");
	if($mailbox->execute())
		return array("code" => 201, "data" => array('id' => ($next_id['MAX(id)']+1), 'domain' => strtolower($input->body->domain)), "message" => "Domaine ajouté avec succès");
	else
		return array("code" => 400, "message" => $mailbox->errorInfo()[2]);
};


$patch = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	$input->id = check('id', $input->path[2], 'strictly_positive_integer', true);
	$input->body->domain = check('domain', $input->body->domain, 'topdomain', true);

	if (exists($connection, 'server', 'domains', 'domain', $input->body->domain))
		return array("code" => 409, "message" => "Erreur - Ce domaine existe déjà");

	$mailbox = $connection->prepare("UPDATE `server`.`domains` SET domain = '" . strtolower($input->body->domain) . "' WHERE id = '" . $input->id . "'");
	if($mailbox->execute())
		return array("code" => 200, "data" => array('id' => $input->id, 'domain' => strtolower($input->body->domain)), "message" => "Domaine ajouté avec succès");
	else
		return array("code" => 400, "message" => $mailbox->errorInfo()[2]);
};


$delete = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	admin_only();

	$input->id = check('id', $input->path[2], 'strictly_positive_integer', true);
	
	if (!exists($connection, 'server', 'domains', 'id', $input->id))
		return array("code" => 409, "message" => "Erreur - Ce domaine n'existe pas dans la base");
	
	$delete = $connection->query("DELETE FROM server.domains WHERE id = '" . $input->id . "'");
	if($delete->execute())
	{
		exec('sudo /usr/sbin/postfix reload');
		return array("code" => 200, "message" => "Domaine supprimé avec succès");
	}
	else
		return array("code" => 400, "message" => $delete->errorInfo()[2]);
};
?>
