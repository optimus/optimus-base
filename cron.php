<?php
function fetch_notifications($source)
{
	error_log('--> CRON JOB');
	error_log('--> getting notifications from ' . $source);
	$notifications = file_get_contents($source);
	$notifications = json_decode($notifications);
	if (sizeof($notifications) > 0)
	{
		error_log('--> ' . sizeof($notifications) . ' notification(s) found');
		$connection = new PDO("mysql:host=optimus-databases", getenv('MARIADB_ROOT_USER'), getenv('MARIADB_ROOT_PASSWORD'), array(PDO::ATTR_TIMEOUT => 30));
		if ($connection)
			foreach($notifications as $notification)
			{
				$query = "SELECT id FROM `server`.`users`";
				if ($notification->admin_only == true)
					$query .= " WHERE admin = 1";
				$query .= " ORDER BY id";
				$users = $connection->query($query)->fetchAll(PDO::FETCH_OBJ);
				foreach($users as $user)
				{
					$notification_exists = $connection->prepare("SELECT id FROM `user_" . $user->id . "`.`notifications` WHERE colorclass=:colorclass AND icon=:icon AND title=:title AND text=:text AND date=:date AND link=:link");
					$notification_exists->bindParam(':colorclass', $notification->colorclass, PDO::PARAM_STR);
					$notification_exists->bindParam(':icon', $notification->icon, PDO::PARAM_STR);
					$notification_exists->bindParam(':title', $notification->title, PDO::PARAM_STR);
					$notification_exists->bindParam(':text', $notification->text, PDO::PARAM_STR);
					$notification_exists->bindParam(':date', $notification->date, PDO::PARAM_STR);
					$notification_exists->bindParam(':link', $notification->link, PDO::PARAM_STR);
					$notification_exists->execute();
					if ($notification_exists->rowCount() == 0)
					{
						$add_notification = $connection->prepare("INSERT INTO `user_" . $user->id . "`.`notifications` SET colorclass=:colorclass, icon=:icon, title=:title, text=:text, date=:date, link=:link");
						$add_notification->bindParam(':colorclass', $notification->colorclass, PDO::PARAM_STR);
						$add_notification->bindParam(':icon', $notification->icon, PDO::PARAM_STR);
						$add_notification->bindParam(':title', $notification->title, PDO::PARAM_STR);
						$add_notification->bindParam(':text', $notification->text, PDO::PARAM_STR);
						$add_notification->bindParam(':date', $notification->date, PDO::PARAM_STR);
						$add_notification->bindParam(':link', $notification->link, PDO::PARAM_STR);
						if ($add_notification->execute())
							error_log('--> notification added for user ' . $user->id);
						else
							error_log('--> ERROR : ' . $add_notification->errorInfo()[2]);
					}
				}
			}
			$connection = null;
		}
}

function cron()
{
	global $last_notifications_check, $last_disk_freespace_check;

	if (time() > $last_notifications_check + 3600 - rand(0, 10))
	{
		fetch_notifications('https://api.cybertron.fr/optimus-notifications');
		$last_notifications_check = time();
	}

	if (time() > $last_disk_freespace_check + 60)
	{
		$system_disk = preg_split('/\s+/',shell_exec("df -P | grep '/dev/sda1'"));
		$data_disk = preg_split('/\s+/',shell_exec("df -P | grep '/dev/mapper/cryptsda2'"));

		if (intval($system_disk[4]) > 90 || intval($data_disk[4]) > 90)
		{
			$connection = new PDO("mysql:host=optimus-databases", getenv('MARIADB_ROOT_USER'), getenv('MARIADB_ROOT_PASSWORD'), array(PDO::ATTR_TIMEOUT => 30));
			$users = $connection->query("SELECT id FROM `server`.`users` WHERE `admin` = 1 ORDER BY id")->fetchAll(PDO::FETCH_OBJ);
			
			foreach($users as $user)
			{
				$systemdisk_notification_exists = $connection->query("SELECT id FROM `user_" . $user->id . "`.`notifications` WHERE title='Alerte disque système' AND date > DATE_SUB(CURDATE(), INTERVAL 1 DAY)");
				if ($systemdisk_notification_exists->rowCount() == 0)
				{
					if (intval($system_disk[4]) > 95)
						$add_notification = $connection->query("INSERT INTO `user_" . $user->id . "`.`notifications` SET colorclass='is-danger', icon='fas fa-hard-drive', title='Alerte disque Système', text='Le disque système a atteint " . intval($system_disk[4]) . "% de sa capacité', link='optimus-base/administration#status'");
					else if (intval($system_disk[4]) > 90)
						$add_notification = $connection->query("INSERT INTO `user_" . $user->id . "`.`notifications` SET colorclass='is-warning', icon='fas fa-hard-drive', title='Alerte disque Système', text='Le disque système a atteint " . intval($system_disk[4]) . "% de sa capacité', link='optimus-base/administration#status'");
					error_log('--> DISK ALERT : system disk at ' . intval($system_disk[4]) . '%');
				}

				$datadisk_notification_exists = $connection->query("SELECT id FROM `user_" . $user->id . "`.`notifications` WHERE title='Alerte disque de données'AND date > DATE_SUB(CURDATE(), INTERVAL 1 DAY)");
				if ($datadisk_notification_exists->rowCount() == 0)
				{
					if (intval($data_disk[4]) > 95)
						$add_notification = $connection->query("INSERT INTO `user_" . $user->id . "`.`notifications` SET colorclass='is-danger', icon='fas fa-hard-drive', title='Alerte disque de données', text='Le disque de donées a atteint " . intval($data_disk[4]) . "% de sa capacité', link='optimus-base/administration#status'");
					else if (intval($data_disk[4]) > 90)
						$add_notification = $connection->query("INSERT INTO `user_" . $user->id . "`.`notifications` SET colorclass='is-warning', icon='fas fa-hard-drive', title='Alerte disque de données', text='Le disque de données a atteint " . intval($data_disk[4]) . "% de sa capacité', link='optimus-base/administration#status'");
				}
			}
		}
		$connection = null;
		$last_disk_freespace_check = time();
	}
}
?>