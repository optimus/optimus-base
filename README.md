# OPTIMUS-BASE

coeur du serveur optimus.
Ce module gère les fonctions de bas niveau de l'écosystème optimus entre les bases de données du serveur (OPTIMUS-DATA-BASES) et les clients connectés via l'API ou le websocket.

Il constitue le client de base du serveur Optimus et contient les configurations de base pour les utilisateurs, autorisations, notifications, routes, domaines...




