5.16 (2025-03-05)
Ajout d'un menu supérieur permettant la sélection de la langue (le choix est mémorisé dans le localstorage et accessible dans store.language)
La langue sélectionnée est passée dans le header Accept-Language de chaque requête réalisée avec la fonction rest()


5.15 (2025-02-27)
Modification du composant optimus_webdav_explorer pour tenir compte de la nouvelle api optimus-drive

5.14 (2025-02-24)
Correction d'un bug dans le composant optimus_webdav_explorer qui empêchait l'ouverture des fichiers PDF et ZIP après conversion

5.13 (2025-02-15)
Ajout d'une fonction permettant de détecter les réglages OVH MX PLAN
Suppression des statistiques de consommation CPU / RAM des serveurs hébergés chez OVH (deprecated API)

5.12 (2025-01-18)
Correction d'un bug dans le composant optimus-form (le mauvais owner était parfois pris en compte)

5.11 (2024-12-08)
Dans le tableau d'administration des utilisateurs, une icone permet de différencier les entreprises
Correction d'un bug d'affichage lors de la consultation des ressources partagées
Dans le composant optimus-table, il est désormais possible de préciser le type d'utilisateur à prendre en compte dans le menu owner_selector

5.10 (2024-11-19)
L'accès aux données d'usage CPU / RAM des serveurs hébergés chez OVH est rétabli
Il n'est plus possible de désactiver le dernier administrateur (pour empêcher d'être bloqué hors du système)
Mise à jour des librairies Tabulator, Chart.js, Prism
Ajout de la librairie BulmaSlider
Modification du processus de changement de l'identifiant et du mot de passe des utilisateurs
Ajout d'un paramètre pour décider l'ordre d'affichage des options dans les menus
Ajout des fonctions modal.show() et modal.hide() pour cacher temporairement un modal
Modification du processus de logout pour s'assurer que tous les services ont pu pleinement exécuter leur code de logout
Modification de la fonction populate() pour récupérer les données sans nécessairement les afficher

5.06 (2024-05-05)
Mise à jour vers tabulator 6.2.1

5.05 (2024-05-01)
Ajout d'un nouveau composant optimus-datepicker qui a vocation à uniformiser l'interface sur toutes les plateformes et OPTIMUS
Ajout d'un éditeur optimus-datepicker dans le composant optimus-table
Modification du composant optimus-form pour qu'il ajoute automatiquement des optimus-datepickers

5.04 (2024-04-21)
Correction de bugs dans les fonctions "Filtres", "Imprimer" et "Export" des tableaux
En mode "fullscreen" et "wpa" un lien "retour" apparait en haut à gauche de l'écran pour pallier l'absence de boutons navigateur
Ajout des fonctions modal.hide() et modal.show()
Le Websocket, s'il est deconnecté, se reconnecte automatiquement lorsque la fenêtre retrouve un focus


5.03 (2024-04-18)
Mise à jour vers tabulator 6.2 et amélioration et corrections de bugs d'affichage en dark mode
Ajout d'un paramètre 'type' pour les utilisateurs
Correction de bugs dans la recherche avancée du composant "optimus-table"
Renommage des onglets "partages" en "autorisations"
Renommage des fichiers SQL pour les rendre compatibles avec la nouvelle version du script init.php
Supression du cache Localstorage des constantes lors de l'installation d'un nouveau service.
Ajout d'une page d'erreur pour rediriger les utilisateurs qui ne disposent pas des droits admins.

5.01 (2023-03-27)
Cliquer sur un onglet déjà actif recharge désormais le contenu de l'onglet

5.00 (2023-03-01)
Réorganisation du code en vue du lancement de la nouvelle plateforme OPTIMUS V5