export default class OptimusBase
{
	constructor()
	{
		this.name = 'optimus-base'

		this.swagger_modules =
			[
				{
					id: "optimus-base",
					title: "OPTIMUS BASE",
					path: "/services/optimus-base/optimus-base.yaml",
					filters: ["allowed-origins", "authorizations", "common", "domains", "notifications", "ovh", "preferences", "server", "services", "users"]
				}
			]

		this.leftmenus =
			[
				{
					id: 'home',
					title: 'ACCUEIL',
					items:
						[
							{
								id: 'home-dashboard',
								icon: 'fas fa-house',
								title: 'Accueil',
								onclick: () => router('optimus-base/dashboard')
							}
						]
				}
			]

		this.topmenus =
			[
				{
					id: 'helpmenu',
					icon: 'fas fa-question-circle',
					items:
						[
							{
								id: 'helpmenu-social-networks',
								type: 'header',
								title: 'Réseaux sociaux',
								position: 10
							},
							{
								id: 'helpmenu-discord',
								title: 'Discord',
								icon: 'fa-brands fa-discord',
								onclick: () => window.open('https://discord.com/channels/787456009097576470/787456009773514775', '_blank'),
								position: 20
							},
							{
								id: 'helpmenu-facebook',
								title: 'Facebook',
								icon: 'fa-brands fa-facebook',
								onclick: () => window.open('https://www.facebook.com/groups/1341013276068344', '_blank'),
								position: 30
							},
							{
								id: 'helpmenu-youtube',
								title: 'Youtube',
								icon: 'fa-brands fa-youtube',
								onclick: () => window.open('https://www.youtube.com/channel/UC43zYDmygpcnGEzSaOPhJFA', '_blank'),
								position: 40
							},
							{
								id: 'helpmenu-association',
								type: 'header',
								title: 'Soutenez l\'association',
								position: 50
							},
							{
								id: 'helpmenu-association-website',
								title: 'www.cybertron.fr',
								icon: 'fas fa-globe',
								onclick: () => window.open('https://www.cybertron.fr', '_blank'),
								position: 60
							},
							{
								id: 'helpmenu-association-donation',
								title: 'Don à l\'association',
								icon: 'fas fa-hand-holding-dollar',
								onclick: () => router('optimus-base/donate'),
								position: 70
							}
						]
				},
				{
					id: 'localization',
					icon: 'fas fa-globe',
					items:
						[
							{
								id: 'english',
								title: 'Anglais',
								icon: 'fi fi-gb',
								onclick: () => { localStorage.setItem('language', 'en'); window.location.reload() },
								position: 10
							},
							{
								id: 'french',
								title: 'Français',
								icon: 'fi fi-fr',
								onclick: () => { localStorage.setItem('language', 'fr'); window.location.reload() },
								position: 20
							},
							{
								id: 'arabic',
								title: 'Arabe',
								icon: 'fi fi-sa',
								onclick: () => { localStorage.setItem('language', 'ar'); window.location.reload() },
								position: 30
							}
						]
				}
			]

		store.services.push(this)
	}

	login()
	{
		leftmenu.create('home', 'ACCUEIL')
		leftmenu['home'].add('home-dashboard', 'Accueil', 'fas fa-house', () => router('optimus-base/dashboard'))

		topmenus.create('helpmenu', 'fas fa-question-circle')
		topmenus['helpmenu'].add("helpmenu-social-networks", "header", "Réseaux sociaux", null, null, null, 10)
		topmenus['helpmenu'].add("helpmenu-discord", "item", "Discord", "fa-brands fa-discord", () => window.open('https://discord.com/channels/787456009097576470/787456009773514775', '_blank'), null, 20)
		topmenus['helpmenu'].add("helpmenu-facebook", "item", "Facebook", "fa-brands fa-facebook", () => window.open('https://www.facebook.com/groups/1341013276068344', '_blank'), null, 30)
		topmenus['helpmenu'].add("helpmenu-youtube", "item", "Youtube", "fa-brands fa-youtube", () => window.open('https://www.youtube.com/channel/UC43zYDmygpcnGEzSaOPhJFA', '_blank'), null, 40)
		topmenus['helpmenu'].add("helpmenu-association", "header", "Soutenez l'association", null, null, null, 50)
		topmenus['helpmenu'].add("helpmenu-association-website", "item", "www.cybertron.fr", "fas fa-globe", () => window.open('https://www.cybertron.fr', '_blank'), null, 60)
		topmenus['helpmenu'].add("helpmenu-association-donation", "item", "Don à l'association", "fas fa-hand-holding-dollar", () => router('optimus-base/donate'), null, 70)

		topmenus.create('localization', 'fas fa-globe')
		topmenus['localization'].add('english', 'item', 'Anglais', 'fi fi-gb', () => { localStorage.setItem('language', 'en'); window.location.reload() }, null, 10)
		topmenus['localization'].add('french', 'item', 'Français', 'fi fi-fr', () => { localStorage.setItem('language', 'fr'); window.location.reload() }, null, 20)
		topmenus['localization'].add('arab', 'item', 'Arabe', 'fi fi-sa', () => { localStorage.setItem('language', 'ar'); window.location.reload() }, null, 30)
	}

	logout()
	{
	}

	async global_search(search_query)
	{
		if (store.user.admin == 1)
			return [
				{
					icon: 'fas fa-user',
					title: 'Utilisateurs',
					data: rest(store.user.server + '/optimus-base/users', 'GET', { filter: [{ field: "*", type: "like", value: search_query }], references: true, size: 12, page: 1 })
						.then(users => users?.data?.map(user => { return { displayname: user.displayname, route: 'optimus-base/users/editor?id=' + user.id + '#general' } }))
				}
			]
	}

	dashboard()
	{
	}
}

window.stopwatch_init = async () =>
{
	if (window.stopwatch_updater)
		clearInterval(window.stopwatch_updater)
	let stored = await rest(store.user.server + '/optimus-base/users/' + store.user.id + '/preferences/stopwatch', 'GET', null, null)
	if (stored.data)
		stored = JSON.parse(stored.data)

	store.stopwatch =
	{
		start: stored?.start || Math.round(new Date().getTime() / 1000),
		end: stored?.end || Math.round(new Date().getTime() / 1000),
		status: stored?.status || 'stopped',
		unloaded_at: store?.unloaded_at || null
	}

	if (store.stopwatch.unloaded_at)
	{
		let now = Math.round(new Date().getTime() / 1000)
		let delta = now - unloaded_at.data
		store.stopwatch.start += delta
		store.stopwatch.end += delta
		store.stopwatch.unloaded_at = null
	}

	if (store.stopwatch.status == 'running')
		window.stopwatch_updater = setInterval(window.stopwatch_update, 1000)

	rest(store.user.server + '/optimus-base/users/' + store.user.id + '/preferences/stopwatch', 'POST', JSON.stringify(store.stopwatch))
}

window.stopwatch_update = () =>
{
	let timer = store.stopwatch.end - store.stopwatch.start
	let time = new Date(0)
	time.setSeconds(timer)
	if (document.getElementById('dashboard-stopwatch'))
		document.getElementById('dashboard-stopwatch').innerText = time.toISOString().substring(11, 19)
	store.stopwatch.end++
	if (timer % 60 == 0)
	{
		store.stopwatch.unloaded_at = null
		rest(store.user.server + '/optimus-base/users/' + store.user.id + '/preferences/stopwatch', 'POST', JSON.stringify(store.stopwatch))
	}
}

window.addEventListener('beforeunload', () => 
{
	if (store.stopwatch.status == 'running')
	{
		store.stopwatch.unloaded_at = Math.round(new Date().getTime() / 1000)
		rest(store.user.server + '/optimus-base/users/' + store.user.id + '/preferences/stopwatch', 'POST', JSON.stringify(store.stopwatch))
	}
})

window.stopwatch_start = () =>
{
	window.stopwatch_init()
	store.stopwatch.status = 'running'
	window.stopwatch_updater = setInterval(window.stopwatch_update, 1000)
	document.getElementById('dashboard-stopwatch-playbutton').classList.add('is-hidden')
	document.getElementById('dashboard-stopwatch-pausebutton').classList.remove('is-hidden')
	rest(store.user.server + '/optimus-base/users/' + store.user.id + '/preferences/stopwatch', 'POST', JSON.stringify(store.stopwatch))
}

window.stopwatch_reset = () => 
{
	clearInterval(window.stopwatch_updater)
	store.stopwatch.status = 'stopped'
	store.stopwatch.start = Math.round(new Date().getTime() / 1000)
	store.stopwatch.end = Math.round(new Date().getTime() / 1000)
	localStorage.setItem('stopwatch_end', Math.round(new Date().getTime() / 1000))
	document.getElementById('dashboard-stopwatch').innerText = '00:00:00'
	document.getElementById('dashboard-stopwatch-playbutton').classList.remove('is-hidden')
	document.getElementById('dashboard-stopwatch-pausebutton').classList.add('is-hidden')
	rest(store.user.server + '/optimus-base/users/' + store.user.id + '/preferences/stopwatch', 'POST', JSON.stringify(store.stopwatch))
}

window.stopwatch_pause = () => 
{
	clearInterval(window.stopwatch_updater)
	store.stopwatch.status = 'stopped'
	document.getElementById('dashboard-stopwatch-playbutton').classList.remove('is-hidden')
	document.getElementById('dashboard-stopwatch-pausebutton').classList.add('is-hidden')
	rest(store.user.server + '/optimus-base/users/' + store.user.id + '/preferences/stopwatch', 'POST', JSON.stringify(store.stopwatch))
}
