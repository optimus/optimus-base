export default class OptimusBaseUserTabs
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.id
	}

	async init()
	{
		loader(this.target)
		let response = await rest('https://' + this.server + '/optimus-base/users/' + this.owner)
		if (response.code == 404)
			return load('/404.html', main) && loader(this.target, false)
		await load('/services/optimus-base/users/editor.html', this.target)
		main.querySelector('.title').innerText = response.data.displayname.toUpperCase()

		let tabs =
			[
				{
					id: "tab_account",
					text: "Compte",
					link: "/services/optimus-base/users/account.js",
					default: true,
					position: 100
				},
				{
					id: "tab_services",
					text: "Services",
					link: "/services/optimus-base/users/services.js",
					position: 200
				},
				{
					id: "tab_shares",
					text: "Partages",
					link: "/services/optimus-base/users/shares.js",
					position: 300
				},
				{
					id: "tab_notifications",
					text: "Notifications",
					link: "/services/optimus-base/users/notifications.js",
					position: 400,
				},
			]

		let owner_services = (this.owner != store.user.id) ? await rest(store.user.server + '/optimus-base/users/' + this.owner + '/services').then(response => response.data.map(service => service.name)) : store.services.map(service => service.name)
		store.services.map(service => 
		{
			if (owner_services.includes(service.name) && service.optimus_base_user_tabs)
				tabs = tabs.concat(service.optimus_base_user_tabs)
		})

		await load('/components/optimus_tabs.js', this.target,
			{
				router: true,
				tabs_container: document.getElementById('user_editor_tabs'),
				content_container: document.getElementById('user_editor_tabs_container'),
				tabs: tabs,
				params: response
			})

		loader(this.target, false)
	}
}