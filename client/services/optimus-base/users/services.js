export default class OptimusBaseUserTabServices
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.id = store.queryParams.id
	}

	async init()
	{
		await load('/services/optimus-base/users/services.html', this.target)
		loader(this.target)

		let component = this

		let availableServices = await rest('https://' + this.server + '/optimus-base/services')
		let userServices = await rest('https://' + this.server + '/optimus-base/users/' + this.id + '/services')

		for (let service of availableServices.data)
			if (service.name != 'optimus-base' && service.name != 'optimus-databases')
			{
				let clone = document.getElementById('service-template').content.cloneNode(true)
				clone.querySelector('.serviceName').innerText = service.displayname
				clone.querySelector('.serviceVersion').innerText = service.version_display === '' ? 'inconnue' : service.version_display
				clone.querySelector('.serviceDescription').innerText = service.short_description.fr
				clone.querySelector('.serviceStatus').checked = userServices.data.some(el => el.name == service.name)
				clone.querySelector('.serviceStatus').id = 'switch_' + service.name
				clone.querySelector('.serviceStatus + label').htmlFor = 'switch_' + service.name
				clone.querySelector('.serviceStatus + label').active = userServices.data.some(el => el.name == service.name)
				clone.querySelector('.serviceStatus + label').onclick = function ()
				{
					if (store.origin == 'https://optimus.demoptimus5.ovh')
					{
						this.click()
						return optimusToast("Vous ne pouvez pas désactiver les services du compte demo", 'is-warning')
					}

					rest('https://' + component.server + '/' + this.htmlFor.replace('switch_', '') + '/' + component.id + '/service', this.active ? 'DELETE' : 'POST', null, 'toast', this)
						.then(response => 
						{
							let selected_service = response.object.htmlFor.replace('switch_', '')
							if (response.code >= 200 && response.code <= 299)
							{
								if (store.user.id == component.id)
									if (response.object.active)
										store.services.filter(service => service.name == selected_service)[0].logout()
									else
										load('/services/' + selected_service + '/service.js').then(service => service.login()).then(() => load_user_preferences())
								response.object.active = !response.object.active
								document.getElementById(response.object.htmlFor).checked = response.object.active
							}
							else
								document.getElementById(response.object.htmlFor).checked = !document.getElementById(response.object.htmlFor).checked
						})
				}
				document.getElementById('services_container').appendChild(clone)

			}

		loader(this.target, false)
	}
}
