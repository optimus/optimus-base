export default class serverUsers
{
	constructor(target, params)
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		let table = await load('/components/optimus_table/index.js', this.target,
			{
				id: 'users',
				title: null,
				version: 3,
				resource: 'users',
				addIcon: 'fa-user-plus',
				url: 'https://' + store.user.server + '/optimus-base/users',
				striped: true,
				bordered: true,
				columnSeparator: true,
				fullscreen: true,
				tabulator:
				{
					progressiveLoad: 'auto',
					columns:
						[
							{ field: "id", title: 'ID', sorter: 'number', sorterParams: { type: 'integer' }, hozAlign: "center" },
							{
								field: 'type', title: 'Type', hozAlign: "center", formatter: cell =>
								{
									if (cell.getValue() == 1)
										return '<i class="fa fa-user"></i>'
									else if (cell.getValue() == 2)
										return '<i class="fa fa-building"></i>'
								}
							},
							{ field: 'displayname', title: 'Nom' },
							{ field: 'email', title: 'Email' },
							{ field: 'server', title: 'Serveur' },
							{ field: 'firstname', title: 'Prénom' },
							{ field: 'lastname', title: 'Nom de famille' },
							{ field: 'status', title: 'Actif', formatter: "tickCross", hozAlign: "center" },
							{ field: 'admin', title: 'Admin', formatter: "tickCross", hozAlign: "center" },
						],
				},
				rowClick: (event, row) => router('optimus-base/users/editor?id=' + row.getIndex() + '#general'),
				addClick: () => rest(store.user.server + '/optimus-base/users', 'POST').then(data => router('optimus-base/users/editor?id=' + data.data.id + '#general'))
			})
		document.querySelector('.owner-button').classList.add('is-hidden')
	}
}