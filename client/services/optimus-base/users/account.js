export default class OptimusBaseUserTabGeneral
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.id = store.queryParams.id || store.user.id
	}

	async init()
	{
		await load('/services/optimus-base/users/account.html', this.target)
		loader(this.target)
		this.user = await rest('https://' + this.server + '/optimus-base/users/' + this.id).then(response => response.data)

		await load('/components/optimus_form.js', this.target, this.user).then(() => this.target.querySelectorAll('.box').forEach(object => loader(object, false)))
		main.querySelector('.title').innerText = this.user.displayname.toUpperCase()
		if (this.user?.restrictions?.includes('delete'))
			this.target.querySelector('.delete-button').classList.add('is-hidden')

		if (store.origin == 'https://optimus.demoptimus5.ovh')
			this.target.querySelector('[data-field="email"]').setAttribute('disabled', 'disabled')

		this.target.querySelector('.email-change-button').onclick = () => modal.open('/services/optimus-base/users/account_email_change_modal.js', false, this)

		this.target.querySelector('.password-change-button').onclick = () => modal.open('/services/optimus-base/users/account_password_change_modal.js', false, this)

		loader(this.target, false)
	}
}