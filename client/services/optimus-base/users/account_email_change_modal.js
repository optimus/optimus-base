export default class OptimusAvocatsDossierIntervenantsAddIntervenantModal
{
	constructor(target, params) 
	{
		this.target = target
		this.component = params
	}

	async init()
	{
		await load('/services/optimus-base/users/account_email_change_modal.html', this.target)

		let domains = await rest('https://' + this.component.server + '/optimus-base/domains').then(response => response.data)
		domains.forEach(domain => modal.querySelector('.mailbox-alias-domains').options.add(new Option(domain.domain)))

		modal.querySelector('.mailbox-alias-input').value = this.component.target.querySelector("[data-field='email']").value.split('@')[0]
		modal.querySelector('.mailbox-alias-domains').value = this.component.target.querySelector("[data-field='email']").value.split('@')[1]
		modal.querySelector('.mailbox-alias-input').onkeyup = event => event.key == "Enter" && modal.querySelector('.change-button').click()
		if (this.component.id == store.user.id)
			modal.querySelector('.notification').classList.remove('is-hidden')

		setTimeout(() => modal.querySelector('.mailbox-alias-input').focus(), 10)

		modal.querySelector('.change-button').onclick = () =>
			rest('https://' + this.component.server + '/optimus-base/users/' + this.component.id, 'PATCH', { email: modal.querySelector('.mailbox-alias-input').value.toLowerCase() + '@' + modal.querySelector('.mailbox-alias-domains').value.toLowerCase() })
				.then(response => 
				{
					if (response.code == 200)
						if (this.component.user.id == store.user.id)
							user_logout()
						else
						{
							this.component.target.querySelector("[data-field='email']").value = modal.querySelector('.mailbox-alias-input').value.toLowerCase() + '@' + modal.querySelector('.mailbox-alias-domains').value.toLowerCase()
							modal.close()
						}
				})

		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.cancel-button').onclick = () => modal.close()
	}
}