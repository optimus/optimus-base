export default class OptimusAvocatsDossierIntervenantsAddIntervenantModal
{
	constructor(target, params) 
	{
		this.target = target
		this.component = params
	}

	async init()
	{
		await load('/services/optimus-base/users/account_password_change_modal.html', this.target)

		modal.querySelector('.password-input').onkeyup = event => event.key == "Enter" && modal.querySelector('.change-button').click()

		if (this.component.user.id == store.user.id)
			modal.querySelector('.logout-notification').classList.remove('is-hidden')

		setTimeout(() => modal.querySelector('.password-input').focus(), 10)

		modal.querySelector('.password-view-icon').onmousedown = () => modal.querySelector('.password-input').type = "text"
		modal.querySelector('.password-view-icon').onmouseup = () => modal.querySelector('.password-input').type = "password"

		modal.querySelector('.change-button').onclick = () =>
			rest('https://' + this.component.server + '/optimus-base/users/' + this.component.id, 'PATCH', { password: modal.querySelector('.password-input').value })
				.then(response => 
				{
					if (response.code == 200)
						if (this.component.user.id == store.user.id)
							user_logout()
						else
							modal.close()
				})

		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.cancel-button').onclick = () => modal.close()
	}
}