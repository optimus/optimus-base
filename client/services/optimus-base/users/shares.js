export default class serverUsers
{
	constructor(target, params)
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		let table = await load('/components/optimus_authorizations/index.js', this.target,
			{
				server: store.user.server,
				url: 'https://{server}/optimus-base/authorizations',

				showTitle: true,
				title: 'Ressources que cet utilisateur partage',
				showSubtitle: true,
				subtitle: null,
				showAddButton: 'top',
				pagination: true,

				striped: true,
				bordered: true,
				columnSeparator: true,

				columns: ['user', 'resource', 'resource_displayname', 'read', 'write', 'create', 'delete'],
				headerFilterInputs: ['user', 'owner', 'resource', 'resource_displayname'],

				filters:
				{
					owner: this.id,
				},

				editor:
				{
					read: true
				},
			})
	}
}