export default class OptimusBaseUserTabNotifications
{
	constructor(target, params)
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		let table = await load('/components/optimus_table/index.js', this.target,
			{
				id: 'notifications',
				title: null,
				resource: 'notifications',
				addIcon: 'fa-plus',
				url: 'https://' + store.user.server + '/optimus-base/users/' + this.id + '/notifications',
				striped: true,
				bordered: true,
				columnSeparator: true,
				fullscreen: true,
				tabulator:
				{
					progressiveLoad: 'auto',
					columns:
						[
							{ field: "id", title: 'ID', sorter: 'number', sorterParams: { type: 'integer' }, hozAlign: "center" },
							{ field: "icon", title: '', titleFormatter: () => undefined, formatter: cell => '<i class="' + cell.getValue() + '"></i>', width: 41, resizable: false, headerSort: false, frozen: false, print: false, download: false, clipboard: false },
							{ field: 'date', title: 'Date', sorter: 'datetime', sorterParams: { format: "yyyy-MM-dd HH:mm:ss" }, hozAlign: 'center' },
							{ field: 'title', title: 'Titre' },
							{ field: 'text', title: 'Message' },
							{ field: 'link', title: 'Lien' },
							{
								field: 'archived', title: 'Archivée', formatter: "tickCross", formatterParams: { tickElement: "<i class='fa fa-check'></i>", crossElement: false }, hozAlign: "center",
								cellClick: function (e, cell)
								{
									rest(store.user.server + '/optimus-base/users/' + (store.queryParams.owner || store.user.id) + '/notifications/' + cell.getRow().getData().id, 'PATCH', { archived: !cell.getValue() }, 'toast')
										.then(response => 
										{
											if (response.code == 200)
											{
												if (response.data.archived == true)
													notificationmenu.remove(cell.getRow().getData().id.toString())
												else
													notificationmenu.add(response.data.id, response.data.colorclass, response.data.icon, response.data.title, response.data.text, response.data.datetime, response.data.link)
												cell.setValue(response.data.archived)
											}
										})
								}
							},
							{
								field: "delete", title: '', titleFormatter: () => undefined, formatter: () => '<i class="fas fa-trash is-clickable has-text-danger"></i>', width: 41, resizable: false, headerSort: false, frozen: true, print: false, download: false, clipboard: false,
								cellClick: function (e, cell)
								{
									rest(store.user.server + '/optimus-base/users/' + (store.queryParams.owner || store.user.id) + '/notifications/' + cell.getRow().getData().id, 'DELETE', {}, 'toast')
										.then(data => 
										{
											if (data.code == 200)
											{
												notificationmenu.remove(cell.getRow().getData().id)
												cell.getTable().setData()
											}
										})
								}
							},
						],
				},
				rowClick: () => true,
			})

		this.target.querySelector('.optimus-table-changeview-buttons > .button.owner-button').classList.add('is-hidden')
		this.target.querySelector('.optimus-table-changeview-buttons > .button.add-button').classList.add('is-hidden')

		this.websocketIncomingListener = message => 
		{
			if (message.detail.command == 'notification')
				table.setData()
		}
		optimusWebsocket.addEventListener('incoming', this.websocketIncomingListener)
	}

	onUnload()
	{
		optimusWebsocket.removeEventListener('incoming', this.websocketIncomingListener)
		return this
	}
}