export default class userAccount
{
	constructor() 
	{
	}

	async init()
	{
		store.queryParams.id = store.user.id
		await load('/services/optimus-base/users/shares.js', main)
		main.querySelector('.title').innerText = 'Ressources que vous partagez'
	}
}