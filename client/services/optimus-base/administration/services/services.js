
export default class Services
{
	constructor(target)
	{
		this.target = target

		this.websocketIncomingListener = message => 
		{
			if (message.detail.command == 'service_install' && document.getElementById('service_' + message.detail.data.service))
			{
				document.getElementById('service_' + message.detail.data.service).querySelector('.service-update-button')?.classList.toggle('is-loading', message.detail.data.progress < 100)
				document.getElementById('service_' + message.detail.data.service).querySelector('.service-install-button')?.classList.toggle('is-loading', message.detail.data.progress < 100)
				document.getElementById('service_' + message.detail.data.service).querySelector('.service-update-button')?.classList.toggle('is-hidden', message.detail.data.progress == 100)
				document.getElementById('service_' + message.detail.data.service).querySelector('.progress-container').classList.toggle('is-hidden', message.detail.data.progress == 100)
				document.getElementById('service_' + message.detail.data.service).querySelector('.progress').value = message.detail.data.progress
				if (message.detail.data.progress == 100)
				{
					for (let key in localStorage)
						if (key.includes('constants_'))
							delete localStorage[key]

					optimusToast('Le service ' + message.detail.data.service + ' a été installé avec succès', 'is-success')
					if (document.getElementById('service_' + message.detail.data.service).querySelector('.service-install-button'))
					{
						document.getElementById('service_' + message.detail.data.service).querySelector('.service-install-button').disabled = true
						document.getElementById('service_' + message.detail.data.service).querySelector('.service-install-button > span:last-of-type').innerText = 'Installé!'
					}
					document.getElementById('service_' + message.detail.data.service).querySelector('.service-title').innerText = message.detail.data.manifest.displayname
					if (document.getElementById('service_' + message.detail.data.service).querySelector('.service-version-branch'))
						document.getElementById('service_' + message.detail.data.service).querySelector('.service-version-branch').innerText = message.detail.data.manifest.version_display + '-' + message.detail.data.branch
					if (document.getElementById('service_' + message.detail.data.service).querySelector('.service-start-button'))
						document.getElementById('service_' + message.detail.data.service).querySelector('.service-start-button').classList.add('is-hidden')
					if (document.getElementById('service_' + message.detail.data.service).querySelector('.service-stop-button'))
						document.getElementById('service_' + message.detail.data.service).querySelector('.service-stop-button').classList.remove('is-hidden')
					if (message.detail.data.service == 'optimus-base')
					{
						loader(document.body)
						setTimeout(() => window.location.reload(), 2000)
					}
					else
						rest(store.user.server + '/optimus-base/users/' + store.user.id + '/services')
							.then(response =>
								response.data.find(service => service.name == message.detail.data.service) &&
								load('/services/' + message.detail.data.service + '/service.js')
									.then(() => store.services.find(service => service.name == message.detail.data.service).login()))
				}
			}
		}
		optimusWebsocket.removeEventListener('incoming', this.websocketIncomingListener)
		optimusWebsocket.addEventListener('incoming', this.websocketIncomingListener)
	}

	async init()
	{
		if (store.user.admin != 1)
			return optimusToast('ce module est réservé aux administrateurs', 'is-danger')

		await load('/services/optimus-base/administration/services/services.html', this.target)

		//ADD SERVICE BUTTON
		this.target.querySelector('.add-service-button').onclick = () => this.listAvailableServices()

		//RETURN BUTTON
		this.target.querySelector('.return-button').onclick = () => this.listInstalledServices()

		//BRANCHES SELECTOR
		this.current_branch = await rest(store.user.server + '/optimus-base/server/preferences/branch', 'GET', null, null).then(response => response.data)
		if (store.services.find(service => service.name == 'optimus-devtools'))
			this.target.querySelector('.branch-column').classList.remove('is-hidden')
		else
			this.target.querySelector('.beta-column').classList.remove('is-hidden')
		this.target.querySelector('.beta-switch').checked = this.current_branch == 'beta'
		this.target.querySelector('.beta-switch').onchange = event => 
		{
			if (event.target.checked == true)
				modal.open('/services/optimus-base/administration/services/beta_modal.js', false, this)
			else
				modal.open('/services/optimus-base/administration/services/stable_modal.js', false, this)
		}
		this.target.querySelector('.branches-selector > select').value = this.current_branch
		this.target.querySelector('.branches-selector > select').onchange = event => modal.open('/services/optimus-base/administration/services/' + event.target.value + '_modal.js', false, this)

		//LISTE LES SERVICES INSTALLES
		this.listInstalledServices()
	}

	//LISTE LES SERVICES INSTALLES
	async listInstalledServices()
	{
		loader(this.target, true)
		clear(document.getElementById('services-container'))
		this.target.querySelector('.add-service-column').classList.remove('is-hidden')
		this.target.querySelector('.return-column').classList.add('is-hidden')

		this.current_branch = await rest(store.user.server + '/optimus-base/server/preferences/branch', 'GET', null, null).then(response => response.data)

		rest(store.user.server + '/optimus-base/services', 'GET')
			.then(services => 
			{
				//CREE UN BLOCK POUR CHAQUE SERVICE INSTALLE
				for (const service of services.data)
				{
					const template = document.querySelector('#service-template').content.cloneNode(true)

					//ICON AND TITLE
					template.querySelector('div').id = 'service_' + service.name
					template.querySelector('.service-icon').classList.add(...service.icon.split(' '))
					template.querySelector('.service-title').innerText = service.displayname
					template.querySelector('.service-version-branch').innerText = service.version_display + '-' + service.branch

					//START BUTTON
					template.querySelector('.service-start-button').classList.toggle('is-hidden', service.status)
					template.querySelector('.service-start-button').onclick = function ()
					{
						this.classList.add('is-loading')
						rest(store.user.server + '/optimus-base/services/' + service.name + '/start', 'POST')
							.then(response => 
							{
								if (response.code == 200)
								{
									this.classList.remove('is-loading')
									this.classList.add('is-hidden')
									document.getElementById('service_' + service.name).querySelector('.service-stop-button').classList.remove('is-hidden')
									document.getElementById('service_' + service.name).querySelector('.service-stats-button').classList.remove('is-invisible')
								}
							})
					}

					//UPDATE BUTTON
					template.querySelector('.service-update-button').onclick = () => rest(store.user.server + '/optimus-base/services', 'POST',
						{
							"registry": service.registry,
							"group": service.group,
							"project": service.name,
							"version": service.version,
							"branch": service.branch
						}, service.name == 'optimus-base' ? null : 'toast')
						.then(response =>
						{
							if (response.code != 201)
							{
								optimusToast('Le service ' + message.detail.data.service + ' a été installé avec succès', 'is-success')
								if (document.getElementById('service_' + message.detail.data.service).querySelector('.service-install-button'))
								{
									document.getElementById('service_' + message.detail.data.service).querySelector('.service-install-button').disabled = true
									document.getElementById('service_' + message.detail.data.service).querySelector('.service-install-button > span:last-of-type').innerText = 'Installé!'
								}
								document.getElementById('service_' + message.detail.data.service).querySelector('.service-title').innerText = message.detail.data.manifest.displayname
								if (document.getElementById('service_' + message.detail.data.service).querySelector('.service-version-branch'))
									document.getElementById('service_' + message.detail.data.service).querySelector('.service-version-branch').innerText = message.detail.data.manifest.version_display + '-' + message.detail.data.branch
								if (document.getElementById('service_' + message.detail.data.service).querySelector('.service-start-button'))
									document.getElementById('service_' + message.detail.data.service).querySelector('.service-start-button').classList.add('is-hidden')
								if (document.getElementById('service_' + message.detail.data.service).querySelector('.service-stop-button'))
									document.getElementById('service_' + message.detail.data.service).querySelector('.service-stop-button').classList.remove('is-hidden')
								if (message.detail.data.service == 'optimus-base')
								{
									loader(document.body)
									setTimeout(() => window.location.reload(), 2000)
								}
							}
						})

					//STOP BUTTON
					template.querySelector('.service-stop-button').classList.toggle('is-hidden', !service.status)
					if (service.name == 'optimus-base' || service.name == 'optimus-databases')
					{
						template.querySelector('.service-users-button').disabled = true
						template.querySelector('.service-users-button').title = 'Tous les utilisateurs doivent nécessairement activer ce service'
						template.querySelector('.service-delete-button').disabled = true
						template.querySelector('.service-delete-button').title = 'Ce service ne peut pas être supprimé car il est nécessaire au fonctionnement du système'
						template.querySelector('.service-stop-button').disabled = true
						template.querySelector('.service-stop-button').title = 'Ce service ne peut pas être stoppé car il est nécessaire au fonctionnement du système'
					}
					else
						template.querySelector('.service-stop-button').onclick = function ()
						{
							this.classList.add('is-loading')
							rest(store.user.server + '/optimus-base/services/' + service.name + '/stop', 'POST')
								.then(response => 
								{
									if (response.code == 200)
									{
										this.classList.remove('is-loading')
										this.classList.add('is-hidden')
										document.getElementById('service_' + service.name).querySelector('.service-start-button').classList.remove('is-hidden')
										document.getElementById('service_' + service.name).querySelector('.service-stats-button').classList.add('is-invisible')
									}
								})
						}

					//LOGS BUTTON
					template.querySelector('.service-logs-button').onclick = () => modal.open('/services/optimus-base/administration/services/logs_modal.js', false, service)

					//STATS BUTTON
					template.querySelector('.service-stats-button').classList.toggle('is-invisible', service.status == 0)
					template.querySelector('.service-stats-button').onclick = () => modal.open('/services/optimus-base/administration/services/stats_modal.js', false, service)

					//INFO BUTTON
					template.querySelector('.service-info-button').onclick = () => modal.open('/services/optimus-base/administration/services/info_modal.js', false, service)

					//USER MANAGEMENT BUTTON
					template.querySelector('.service-users-button').onclick = () => modal.open('/services/optimus-base/administration/services/users_modal.js', false, service)

					//DELETE BUTTON
					template.querySelector('.service-delete-button').onclick = () => modal.open('/services/optimus-base/administration/services/delete_modal.js', false, service)

					document.getElementById('services-container').appendChild(template)

					//CHECK FOR UPDATE
					rest(store.user.server + '/optimus-base/services/' + service.name + '/hasupdate')
						.then(response => response.data === true ? document.getElementById('service_' + service.name).querySelector('.service-update-button').classList.remove('is-loading') : document.getElementById('service_' + service.name).querySelector('.service-update-button').classList.add('is-hidden'))

					//CHECK FOR ALTERNATIVE VERSIONS
					fetch('https://git.cybertron.fr/api/v4/projects?topic=service&simple=true&search=' + service.name)
						.then(response => response.json())
						.then(response => 
						{
							this.getVersions(response[0].id)
								.then(versions => 
								{
									if (this.current_branch == 'stable')
										return versions.filter(version => version.includes('stable'))
									else if (this.current_branch == 'beta')
										return versions.filter(version => version.includes('stable') || version.includes('beta'))
									else
										return versions
								})
								.then(versions => 
								{
									if (versions.length > 1)
									{
										document.getElementById('service_' + service.name).querySelector('.service-version-branch-change').classList.remove('is-hidden')
										document.getElementById('service_' + service.name).querySelector('.service-title-version').classList.add('is-clickable')
										document.getElementById('service_' + service.name).querySelector('.service-title-version').onclick = () => modal.open('/services/optimus-base/administration/services/change_branch_modal.js', false, { service: service, getVersions: this.getVersions })
									}
								})
						})
				}
				loader(this.target, false)
			})
	}

	//LISTE LES SERVICES DISPONIBLES SUR LE GIT
	async listAvailableServices()
	{
		clear(document.getElementById('services-container'))
		this.target.querySelector('.add-service-column').classList.add('is-hidden')
		this.target.querySelector('.return-column').classList.remove('is-hidden')
		this.target.querySelector('.return-button').classList.add('is-loading')

		this.current_branch = await rest(store.user.server + '/optimus-base/server/preferences/branch', 'GET', null, null).then(response => response.data)

		let installed_services = await rest(store.user.server + '/optimus-base/services')
		installed_services = installed_services.data.map(service => service.name)

		let projects = await fetch('https://git.cybertron.fr/api/v4/projects?topic=service&simple=true').then(response => response.json())
		projects = projects.filter(project => !installed_services.includes(project.name))

		for (let project of projects)
		{
			project.versions = await this.getVersions(project.id)
			if (this.current_branch == 'stable')
				project.versions = project.versions.filter(version => version.includes('stable'))
			else if (this.current_branch == 'beta')
				project.versions = project.versions.filter(version => (version.includes('stable') || version.includes('beta')))
			project.versions.sort().reverse()
		}
		projects = projects.filter(project => project.versions.length > 0)

		if (projects.length == 0)
			this.target.querySelector('#services-container').innerText = 'Tous les services disponibles ont été installés'

		for (let project of projects)
		{
			let template = this.target.querySelector('#new-service-template').content.cloneNode(true)
			template.querySelector('div').id = 'service_' + project.name

			for (let version of project.versions)
				template.querySelector('.service-versions > select').add(new Option(version, version))

			template.querySelector('.service-versions > select').onchange = async event => this.getManifest(project.id, event.target.value)

			template.querySelector('.service-install-button').onclick = () => rest(store.user.server + '/optimus-base/services', 'POST',
				{
					"registry": 'git.cybertron.fr:5050',
					"group": project.namespace.name,
					"project": project.name,
					"version": document.getElementById('service_' + project.name).querySelector('.service-versions > select').value.split('-')[0],
					"branch": document.getElementById('service_' + project.name).querySelector('.service-versions > select').value.split('-')[1]
				}, project.name == 'optimus-base' ? null : 'toast')
				.then(response =>
				{
					if (response.code == 201)
						document.getElementById('service_' + project.name).querySelector('.service-versions').classList.add('is-hidden')
					else
					{
						document.getElementById('service_' + project.name).querySelector('.progress-container').classList.add('is-hidden')
						document.getElementById('service_' + project.name).querySelector('.service-install-button').classList.remove('is-loading')
					}
				})

			this.target.querySelector('#services-container').appendChild(template)

			this.getManifest(project.id, project.versions[0])
		}

		this.target.querySelector('.return-button').classList.remove('is-loading')
	}

	//RECUPERE LE MANIFEST DE LA VERSION ET MET A JOUR LA BOX
	async getManifest(project_id, version)
	{
		return fetch('https://git.cybertron.fr/api/v4/projects/' + project_id + '/repository/files/manifest.json?ref=' + version, { headers: { 'Content-Type': 'application/json; charset=UTF-8' } })
			.then(async response => 
			{
				if (!response.ok)
					return optimusToast('Cette branche ne peut pas être installée car elle ne contient pas de fichier manifest', 'is-danger')
				response = await response.json()
				response = decodeURIComponent(atob(response.content).split('').map(c => '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)).join(''))
				let manifest = JSON.parse(response)

				document.getElementById('service_' + manifest.name).querySelector('.service-info-button').onclick = () => modal.open('/services/optimus-base/administration/services/info_modal.js', false, manifest)

				if (manifest?.icon)
					document.getElementById('service_' + manifest.name).querySelector('.service-icon').classList.add(...manifest.icon.split(' '))
				if (manifest?.displayname)
					document.getElementById('service_' + manifest.name).querySelector('.service-title').innerText = manifest.displayname
				if (manifest?.short_description?.fr)
					document.getElementById('service_' + manifest.name).querySelector('.service-short-description').innerText = manifest.short_description.fr

				return manifest
			})
	}

	//REINSTALLE TOUS LES SERVICES EXISTANTS SOUS UNE NOUVELLE BRANCHE
	async reinstall(newbranch)
	{
		let services = await rest(store.user.server + '/optimus-base/services', 'GET')
		let base = services.data.splice(services.data.findIndex(service => service.name == 'optimus-base'), 1)
		services.data.push(...base)

		for (let service of services.data)
			await rest(store.user.server + '/optimus-base/services', 'POST',
				{
					"registry": service.registry,
					"group": "optimus",
					"project": service.name,
					"version": service.version,
					"branch": newbranch
				}, null)
	}

	//RECUPERE LA LISTE DES VERSIONS DISPONIBLES SUR LE GIT
	async getVersions(project_id)
	{
		return fetch('https://git.cybertron.fr/api/v4/projects/' + project_id + '/repository/branches', { headers: { 'Content-Type': 'application/json; charset=UTF-8', } })
			.then(async response => 
			{
				if (!response.ok)
					return false
				response = await response.json()
				response = response.map(version => version.name)
				return response
			})
	}

	onUnload()
	{
		optimusWebsocket.removeEventListener('incoming', this.websocketIncomingListener)
		return this
	}
}