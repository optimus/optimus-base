export default class serviceBranchStableModal
{
	constructor(target, params)
	{
		this.target = target
		this.service = params.service
		this.getVersions = params.getVersions
	}

	async init()
	{
		if (store.user.admin != 1)
		{
			setTimeout(() => modal.close(), 100)
			return optimusToast('ce module est réservé aux administrateurs', 'is-danger')
		}

		await load('/services/optimus-base/administration/services/change_branch_modal.html', this.target)

		this.target.querySelector('.modal-card-close').onclick = () => modal.close()

		this.target.querySelector('.cancel-button').onclick = () => modal.close()

		let service = await rest(store.user.server + '/optimus-base/services/' + this.service.name).then(response => response.data)
		let current_branch = await rest(store.user.server + '/optimus-base/server/preferences/branch').then(response => response.data)

		let projects = await fetch('https://git.cybertron.fr/api/v4/projects?topic=service&simple=true&search=' + service.name).then(response => response.json())
		let project = projects[0]
		project.versions = await this.getVersions(project.id)

		if (current_branch == 'stable')
			project.versions = project.versions.filter(version => version.includes('stable'))
		else if (current_branch == 'beta')
			project.versions = project.versions.filter(version => version.includes('stable') || version.includes('beta'))

		project.versions = project.versions.filter(version => version != service.version + '-' + service.branch)

		if (project.versions.length > 0)
			for (let version of project.versions)
				this.target.querySelector('.branches-selector').add(new Option(version, version))

		this.target.querySelector('.change-branch-button').onclick = () => modal.close() &&
			rest(store.user.server + '/optimus-base/services', 'POST',
				{
					"registry": service.registry,
					"group": "optimus",
					"project": service.name,
					"version": service.version,
					"branch": modal.querySelector('.branches-selector').value.split('-')[1]
				}, null)
	}
}