export default class serviceBranchDevModal
{
	constructor(target, params)
	{
		this.target = target
		this.params = params
		this.params.branch = 'dev'
	}

	async init()
	{
		if (store.user.admin != 1)
		{
			setTimeout(() => modal.close(), 100)
			return optimusToast('ce module est réservé aux administrateurs', 'is-danger')
		}

		await load('/services/optimus-base/administration/services/dev_modal.html', this.target)

		this.target.querySelector('.modal-card-close').onclick = () => modal.close()

		this.target.querySelector('.cancel-button').onclick = () =>
			rest(store.user.server + '/optimus-base/server/preferences/branch')
				.then(response => document.querySelector('.branches-selector > select').value = response.data)
				.then(() => modal.close())

		this.target.querySelector('.change-branch-button').onclick = () =>
			rest(store.user.server + '/optimus-base/server/preferences/branch', 'POST', this.params.branch)
				.then(() => modal.open('/services/optimus-base/administration/services/reinstall_modal.js', false, this.params, false))
	}
}