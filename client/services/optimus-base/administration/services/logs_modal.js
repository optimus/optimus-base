
export default class serviceLogsModal
{
	constructor(target, params)
	{
		this.target = target
		this.service = params
	}

	async init()
	{
		if (store.user.admin != 1)
		{
			setTimeout(() => modal.close(), 100)
			return optimusToast('ce module est réservé aux administrateurs', 'is-danger')
		}

		loader(this.target, true, 'list')
		await load('/services/optimus-base/administration/services/logs_modal.html', this.target)

		this.target.querySelector('.modal-card-close').onclick = () => modal.close()

		await rest(store.user.server + '/optimus-base/services/' + this.service.name + '/logs', 'GET')
			.then(logs => 
			{
				logs.data.reverse()
				for (let line of logs.data)
				{
					let template = this.target.querySelector('#log-block').content.cloneNode(true)
					template.querySelector('.log-date').innerText = new Date(line.split(' ')[0]).toISOString().replace('T', ' ').replace('Z', ' ')
					template.querySelector('.log-message').innerText = line.substring(line.indexOf(' ') + 1)
					this.target.querySelector('.modal-card-body').appendChild(template)
				}
			})
		loader(this.target, false)
	}
}