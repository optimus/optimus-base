

export default class serviceDeleteModal
{
	constructor(target, params)
	{
		this.target = target
		this.service = params
	}

	async init()
	{
		if (store.user.admin != 1)
		{
			setTimeout(() => modal.close(), 100)
			return optimusToast('ce module est réservé aux administrateurs', 'is-danger')
		}

		await load('/services/optimus-base/administration/services/delete_modal.html', this.target)

		this.target.querySelector('.modal-card-close').onclick = () => modal.close()
		this.target.querySelector('.cancel-button').onclick = () => modal.close()

		this.target.querySelector('.delete-button').onclick = () => rest(store.user.server + '/optimus-base/services/' + this.service.name, 'DELETE',
			{
				delete_service_databases: document.getElementById('delete-service-databases').checked,
				delete_users_databases: document.getElementById('delete-users-databases').checked,
			})
			.then(response =>
			{
				if (response.code == 200)
				{
					if (response.errors)
					{
						optimusToast('Des erreurs non bloquantes sont survenues pendant le traitement des bases de données. Voir la console pour plus de détail.', 'is-warning')
						console.table(response.errors)
					}
					modal.close()
					router('optimus-base/administration#services')
				}
			})
	}
}