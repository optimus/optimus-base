

export default class serviceBranchReinstallModal
{
	constructor(target, params)
	{
		this.target = target
		this.params = params
	}

	async init()
	{
		if (store.user.admin != 1)
		{
			setTimeout(() => modal.close(), 100)
			return optimusToast('ce module est réservé aux administrateurs', 'is-danger')
		}

		await load('/services/optimus-base/administration/services/reinstall_modal.html', this.target)

		this.target.querySelector('.branch').innerText = this.params.branch

		this.target.querySelector('.modal-card-close').onclick = () => modal.close()

		this.target.querySelector('.no-button').onclick = () => 
		{
			modal.close()
			this.params.listAvailableServices()
		}

		this.target.querySelector('.yes-button').onclick = () =>
		{
			console.log(this)
			this.params.listInstalledServices()
			this.params.reinstall(this.params.branch)
			modal.close()
		}
	}
}