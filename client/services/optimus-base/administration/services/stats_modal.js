
export default class serviceStatsModal
{
	constructor(target, params)
	{
		this.target = target
		this.service = params
	}

	async init()
	{
		await load('/services/optimus-base/administration/services/stats_modal.html', this.target)

		if (store.user.admin != 1)
		{
			setTimeout(() => modal.close(), 100)
			return optimusToast('ce module est réservé aux administrateurs', 'is-danger')
		}

		this.target.querySelector('.modal-card-close').onclick = () => modal.close()

		rest(store.user.server + '/optimus-base/services/' + this.service.name + '/stats', 'GET')
			.then(stats => 
			{
				modal.querySelector('.stats-cpu-usage').classList.remove('is-loading')
				modal.querySelector('.stats-cpu-usage').innerText = Math.round(stats.data.cpu_usage) + ' %'

				modal.querySelector('.stats-memory-usage').classList.remove('is-loading')
				modal.querySelector('.stats-memory-usage').innerText = Math.round(stats.data.memory_usage) + ' %'
			})
	}
}