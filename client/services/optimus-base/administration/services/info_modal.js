export default class serviceInfoModal
{
	constructor(target, params)
	{
		this.target = target
		this.service = params
	}

	async init()
	{
		if (store.user.admin != 1)
		{
			setTimeout(() => modal.close(), 100)
			return optimusToast('ce module est réservé aux administrateurs', 'is-danger')
		}

		await load('/services/optimus-base/administration/services/info_modal.html', this.target)
		loader(document.querySelector('.modal-card'), true)

		this.target.querySelector('.modal-card-close').onclick = () => modal.close()

		let service = this.service
		if (!service.icon)
		{
			let info = await rest(store.user.server + '/optimus-base/services/' + this.service.name)
			service = info.data
		}

		//IDENTIFICATION
		if (service.icon)
			this.target.querySelector('.service-icon').classList.add(...service.icon.split(' '))
		this.target.querySelector('.service-display-name').innerText = service.displayname || null
		this.target.querySelector('.service-name').innerText = service.name || null
		this.target.querySelector('.service-version-display').innerText = service.version_display || null
		this.target.querySelector('.service-branch').innerText = service.branch || null
		let date = new Date(service.version_date.substring(0, 4) + '-' + service.version_date.substring(4, 6) + '-' + service.version_date.substring(6, 8) + 'T' + service.version_date.substring(8, 10) + ':' + service.version_date.substring(10, 12) + ':' + service.version_date.substring(12, 14) + 'Z')
		this.target.querySelector('.service-version-date').innerText = service.version_date ? date.toLocaleDateString() + ' ' + date.toLocaleTimeString() : null

		//RESUME
		if (service.short_description.fr)
		{
			this.target.querySelector('.service-short-description-block').classList.remove('is-hidden')
			this.target.querySelector('.service-short-description').innerText = service.short_description.fr
		}

		//DESCRIPTION
		if (service.description.fr)
		{
			this.target.querySelector('.service-description-block').classList.remove('is-hidden')
			this.target.querySelector('.service-description').innerText = service.description.fr || null
		}

		//EDITEUR
		if (service.publisher)
		{
			this.target.querySelector('.service-publisher-block').classList.remove('is-hidden')
			this.target.querySelector('.service-publisher-name').innerText = service.publisher.name || null
			this.target.querySelector('.service-publisher-type').innerText = service.publisher.type || null

			for (let publisher_link of service.publisher.links)
			{
				let a = document.createElement('a')
				a.href = publisher_link.link
				a.target = '_blank'

				let span_icon = document.createElement('span')
				span_icon.classList.add('icon', 'is-medium')

				let i = document.createElement('i')
				i.classList.add('fa-lg', 'fa-fw', ...publisher_link.icon.split(' '))

				a.appendChild(span_icon)
				span_icon.appendChild(i)

				this.target.querySelector('.service-publisher-links').appendChild(a)
			}
		}

		//LIENS
		if (service.links)
		{
			this.target.querySelector('.service-links-block').classList.remove('is-hidden')
			for (let link of service.links)
			{
				let li = document.createElement('li')

				let a = document.createElement('a')
				a.href = link.link.replace('-branch', '-' + service.branch)
				a.target = '_blank'
				a.innerText = link.text

				li.appendChild(a)

				this.target.querySelector('.service-links').appendChild(li)
			}
		}

		//PREREQUIS
		if (service.prerequisites)
		{
			this.target.querySelector('.service-prerequisites-block').classList.remove('is-hidden')
			for (let prerequisite of service.prerequisites)
			{
				let li = document.createElement('li')
				li.innerText = prerequisite
				this.target.querySelector('.service-prerequisites').appendChild(li)
			}
		}

		//DEVELOPPEURS
		if (service.developers)
		{
			this.target.querySelector('.service-developers-block').classList.remove('is-hidden')
			for (let developer of service.developers)
			{
				let p = document.createElement('p')

				let span = document.createElement('span')
				span.classList.add('mr-1')
				span.innerText = developer?.firstname + ' ' + developer?.lastname.toUpperCase()

				p.appendChild(span)

				if (developer.links)
					for (let developer_link of developer.links)
					{
						let a = document.createElement('a')
						a.href = developer_link.link
						a.target = '_blank'

						let span = document.createElement('span')
						span.classList.add('icon', 'ml-1')

						let i = document.createElement('i')
						i.classList.add('fa-lg', 'fa-fw', ...developer_link.icon.split(' '))

						span.appendChild(i)
						a.appendChild(span)
						p.appendChild(a)
					}

				this.target.querySelector('.service-developers').appendChild(p)
				loader(document.querySelector('.modal-card'), false)
			}
		}
	}

}

