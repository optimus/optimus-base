

export default class ServiceUsersModal
{
	constructor(target, params)
	{
		this.target = target
		this.service = params
	}

	async init()
	{
		if (store.user.admin != 1)
		{
			setTimeout(() => modal.close(), 100)
			return optimusToast('ce module est réservé aux administrateurs', 'is-danger')
		}

		await load('/services/optimus-base/administration/services/users_modal.html', this.target, null, false)

		this.target.querySelector('.modal-card-close').onclick = () => modal.close()
		this.target.querySelector('.cancel-button').onclick = () => modal.close()

		rest(store.user.server + '/optimus-base/users')
			.then(users => 
			{
				for (let user of users.data)
					rest(store.user.server + '/optimus-base/users/' + user.id + '/services')
						.then(user_services =>
						{
							let template = modal.querySelector('#user_template').content.cloneNode(true)

							template.querySelector('.user-name').innerText = user.displayname

							if (this.service.name == 'optimus-base')
							{
								template.querySelector('.service-switch').checked = true
								template.querySelector('.service-switch').disabled = true
							}
							else if (user_services.data.find(service => service.name == this.service.name))
								template.querySelector('.service-switch').checked = true

							template.querySelector('input').id = user.id
							template.querySelector('label').htmlFor = user.id
							template.querySelector('.service-switch').onclick = event => rest(store.user.server + '/' + this.service.name + '/' + event.target.id + '/service', event.target.checked ? 'POST' : 'DELETE')
								.then(response => 
								{
									if (response.code == 201)
									{
										if (event.target.id == store.user.id)
											load('/services/' + this.service.name + '/service.js').then(service => service.login())
										event.target.checked = true
									}
									else if (response.code == 200)
									{
										if (event.target.id == store.user.id)
											store.services.find(service => service.name == this.service.name).logout()
										event.target.checked = false
									}
								})

							modal.querySelector('.modal-card-body').appendChild(template)
						})
			})


	}
}