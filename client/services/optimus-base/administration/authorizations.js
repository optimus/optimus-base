export default class contactShare
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.owner = store.user.id
	}

	async init()
	{
		await load('/components/optimus_authorizations/index.js', this.target,
			{
				server: store.user.server,
				url: 'https://{server}/optimus-base/authorizations',

				showTitle: true,
				title: 'Partages actifs',
				showSubtitle: false,
				subtitle: null,
				showAddButton: 'top',
				pagination: false,

				striped: true,
				bordered: true,
				columnSeparator: true,

				columns: ['owner', 'user', 'resource', 'resource_displayname', 'read', 'write', 'create', 'delete'],
				headerFilterInputs: null,

				filters:
				{
					owner: null,
					user: null,
					resource: null,
				},

				editor:
				{
					blocks: ['server', 'owner', 'resource-type', 'resource', 'user', 'rights'],
					read: true,
					write: false,
					delete: false
				}
			})
	}
}