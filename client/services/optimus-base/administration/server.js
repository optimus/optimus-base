export default class serverStatus
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	async init()
	{
		await load('/services/optimus-base/administration/server.html', this.target)

		Object.defineProperty(Number.prototype, 'humanReadableFilesize', {
			value: function (a, b, c, d)
			{
				return (a = a ? [1e3, 'k', 'B'] : [1024, 'K', 'iB'], b = Math, c = b.log,
					d = c(this) / c(a[0]) | 0, this / b.pow(a[0], d)).toFixed(2)
					+ ' ' + (d ? (a[1] + 'MGTPEZY')[--d] + a[2] : 'Bytes')
			}, writable: false, enumerable: false, configurable: true
		})

		loader(document.getElementById('system_block'), true)
		loader(document.getElementById('disk_block'), true)
		loader(document.getElementById('ovh_vps_info_block'), true)
		loader(document.getElementById('ovh_vps_cpu_usage_block'), true)
		loader(document.getElementById('ovh_vps_memory_usage_block'), true)

		rest(store.user.server + '/optimus-base/server/status', 'GET')
			.then(response =>
			{
				document.getElementById('os').innerText = response.data.system.os
				document.getElementById('os_type').innerText = response.data.system.os_type
				document.getElementById('kernel_version').innerText = response.data.system.kernel_version
				document.getElementById('architecture').innerText = response.data.system.architecture
				if (response.data.nginx)
					document.getElementById('web_server').innerText = 'Nginx' + response.data.nginx.version
				if (response.data.docker)
					document.getElementById('containers_manager').innerText = 'Docker ' + response.data.docker.version
				loader(document.getElementById('system_block'), false)

				for (let disk of response.data.disk)
				{
					let template = document.getElementById('disk_template').content.cloneNode(true)
					template.querySelector('.disk-name').innerText = disk.name
					template.querySelector('.disk-device').innerText = '(' + disk.device + ')'
					template.querySelector('.disk-size').innerText = disk.used.humanReadableFilesize() + ' / ' + disk.total.humanReadableFilesize()
					template.querySelector('.progress').value = Math.round(100 * disk.used / disk.total)
					template.querySelector('.progress-value').innerText = disk.usage + ' %'
					if (disk.usage > 90)
						template.querySelector('.progress').classList.add('is-danger')
					else if (disk.usage > 70)
						template.querySelector('.progress').classList.add('is-warning')
					else
						template.querySelector('.progress').classList.add('is-success')
					document.getElementById('disk_block').appendChild(template)
				}
				loader(document.getElementById('disk_block'), false)
			})

		rest(store.user.server + '/optimus-base/ovh/vps/info', 'GET')
			.then(response =>
			{
				if (!document.getElementById('ovh_vps_model'))
					return loader(document.getElementById('ovh_vps_model'), false)
				if (response.data.id)
				{
					let vps = response.data
					document.getElementById('ovh_vps_model').innerText = vps.model + ' (' + vps.version + ')'
					document.getElementById('ovh_vps_id').innerText = vps.id
					document.getElementById('ovh_vps_vcores').innerText = vps.vcores
					document.getElementById('ovh_vps_memory').innerText = vps.memory.humanReadableFilesize()
					document.getElementById('ovh_vps_disk').innerText = vps.disk.humanReadableFilesize()
					if (vps.disk_freespace > (1024 * 1024 * 1024))
					{
						document.getElementById('ovh_vps_disk_freespace_size').innerText = Math.round(vps.disk_freespace / 1024 / 1024 / 1024)
						document.getElementById('ovh_vps_disk_freespace_warning').classList.remove('is-hidden')
					}
					document.getElementById('reboot_button').onclick = () => 
					{
						document.getElementById('reboot_button').classList.add('is-loading')
						rest(store.user.server + '/optimus-base/ovh/vps/reboot', 'POST')
							.then(response => 
							{
								if (response.code == 200)
								{

									loader(document.body, true)
									setTimeout(() => window.location.reload(), 10000)
								}
							})
					}
					document.getElementById('reboot_button').classList.remove('is-loading')
					loader(document.getElementById('ovh_vps_info_block'), false)
				}
				else
					document.getElementById('ovh_vps_info_block').classList.add('is-hidden')
			})

		rest(store.user.server + '/optimus-base/ovh/vps/automatedbackup', 'GET')
			.then(response =>
			{
				if (!document.getElementById('ovh_vps_automated_backup_restorepoints_loader'))
					return loader(document.getElementById('ovh_vps_automated_backup_restorepoints_loader'), false)
				if (response.data?.enabled == true)
				{
					document.getElementById('ovh_vps_automated_backup_restorepoints_loader').classList.add('is-hidden')
					for (let restorepoint of response.data.restorepoints)
						document.getElementById('ovh_vps_automated_backup_restorepoints').innerHTML += '<div><i class="fas fa-check pr-2"></i> ' + new Date(restorepoint).toLocaleString() + '</div>'
				}
				else
				{
					document.getElementById('ovh_vps_automated_backup_block').classList.add('is-hidden')
					document.getElementById('ovh_vps_automated_backup_warning').classList.remove('is-hidden')
				}
			})

		// rest(store.user.server + '/optimus-base/ovh/vps/cpu/usage', 'GET')
		// 	.then(response => 
		// 	{
		// 		if (!document.getElementById('cpu_usage') || response.data.values.length == 0)
		// 			return loader(document.getElementById('ovh_vps_cpu_usage_block'), false)

		// 		document.getElementById('cpu_usage').innerText = '(' + Math.round(response.data.values.pop().value) + '%)'

		// 		clear(document.getElementById('cpu_usage_charts'))
		// 		load('/components/optimus_chart.js', document.getElementById('cpu_usage_charts'),
		// 			{
		// 				type: 'line',
		// 				data:
		// 				{
		// 					labels: response.data.values.map(usage => new Date(usage.timestamp * 1000).toLocaleTimeString().substr(0, 5)),
		// 					datasets:
		// 						[
		// 							{
		// 								data: response.data.values.map(usage => usage.value),
		// 								borderWidth: 1,
		// 								pointStyle: false
		// 							}
		// 						]
		// 				},
		// 				options: { plugins: { legend: { display: false } } }
		// 			})
		// 			.then(() => loader(document.getElementById('ovh_vps_cpu_usage_block'), false))
		// 	})
		loader(document.getElementById('ovh_vps_cpu_usage_block'), false)

		// rest(store.user.server + '/optimus-base/ovh/vps/memory/usage', 'GET')
		// 	.then(response => 
		// 	{
		// 		if (!document.getElementById('cpu_usage') || response.data.values.length == 0)
		// 			return loader(document.getElementById('ovh_vps_memory_usage_block'), false)

		// 		document.getElementById('memory_usage').innerText = '(' + Math.round(response.data.values.pop().value * 1024 * 1024).humanReadableFilesize() + ')'

		// 		clear(document.getElementById('memory_usage_charts'))
		// 		load('/components/optimus_chart.js', document.getElementById('memory_usage_charts'),
		// 			{
		// 				type: 'line',
		// 				data:
		// 				{
		// 					labels: response.data.values.map(usage => new Date(usage.timestamp * 1000).toLocaleTimeString().substr(0, 5)),
		// 					datasets:
		// 						[
		// 							{
		// 								data: response.data.values.map(usage => usage.value / 1024),
		// 								borderWidth: 1,
		// 								pointStyle: false
		// 							}
		// 						]
		// 				},
		// 				options: { plugins: { legend: { display: false } } }
		// 			})
		// 			.then(() => loader(document.getElementById('ovh_vps_memory_usage_block'), false))
		// 	})
		loader(document.getElementById('ovh_vps_memory_usage_block'), false)
	}
}