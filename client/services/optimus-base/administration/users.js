export default class userAccount
{
	constructor(target) 
	{
		this.target = target
	}

	async init()
	{
		await load('/services/optimus-base/users/index.js', this.target)
	}
}