export default class AdministrationDomains
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
	}

	async init()
	{
		await load('/services/optimus-base/administration/domains.html', this.target)
		if (store.user.admin == 1)
		{
			document.getElementById('add-domain').addEventListener('click', () => add_domain({}))
			document.getElementById('add-domain').classList.add('is-hidden')

			await rest('https://' + store.user.server + '/optimus-base/domains', 'GET', null, 'toast')
				.then(domains =>
				{
					if (domains.data)
						for (const domain of domains.data)
							add_domain(domain)
				})
		}
		else
			optimusToast('ce module est réservé aux administrateurs', 'is-danger')


		async function add_domain(domain)
		{
			const template = document.getElementById('domain_template').content.cloneNode(true)
			const container = template.querySelector('div')
			const input = template.querySelector('input')
			const certificate_expiry = template.querySelector('.certificate_expiry')
			const trash_icon = template.querySelector('.icon .fa-trash')
			const check_icon = template.querySelector('.icon .fa-check')

			container.id = domain.id || ''
			if (domain.id == 1)
				input.disabled = true

			input.value = domain.domain || null
			input.lastvalue = domain.domain || null

			if (domain.certificate_expiry)
				certificate_expiry.innerText = ('Validité du certificat : ' + new Date(domain.certificate_expiry).toLocaleDateString()) || null

			input.onblur = function ()
			{
				if (container.id == '' && this.value == '')
					return this.parentNode.remove()

				this.value = this.value.toLowerCase()

				if (this.value != '' && !this.value.match(new RegExp(/^([a-z0-9][a-z0-9-]*[a-z0-9]\.)+([a-z0-9][a-z0-9-]*[a-z0-9])$/)))
				{
					this.setCustomValidity('La valeur attendue est un nom de domaine.\nExemple : www.cybertron.fr')
					this.reportValidity()
					return false
				}

				if (this.value == this.lastvalue)
					return false

				if (container.id == '')
					rest('https://' + store.user.server + '/optimus-base/domains', 'POST', { 'domain': this.value }, 'toast')
						.then(result =>
						{
							container.id = result.data.id
							this.validateAnimation()
						})
				else if (this.value != '')
					rest('https://' + store.user.server + '/optimus-base/domains/' + this.parentNode.id, 'PATCH', { 'domain': this.value }, 'toast')
						.then(result =>
						{
							if (result.code != 200)
								return input.focus()
							else
								this.validateAnimation()
						})
				else
					trash_icon.click()

				this.lastvalue = this.value
			}

			input.validateAnimation = function ()
			{
				check_icon.classList.remove('is-hidden')
				setTimeout(() => check_icon.classList.add('animate__fadeOut'), 500)
				setTimeout(() => { check_icon.classList.add('is-hidden'); check_icon.classList.remove('animate__fadeOut') }, 1000)
			}

			input.onkeyup = function (event)
			{
				if (event.key == "Enter")
					this.blur()
			}

			if (domain.domain == null)
				input.focus()

			trash_icon.onclick = function ()
			{
				if (container.id == '')
					container.parentNode.removeChild(container)
				else
					rest('https://' + store.user.server + '/optimus-base/domains/' + container.id, 'DELETE', {})
						.then((result) =>
						{
							if (result.code == 200)
								container.parentNode.removeChild(container)
							optimusToast(result.message, 'is-success')
						})
			}

			if (domain.id == 1)
				trash_icon.classList.add('is-hidden')

			document.getElementById('domains').appendChild(template)

			if (input.value == '')
				input.focus()
		}
	}
}