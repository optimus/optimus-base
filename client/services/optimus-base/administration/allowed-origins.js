export default class AdministrationAllowedOrigins
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
	}

	async init()
	{
		await load('/services/optimus-base/administration/allowed-origins.html', this.target)
		if (store.user.admin == 1)
		{
			document.getElementById('add-origin').addEventListener('click', () => add_origin({}))

			await rest('https://' + store.user.server + '/optimus-base/allowed-origins', 'GET', null, 'toast')
				.then(origins =>
				{
					if (origins.data)
						for (const origin of origins.data)
							add_origin(origin)
				})

		}
		else
			optimusToast('ce module est réservé aux administrateurs', 'is-danger')


		async function add_origin(origin)
		{
			const template = document.getElementById('allowed_origin_template').content.cloneNode(true)
			const container = template.querySelector('div')
			const input = template.querySelector('input')
			const trash_icon = template.querySelector('.icon .fa-trash')
			const check_icon = template.querySelector('.icon .fa-check')

			container.id = origin.id || ''
			if (origin.id == 1)
			{
				input.disabled = true
				document.getElementById('allowed_origin_1').innerText = origin.origin
			}
			input.value = origin.origin || null
			input.lastvalue = origin.origin || null

			input.onblur = function ()
			{
				if (container.id == '' && this.value == '')
					return this.parentNode.remove()

				this.value = this.value.toLowerCase()

				if (this.value != '' && !this.value.match(new RegExp(/^(\*)$|^(localhost|(\*\.)?([a-z0-9][a-z0-9-]*[a-z0-9]\.)+[a-z0-9][a-z0-9-]*[a-z0-9]|(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:\.(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3})(:((6553[0-5])|(655[0-2][0-9])|(65[0-4][0-9]{2})|(6[0-4][0-9]{3})|([1-5][0-9]{4})|([0-5]{0,5})|([0-9]{1,4})))?$/,)))
				{
					this.setCustomValidity('La valeur attendue est une origine.\nExemples : * localhost: 8080 *.optimus - avocats.fr  www.optimus - avocats.fr: 9091  192.168.0.5: 3232')
					this.reportValidity()
					return false
				}

				if (this.value == this.lastvalue)
					return false

				if (container.id == '')
					rest('https://' + store.user.server + '/optimus-base/allowed-origins', 'POST', { 'origin': this.value }, 'toast')
						.then(result =>
						{
							container.id = result.data.id
							this.validateAnimation()
						})
				else if (this.value != '')
					rest('https://' + store.user.server + '/optimus-base/allowed-origins/' + this.parentNode.id, 'PATCH', { 'origin': this.value }, 'toast')
						.then(result =>
						{
							if (result.code != 200)
								return input.focus()
							else
								this.validateAnimation()
						})
				else
					trash_icon.click()

				this.lastvalue = this.value
			}

			input.validateAnimation = function ()
			{
				check_icon.classList.remove('is-hidden')
				setTimeout(() => check_icon.classList.add('animate__fadeOut'), 500)
				setTimeout(() => { check_icon.classList.add('is-hidden'); check_icon.classList.remove('animate__fadeOut') }, 1000)
			}

			input.onkeyup = function (event)
			{
				if (event.key == "Enter")
					this.blur()
			}

			if (origin.origin == null)
				input.focus()

			trash_icon.onclick = function ()
			{
				if (container.id == '')
					container.parentNode.removeChild(container)
				else
					rest('https://' + store.user.server + '/optimus-base/allowed-origins/' + container.id, 'DELETE', {})
						.then((result) =>
						{
							if (result.code == 200)
								container.parentNode.removeChild(container)
							optimusToast(result.message, 'is-success')
						})
			}

			if (origin.id == 1)
				trash_icon.classList.add('is-hidden')

			document.getElementById('allowed_origins').appendChild(template)

			if (input.value == '')
				input.focus()
		}
	}
}