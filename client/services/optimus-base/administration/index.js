export default class administrationHome
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		await load('/services/optimus-base/administration/index.html', this.target)
		loader(this.target)

		let tabs =
			[
				{
					id: "tab_users",
					text: "Utilisateurs",
					link: "/services/optimus-base/administration/users.js",
					default: true,
					position: 100
				},
				{
					id: "tab_server",
					text: "Serveur",
					link: "/services/optimus-base/administration/server.js",
					position: 200
				},
				{
					id: "tab_services",
					text: "Services",
					link: "/services/optimus-base/administration/services/services.js",
					position: 300
				},
				{
					id: "tab_allowed-origins",
					text: "Origines autorisées",
					link: "/services/optimus-base/administration/allowed-origins.js",
					position: 400,
				},
				{
					id: "tab_domains",
					text: "Noms de domaine",
					link: "/services/optimus-base/administration/domains.js",
					position: 500,
				},
				{
					id: "tab_authorizations",
					text: "Autorisations",
					link: "/services/optimus-base/administration/authorizations.js",
					position: 600
				},
			]

		let owner_services = (this.owner != store.user.id) ? await rest(store.user.server + '/optimus-base/users/' + this.owner + '/services').then(response => response.data.map(service => service.name)) : store.services.map(service => service.name)
		store.services.map(service => 
		{
			if (owner_services.includes(service.name) && service.optimus_base_administration_tabs)
				tabs = tabs.concat(service.optimus_base_administration_tabs)
		})

		await load('/components/optimus_tabs.js', this.target,
			{
				router: true,
				tabs_container: document.getElementById('administration_tabs'),
				content_container: document.getElementById('administration_tabs_container'),
				tabs: tabs
			})

		loader(this.target, false)
	}
}