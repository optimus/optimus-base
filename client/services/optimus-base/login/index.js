export default class LoginForm
{
	constructor(target, params)
	{
		this.target = target

		let url = new URL(window.location.href)

		if (params?.server)
			this.server = params.server
		else if (localStorage.getItem('user'))
			this.server = JSON.parse(localStorage.getItem('user')).server
		else if (store?.origin?.includes('demoptimus'))
			this.server = 'api.' + store.origin.replace('https://optimus.', '')
		else
			this.server = ''

		if (params?.username)
			this.username = params.username
		else if (localStorage.getItem('user'))
			this.username = JSON.parse(localStorage.getItem('user')).email
		else if (url.host.includes('demoptimus'))
			this.username = 'demo@' + store.origin.replace('https://optimus.', '')
		else
			this.username = ''

		if (params?.password)
			this.password = params.password
		else if (url.host.includes('demoptimus'))
			this.password = 'D3mod3mo!!!!'
		else
			this.password = ''
	}

	async init()
	{
		await load('/services/optimus-base/login/index.html', this.target)
		let login = this.login
		modal.querySelector('.server').value = this?.server
		modal.querySelector('.email').value = this?.username
		modal.querySelector('.password').value = this?.password

		if (store.innerWidth >= 768)
			if (this?.username)
				setTimeout(() => modal.querySelector('.password').focus(), 100)
			else
				setTimeout(() => modal.querySelector('.email').focus(), 100)

		modal.querySelector('.connect').onclick = () => login()
		modal.querySelector('form').onsubmit = () => login()

		modal.querySelector('.cancel').onclick = () =>
		{
			store.login = 'canceled'
			modal.close()
		}

		modal.querySelector('.server').onchange = () =>
		{
			if (modal.querySelector('.server').value.split('.').length == 2)
				modal.querySelector('.server').value = 'api.' + modal.querySelector('.server').value
		}

		modal.querySelector('.email').onchange = () =>
		{
			if (!modal.querySelector('.email').value.includes('@'))
				modal.querySelector('.email').value += '@' + new URL(window.location).hostname.split('.').slice(1).join('.')
			if (modal.querySelector('.server').value == '' && modal.querySelector('.email').value.includes('@'))
				modal.querySelector('.server').value = 'api.' + modal.querySelector('.email').value.split('@')[1]
		}
	}

	login()
	{
		modal.querySelector('.server').classList.remove('has-text-danger', 'is-danger')
		modal.querySelector('.email').classList.remove('has-text-danger', 'is-danger')
		modal.querySelector('.password').classList.remove('has-text-danger', 'is-danger')

		fetch('https://' + modal.querySelector('.server').value.replace('https://', '') + '/optimus-base/login', {
			method: 'POST',
			credentials: 'include',
			body: JSON.stringify({ email: modal.querySelector('.email').value, password: modal.querySelector('.password').value })
		})
			.then((response) => response.json())
			.then((response) =>
			{
				if (response.code === 200)
				{
					store.login = undefined
					response.data.server = modal.querySelector('.server').value.replace('https://', '')
					if (!store.user.server || response.data.server != store.user.server || !store.user.email || response.data.email != store.user.email)
						user_login(response)
					else
					{
						optimusWebsocket = null
						create_websocket()
						modal.close()
					}
					return response
				}
				else if (response.code === 401)
				{
					modal.querySelector('.email').classList.add('has-text-danger', 'is-danger')
					modal.querySelector('.password').classList.add('has-text-danger', 'is-danger')
					modal.querySelector('.modal-container').classList.remove('animate__zoomIn')
					modal.querySelector('.modal-container').classList.add('animate__animated', 'animate__headShake', 'animate__slow')
					modal.querySelector('.modal-container').style.animationDuration = '0.8s'
					setTimeout(function ()
					{
						modal.querySelector('.modal-container').classList.remove('animate__animated', 'animate__headShake')
					}, 500)
					return response
				}
				else
					optimusToast(response.message, 'is-danger')
			})
			.catch((error) =>
			{
				if (error.message == 'Failed to fetch')
				{
					optimusToast('Le serveur spécifié ne répond pas', 'is-danger')
					modal.querySelector('.server').classList.add('has-text-danger', 'is-danger')
				}
				else
					optimusToast('Erreur lors de la connexion au serveur\n' + error.message, 'is-danger')
			})
		return false
	}

	autologin()
	{
		fetch('https://' + this.server.replace('https://', '') + '/optimus-base/login', {
			method: 'POST',
			credentials: 'include',
			body: JSON.stringify({ email: this.username, password: this.password })
		})
			.then(response => response.json())
			.then(response =>
			{
				if (response.code === 200)
				{
					store.login = undefined
					response.data.server = this.server.replace('https://', '')
					user_login(response)
					optimusToast('Connexion réussie pour ' + response.data.displayname, 'is-success')
					return response
				}
				else if (response.code === 401)
					return optimusToast(response.message)
				else
					return response.then(data => optimusToast(data.message, 'is-danger'))
			})
			.catch((error) =>
			{
				if (error.message == 'Failed to fetch')
					return optimusToast('Le serveur spécifié ne répond pas', 'is-danger')
				else
					return optimusToast('Erreur lors de la connexion au serveur\n' + error.message, 'is-danger')
			})
	}
}
