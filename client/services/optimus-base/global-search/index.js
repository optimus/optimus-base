export default class globalSearch
{
	constructor(target, params) 
	{
		this.target = target
		this.query = document.querySelector('.optimus-global-search').value
	}

	async init()
	{
		await load('/services/optimus-base/global-search/index.html', this.target)
		loader(this.target)

		if (this.query)
			store.services.forEach(service =>
			{
				if (!service.global_search)
					return false
				let search_results = service.global_search(this.query)
				if (search_results)
					search_results.then(results =>
					{
						if (results)
							for (let result of results)
								if (result.data)
									result.data.then(items => 
									{
										if (items?.length > 0)
										{
											const template = document.getElementById('global-search-result').content.cloneNode(true)
											template.querySelector('div').id = 'global_search_' + service.name + '_' + result.title

											template.querySelector('.icon-text > span:last-of-type').innerText = result.title

											if (result.icon)
												for (let icon_class of result.icon.split(' '))
													template.querySelector('.icon > i').classList.add(icon_class)

											if (result.icon_color)
												template.querySelector('.icon').style.color = result.icon_color

											items.forEach(item =>
											{
												let span = document.createElement('span')
												span.classList.add('tag', item.colorclass || 'is-info', 'm-2', 'is-clickable')
												span.title = item.title || ''
												span.innerText = item.displayname
												span.addEventListener('click', () => router(item.route))
												template.querySelector('.box').appendChild(span)
											})

											if (document.getElementById('global_search_' + service.name + '_' + result.title))
												document.getElementById('global_search_' + service.name + '_' + result.title).remove()
											this.target.appendChild(template)
										}
									})
					})
			})

		loader(this.target, false)
	}
}