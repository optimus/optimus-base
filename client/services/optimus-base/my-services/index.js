export default class userAccount
{
	constructor() 
	{
	}

	async init()
	{
		store.queryParams.id = store.user.id
		await load('/services/optimus-base/users/services.js', main)
		main.querySelector('.standalone-title').classList.remove('is-hidden')
	}
}