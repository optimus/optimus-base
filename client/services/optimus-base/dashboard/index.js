export default class Dashboard
{
	constructor(target, params)
	{
		this.target = target
		this.params = params
	}

	async init()
	{
		await load('/services/optimus-base/dashboard/index.html', this.target)
		//CLOCK
		this.update_time()
		this.time_updater = setInterval(() => this.update_time(), 1000)

		//QUICK ACTIONS
		window.quickactions_add = (id, type, title, icon, action, colorclass) =>
		{
			if (document.getElementById('quickactions-menu').querySelector('#' + id))
				return false
			let menu_item
			if (type == 'divider')
			{
				menu_item = document.createElement('hr')
				menu_item.classList.add('dropdown-divider')
			}
			else if (type == 'header')
			{
				menu_item = document.createElement('div')
				menu_item.classList.add('optimus-topmenu-header')
				menu_item.innerText = title
			}
			else if (type == 'item')
			{
				menu_item = document.querySelector('#dropdown-item').content.cloneNode(true).querySelector('a')
				menu_item.onmousedown = action
				if (colorclass)
					menu_item.classList.add(colorclass)
				if (icon)
				{
					menu_item.querySelector('span:first-child').className = 'icon fa-lg mr-2'
					menu_item.querySelector('span:first-child > i').className = icon
				}
				menu_item.querySelector('span:last-child').innerText = title
			}
			else
				return false
			menu_item.id = id
			document.getElementById('quickactions-menu').appendChild(menu_item)
		}

		//DEMO
		if (store.origin == 'https://optimus.demoptimus5.ovh' || store.origin == 'https://optimus.vestoptimus.ovh')
			document.getElementById('demo_dashboard_message').classList.remove('is-hidden')

		//FIRST BOOT (NO SERVICE INSTALLED)
		if (store.services.length == 1)
			document.getElementById('no_service_message').classList.remove('is-hidden')

		//RESIZER
		window.dashboard_module_resize = element =>
		{
			element.start_size = parseInt(element.parentNode.className.split(' is-')[1].split(' ')[0])
			element.start_width = element.parentNode.offsetWidth
			element.size = element.start_size
			element.width = element.start_width
			element.offset = Math.round(element.width / element.size)
			element.startX = event.clientX
			element.previous = event.clientX - element.offset
			element.next = event.clientX + element.offset
			element.classList.remove('fa-gear')
			element.classList.add('fa-left-right')
			event.dataTransfer.effectAllowed = 'move'

			element.ondrag = event =>
			{
				if (element.size > 3 && event.clientX && event.clientX < element.previous)
				{
					element.previous -= element.offset
					element.next -= element.offset
					element.parentNode.classList.remove('is-' + element.size)
					element.parentNode.classList.add('is-' + (element.size - 1))
					element.size--
					if (element.parentNode.querySelector('.optimus-charts'))
						window[element.parentNode?.querySelector('.optimus-charts')?.id?.replaceAll('-', '_')]()
				}
				if (element.size < 12 && event.clientX && event.clientX > element.next)
				{
					element.previous += element.offset
					element.next += element.offset
					element.parentNode.classList.remove('is-' + element.size)
					element.parentNode.classList.add('is-' + (element.size + 1))
					element.size++
					if (element.parentNode.querySelector('.optimus-charts'))
						window[element.parentNode?.querySelector('.optimus-charts')?.id?.replaceAll('-', '_')]()
				}
			}

			element.ondragend = event =>
			{
				element.classList.add('fa-gear')
				element.classList.remove('fa-left-right')
			}
		}


		store.services.forEach(service => service?.dashboard())

		// import('/libs/sortable.min.js')
		// 	.then(() => Sortable.create(document.getElementById('dashboard'),
		// 		{
		// 			animation: 200,
		// 			handle: '.box > *',
		// 			forceFallback: true
		// 		}))

		window.dashboard_resize = event =>
		{
			let resizer = event.target
			let element = resizer.parentNode
			element.start_size = parseInt(element.className.split(' is-')[1].split(' ')[0])
			element.start_width = element.offsetWidth
			element.size = element.start_size
			element.width = element.start_width
			element.offset = Math.round(element.width / element.size)
			element.startX = event.clientX
			element.previous = event.clientX - element.offset
			element.next = event.clientX + element.offset

			resizer.ondrag = event =>
			{
				if (element.size > 3 && event.clientX && event.clientX < element.previous)
				{
					element.previous -= element.offset
					element.next -= element.offset
					element.classList.remove('is-' + element.size)
					element.classList.add('is-' + (element.size - 1))
					element.size--
					if (element.querySelector('.optimus-charts'))
						window[element?.querySelector('.optimus-charts')?.id?.replaceAll('-', '_')]()
				}
				if (element.size < 12 && event.clientX && event.clientX > element.next)
				{
					element.previous += element.offset
					element.next += element.offset
					element.classList.remove('is-' + element.size)
					element.classList.add('is-' + (element.size + 1))
					element.size++
					if (element.querySelector('.optimus-charts'))
						window[element?.querySelector('.optimus-charts')?.id?.replaceAll('-', '_')]()
				}
			}

			resizer.onclick = event =>
			{
				event.preventDefault()
				event.stopPropagation()
			}
		}
	}

	update_time()
	{
		if (!main.querySelector('#dashboard-date'))
			return clearInterval(this.time_updater)

		let now = new Date()
		let today = now.toLocaleDateString("fr-FR", { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' })
		main.querySelector('#dashboard-date').innerText = today[0].toUpperCase() + today.slice(1)
		main.querySelector('#dashboard-time').innerText = now.toLocaleTimeString()
	}
}