//VARIABLES INITIALIZATION
const optimus = document.querySelector('.optimus-base')
const curtain = document.querySelector('.optimus-curtain')
const brand = document.querySelector('.optimus-brand')
const logo = document.querySelector('.optimus-logo')
const version = document.querySelector('.optimus-version')
const leftmenu = document.querySelector('.optimus-leftmenu')
const leftmenu_trigger = document.querySelector('.optimus-leftmenu-trigger')
const darkmode_trigger = document.querySelector('.optimus-darkmode-trigger')
const login_trigger = document.querySelector('.optimus-login-trigger')
const topmenus = document.querySelector('.optimus-topmenus')
const notificationmenu = document.querySelector('.optimus-notificationmenu')
const notificationmenu_trigger = document.querySelector('.optimus-notificationmenu-trigger')
const notificationmenu_badge = document.querySelector('.optimus-notificationmenu-badge')
const main = document.querySelector('.optimus-main')
const modal = document.querySelector('.optimus-modal')
const store = new Object
store.innerWidth = window.innerWidth
store.leftmenu = { items: new Array }
store.topmenu = { items: new Array }
store.user = new Object
store.services = new Array
store.components = new Array
const leftmenu_order = JSON.parse(localStorage.getItem('leftmenu_order')) || new Array
const leftmenu_collapsed = JSON.parse(localStorage.getItem('leftmenu_collapsed')) || new Array
var onWindowResize = new Array
var onUnload = new Array
var onThemeChange = new Array
var progress = new Mprogress()
var Prism = { manual: true }

store.libraries = new Object
store.libraries.animate = { name: 'Animate', version: '4.1.1', link: 'https://animate.style' }
store.libraries.bulma = { name: 'Bulma', version: '0.9.4', link: 'https://bulma.io' }
store.libraries.bulmaCalendar = { name: 'Bulma Calendar', version: '6.1.19', link: 'https://bulma-calendar.onrender.com' }
store.libraries.bulmaOSteps = { name: 'Bulma-O-Steps', version: '1.1.0', link: 'https://octoshrimpy.github.io/bulma-o-steps' }
store.libraries.bulmaSlider = { name: 'Bulma Slider', version: '2.0.5', link: 'https://wikiki.github.io/form/slider/' }
store.libraries.bulmaSwitch = { name: 'Bulma Switch', version: '2.0.4', link: 'https://wikiki.github.io/form/switch' }
store.libraries.bulmaToast = { name: 'Bulma Toast', version: '2.4.1', link: 'https://rfoel.github.io/bulma-toast' }
store.libraries.bulmaTooltip = { name: 'Bulma Tooltip', version: '1.2', link: 'https://bulma-tooltip.netlify.app' }
store.libraries.chartjs = { name: 'Chart.js', version: '4.4.2', link: 'https://github.com/chartjs/Chart.js' }
store.libraries.flagIcons = { name: 'Flag Icons', version: '6.6.5', link: 'https://flagicons.lipis.dev' }
store.libraries.fontAwesome = { name: 'Font Awesome', version: '6.4.2', link: 'https://fontawesome.com' }
store.libraries.fullCalendar = { name: 'Fullcalendar', version: '6.1.9', link: 'https://fullcalendar.io' }
store.libraries.geoSearch = { name: 'Geosearch', version: '3.7.0', link: 'https://github.com/smeijer/leaflet-geosearch' }
store.libraries.leaflet = { name: 'Leaflet', version: '1.9.3', link: 'https://leafletjs.com' }
store.libraries.mProgress = { name: 'MProgress', version: ' 0.1.1', link: 'https://github.com/lightningtgc/MProgress.js' }
store.libraries.prism = { name: 'Prism', version: '1.29.0', link: 'https://prismjs.com' }
store.libraries.tabulator = { name: 'Tabulator', version: '6.3.0', link: 'http://tabulator.info' }
store.libraries.RRule = { name: 'RRule', version: '2.7.1', link: 'https://github.com/jakubroztocil/rrule' }
store.libraries.sortablejs = { name: 'Sortable', version: '1.15', link: 'https://github.com/SortableJS/Sortable' }
store.libraries.sheetjs = { name: 'SheetJS', version: '0.19.1', link: 'https://git.sheetjs.com/sheetjs/sheetjs' }
store.libraries.jsPDF = { name: 'jsPDF', version: '2.51', link: 'https://github.com/parallax/jsPDF' }
store.libraries.jsPDF_AutoTable = { name: 'jsPDF-AutoTable', version: '3.5.28', link: 'https://github.com/simonbengtsson/jsPDF-AutoTable' }
store.libraries.swaggerUI = { name: 'Swagger UI', version: '4.15.5', link: 'https://github.com/swagger-api/swagger-ui' }
store.libraries.luxon = { name: 'Luxon', version: '3.2.0', link: 'https://github.com/moment/luxon' }

store.resources = new Array()
store.resources.push({ id: 'all', name: 'resources', description: 'Toutes les ressources', path: '*' })
store.resources.services = new Array()

store.translations = new Object()
store.language = localStorage.getItem('language') || 'fr'

if ((navigator.userAgent.indexOf('Opera') || navigator.userAgent.indexOf('OPR')) != -1) store.navigator = 'Opera'
else if (navigator.userAgent.indexOf('Chrome') != -1) store.navigator = 'Chrome'
else if (navigator.userAgent.indexOf('Mobile/15E149') != -1) store.navigator = 'Chrome'
else if (navigator.userAgent.indexOf('Mobile/13G36') != -1) store.navigator = 'Chrome'
else if (navigator.userAgent.indexOf('Safari') != -1) store.navigator = 'Safari'
else if (navigator.userAgent.indexOf('Firefox') != -1) store.navigator = 'Firefox'
else if (navigator.userAgent.indexOf('MSIE') != -1) store.navigator = 'IE'
else if (navigator.userAgent.indexOf('Edge') != -1) store.navigator = 'Edge'
else if (navigator.userAgent.indexOf('Opera') != -1) store.navigator = 'Opera'
else store.navigator = 'Unknown'

//SERVICE WORKER COMMUNICATION
const serviceworkerBroadcast = new BroadcastChannel('serviceworker')
serviceworkerBroadcast.onmessage = event =>
{
	if (event.data.message == 'update_cache_start')
	{
		Object.keys(localStorage).filter(name => name.includes('constants_')).forEach(name => localStorage.removeItem(name))
		progress.start()
	}
	else if (event.data.message == 'update_cache')
		progress.set(event.data.data)
}
navigator.serviceWorker.addEventListener("message", (event) => console.log(event.data.msg, event.data.url))

//GLOBAL EVENT LISTENERS
brand.addEventListener('mousedown', () => router(store.default_page))

window.addEventListener('popstate', () =>
{
	store.back = true
	if (window.location.href.replace(store.origin, '').slice(1) == 'modal.show()')
	{
		history.go(-1)
		setTimeout(() => modal.show(), 50)
	}
	else
	{
		modal.classList.remove('is-active')
		router(window.location.href.replace(store.origin, '').slice(1), false)
	}
})

window.addEventListener('resize', () =>
{
	if (window.innerWidth == store.innerWidth)
		return false
	clearTimeout(window.resizedFinished)
	window.resizedFinished = setTimeout(() =>
	{
		optimus.redraw()
		onWindowResize.forEach(component => component.onWindowResize())
	}, 250)
})

window.onfocus = () => (optimusWebsocket && optimusWebsocket.readyState != 1) && create_websocket()

window.addEventListener('scroll', () => window.scroll(0, 0))

window.addEventListener('beforeunload', () =>
{
	clear(main)
	save_user_preferences()
})

store.touchscreen = false
window.addEventListener('touchstart', () =>
{
	store.touchscreen = true
	leftmenu.querySelectorAll('div').forEach((div) =>
	{
		div.draggable = false
		div.querySelector('label').style.removeProperty('cursor')
	})
	leftmenu.oncontextmenu = function (e)
	{
		leftmenu.editor_mode(e)
		return false
	}
})

//REDRAW INTERFACE IF RESOLUTION CHANGES
optimus.redraw = () =>
{
	if (window.matchMedia('screen and (min-width:1280px)').matches)
		leftmenu.toggle('visible')
	else
		leftmenu.toggle('hidden')

	store.innerWidth = window.innerWidth
}

//CURTAIN
curtain.onclick = () =>
{
	if (window.matchMedia('screen and (max-width:1279px)').matches)
	{
		optimus.classList.add('menu-hidden')
		optimus.classList.add('menu-minimized')
		notificationmenu.classList.add('notificationmenu-hidden')
		curtain.classList.remove('menu-hidden')
		curtain.classList.remove('notificationmenu-hidden')
	}
}

//LEFTMENU
leftmenu.toggle = status =>
{
	if (!status)
	{
		if (optimus.classList.contains('menu-hidden'))
			store.leftmenu.status = 'visible'
		else if (optimus.classList.contains('menu-minimized'))
			store.leftmenu.status = 'hidden'
		else
			store.leftmenu.status = 'minimized'
	}
	else
		store.leftmenu.status = status

	if (store.leftmenu.status == 'visible')
	{
		optimus.classList.remove('menu-hidden')
		optimus.classList.remove('menu-minimized')
		if (window.matchMedia('screen and (max-width:1279px)').matches)
			curtain.classList.add('menu-hidden')
	}
	else if (store.leftmenu.status == 'hidden')
	{
		optimus.classList.add('menu-hidden')
		optimus.classList.add('menu-minimized')
		if (window.matchMedia('screen and (max-width:1279px)').matches)
			curtain.classList.remove('menu-hidden')
	}
	else if (store.leftmenu.status == 'minimized')
	{
		optimus.classList.remove('menu-hidden')
		optimus.classList.add('menu-minimized')
		if (window.matchMedia('screen and (max-width:1279px)').matches)
			curtain.classList.remove('menu-hidden')
	}

	onWindowResize.forEach(component => component.onWindowResize())

	return store.leftmenu.status
}

leftmenu.create = function (id, title, collapsed)
{
	if (leftmenu.querySelector('#' + id))
		return leftmenu.querySelector('#' + id).classList.remove('is-hidden')

	let template = document.querySelector('template#menu-block').content.cloneNode(true)
	template.querySelector('div').id = id
	template.querySelector('input').id = 'leftmenu_header_' + id
	template.querySelector('input').onclick = function ()
	{
		if (this.checked)
			leftmenu[this.parentNode.id].toggle('expanded')
		else
			leftmenu[this.parentNode.id].toggle('collapsed')
	}
	template.querySelector('label').setAttribute('for', 'leftmenu_header_' + id)
	template.querySelector('label > span:first-child').innerText = title || id
	leftmenu.appendChild(template)

	leftmenu.editor_mode = function ()
	{
		leftmenu.querySelectorAll('div').forEach(div =>
		{
			div.draggable = true
			div.ontouchstart = div.ondragstart
			div.ontouchmove = store.navigator == 'Firefox' ? div.ondragover : div.ondrag
			div.ontouchend = div.ondragend
		})

		leftmenu.querySelectorAll('input').forEach((input) => (input.checked = false))

		if (!leftmenu.querySelector('button'))
		{
			let button = document.createElement('button')
			button.classList.add('button', 'is-flex', 'mt-4')
			button.style.margin = 'auto'
			button.innerText = 'Quitter le mode édition'
			button.onclick = () =>
			{
				leftmenu.querySelectorAll('div').forEach(div =>
				{
					if (!leftmenu_collapsed.includes(div.id))
						leftmenu[div.id].toggle('expanded')
				})
				this.parentNode.querySelectorAll('div').forEach(div =>
				{
					div.draggable = false
					div.ontouchstart = null
					div.ontouchmove = null
					div.ontouchend = null
				})
				button.remove()
			}
			leftmenu.appendChild(button)
		}
	}

	leftmenu.querySelectorAll('div').forEach(div =>
	{
		div.draggable = true
		div.querySelector('label').style.cursor = 'grab'

		div.ondragstart = function (e)
		{
			e.stopPropagation()
			if (store.navigator == 'Firefox' && store.touchscreen == true)
				leftmenu.dragged = e.target.parentNode.parentNode
			else
				leftmenu.dragged = e.target

			leftmenu.y = store.touchscreen ? e.targetTouches[0].pageY : e.clientY

			div.style.outline = '1px dashed #ff0000'
			div.style.outlineOffset = '-1px'
			div.querySelector('label').style.color = '#ff0000'

			e?.dataTransfer?.setDragImage(e.target, window.outerWidth, window.outerHeight, 0, 0)
		}

		if (store.navigator != 'Firefox')
			div.ondrag = function (e)
			{
				let y = store.touchscreen ? e.targetTouches[0].pageY : e.pageY
				if (y != 0)
					if (div?.nextElementSibling?.nodeName == 'DIV' && y > div?.nextElementSibling?.getBoundingClientRect()?.top + 20)
					{
						let dragged_index = leftmenu_order.indexOf(div.id)
						let swap_index = leftmenu_order.indexOf(div.nextElementSibling.id)
						leftmenu_order[dragged_index] = div.nextElementSibling.id
						leftmenu_order[swap_index] = div.id
						leftmenu.insertBefore(div, div.nextElementSibling.nextElementSibling)
					}
					else if (div?.previousElementSibling?.nodeName == 'DIV' && y < div?.previousElementSibling?.getBoundingClientRect()?.top + 20)
					{
						let dragged_index = leftmenu_order.indexOf(div.id)
						let swap_index = leftmenu_order.indexOf(div.previousElementSibling.id)
						leftmenu_order[dragged_index] = div.previousElementSibling.id
						leftmenu_order[swap_index] = div.id
						leftmenu.insertBefore(div, div.previousElementSibling)
					}
			}

		if (store.navigator == 'Firefox')
			div.ondragover = function (e)
			{
				let y = store.touchscreen ? e.targetTouches[0].clientY : e.clientY

				if (leftmenu.dragged?.nextElementSibling?.nodeName == 'DIV' && y > leftmenu.y + 10 && y > leftmenu.dragged.getBoundingClientRect()?.bottom)
				{
					let dragged_index = leftmenu_order.indexOf(leftmenu.dragged.id)
					let swap_index = leftmenu_order.indexOf(leftmenu.dragged.nextElementSibling.id)
					leftmenu_order[dragged_index] = leftmenu.dragged.nextElementSibling.id
					leftmenu_order[swap_index] = leftmenu.dragged.id
					leftmenu.insertBefore(leftmenu.dragged, leftmenu.dragged.nextElementSibling.nextElementSibling)
					leftmenu.y = y
				}
				else if (y < leftmenu.y - 10 && y < leftmenu.dragged.getBoundingClientRect()?.top)
				{
					let dragged_index = leftmenu_order.indexOf(leftmenu.dragged.id)
					let swap_index = leftmenu_order.indexOf(leftmenu.dragged.previousElementSibling.id)
					leftmenu_order[dragged_index] = leftmenu.dragged.previousElementSibling.id
					leftmenu_order[swap_index] = leftmenu.dragged.id
					leftmenu.insertBefore(leftmenu.dragged, leftmenu.dragged.previousElementSibling)
					leftmenu.y = y
				}
			}

		div.ondragend = () =>
		{
			div.style.removeProperty('outline')
			div.querySelector('label').style.removeProperty('color')
		}
	})

	let menu_block = leftmenu.querySelector('div:last-of-type > ul')
	leftmenu[id] = menu_block
	menu_block.add = function (id, title, icon, action)
	{
		if (leftmenu.querySelector('#' + id)) return false
		let template = document.querySelector('template#menu-item').content.cloneNode(true)
		template.id = id
		template.querySelector('li').id = id
		template.querySelector('li').title = title || id
		template.querySelectorAll('span')[0].innerText = title || id
		if (icon)
			template.querySelector('i').className += ' ' + icon
		else
			template.querySelector('i').className += ' fas fa-blank'
		this.appendChild(template)

		let item = this.querySelector('li:last-of-type')
		leftmenu[menu_block.parentNode.id][id] = item

		item.badge = function (value)
		{
			let oldvalue = parseInt(this.querySelectorAll('li > a > span')[1].innerText)
			let newvalue
			if (isNaN(oldvalue))
				oldvalue = 0

			if (typeof value == 'number')
				newvalue = value
			else if (value.slice(0, 1) == '+')
				newvalue = oldvalue + parseInt(value.slice(1))
			else if (value.slice(0, 1) == '-')
				newvalue = oldvalue - parseInt(value.slice(1))
			else if (typeof value == 'string')
				newvalue = value

			if (newvalue == 0)
				this.querySelectorAll('span')[1].innerText = null
			else
				this.querySelectorAll('span')[1].innerText = newvalue
			return newvalue
		}

		item.remove = function ()
		{
			if (this.parentNode.childElementCount == 1)
				this.parentNode.parentNode.classList.add('is-hidden')
			this.parentNode.removeChild(this)
		}

		item.onclick = action
		return item
	}

	menu_block.toggle = function (status)
	{
		if (status == 'collapsed')
			this.status = 'collapsed'
		else if (status == 'expanded')
			this.status = 'expanded'
		else if (this.status == 'collapsed')
			this.status = 'expanded'
		else
			this.status = 'collapsed'

		if (this.status == 'collapsed')
		{
			this.parentNode.querySelector('input').checked = false
			if (!leftmenu_collapsed.includes(this.parentNode.id))
				leftmenu_collapsed.push(this.parentNode.id)
		}
		else if (this.status == 'expanded')
		{
			this.parentNode.querySelector('input').checked = true
			if (leftmenu_collapsed.includes(this.parentNode.id))
				leftmenu_collapsed.splice(leftmenu_collapsed.indexOf(this.parentNode.id), 1)
		}
	}

	menu_block.remove = () => menu_block.parentNode.remove()

	if (collapsed === true)
		menu_block.toggle('collapsed')

	if (!leftmenu_order.includes(id))
		leftmenu_order.push(id)

	return menu_block
}

//TOPMENUS
topmenus.create = function (id, icon)
{
	if (topmenus.querySelector('#' + id))
		return false
	let template = document.querySelector('template#dropdown-menu').content.cloneNode(true)
	template.querySelector('div').id = id
	template.querySelector('i').className += ' ' + icon
	document.querySelector('.optimus-topmenus').prepend(template)
	let dropdown_menu = document.querySelector('.optimus-topmenus > div:first-child')
	let dropdown_content = document.querySelector('.optimus-topmenus > div:first-child .dropdown-content')

	dropdown_content.add = function (id, type, title, icon, action, colorclass, position = 9999)
	{
		if (topmenus.querySelector('#' + id))
			return false
		let menu_item
		if (type == 'divider')
		{
			menu_item = document.createElement('hr')
			menu_item.classList.add('dropdown-divider')
		}
		else if (type == 'header')
		{
			menu_item = document.createElement('div')
			menu_item.classList.add('optimus-topmenu-header')
			menu_item.innerText = title
		}
		else if (type == 'item')
		{
			menu_item = document.querySelector('#dropdown-item').content.cloneNode(true).querySelector('a')
			menu_item.onmousedown = action
			if (colorclass)
				menu_item.classList.add(colorclass)
			if (icon)
			{
				menu_item.querySelector('span:first-child').className = 'icon fa-lg mr-2'
				menu_item.querySelector('span:first-child > i').className = icon
			}
			menu_item.querySelector('span:last-child').innerText = title
		}
		else
			return false
		menu_item.id = id
		menu_item.dataset.position = position
		dropdown_content.appendChild(menu_item)
		menu_item = dropdown_content.querySelector('.dropdown-content > *:last-child')

		var menu_items = [...dropdown_content.childNodes]
		menu_items.sort((a, b) => (parseInt(a.dataset.position) > parseInt(b.dataset.position)) ? 1 : -1)
		clear(dropdown_content)
		menu_items.forEach(menu_item => dropdown_content.appendChild(menu_item))

		menu_item.remove = function ()
		{
			if (this.parentNode.children.length == 1)
			{
				this.parentNode.parentNode.parentNode.children[0].classList.add('is-hidden')
				this.parentNode.classList.add('is-hidden')
			}
			this.parentNode.removeChild(this)
		}
		this.parentNode.parentNode.querySelector('.optimus-topmenu-trigger').classList.remove('is-hidden')
		this.parentNode.parentNode.querySelector('.dropdown-content').classList.remove('is-hidden')
		return menu_item
	}

	dropdown_content.remove = () => dropdown_content.parentNode.parentNode.remove()

	dropdown_menu.add = dropdown_content.add

	dropdown_content.toggle = function (status)
	{
		if (!status)
			if (dropdown_menu.classList.contains('is-active'))
				status = 'hidden'
			else
				status = 'visible'

		if (status == 'visible')
		{
			notificationmenu.toggle('hidden')
			document.querySelectorAll('.dropdown-content').forEach((item) => item.parentNode.parentNode.classList.remove('is-active'))
			dropdown_menu.classList.add('is-active')
		}
		else
			if (status == 'hidden') dropdown_menu.classList.remove('is-active')
	}

	dropdown_menu.toggle = dropdown_content.toggle

	topmenus[id] = dropdown_content
	return dropdown_content
}

//NOTIFICATION MENU
notificationmenu.toggle = function (status)
{
	if (!status)
		if (notificationmenu.classList.contains('notificationmenu-hidden'))
			status = 'visible'
		else
			status = 'hidden'

	if (status == 'visible')
	{
		notificationmenu.classList.remove('notificationmenu-hidden')
		curtain.classList.add('notificationmenu-hidden')
	}
	else if (status == 'hidden')
	{
		notificationmenu.classList.add('notificationmenu-hidden')
		curtain.classList.remove('notificationmenu-hidden')
	}
}

notificationmenu.add = function (id, colorclass, icon, title, text, datetime, link, onclick, onremove)
{
	let template = document.querySelector('template#notification').content.cloneNode(true)
	template.querySelector('article').id = 'notification_' + id
	if (!datetime) datetime = new Date()
	template.querySelector('article').dataset.datetime = datetime.toISOString().slice(0, 19).replace('T', '').replaceAll('-', '').replaceAll(':', '')
	if (onclick || link)
		template.querySelector('article').classList.add('is-clickable')
	template.querySelector('article').addEventListener('mousedown', function (e)
	{
		if (link.substring(0, 8) == 'https://')
			window.open(link)
		else
			router(link)
		if (typeof onclick == 'function')
			onclick()
	})
	template.querySelector('article').classList.add('added')
	template.querySelector('article').classList.add(colorclass)
	template.querySelector('.message-header > i').className += icon
	template.querySelector('.message-title').innerHTML = title
	template.querySelector('.message-body').innerHTML = text
	template.querySelector('.message-time').innerHTML = 'Le ' + datetime.toLocaleDateString() + ' à ' + datetime.toLocaleTimeString().substring(0, 5)
	template.querySelector('.delete').addEventListener('mousedown', function (e)
	{
		rest(store.user.server + '/optimus-base/users/' + store.user.id + '/notifications/' + id, 'PATCH', { archived: true }, 'toast')
			.then(response => 
			{
				if (response.code == 200)
				{
					notificationmenu.remove(document.getElementById(e.target.parentNode.parentNode.id))
					if (typeof onremove == 'function')
						onremove()
				}
			})
		e.stopPropagation()
	})
	for (let article of document.querySelectorAll('.optimus-notificationmenu > article'))
		if (parseInt(template.querySelector('article').dataset.datetime) >= parseInt(article.dataset.datetime))
		{
			notificationmenu.insertBefore(template, article)
			break
		}
	notificationmenu.appendChild(template)
	notificationmenu_badge.innerText = document.querySelectorAll('.optimus-notificationmenu > article').length || null
	notificationmenu_badge.style.opacity = 1
	notificationmenu_trigger.style.opacity = 1
	notificationmenu_trigger.parentNode.classList.remove('is-hidden')
	return template
}

notificationmenu.remove = function (item)
{
	if (typeof item == 'number')
		item = document.querySelectorAll('.optimus-notificationmenu > article')[item]
	else if (typeof item == 'string')
	{
		if (document.getElementById('notification_' + item))
			item = document.getElementById('notification_' + item)
		else
			for (let obj of document.querySelectorAll('.optimus-notificationmenu .message-header > p'))
				if (obj.innerText.includes(item))
					item = obj.parentNode.parentNode
	}

	if (typeof item != 'object')
		return false

	item.querySelector('.delete').dispatchEvent(new Event('click'))
	item.classList.add('removed')
	notificationmenu_badge.innerText = document.querySelectorAll('.optimus-notificationmenu > article').length - 1
	item.addEventListener('transitionend', e =>
	{
		if (e.propertyName === 'opacity')
			e.target.remove()
	})
	if (document.querySelectorAll('.optimus-notificationmenu > article').length == 1)
	{
		notificationmenu.classList.add('notificationmenu-hidden')
		curtain.classList.remove('notificationmenu-hidden')
		notificationmenu_badge.style.opacity = 0
		notificationmenu_trigger.style.opacity = 0
		setTimeout(() => notificationmenu_trigger.parentNode.classList.add('is-hidden'), 200)
	}
	return true
}

//THEME
function theme(name, darkmode)
{
	store.theme = name
	if (document.getElementById('theme').href != store.origin + '/css/' + store.theme + '.css')
		document.getElementById('theme').href = '/css/' + store.theme + '.css'
	if (typeof darkmode === 'boolean')
	{
		if (darkmode === true)
		{
			store.darkmode = true
			if (document?.getElementById('theme-dark')?.href != store.origin + '/css/' + store.theme + '-dark.css')
			{
				document.getElementById('theme-dark')?.remove()
				let link = document.createElement('link')
				link.id = 'theme-dark'
				link.rel = 'stylesheet'
				link.href = '/css/' + store.theme + '-dark.css'
				link.onload = () => onThemeChange.forEach(component => component.onThemeChange())
				document.head.appendChild(link)
				document.querySelector('.optimus-darkmode-trigger').classList.add('is-hidden')
				document.querySelector('.optimus-lightmode-trigger').classList.remove('is-hidden')
			}
		}
		else
		{
			store.darkmode = false
			document.getElementById('theme-dark')?.remove()
			onThemeChange.forEach(component => component.onThemeChange())
			document.querySelector('.optimus-darkmode-trigger').classList.remove('is-hidden')
			document.querySelector('.optimus-lightmode-trigger').classList.add('is-hidden')
		}
		if (document.getElementById('webmail_container'))
			document.getElementById('webmail_container').contentWindow.postMessage('darkmode=' + store.darkmode, 'https://' + store.user.server.replace('api', 'webmail'))
	}
	else
	{
		delete store.darkmode
		document.getElementById('theme-dark')?.remove()
		document.querySelector('.optimus-darkmode-trigger').classList.add('is-hidden')
		document.querySelector('.optimus-lightmode-trigger').classList.add('is-hidden')
	}
	localStorage.setItem('theme', name)
	localStorage.setItem('darkmode', JSON.stringify(darkmode))
}

//TOAST
bulmaToast.setDefaults({
	message: '',
	type: 'is-primary',
	position: 'bottom-right',
	dismissible: true,
	duration: 4000,
	pauseOnHover: true,
	animate: { in: 'fadeIn', out: 'fadeOut' }
})

function optimusToast(message, color, otherParams)
{
	bulmaToast.setDoc(window.document)
	message = '<p class="optimus-toast has-text-left">' + message + '</p>'
	bulmaToast.toast({ message: message, type: color || 'is-primary', ...otherParams })
}

//MODAL
modal.open = async (path = '/blank.js', dismissable, params, animation = true) =>
{
	store.hidden_modal = false
	if (modal.classList.contains('is-active'))
		modal.querySelector('.modal-container').classList.add('is-hidden')
	if (modal?.closer)
		clearTimeout(modal.closer)
	clear(modal)
	let template = document.getElementById('modal').content.cloneNode(true)
	modal.appendChild(template)
	//FIX PROVISOIRE CAUSE BUG AFFICHAGE CHROME

	modal.querySelector('.modal-background').style.opacity = 0.86
	return load(path, modal.querySelector('.modal-container'), params)
		.then(() =>
		{
			if (!modal.querySelector('.modal-container').firstElementChild)
				return
			if (modal.querySelector('.modal-container')?.children.length > 1)
				return optimusToast('Le fichier HTML du modal contient plusieurs objets à la racine. Seul le premier noeud est importé', 'is-warning')
			modal.querySelector('.modal-container').firstElementChild.classList.add('animate__animated', 'animate__zoomIn')
			modal.querySelector('.modal-container').classList.remove('animate__zoomOut')
			if (animation)
				modal.querySelector('.modal-container').firstElementChild.style.animationDuration = '.3s'
			else
				modal.querySelector('.modal-container').firstElementChild.style.animationDuration = '.01s'

			modal.querySelector('.modal-container').firstElementChild.classList.add('modal-container')
			modal.querySelector('.modal-container').replaceWith(modal.querySelector('.modal-container')?.firstElementChild)
		})
		.finally(() =>
		{
			modal.classList.add('is-active', 'animate__animated', 'animate__fadeIn')
			modal.classList.remove('animate__fadeOut')
			if (animation)
				modal.style.animationDuration = '.3s'
			else
				modal.style.animationDuration = '.01s'

			modal.style.zIndex = 10001
			modal.querySelector('.modal-container').style.zIndex = 10002

			modal.querySelector('.modal-close')?.classList?.toggle('is-hidden', !dismissable)
			if (dismissable === true)
			{
				modal.querySelector('.modal-close').onclick = () => modal.close()
				modal.querySelector('.modal-background').onmousedown = () => modal.close()
				modal.onkeydown = event => event.key === "Escape" && modal.close()
			}
		})
}

modal.close = event =>
{
	if (modal.classList.contains('is-active'))
	{
		modal.classList.remove('animate__fadeIn')
		modal.classList.add('animate__fadeOut')
		modal.querySelector('.modal-container').classList.add('animate__zoomOut')
		modal.querySelector('.modal-container').classList.remove('animate__zoomIn')
		if (event?.key && event.key != 'Escape')
			return false
		modal.closer = setTimeout(() =>
		{
			modal.classList.remove('is-active')
			clear(modal)
		}, 300)
		modal.querySelector('.modal-close').onclick = undefined
		modal.querySelector('.modal-background').onclick = undefined
		modal.onkeydown = undefined
		modal.onshow = undefined
		return true
	}
}

modal.show = () => 
{
	if (!store.hidden_modal)
		return false
	modal.classList.add('is-active')
	if (modal.onshow)
		modal.onshow()
}

modal.hide = () => 
{
	store.hidden_modal = true
	window.history.pushState({ previous: window.location.href }, '', '/modal.show()')
	modal.classList.remove('is-active')
	if (modal.onhide)
		modal.onhide()
}


//DATEPICKER
function datepicker(selector, options)
{
	const default_options =
	{
		lang: 'fr',
		showHeader: false,
		dateFormat: 'dd/MM/yyyy',
		color: 'link',
		cancelLabel: 'Annuler',
		clearLabel: 'Effacer',
		todayLabel: "Aujourd'hui",
		nowLabel: 'Maintenant',
		validateLabel: 'Valider',
		displayMode: 'dialog',
		weekStart: 1
	}
	return bulmaCalendar.attach(selector, { ...default_options, ...options })[0]
}

//STEPS
function steps(id)
{
	let ul = document.getElementById(id)
	ul.isDashed = ul.classList.contains('is-dashed')
	ul.classList.remove('is-dashed')
	ul.step = function (step)
	{
		let lis = this.querySelectorAll('li')
		for (let i = 0; i < lis.length; i++)
		{
			lis[i].classList.remove('is-active')
			if (i === step)
				lis[i].classList.add('is-active')
			if (this.isDashed)
				if (i < step)
					lis[i].classList.remove('is-dashed')
				else
					lis[i].classList.add('is-dashed')
		}
		ul.currentStep = Array.from(ul.querySelectorAll('li')).findIndex((li) => li.classList.contains('is-active'))
	}
	ul.currentStep = Array.from(ul.querySelectorAll('li')).findIndex((li) => li.classList.contains('is-active'))
	if (ul.currentStep == -1)
		ul.currentStep = 0
	ul.step(ul.currentStep)
	return ul
}

//LOADER
function loader(obj = main, visible = true, type = 'default')
{
	if (!obj)
		return false

	if (visible == false)
	{
		obj?.querySelector('.optimus-loader')?.remove()
		clearTimeout(obj.loaderAnimation)
		obj.style.transition = 'opacity .15s ease-out'
		obj.style.opacity = 1
		return true
	}

	if (obj?.querySelector('.optimus-loader'))
		return false

	obj.style.filter = 'grayscale(0)'
	obj.style.transition = 'opacity 0s ease-out'
	obj.style.opacity = 0
	obj.loaderAnimation = setTimeout(() =>
	{
		obj.style.transition = 'opacity .15s ease-out'
		obj.style.opacity = 1
		loader_module = document.createElement('object')
		loader_module.type = 'image/svg+xml'
		loader_module.data = '/images/skeleton-' + type + '.svg'
		loader_module.style.borderRadius = window.getComputedStyle(obj).getPropertyValue('border-radius')
		loader_module.classList.add('optimus-loader')
		obj.append(loader_module)
	}, 150)
}

//PAUSE
function pause(ms)
{
	return new Promise(resolve => setTimeout(resolve, ms))
}

//REST API JSON CLIENT
async function rest(path, method = 'GET', data, errormanager = 'toast', object)
{
	while (store.login == true)
		await pause(200)

	let fetch_endpoint = path
	if (!path.includes('https://'))
		fetch_endpoint = 'https://' + path

	let fetch_options = { headers: { 'Accept-Language': store.language }, method: method, credentials: 'include' }

	if (method == 'GET' && data)
		fetch_endpoint += (path.includes('?') ? '&' : '?') + 'data=' + JSON.stringify(data)
	else if (method != 'GET' && data)
		fetch_options.body = JSON.stringify(data)

	return fetch(fetch_endpoint, fetch_options)
		.then(response => response.json())
		.then(jsonresponse =>
		{
			if (object)
				jsonresponse.object = object

			if (jsonresponse.code >= 200 && jsonresponse.code < 400)
				return jsonresponse
			else if (jsonresponse.code == 401)
			{
				if (store.login == 'canceled')
				{
					store.login = undefined
					//localStorage.removeItem('user')
					return jsonresponse
				}
				else if (!document.getElementById('login'))
				{
					modal.open('/services/optimus-base/login/index.js')
					store.login = true
				}
				return rest(path, method, data, errormanager, object)
			}
			else
			{
				if (errormanager == 'toast')
					optimusToast(jsonresponse.code + '\n' + jsonresponse.message, 'is-danger')
				else if (errormanager == 'console')
					console.log(jsonresponse)
				return jsonresponse
			}
		})
		.catch((error) =>
		{
			if (errormanager == 'toast')
				optimusToast(error, 'is-danger')
			else if (errormanager == 'console')
				console.log(error)
		})
}

//USER LOGIN / LOGOUT
async function user_login(response)
{
	if (store.login == 'canceled')
		return (store.login = undefined)

	store.user = { ...store.user, ...response.data }
	localStorage.setItem('user', JSON.stringify(response.data))

	rest(store.user.server + '/optimus-base/users_services/' + store.user.id, 'GET')
		.then(response =>
		{
			let unavailable_services = []
			for (let unavailable_service of response.data.filter(service => service.status == 0))
				unavailable_services.push(unavailable_service.name)
			if (unavailable_services > 0)
				optimusToast('Les services suivants sont indisponibles : ' + unavailable_services.join(', '), 'is-warning')
			return response
		})
		.then(response => Promise.all([...response.data.map(async service => { if (service.status == '1') await load('/services/' + service.name + '/service.js').then(service => service.login()); else optimusToast("Le service " + service.name + " n'est pas lancé. Contactez l'administrateur du serveur", "is-warning") })]))
		.then(() =>
		{
			load_user_preferences()
			while (document.querySelector('.optimus-notificationmenu').childElementCount > 0)
				notificationmenu.remove(document.querySelector('.optimus-notificationmenu > article:last-child'))
			rest(store.user.server + '/optimus-base/users/' + store.user.id + '/notifications', 'GET', { filter: [{ field: "archived", type: "=", value: false }, { field: "date", type: "<=", value: new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60 * 1000).toJSON().slice(0, 19).replace('T', ' ') }] }, 'toast')
				.then(response => response.data.forEach(notification => notificationmenu.add(notification.id, notification.colorclass, notification.icon, notification.title, notification.text, new Date(notification.date), notification.link)))
			if (store.redirect)
			{
				router(store.redirect)
				delete store.redirect
			}
			else
				router(store.default_page)
		})

	store.user.secondary_servers = new Array
	rest(store.user.server + '/optimus-base/users_servers/' + store.user.id, 'GET')
		.then(response => response.data.map(servers => store.user.secondary_servers.push(servers.server)))

	login_trigger.classList.add('is-hidden')
	topmenus.create('usermenu', 'fas fa-user')
	topmenus['usermenu'].add('user-displayname', 'header', store.user.displayname, null, null, null, 100)
	topmenus['usermenu'].add('my-account', 'item', 'Mon compte', null, () => router('optimus-base/my-account'), null, 200)
	topmenus['usermenu'].add('my-services', 'item', 'Mes services', null, () => router('optimus-base/my-services'), null, 300)
	topmenus['usermenu'].add('my-shares', 'item', 'Mes partages', null, () => router('optimus-base/my-shares'), null, 400)
	topmenus['usermenu'].add('my-notifications', 'item', 'Mes notifications', null, () => router('optimus-base/my-notifications'), null, 500)
	if (store.user.admin == 1)
	{
		topmenus['usermenu'].add('admin-divider', 'divider', null, null, null, null, 1000)
		topmenus['usermenu'].add('admin-menu', 'item', 'Administration', null, () => router('optimus-base/administration#users'), 'has-text-success', 1100)
	}
	topmenus['usermenu'].add('login-divider', 'divider', null, null, null, null, 9000)
	topmenus['usermenu'].add('logout', 'item', 'Logout', null, () => user_logout(), 'has-text-danger', 9100)
	stopwatch_init()
	create_websocket()
	modal.close()
}

function create_websocket()
{
	optimusWebsocket = new WebSocket("wss://" + store.user.server + ":20000")
	optimusWebsocket.addEventListener('open', event =>
	{
		document.querySelector('.optimus-websocket-indicator').classList.remove('has-text-danger')
		optimusWebsocket.addEventListener('message', event => { optimusWebsocket.dispatchEvent(new CustomEvent('incoming', { detail: JSON.parse(event.data) })) })
		optimusWebsocket.addEventListener('incoming', event => 
		{
			if (event.detail.code >= 400)
				optimusToast(event.detail.message, 'is-danger')
			if (event?.detail?.command == 'notification')
			{
				notification = event.detail.data
				if (new Date(notification.date) < new Date())
					notificationmenu.add(notification.id, notification.colorclass, notification.icon, notification.title, notification.text, new Date(notification.date), notification.link)
			}
			if (event?.detail?.command == 'token_expired')
			{
				modal.open('/services/optimus-base/login/index.js')
				optimusToast('Votre session a expiré', 'is-warning')
			}
			if (event?.detail?.command == 'service_uninstall' && store.services.find(service => service.name == event.detail.data.service))
			{
				store.services.find(service => service.name == event.detail.data.service).logout()
				optimusToast("Le service " + event.detail.data.service + " a été supprimé par l'administrateur", 'is-warning')

			}
		})
		optimusWebsocket.out = data => 
		{
			if (!data.id)
				data.id = Math.random().toString(36).substr(2, 9)
			optimusWebsocket.send(JSON.stringify(data))
		}
	})
}

async function user_logout()
{
	save_user_preferences()
	store.services.forEach(service => service.logout())
	setTimeout(() => rest(store.user.server + '/optimus-base/logout', 'GET')
		.then(() =>
		{
			localStorage.removeItem('user')
			window.location.reload()
		})
		, 500)
}

function load_user_preferences()
{
	leftmenu_order.forEach((service) =>
	{
		if (leftmenu.querySelector('#' + service))
		{
			leftmenu.append(leftmenu.querySelector('#' + service))
			if (leftmenu_collapsed.includes(service))
				leftmenu[service].toggle('collapsed')
			else
				leftmenu[service].toggle('expanded')
		}
	})
}

function save_user_preferences()
{
	localStorage.setItem('leftmenu_order', JSON.stringify(leftmenu_order))
	localStorage.setItem('leftmenu_collapsed', JSON.stringify(leftmenu_collapsed))
}

function delete_user_preferences()
{
	localStorage.removeItem('leftmenu_order')
	localStorage.removeItem('leftmenu_collapsed')
	leftmenu_order.splice(0, 99)
	leftmenu_collapsed.splice(0, 99)
}

//ROUTER
async function router(route, history = true, target = main, params)
{
	store.url = new URL(new URL(location.href).origin + '/' + route)
	store.origin = store.url.origin

	store.route = route.replace(/\/$/, '').replace(/^\//, '').replace(new URL(location.href).origin, '').split('#')[0]
	store.hash = store.url.hash.slice(1).replace('_', '/')
	store.path = store.route.split('?')[0].replace(/\/$/, '').replace(/^\//, '')
	store.hashParams = store.url.hash.length > 0 ? store.url.hash.slice(1).split('/') : new Array()
	store.query = store.url.search.slice(1)
	store.queryParams = Object.fromEntries(new URLSearchParams(store.query))

	if (history === true && window.location.href != store.url.origin + '/' + route)
		window.history.pushState({ previous: window.location.href }, '', '/' + route)

	if (store.path.split('/').length == 2)
		store.path = '/services/' + store.path + '/index'
	else
		store.path = '/services/' + store.path

	for (let li of document?.querySelectorAll('.optimus-leftmenu li'))
		li.classList.toggle('is-active', li.onclick.toString().includes(store.route.split('?')[0].split('/')[0] + '/' + store.route.split('?')[0].split('/')[1]))

	for (let a of document?.querySelectorAll('.optimus-topmenu .dropdown-content a'))
		a.classList.toggle('is-active', a.onmousedown.toString().includes(store.route.split('?')[0].split('/')[0] + '/' + store.route.split('?')[0].split('/')[1]))

	for (let topmenu of topmenus?.children)
		topmenu.toggle('hidden')

	curtain.click()

	return load(store.path + '.js', target, params)
}

//LOADERS
async function load(path, target, params)
{
	if (target == main && path.slice(-3) == '.js' && path.slice(0, 10) == '/services/')
	{
		onWindowResize = new Array
		onUnload.forEach(component => delete component.onUnload())
		onUnload = new Array
		onThemeChange = new Array
	}
	if (path.slice(-5) == '.html')
		return fetch(path)
			.then((response) =>
			{
				if (!response.ok || response.status == 207)
					fetch('/' + (response.status == 207 ? '404' : response.status) + '.html')
						.then(errorpage =>
						{
							if (!errorpage.ok)
								throw { statusText: errorpage.statusText, status: errorpage.status }
							else
								return errorpage.text()
						})
						.then(errorpage =>
						{
							if (target)
							{
								clear(target)
								target.innerHTML += errorpage
							}
							else
								return optimusToast('Erreur de chargement de la page ' + path, 'is-danger')
						})
				else if (response.status != 207)
				{
					return response.text()
				}
				else
					return false
			})
			.then(response =>
			{
				if (store.translations)
					for (const [key, value] of Object.entries(store.translations))
						response = response.replaceAll(key, value)
				return response
			})
			.then(response =>
			{
				if (target)
				{
					clear(target)
					if (target == main)
						main.appendChild(document.getElementById('optimus-back-link').content.cloneNode(true))
					target.innerHTML += response
				}
				else
					return response
			})
			.catch(function (error)
			{
				if (error.status)
				{
					clear(target)
					target.innerHTML += 'ERREUR ' + error.status + '<br/>' + error.statusText
				}
			})

	return import(path)
		.then(({ default: module }) =>
		{
			let component = 'none'
			if (typeof module != 'undefined')
				component = new module(target, params)

			if (typeof component?.init != 'undefined')
			{
				if (typeof component?.onWindowResize === 'function')
					onWindowResize.push(component)
				if (typeof component?.onUnload === 'function')
					onUnload.push(component)
				if (typeof component?.onThemeChange === 'function')
					onThemeChange.push(component)
				return component.init()
			}

			return component
		})
		.catch(error =>
		{
			if (target && (error.message.includes('Failed to fetch dynamically imported module') || error.message.includes('Failed to load module script')))
				fetch('/404.html')
					.then(response => response.text())
					.then(response =>
					{
						clear(target)
						target.innerHTML += response
						return false
					})
			else
			{
				console.error(error)
				optimusToast(error.toString(), 'is-danger')
			}
		})
		.finally(response => response)
}

async function load_script(path)
{
	if (document.head.querySelector('script[src="' + path + '"]'))
		return document.head.querySelector('script[src="' + path + '"]').loading

	let loading_promise = new Promise(resolve =>
	{
		let script = document.createElement('script')
		script.src = path
		script.onload = () => resolve()
		document.head.appendChild(script)
	})
	document.head.querySelector('script[src="' + path + '"]').loading = loading_promise
	return loading_promise
}

async function load_CSS(path)
{
	if (!document.head.querySelector('link[href="' + path + '"]'))
		return new Promise(resolve =>
		{
			let link = document.createElement('link')
			link.rel = 'stylesheet'
			link.type = 'text/css'
			link.href = path
			link.onload = () =>
			{
				resolve()
			}
			document.head.appendChild(link)
		})
	else
		return Promise.resolve()
}

function clear(object)
{
	object.innerText = ''
	object.querySelectorAll('*:not(.optimus-loader)').forEach(object => object.remove())
}

//SERVICE WORKER UPDATER
if ('serviceWorker' in navigator)
	window.addEventListener('load', async () =>
	{
		const registration = await navigator.serviceWorker.register('/serviceworker.js')

		if (registration.waiting)
			registration.waiting.postMessage('skipWaiting')

		registration.addEventListener('updatefound', () =>
		{
			if (registration.installing)
				registration.installing.addEventListener('statechange', () =>
				{
					if (registration.waiting)
						if (navigator.serviceWorker.controller)
							registration.waiting.postMessage('skipWaiting')
				})

			let refreshing = false

			navigator.serviceWorker.addEventListener('controllerchange', () =>
			{
				if (!refreshing)
				{
					window.location.reload()
					refreshing = true
				}
			})
		})
	})

//INIT
async function optimus_init()
{
	window.document.title = store.title
	document.querySelector('.optimus-version').innerText = store.version
	optimus.redraw()
	logo.src = store.logo
	theme(store.theme, store.darkmode)

	let url = new URL(window.location.href)
	store.redirect = url.pathname.slice(1) + url.search + url.hash

	await load('/services/optimus-base/service.js').then(service => service.login())

	if (localStorage.getItem('user'))
	{
		user = { data: JSON.parse(localStorage.getItem('user')) }
		rest(user.data.server + '/optimus-base/logged')
			.then(response =>
			{
				if (response.code == 200 && !topmenus['usermenu'])
					user_login(user)
			})
	}
	else if (store.redirect.startsWith('optimus-devtools/client-doc') || store.redirect.startsWith('optimus-devtools/api-doc'))
	{
		leftmenu.toggle('hidden')
		router(store.redirect)
	}
	else
		router(store.default_page)
			.then(() => 
			{
				store.redirect = url.pathname.slice(1) + url.search + url.hash
				login_trigger.click()
			})
}

//HIGHLIHT CODE WITH PRISM
async function highlight_code(target = main)
{
	load_CSS('/libs/prism.css')
		.then(() => load_script('/libs/prism.js')
			.then(() => Prism.highlightAllUnder(target)))
}


//POPULATE
async function populate(obj, endpoint, order, addzero, setvalue, filter)
{
	let results
	let constant = endpoint.split('/').slice(-1)
	if (localStorage.getItem('constants_' + constant))
	{
		results = JSON.parse(localStorage.getItem('constants_' + constant))
		populateStart(results)
		return results
	}
	else
	{
		return rest(endpoint).then(response =>
		{
			if (response.code == 200)
				localStorage.setItem('constants_' + constant, JSON.stringify(response.data))
			populateStart(response.data)
			return response.data
		})
	}

	function populateStart(results)
	{
		if (!obj)
			return
		clear(obj)
		if (addzero == true)
			obj.innerHTML = '<option value="0"></option>'
		if (order == true)
			results.sort(function (x, y)
			{
				const a = String(x.value).toUpperCase()
				const b = String(y.value).toUpperCase()
				if (a > b)
					return 1
				if (a < b)
					return -1
				return 0
			})

		results.forEach(element =>
		{
			if (!filter || (filter && element[Object.keys(element)[1]] == filter))
				obj.options[obj.options.length] = new Option(element.value, element.id)
		})

		if (setvalue)
			obj.value = setvalue
		else
			obj.selectedIndex = 0
	}
}

//GET CONSTANTS
async function get_constants(endpoint)
{
	let constant = endpoint.split('/').slice(-1)
	if (localStorage.getItem('constants_' + constant))
		return JSON.parse(localStorage.getItem('constants_' + constant))
	else
		return await rest(endpoint).then(response =>
		{
			if (response.code == 200)
			{
				localStorage.setItem('constants_' + constant, JSON.stringify(response.data))
				return response.data
			}
		})
}

//ADMIN ONLY
function admin_only()
{
	if (store.user.admin == 0)
	{
		setTimeout(() => modal.close(), 20)
		load('/services/optimus-base/admin_only.js')
		return true
	}
	return false
}