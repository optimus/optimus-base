const buildDate = 20250305225821

const cacheName = "optimus"
const assets = [
	"index.html",
	"index.js",
	"404.html",
	"components/optimus_table/owner_selector_modal.html",
	"components/optimus_table/owner_selector_modal.js",
	"css/default.css",
	"css/optimus.css",
	"css/optimus-dark.css",
	"images/optimus-logo.svg",
	"images/skeleton-bars.svg",
	"images/skeleton-datatable.svg",
	"images/skeleton-default.svg",
	"images/skeleton-image.svg",
	"images/skeleton-list.svg",
	"optimus.js",
	"libs/chart.umd.min.js",
	"libs/chart.umd.js.map",
	"libs/fullcalendar/locales/fr.global.min.js",
	"libs/fullcalendar/index.global.min.js",
	"libs/leaflet/images/layers-2x.png",
	"libs/leaflet/images/layers.png",
	"libs/leaflet/images/marker-icon-2x.png",
	"libs/leaflet/images/marker-icon.png",
	"libs/leaflet/images/marker-shadow.png",
	"libs/leaflet/leaflet.css",
	"libs/leaflet/leaflet-src.esm.js",
	"libs/leaflet/leaflet-src.esm.js.map",
	"libs/rrule/index.global.min.js",
	"libs/rrule/rrule.min.js",
	"libs/tabulator/css/tabulator_bulma.min.css",
	"libs/tabulator/css/tabulator_bulma.min.css.map",
	"libs/tabulator/css/tabulator.min.css",
	"libs/tabulator/css/tabulator.min.css.map",
	"libs/tabulator/js/tabulator_esm.min.js",
	"libs/tabulator/js/tabulator_esm.min.js.map",
	"libs/tabulator/js/tabulator.min.js",
	"libs/tabulator/js/tabulator.min.js.map",
	"libs/animate.min.css",
	"libs/bulma-calendar.min.css",
	"libs/bulma-calendar.min.js",
	"libs/bulma-o-steps.min.css",
	"libs/bulma-switch.min.css",
	"libs/bulma-toast.min.js",
	"libs/bulma-tooltip.min.css",
	"libs/bulma.min.css",
	"libs/fa-brands.min.css",
	"libs/fa-brands.woff2",
	"libs/fa-solid.min.css",
	"libs/fa-solid.woff2",
	"libs/fa.min.css",
	"libs/flag-icons.min.css",
	"libs/geosearch.min.css",
	"libs/geosearch.module.min.js",
	"libs/jspdf.plugin.autotable.min.js",
	"libs/jspdf.umd.min.js",
	"libs/jspdf.umd.min.js.map",
	"libs/luxon.min.js",
	"libs/mprogress.min.css",
	"libs/mprogress.min.js",
	"libs/prism.css",
	"libs/prism.js",
	"libs/sortable.min.js",
	"libs/xlsx.core.min.js"
]


self.addEventListener('install', async event =>
{
	const serviceworkerBroadcast = new BroadcastChannel('serviceworker')
	serviceworkerBroadcast.postMessage({ message: 'update_cache_start' })

	self.skipWaiting()

	event.waitUntil((async () =>
	{
		try
		{
			const cache = await caches.open(cacheName)
			const total = assets.length
			let installed = 0

			Promise.all(assets.map(async url =>
			{
				let controller
				serviceworkerBroadcast.postMessage(url)
				try
				{
					controller = new AbortController()
					const { signal } = controller
					const req = new Request(url, { cache: 'reload' })
					const res = await fetch(req, { signal })

					if (res && res.status === 200)
					{
						await cache.put(req, res.clone())
						installed += 1
						serviceworkerBroadcast.postMessage({ message: 'update_cache', data: installed / total })
					}
					else
						console.info(`unable to fetch ${url} (${res.status})`)
				}
				catch (e)
				{
					console.info(`unable to fetch ${url}, ${e.message}`)
					controller.abort()
				}
			}))
				.then(response =>
				{
					if (installed === total)
						console.info(`application successfully installed (${installed}/${total} files added in cache)`)
					else
						console.info(`application partially installed (${installed}/${total} files added in cache)`)
				})
		}
		catch (e)
		{
			console.error(`unable to install application, ${e.message}`)
		}
	})())
})


self.addEventListener('fetch', event =>
{
	event.respondWith((async () =>
	{
		if (event.request.mode === "navigate" && event.request.method === "GET" && registration.waiting && (await clients.matchAll()).length < 2)
		{
			registration.waiting.postMessage('skipWaiting')
			return new Response("", { headers: { "Refresh": "0" } })
		}
		return await caches.match(event.request) || fetch(event.request)
	})())
})


self.addEventListener('activate', activateEvent =>
{
	activateEvent.waitUntil(
		caches.keys().then((keyList) =>
		{
			return Promise.all(keyList.map(key =>
			{
				if ((cacheName).indexOf(key) === -1)
					return caches.delete(key)
			}))
		})
	)
})


self.addEventListener('message', messageEvent =>
{
	if (messageEvent.data === 'skipWaiting')
		return skipWaiting()
})