store.default_services = []
store.default_page = 'optimus-base/dashboard'
store.theme = localStorage.getItem('theme') || 'optimus'
store.darkmode = JSON.parse(localStorage.getItem('darkmode')) || false
store.logo = '/images/optimus-logo.svg'
store.title = 'OPTIMUS 5.15'
store.version = '5.15'
optimus_init()