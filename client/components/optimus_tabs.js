export default class OptimusTabs
{
	constructor(target, params) 
	{
		this.target = target
		this.router = params.router
		this.tabs_container = params.tabs_container
		this.content_container = params.content_container
		this.tabs = params.tabs
		this.params = params.params
		return this
	}

	init()
	{
		let ul = document.createElement('ul')
		this.tabs_container.appendChild(ul)

		for (let tab of this.tabs)
			this.add(tab)

		if (store.hashParams.length > 0)
		{
			for (let hashParams of store.hashParams)
				for (let tab of this.tabs)
					if (tab.id == 'tab_' + hashParams)
						return this.change(tab.id)
					else if (tab.id == 'tab_' + store.hashParams.join('_'))
						return this.change(tab.id)
		}

		for (let tab of this.tabs)
			if (tab?.default == true)
				this.change(tab.id, true)

		return false
	}

	change(tab, replace = false)
	{
		onUnload.forEach(component => delete component.onUnload())
		onUnload = new Array
		let lis = this.tabs_container.querySelectorAll('li')

		if (Number.isInteger(tab))
			tab = lis[tab]
		else if (document.getElementById(tab))
			tab = document.getElementById(tab)
		else
			return false

		if (tab.dataset.link)
		{
			load(tab.dataset.link, document.querySelector('#' + this.tabs_container.id + '_container'), this.params)
			if (this.router && !store.back && window.location.href != store.url.origin + '/' + store.route && store.hashParams[0] != tab.dataset.link.replace('.html', '').split('/').pop())
			{
				store.hashParams[0] = tab.dataset.link.replace('.html', '').split('/').pop()
				let split = store.hashParams[0].replace('.js', '').replace('.html', '').split('_')
				store.hashParams[0] = split[0]
				if (split[1])
					store.hashParams[1] = split[1]
				if (window.location.href != store.url.href.split('#')[0] + '#' + store.hashParams.join('/'))
					if (replace)
						window.history.replaceState({ previous: window.location.href }, '', store.url.href.split('#')[0] + '#' + tab.id.replace('tab_', '').replace('_', '/'))
					else
						window.history.pushState({ previous: window.location.href }, '', store.url.href.split('#')[0] + '#' + tab.id.replace('tab_', '').replace('_', '/'))
			}
			store.back = undefined
		}

		lis.forEach(li => li.classList.remove('is-active'))
		tab.classList.add('is-active')

		return true
	}

	add(tab)
	{
		let component = this

		let ul = this.tabs_container.querySelector('ul')

		let li = document.createElement('li')
		if (tab.id)
			li.id = tab.id
		if (tab.link)
			li.dataset.link = tab.link
		if (tab.position)
			li.dataset.position = tab.position
		if (tab.default)
			li.dataset.default = tab.default
		if (tab.class)
			for (let newclass of tab.class.split(' '))
				li.classList.add(newclass)

		if (!tab.position || !ul.hasChildNodes() || +li.dataset.position >= +ul.querySelector('li:last-of-type').dataset.position)
			ul.appendChild(li)
		else
			for (let after_tab of ul.querySelectorAll('li'))
				if (+after_tab.dataset.position > +li.dataset.position)
				{
					ul.insertBefore(li, after_tab)
					break
				}

		let a = document.createElement('a')
		a.innerText = tab.text
		li.appendChild(a)

		li.addEventListener('mouseup', function () { component.change(this.id) })
		li.style.zIndex = 0
	}

	remove(tab)
	{
		if (Number.isInteger(tab))
		{
			let lis = this.tabs_container.querySelectorAll('li')
			tab = lis[tab]
		}
		else if (document.getElementById(tab))
			tab = document.getElementById(tab)
		else
			return false
		tab.remove()
	}

	show(tab)
	{
		if (Number.isInteger(tab))
		{
			let lis = this.tabs_container.querySelectorAll('li')
			tab = lis[tab]
		}
		else if (document.getElementById(tab))
			tab = document.getElementById(tab)
		else
			return false
		tab.classList.remove('is-hidden')
	}

	hide(tab)
	{
		if (Number.isInteger(tab))
		{
			let lis = this.tabs_container.querySelectorAll('li')
			tab = lis[tab]
		}
		else if (document.getElementById(tab))
			tab = document.getElementById(tab)
		else
			return false
		tab.classList.add('is-hidden')
	}
}