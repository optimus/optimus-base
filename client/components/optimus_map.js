load_CSS('/libs/leaflet/leaflet.css')
load_CSS('/libs/geosearch.min.css')
import { OpenStreetMapProvider, GeoSearchControl } from '/libs/geosearch.module.min.js'
export default class OptimusMap
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		return this
	}

	async init()
	{
		return new Promise(resolve =>
		{
			let div = document.createElement('div')
			div.classList.add('optimus-map')
			div.id = 'optimus-map-' + new Date().getTime()
			clear(this.target)
			div.style.height = this.target.offsetHeight - parseInt(window.getComputedStyle(this.target).paddingTop) - parseInt(window.getComputedStyle(this.target).paddingBottom) + 'px'
			this.target.appendChild(div)

			this.leaflet = L.map(div.id)
			let leaflet = this.leaflet

			this.target.querySelector('#' + div.id).leaflet = leaflet

			this.target.querySelector('.leaflet-top.leaflet-right').style.zIndex = 50000

			leaflet.on('click', (e) => leaflet.setView(e.latlng))

			leaflet.on('load', () => resolve(this))

			//DISPLAY A POINTER AT THE CENTER OF THE MAP
			if (this.params.Pointer)
			{
				let marker = L.marker(null, { icon: new L.icon({ iconUrl: '/images/crosshair.png', iconSize: [38, 38] }) }).addTo(leaflet)
				leaflet.on('move', () => marker.setLatLng(leaflet.getCenter()))
			}

			//LOCATE USER
			if (this.params.StartPoint == 'user')
			{
				leaflet.locate({ setView: true, maxZoom: 14 })
				function onLocationFound(e)
				{
					var radius = e.accuracy
					L.marker(e.latlng)
						.addTo(leaflet)
						.bindPopup('Vous êtes actuellement situé à ' + Math.round(radius) + ' mètres de ce point')
						.openPopup()
					L.circle(e.latlng, radius).addTo(leaflet)
				}
				leaflet.on('locationfound', onLocationFound)
			}
			else if (this.params.StartPoint)
			{
				fetch('https://nominatim.openstreetmap.org/search?format=json&q=' + this.params.StartPoint)
					.then(response => response.json())
					.then(response => 
					{
						if (response.length > 0)
						{
							leaflet.setView({ lng: response[0].lon, lat: response[0].lat }, 18)
							L.marker({ lng: response[0].lon, lat: response[0].lat }).addTo(leaflet)
						}
						else
							optimusToast("L'adresse spécifiée n'a pas pu être localisée", 'is-warning')
					})
			}
			else
			{
				leaflet.setView({ lng: 2.349014, lat: 48.864716 }, 8)
				L.marker({ lng: 2.349014, lat: 48.864716 }).addTo(leaflet)
			}


			function onLocationError(e)
			{
				optimusToast(e.message, 'is-warning')
			}
			leaflet.on('locationerror', onLocationError)


			// CONFIGURE LAYERS (roads, sattelite view, hybrid view, landforms ...)
			let layers = L.control.layers()
			layers.addBaseLayer(L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 19, attribution: '© OpenStreetMap', className: 'map-tiles' }), 'Routes (Openstreetmaps)')
			if (this.params.GoogleMaps)
			{
				layers.addBaseLayer(L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', { maxZoom: 19, subdomains: ['mt0', 'mt1', 'mt2', 'mt3'], attribution: '© Google', className: 'map-tiles' }), 'Routes (Google Maps)')
				layers.addBaseLayer(L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', { maxZoom: 19, subdomains: ['mt0', 'mt1', 'mt2', 'mt3'], attribution: '© Google' }), 'Satellite (Google Maps)')
				layers.addBaseLayer(L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}', { maxZoom: 19, subdomains: ['mt0', 'mt1', 'mt2', 'mt3'], attribution: '© Google' }), 'Hybride (Google Maps)')
				layers.addBaseLayer(L.tileLayer('http://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}', { maxZoom: 19, subdomains: ['mt0', 'mt1', 'mt2', 'mt3'], attribution: '© Google', className: 'map-tiles' }), 'Relief (Google Maps)')
			}
			if (layers._layers.length > 1)
				layers.addTo(leaflet)


			// LOAD THE FIRST LAYER (OPENSTREETMAPS ROADS)
			layers._layers[0].layer.addTo(leaflet)


			// ADD GEOSEARCH (address search engine)
			if (this.params.GeoSearch)
			{
				const provider = new OpenStreetMapProvider()
				const searchinput = new GeoSearchControl({
					provider: provider,
					style: 'bar',
					updateMap: true,
					autoClose: true,
					searchLabel: 'Entrez une adresse'
				})
				leaflet.addControl(searchinput)
			}

			// ADD STREET VIEW BUTTON TOP RIGHT
			if (this.params.GoogleStreetView)
			{
				var layerControl = L.Control.extend({
					options: {
						position: 'topright'
					},
					onAdd: function (map)
					{
						var button = L.DomUtil.create('button', 'leaflet-bar leaflet-control leaflet-control-custom')
						button.innerHTML = '<i class="fas fa-lg fa-street-view"></i>'
						button.title = 'Google Street View'
						button.style.cursor = 'pointer'
						button.style.width = '48px'
						button.style.height = '48px'
						button.onclick = () =>
						{
							window.open('https://www.google.com/maps?layer=c&cbll=' + L.Util.formatNum(leaflet.getCenter().lat, 6) + ',' + L.Util.formatNum(leaflet.getCenter().lng, 6))
						}
						return button
					}
				})
				leaflet.addControl(new layerControl())
			}
		})
	}

	onWindowResize()
	{
		setTimeout(() => this.leaflet.invalidateSize(true), 250)
	}

	onUnload()
	{
		delete this.leaflet
		return this
	}
}

