export default class OptimusForm
{
	constructor(target, params) 
	{
		this.target = target
		this.data = params
		store.history = new Array

		this.validators =
			[
				{
					name: 'integer',
					regExp: new RegExp(/^-?\d+$/),
					errorMessage: 'La valeur attendue est un nombre entier'
				},
				{
					name: 'positive_integer',
					regExp: new RegExp(/^\d+$/),
					errorMessage: 'La valeur attendue est un nombre entier supérieur ou égal à 0'
				},
				{
					name: 'negative_integer',
					regExp: new RegExp(/^-\d+$/),
					errorMessage: 'La valeur attendue est un nombre entier inférieur ou égal à 0'
				},
				{
					name: 'strictly_positive_integer',
					regExp: /^[1-9]\d*$/,
					errorMessage: 'La valeur attendue est un nombre entier strictement positif'
				},
				{
					name: 'strictly_negative_integer',
					regExp: /^-[1-9]\d*$/,
					errorMessage: 'La valeur attendue est un nombre entier strictement négatif'
				},
				{
					name: 'decimal',
					regExp: /^-?(\d*\.)?\d+$/,
					errorMessage: 'La valeur attendue est un nombre entier ou decimal, positif ou negatif. Exemples : -4 5.24 -8.32 0'
				},
				{
					name: 'topdomain',
					regExp: /^([a-z0-9][a-z0-9-]*[a-z0-9]\.)([a-z0-9][a-z0-9-]*[a-z0-9])$/,
					errorMessage: 'La valeur attendue est un nom de domaine de premier niveau. Exemple : mondomaine.com'
				},
				{
					name: 'domain',
					regExp: /^([a-z0-9][a-z0-9-]*[a-z0-9]\.)+([a-z0-9][a-z0-9-]*[a-z0-9])$/,
					errorMessage: 'La valeur attendue est un nom de domaine. Exemple : mondomaine.com ou test.mondomaine.com'
				},
				{
					name: 'ipv4',
					regExp: /^(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:\.(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}$/,
					errorMessage: 'La valeur attendue est une ip V4. Exemple : 124.45.104.54'
				},
				{
					name: 'origin',
					regExp: /^(\*)$|^(localhost|(\*\.)?([a-z0-9][a-z0-9-]*[a-z0-9]\.)+[a-z0-9][a-z0-9-]*[a-z0-9]|(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:\.(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3})(:((6553[0-5])|(655[0-2][0-9])|(65[0-4][0-9]{2})|(6[0-4][0-9]{3})|([1-5][0-9]{4})|([0-5]{0,5})|([0-9]{1,4})))?$/,
					errorMessage: 'La valeur attendue est une origine valide. Exemples : *  localhost:8080  *.optimus-avocats.fr  www.optimus-avocats.fr:9091  192.168.0.5:3232'
				},
				{
					name: 'email',
					regExp: /^(([a-z0-9][a-z0-9-_.]*[a-z0-9])+@([a-z0-9][a-z0-9-]*[a-z0-9]\.)+([a-z0-9][a-z0-9-]*[a-z0-9]))$/,
					errorMessage: 'La valeur attendue est une adresse email. Exemple : test@mondomaine.com'
				},
				{
					name: 'email_list',
					regExp: /^(([a-z0-9][a-z0-9-_.]*[a-z0-9])+@([a-z0-9][a-z0-9-]*[a-z0-9]\.)+([a-z0-9][a-z0-9-]*[a-z0-9]);?)+$/,
					errorMessage: 'La valeur attendue est une liste d\'adresses email séparées par des point-virgule. Exemple : first@test.org;second@test.org,third@test.org'
				},
				{
					name: 'boolean',
					regExp: /^(0|1|true|false)$/,
					errorMessage: 'La valeur attendue est un boolean.\nExemples : 1 ou 0. true ou false'
				},
				{
					name: 'string',
					regExp: /^[\w\t \-œàâäéèêëïîôöùûüÿçÉÈÊÀÙÇ()<>.,?!\"\'+=+\-=\$\€\/_*@&~#%:;°§\[\]\{\}]+$/,
					errorMessage: 'La valeur attendue est une chaine de caractères sur une seule ligne.\nNe sont autorisés que les lettres, chiffres et les caractères spéciaux suivants :\nœ à â ä é è ê ë ï î ô ö ù û ü ÿ ç ( ) . , ? ! " \' + = $ € / _ * @ & ~ # % : ; °'
				},
				{
					name: 'text',
					regExp: /^[\w\t \-œàâäéèêëïîôöùûüÿçÉÈÊÀÙÇ()<>.,?!\"\'+=+\-=\£\$\€\/_*@&~#%:;°§\[\]\{\}]*$/gm,
					errorMessage: 'La valeur attendue est une chaine de caractères sur une ou plusieurs lignes.\nNe sont autorisés que les lettres, chiffres et les caractères spéciaux suivants :\nœ à â ä é è ê ë ï î ô ö ù û ü ÿ ç ( ) . , ? ! " \' + = $ € / _ * @ & ~ # % : ; °'
				},
				{
					name: 'filename',
					regExp: /^[a-zA-Z0-9 ._@\-àâäéèêëïîôöùûüÿç()\']+$/,
					errorMessage: 'La valeur attendue est un nom de fichier ou dossier.\n Ne sont autorisés que les lettres, chiffres et les caractères spéciaux suivants :\n. _ @ à â ä é è ê ë ï î ô ö ù û ü ÿ ç ( ) \''
				},
				{
					name: 'date',
					regExp: /^(((\d\d)(([02468][048])|([13579][26]))-02-29)|(((\d\d)(\d\d)))-((((0\d)|(1[0-2]))-((0\d)|(1\d)|(2[0-8])))|((((0[13578])|(1[02]))-31)|(((0[1,3-9])|(1[0-2]))-(29|30)))))$/,
					errorMessage: 'La valeur attendue est une date au format dd/mm/yyyy'
				},
				{
					name: 'datetime',
					regExp: /^(((\d\d)(([02468][048])|([13579][26]))-02-29)|(((\d\d)(\d\d)))-((((0\d)|(1[0-2]))-((0\d)|(1\d)|(2[0-8])))|((((0[13578])|(1[02]))-31)|(((0[1,3-9])|(1[0-2]))-(29|30)))))\s(([01]\d|2[0-3]):([0-5]\d):([0-5]\d)(.(\d{1,12}))?)$/,
					errorMessage: 'La valeur attendue est une date au format dd/mm/yyyyyy hh:mm:ss'
				},
				{
					name: 'time',
					regExp: /^(([01]\d|2[0-3]):([0-5]\d):([0-5]\d))$/,
					errorMessage: 'La valeur attendue est une date au format hh:mm:ss'
				},
				{
					name: 'resource',
					regExp: /^[a-z-_]{2,64}(\/\d{1,16})?$|^\*$/,
					errorMessage: 'La valeur attendue est une resource.\nNe sont autorisés que les lettres minuscules, chiffres et les caractères spéciaux suivants : - _.'
				},
				{
					name: 'module',
					regExp: /^[a-z0-9-_]+$/,
					errorMessage: 'La valeur attendue est un module.\nNe sont autorisés que les lettres minuscules, chiffres et les caractères spéciaux suivants : - _.'
				},
				{
					name: 'hexcolor',
					regExp: /^(?:[0-9a-fA-F]{3}){1,2}$/,
					errorMessage: 'La valeur attendue est une couleur héxadécimale (sans le #).\nExemple : FF8000'
				},
				{
					name: 'one_uppercase_minimum',
					regExp: /[A-Z]/,
					errorMessage: 'La valeur attendue doit contenir au moins une lettre majuscule'
				},
				{
					name: 'one_lowercase_minimum',
					regExp: /[a-z]/,
					errorMessage: 'La valeur attendue doit contenir au moins une lettre minuscule'
				},
				{
					name: 'one_number_minimum',
					regExp: /[0-9]/,
					errorMessage: 'La valeur attendue doit contenir au moins un nombre'
				},
				{
					name: 'one_specialchar_minimum',
					regExp: /[^\w]/,
					errorMessage: 'La valeur attendue doit contenir au moins un caractère spécial'
				},
			]
		return this
	}

	async init()
	{
		if (main.querySelector('.undo-button'))
		{
			main.querySelector('.undo-button').classList.add('is-hidden')
			main.querySelector('.undo-button').onclick = () => this.undo()
		}

		this.target.querySelectorAll('input[type=text], input[type=password], input[type=date], input[type=number]').forEach(input => this.initInput(input))
		this.target.querySelectorAll('[data-type="resourcefinder"]').forEach(input => this.initResourceFinder(input))
		this.target.querySelectorAll('input[type=checkbox]').forEach(input => this.initCheckbox(input))
		this.target.querySelectorAll('select').forEach(select => this.initSelect(select))
		this.target.querySelectorAll('textarea').forEach(textarea => this.initTextArea(textarea))
		return this
	}

	//INPUT
	initInput(input)
	{
		let component = this

		if (input.type == 'date')
			if (input.classList.contains('optimus-picker'))
			{
				input.classList.add('no-picker')
				input.parentNode.classList.add('has-icons-right')
				if (store.touchscreen)
					input.onclick = () => 
					{
						load('/components/optimus_datepicker.js', modal, { input: input })
						return false
					}
				let span = document.createElement('span')
				span.classList.add('icon', 'is-right', 'is-clickable')
				span.onclick = () => load('/components/optimus_datepicker.js', modal, { input: input })
				let i = document.createElement('i')
				i.classList.add('fas', 'fa-calendar', 'has-text-link', 'p-3')
				span.appendChild(i)
				input.parentNode.appendChild(span)
			}

		if (this.data?.restrictions?.includes('write'))
			input.disabled = true
		if (this.data)
			if (input.dataset.field.includes('.'))
				input.value = this.data[input.dataset.field.split('.')[0]][input.dataset.field.split('.')[1]] || input.value
			else
				input.value = this.data[input.dataset.field] || input.value
		input.validators = input.dataset.validator?.split(' ')
		input.history = [input.value]
		input.lastSavedValue = input.value
		input.server = store.queryParams.server || store.user.server
		input.owner = store.queryParams.owner || store.user.id
		input.endpoint = input.dataset?.endpoint?.replace('{server}', input.server || store.user.server)?.replace('{owner}', input.owner || store.user.id)?.replace('{id}', input.dataset.id || store.queryParams.id)?.replace('{subid}', input.dataset.subid || store.queryParams.subid)

		if (input.dataset.title == 'true')
		{
			main.querySelector('.title').innerText = this.data.displayname.toUpperCase() || input.value.toUpperCase()
			input.value = this.data.displayname || input.value
			input.lastSavedValue = this.data.displayname || input.value
			input.placeholder = this.data.displayname || null
		}

		input.addEventListener('blur', function ()
		{
			if (component.validates(this) != true || input.preventDefaultOnblur)
				return false

			if (input.dataset.endpoint)
				component.save(this)
		})

		input.addEventListener('keyup', function (event)
		{
			input.setCustomValidity('')
			if (event.key === 'Enter')
				this.blur()
			else if (event.key === 'Escape')
			{
				this.value = store.history.slice(-1)
				this.blur()
				component.onValid(this)
			}
			else if (input.dataset.onkeypress == 'true' && event.key != 'Shift' && event.key != 'Control' && event.key != 'Alt')
			{
				component.onValid(this)
				component.validates(this)
			}
			if (input.dataset.title == 'true')
				main.querySelector('.title').innerText = this.value.toUpperCase()
		})
	}

	//RESOURCE FINDER
	initResourceFinder(input)
	{
		let component = this

		if (this.data?.restrictions?.includes('write'))
			input.disabled = true

		input.validators = input.dataset.validator?.split(' ')
		if (input.dataset.server)
			input.server = input.dataset.server
		else
			input.server = store.queryParams.server || store.user.server
		if (input.dataset.owner)
			input.owner = input.dataset.owner
		else
			input.owner = store.queryParams.owner || store.user.id
		input.endpoint = input.dataset?.endpoint?.replace('{server}', input.server || store.user.server)?.replace('{owner}', input.owner || store.user.id)?.replace('{id}', input.dataset.id || store.queryParams.id)?.replace('{subid}', input.dataset.subid || store.queryParams.subid)
		input.history = []
		input.lastSavedValue = input.value
		input.relative = 0
		input.resource_endpoint = store.user.server + store.resources.find(item => item.id == input.dataset.resource).endpoint?.replace('{owner}', input.owner)

		if (input.dataset.storeid == 'true')
		{
			if (this.data)
				if (input.dataset.field.includes('.'))
					input.value = this.data[input.dataset.field.split('.')[0]][input.dataset.field.split('.')[1]] || input.value
				else
					input.value = this.data[input.dataset.field] || input.value
			if (input.dataset.id)
			{
				rest(input.resource_endpoint, 'GET', { filter: [{ field: "id", type: "=", value: input.dataset.id }] })
					.then(response => 
					{
						input.value = response.data[0].displayname
						input.history = [{ id: response.data[0].id, value: response.data[0].displayname }]
						input.lastSavedValue = { id: response.data[0].id, value: response.data[0].displayname }
					})
			}
			else
			{
				input.value = ''
				input.dataset.id = null
			}

		}
		else
		{
			if (this.data)
				input.value = this.data[input.dataset.field] || input.value
			if (input.dataset.value)
				rest(input.resource_endpoint, 'GET', { filter: [{ field: "displayname", type: "=", value: input.dataset.value }] })
					.then(response => 
					{
						input.id = response.data[0].id
						input.history = [{ id: response.data[0].id, value: response.data[0].displayname }]
						input.lastSavedValue = { id: response.data[0].id, value: response.data[0].displayname }
					})
		}

		if (input.dataset.title == 'true')
			main.querySelector('.title').innerText = input.value.toUpperCase()

		input.addEventListener('blur', function (event)
		{
			if (input.nextElementSibling.options.length == 0)
			{
				input.value = ''
				input.dataset.id = null
			}
			else
				for (let i = 0; i < input.nextElementSibling.options.length; i++)
					if (input.nextElementSibling.options[i].classList.contains('is-active'))
					{
						input.value = input.nextElementSibling.options[i].value
						input.dataset.id = input.nextElementSibling.options[i].dataset.id
					}
			setTimeout(() => input.nextElementSibling.style.display = 'none', 100)
			if (component.validates(this) != true)
				return false
			if (input.dataset.endpoint)
				component.save(this)
		})

		input.addEventListener('keyup', function (event)
		{
			input.setCustomValidity('')
			if (event && event.key == 'ArrowDown')
			{
				for (let i = 0; i < input.nextElementSibling.options.length; i++)
					if (input.nextElementSibling.options[i].classList.contains('is-active') && input.nextElementSibling.options[i].nextElementSibling)
					{
						input.nextElementSibling.options[input.active].classList.remove('is-active')
						input.active++
						input.relative++
						input.nextElementSibling.options[input.active].classList.add('is-active')
						if (input.relative > 9)
						{
							input.nextElementSibling.scrollTo(0, input.nextElementSibling.options[input.active - 9]?.offsetTop)
							input.relative = 9
						}
						break
					}
			}
			else if (event && event.key == 'ArrowUp')
			{
				for (let i = 0; i < input.nextElementSibling.options.length; i++)
					if (i > 0 && input.nextElementSibling.options[i].classList.contains('is-active') && input.nextElementSibling.options[i].previousElementSibling)
					{
						input.nextElementSibling.options[input.active].classList.remove('is-active')
						input.active--
						input.relative--
						input.nextElementSibling.options[input.active].classList.add('is-active')
						if (input.relative < 1)
						{
							input.nextElementSibling.scrollTo(0, input.nextElementSibling.options[input.active].offsetTop)
							input.relative = 1
						}
						break
					}
			}
			else if (event && event.key == 'Escape')
				this.blur()
			else if (event && event.key == 'Enter')
				this.blur()
			else if (input.value === '')
			{
				input.nextElementSibling.innerHTML = ''
				input.dataset.id = null
			}
			else if (input.value)
				rest(input.resource_endpoint, 'GET',
					{
						page: 1,
						size: 50,
						sort: [{ field: "displayname", dir: "asc" }],
						filter: [{ field: "displayname", type: "like", value: input.value }]
					})
					.then(response => 
					{
						input.nextElementSibling.innerHTML = ''

						if (!response.data)
							return
						if (response.data.length == 0)
							return false
						for (let item of response.data)
						{
							let option = document.createElement('option')
							option.innerText = item.displayname
							option.value = item.displayname
							option.dataset.id = item.id
							option.onmousedown = function (event)
							{
								for (let i = 0; i < input.nextElementSibling.options.length; i++)
									input.nextElementSibling.options[i].classList.remove('is-active')
								this.classList.add('is-active')
							}
							this.nextElementSibling.appendChild(option)
						}
						input.active = 0
						input.nextElementSibling.options[0].classList.add('is-active')
						input.nextElementSibling.style.display = 'block'
						input.scrollIntoView()
					})
			else if (!input.value)
				input.nextElementSibling.innerHTML = ''
		})

		input.addEventListener('focus', function ()
		{
			// input.onkeyup()
			const event = new Event('keyup')
			input.dispatchEvent(event)
			input.select()
		})
	}

	//SELECT
	initSelect(input)
	{
		let component = this

		if (this.data?.restrictions?.includes('write'))
			input.disabled = true

		if (this.data)
			if (input.dataset.field.includes('.'))
				input.value = this.data[input.dataset.field.split('.')[0]][input.dataset.field.split('.')[1]] || input.value
			else
				input.value = this.data[input.dataset.field] || input.value
		input.history = [input.value]
		input.lastSavedValue = input.value
		input.server = store.queryParams.server || store.user.server
		input.owner = store.queryParams.owner || store.user.id
		input.endpoint = input.dataset?.endpoint?.replace('{server}', input.server || store.user.server)?.replace('{owner}', input.owner || store.user.id)?.replace('{id}', input.dataset.id || store.queryParams.id)?.replace('{subid}', input.dataset.subid || store.queryParams.subid)

		if (input.dataset.title == 'true')
		{
			main.querySelector('.title').innerText = this.data.displayname.toUpperCase() || input.value.toUpperCase()
			input.value = this.data.displayname || input.value
			input.lastSavedValue = this.data.displayname || input.value
			input.placeholder = this.data.displayname || null
		}

		input.addEventListener('change', function ()
		{
			if (component.validates(this) != true || input.preventDefaultOnchange)
				return false
			if (this.dataset?.endpoint)
				component.save(this)
		})

		input.addEventListener('keyup', function (event)
		{
			if (input.preventDefaultKeyup)
				return false
			input.setCustomValidity('')
			if (event.key === 'Enter')
				this.blur()
			else if (event.key === 'Escape')
			{
				this.value = store.history.slice(-1)
				this.blur()
				component.onValid(this)
			}
		})

		if (input.dataset.endpoint && input.dataset.field)
			input.save = () => this.save(input)
		else
			input.save = () => input.history.push(input.value)
	}

	//CHECKBOX
	initCheckbox(input)
	{
		if (this.data?.restrictions?.includes('write'))
			input.disabled = true

		if (this.data)
			if (input.dataset.field.includes('.'))
				input.checked = this.data[input.dataset.field.split('.')[0]][input.dataset.field.split('.')[1]] || false
			else
				input.checked = this.data[input.dataset.field] || false
		input.history = [input.checked]
		input.lastSavedValue = input.checked
		input.server = store.queryParams.server || store.user.server
		input.owner = store.queryParams.owner || store.user.id
		input.endpoint = input.dataset?.endpoint?.replace('{server}', input.server || store.user.server)?.replace('{owner}', input.owner || store.user.id)?.replace('{id}', input.dataset.id || store.queryParams.id)?.replace('{subid}', input.dataset.subid || store.queryParams.subid)
		input.addEventListener('change', () => 
		{
			if (input.preventDefaultOnchange)
				return false
			input.value = input.checked ? true : false
			if (input.dataset.endpoint)
				this.save(input)
		})
	}

	//TEXTAREA
	initTextArea(input)
	{
		let component = this

		if (this.data?.restrictions?.includes('write'))
			input.disabled = true

		if (this.data)
			if (input.dataset.field.includes('.'))
				input.value = this.data[input.dataset.field.split('.')[0]][input.dataset.field.split('.')[1]] || ''
			else
				input.value = this.data[input.dataset.field] || ''
		input.validators = input.dataset.validator?.split(' ')
		input.history = [input.value]
		input.lastSavedValue = input.value
		input.server = store.queryParams.server || store.user.server
		input.owner = store.queryParams.owner || store.user.id
		input.endpoint = input.dataset?.endpoint?.replace('{server}', input.server || store.user.server)?.replace('{owner}', input.owner || store.user.id)?.replace('{id}', input.dataset.id || store.queryParams.id)?.replace('{subid}', input.dataset.subid || store.queryParams.subid)
		input.addEventListener('change', function ()
		{
			if (component.validates(this) != true || input.preventDefaultOnchange)
				return false
			if (input.dataset.endpoint)
				component.save(this)
		})
		input.addEventListener('keyup', function (event)
		{
			input.setCustomValidity('')
			if (event.key === 'Escape')
			{
				this.value = store.history.slice(-1)
				this.blur()
				component.onValid(this)
			}
			else if (input.dataset.onkeypress == 'true' && event.key != 'Shift' && event.key != 'Control' && event.key != 'Alt')
			{
				component.onValid(this)
				component.validates(this)
			}
			if (input.dataset.title == 'true')
			{
				main.querySelector('.title').innerText = this.data.displayname.toUpperCase() || this.value.toUpperCase()
				input.value = this.data.displayname || input.value
				input.lastSavedValue = this.data.displayname || input.value
				input.placeholder = this.data.displayname || null
			}
		})
	}

	//SAVE ENGINE
	save(input)
	{
		let component = this

		if (input.type == 'date' && input.validationMessage != '')
		{
			input.setCustomValidity('Date invalide')
			input.reportValidity()
			return false
		}

		if (input.value != (input.dataset.storeid ? input.lastSavedValue.value : input.lastSavedValue))
		{
			let data = {}
			if (input.dataset.field.includes('.'))
				if (input.dataset.storeid == 'true')
					data[input.dataset.field.split('.')[0]] = { [input.dataset.field.split('.')[1]]: input.dataset.id == 'null' ? null : input.dataset.id }
				else
					data[input.dataset.field.split('.')[0]] = { [input.dataset.field.split('.')[1]]: input.value }
			else
				if (input.dataset.storeid == 'true')
					data[input.dataset.field] = input.dataset.id == 'null' ? null : input.dataset.id
				else
					data[input.dataset.field] = input.value

			return rest(input.endpoint, 'PATCH', data, null, input)
				.then(response => 
				{
					if (response.code >= 200 && response.code <= 299)
					{
						if (input.type == 'checkbox')
							input.history.push(input.checked)
						else if (input.dataset.type == 'resourcefinder')
							input.history.push({ id: input.dataset.id, value: input.value })
						else
							input.history.push(input.value)
						store.history.push(() => component.undo(input))
						if (input.dataset.title == 'true')
						{
							main.querySelector('.title').innerText = response.data.displayname.toUpperCase() || input.value.toUpperCase()
							input.value = response.data.displayname || input.value
							input.placeholder = response.data.displayname || input.value
						}
						if (main.querySelector('.undo-button'))
						{
							main.querySelector('.undo-button').classList.toggle('is-hidden', store.history.length == 0)
							main.querySelector('.undo-button-text').innerText = 'Annuler (' + store.history.length + ')'
						}
						if (input.type == 'checkbox')
							input.lastSavedValue = input.checked
						else if (input.dataset.type == 'resourcefinder')
							input.lastSavedValue = { id: input.dataset.id, value: input.value }
						else
							input.lastSavedValue = input.value
						return true
					}
					else
					{
						if (response.message)
							if (response.code >= 500)
								optimusToast(response.message, 'is-danger')
							else
								this.onInvalid(input, response.message)
						if (input.dataset.type == 'resourcefinder')
						{
							input.value = input.lastSavedValue.value
							input.dataset.id = input.lastSavedValue.id
							input.history.push({ id: input.dataset.id, value: input.value })
						}
						else
						{
							if (input.type == 'checkbox')
								input.checked = !input.checked
							input.value = input.lastSavedValue
							if (input.dataset.title == 'true')
							{
								main.querySelector('.title').innerText = input.lastSavedValue.toUpperCase()
								input.placeholder = input.lastSavedValue
							}
							input.history.push(input.value)
						}
						return false
					}
				})
				.catch(() =>
				{
					if (input.dataset.type == 'resourcefinder')
					{
						input.value = input.lastSavedValue.value
						input.dataset.id = input.lastSavedValue.id
					}
					else
						input.value = input.lastSavedValue
					return false
				})
				.finally(() => 
				{
					if (!input.classList.contains('is-danger'))
						this.validateEffect(input)
				})
		}
		else
			input.blur()
	}

	async undo(input = null)
	{
		if (input)
		{
			if (input.type == 'checkbox')
				input.checked = input.history.slice(-2, -1)[0]
			else if (input.dataset.type == 'resourcefinder')
			{
				input.value = input.history.slice(-2, -1)[0].value
				input.dataset.id = input.history.slice(-2, -1)[0].id
			}
			else
				input.value = input.history.slice(-2, -1)[0]
			input.history.pop()
			let undojob = await this.save(input)
			if (undojob == false)
			{
				if (input.type == 'checkbox')
					input.checked = input.lastSavedValue
				else if (input.dataset.type == 'resourcefinder')
				{
					input.value = input.lastSavedValue.value
					input.dataset.id = input.lastSavedValue.id
				}
				else
					input.value = input.lastSavedValue
				return false
			}
			else
			{
				input.history.pop()
				store.history.pop()
				if (main.querySelector('.undo-button'))
				{
					main.querySelector('.undo-button').classList.toggle('is-hidden', store.history.length == 0)
					main.querySelector('.undo-button-text').innerText = 'Annuler (' + store.history.length + ')'
				}
				return true
			}
		}
		if (store.history.length > 0)
			store.history.pop()()
	}

	validates(input)
	{
		input.setCustomValidity('')
		input.reportValidity()
		input.classList.remove('is-danger')

		if (input.required != true && input.value == '')
			return true
		if (input.validators)
			for (let validator of input.validators)
			{
				let validate = this.matchesValidator(validator, input.dataset.storeid == 'true' ? input.dataset.id : input.value)
				if (validate != true)
					return this.onInvalid(input, validate)
			}
		let validate = this.matchesCondition(input)
		if (validate != true)
			return this.onInvalid(input, validate)
		return true
	}

	matchesValidator(validator, value)
	{
		validator = this.validators.filter(item => item.name == validator)[0]
		if (!validator)
			return 'Le validateur renseigné pour ce champ est inconnu'
		if ((validator.regExp.multiline == false && value.match(validator.regExp) == null) || (validator.regExp.multiline == true && value.match(validator.regExp).length != value.split('\n').length))
			return validator.errorMessage
		return true
	}

	matchesCondition(input)
	{
		if (input.required == true && input.value == '')
			return 'Ce champ doit être obligatoirement renseigné'
		else if ((input.type == 'string' || input.type == 'text' || input.type == 'password') && input.dataset.min && input.value.length < input.dataset.min)
			return 'La valeur attendue doit compter au minimum ' + input.dataset.min + ' caractères'
		else if ((input.type == 'string' || input.type == 'text' || input.type == 'password') && input.dataset.max && input.value.length > input.dataset.max)
			return 'La valeur attendue doit compter au maximum ' + input.dataset.max + ' caractères'
		else if (input.type == 'number' && input.dataset.min && input.value.length < input.dataset.min)
			return 'La valeur attendue doit être supérieure ou égale à ' + input.dataset.min
		else if (input.type == 'number' && input.dataset.max && input.value.length > input.dataset.max)
			return 'La valeur attendue doit être inférieure ou égale à ' + input.dataset.max
		return true
	}

	onInvalid(object, errorMessage)
	{
		object.classList.add('is-danger')
		object.setCustomValidity(errorMessage)
		object.reportValidity()
	}

	onValid(object)
	{
		object.classList.remove('is-danger')
		object.setCustomValidity('')
	}

	validateEffect(input)
	{
		if (input.nodeName == 'INPUT')
			input.parentNode?.classList.add('is-validating')
		else if (input.nodeName == 'SELECT')
			input.parentNode?.classList.add('is-validating')
		else
			input.classList.add('is-validating')

		setTimeout(() => 
		{
			if (input.nodeName == 'INPUT')
				input.parentNode?.classList.remove('is-validating')
			else if (input.nodeName == 'SELECT')
				input.parentNode?.classList.remove('is-validating')
			else
				input.classList.remove('is-validating')
		}, 450)
	}
}