load_CSS('/libs/tabulator/css/tabulator_bulma.min.css')
import { Tabulator, EditModule, FormatModule, InteractionModule, MutatorModule, PageModule, SortModule, FilterModule, AjaxModule } from '/libs/tabulator/js/tabulator_esm.min.js'
Tabulator.registerModule([EditModule, FormatModule, InteractionModule, MutatorModule, PageModule, SortModule, FilterModule, AjaxModule])

export default class OptimusAuthorizations
{
	constructor(target, params) 
	{
		this.target = target
		this.config = params
		this.config.title = params.title || 'Autorisations'
		this.config.subtitle = params.subtitle || ''
		this.server = store.user.server
		return this
	}

	async init()
	{
		loader(this.target)
		await load('/components/optimus_authorizations/index.html', this.target)

		let target = this.target
		let config = this.config

		//APPLY VISUAL SETTINGS
		target.querySelector('.optimus-table').classList.toggle('is-striped', config.striped)
		target.querySelector('.optimus-table').classList.toggle('is-bordered', config.bordered)
		target.querySelector('.optimus-table').classList.toggle('is-bordered-vertical', config.columnSeparator)

		//SHOW CUSTOM TITLE OR HIDE TITLE
		if (config?.showTitle === false)
			target.querySelector('.title').classList.add('is-hidden')
		else
			target.querySelector('.title').innerText = config?.title

		//SHOW CUSTOM SUBTITLE OR HIDE SUBTITLE
		if (config?.showSubtitle === false)
			target.querySelector('.optimus-authorizations-subtitle').classList.add('is-hidden')
		else
			target.querySelector('.optimus-authorizations-subtitle').innerText = config.subtitle

		//HIDE ADD BUTTON
		if (config?.showAddButton === false)
			target.querySelectorAll('.add-authorizations.button').forEach(button => button.classList.add('is-hidden'))
		else if (config?.showAddButton == 'bottom')
			target.querySelectorAll('.add-authorizations.button')[0].classList.add('is-hidden')
		else if (config?.showAddButton == 'top')
			target.querySelectorAll('.add-authorizations.button')[1].classList.add('is-hidden')

		//USE MAX AVAILABLE HEIGHT IN TARGET
		target.querySelector('.optimus-authorizations-table').style.height = target.getBoundingClientRect().bottom - target.querySelector('.optimus-authorizations-table').getBoundingClientRect().top - parseInt(window.getComputedStyle(target).getPropertyValue('padding-bottom')) + 20 + 'px'

		//SET INITIAL FILTERS
		this.initialFilters = new Array
		if (config.filters.owner)
			this.initialFilters.push({ field: 'owner', type: '=', value: config.filters.owner })
		if (config.filters.user)
			this.initialFilters.push({ field: 'user', type: '=', value: config.filters.user })
		if (config.filters.resource)
			this.initialFilters.push({ field: 'resource', type: '=', value: config.filters.resource })

		//INITIALIZE TABULATOR INSTANCE
		this.tabulator = new Tabulator(target.querySelector('.optimus-authorizations-table'),
			{
				columns:
					[
						{
							title: "Propriétaire",
							field: "owner_displayname",
							minWidth: 250,
							maxWidth: 250,
							visible: config?.columns?.includes('owner') ? true : false,
							headerFilter: config?.headerFilterInputs?.includes('owner'),
							headerFilterPlaceholder: 'Filtrer les Propriétaires'
						},
						{
							title: "Utilisateurs autorisés",
							field: "user_displayname",
							minWidth: 250,
							maxWidth: 250,
							visible: config?.columns?.includes('user') ? true : false,
							headerFilter: config?.headerFilterInputs?.includes('user'),
							headerFilterPlaceholder: 'Filtrer les Utilisateurs'
						},
						{
							title: "ID de la ressource",
							field: "resource",
							minWidth: 200,
							maxWidth: 200,
							visible: config?.columns?.includes('resource') ? true : false,
							headerFilter: config?.headerFilterInputs?.includes('resource'),
							headerFilterPlaceholder: 'Filtrer les Ressources (ID)'
						},
						{
							title: "Nom de la ressource",
							field: "resource_displayname",
							minWidth: 350,
							maxWidth: 350,
							visible: config?.columns?.includes('resource_displayname') ? true : false,
							headerFilter: config?.headerFilterInputs?.includes('resource_displayname'),
							headerFilterPlaceholder: 'Filtrer les Ressource (nom)',
						},
						{
							title: "Lire",
							field: "read",
							minWidth: 100,
							maxWidth: 100,
							hozAlign: "center",
							visible: config?.columns?.includes('read') ? true : false,
							headerSort: false, headerHozAlign: "center",
							formatter: "tickCross",
							cellClick: (e, cell) => 
							{
								if (store.user.admin == true || store.user.id == this.config.filters.owner)
									this.updateAuthorization(cell._cell.row.data, 'read')
							}
						},
						{
							title: "Modifier",
							field: "write",
							minWidth: 100,
							maxWidth: 100,
							hozAlign: "center",
							visible: config?.columns?.includes('write') ? true : false,
							headerSort: false,
							headerHozAlign: "center",
							headerHozAlign: "center",
							formatter: "tickCross",
							cellClick: (e, cell) => 
							{
								if (store.user.admin == true || store.user.id == this.config.filters.owner)
									this.updateAuthorization(cell._cell.row.data, 'write')
							}
						},
						{
							title: "Supprimer",
							field: "delete",
							minWidth: 100,
							maxWidth: 100,
							hozAlign: "center",
							visible: config?.columns?.includes('delete') ? true : false,
							headerSort: false,
							headerHozAlign: "center",
							formatter: "tickCross",
							cellClick: (e, cell) => 
							{
								if (store.user.admin == true || store.user.id == this.config.filters.owner)
									this.updateAuthorization(cell._cell.row.data, 'delete')
							}
						},
						{
							title: "Créer",
							field: "create",
							minWidth: 100,
							maxWidth: 100,
							hozAlign: "center",
							visible: config?.columns?.includes('create') ? true : false,
							headerSort: false,
							headerHozAlign: "center",
							formatter: "tickCross",
							formatterParams: { allowEmpty: true },
							cellClick: (e, cell) => 
							{
								if (cell._cell.row.data.resource.includes('/'))
									return false

								if (store.user.admin == true || store.user.id == this.config.filters.owner)
									this.updateAuthorization(cell._cell.row.data, 'create')
							}
						},
						{
							title: '',
							field: "remove",
							minWidth: 40,
							maxWidth: 40,
							hozAlign: "center",
							headerSort: false,
							headerHozAlign: "center",
							formatter: "html",
							mutator: (value, data) => data.remove = '<i class="fas fa-trash-can"></i>',
							cellClick: (e, cell) => 
							{
								if (store.user.admin == true || store.user.id == this.config.filters.owner)
									this.removeAuthorization(cell._cell.row.data.id)
							}
						},
					],
				ajaxURL: this.config.url ? this.config.url.replace('{server}', config.server) : null,
				ajaxRequestFunc: (url, ajaxConfig, params) => new Promise(function (resolve, reject)
				{
					if (!store.user.server)
						modal.open('/services/optimus-base/login/index.js')
					else
						rest(url, ajaxConfig.method, params, 'toast')
							.then(response =>
							{
								if (response.code >= 400)
									reject(response.message)
								else
								{
									for (let i = 0; i < response.data.length; i++)
										if (response.data[i].resource.includes('/'))
											response.data[i].create = null
									resolve(config.pagination == false ? response.data : response)
								}
							})
				}),
				ajaxConfig: {
					method: 'GET',
					credentials: 'include'
				},
				columnHeaderVertAlign: 'middle',
				initialFilter: this.initialFilters,
				dataLoader: false,
				progressiveLoadScrollMargin: 1,
				paginationMode: 'remote',
				pagination: store.innerWidth < 768 ? 'scroll' : true,
				paginationSizeSelector: store.innerWidth < 768 ? false : [25, 50, 100, 500, true],
				paginationCounter: "rows",
				pagination: config.pagination,
				ajaxContentType: 'json',
				layout: 'fitColumns',
				layoutColumnsOnNewData: false,
				sortOrderReverse: true,
				columnHeaderSortMulti: false,
				headerSortElement: "<i class='fas fa-arrow-up'></i>",
				sortMode: 'remote',
				filterMode: "remote",
				locale: 'fr',
				langs: {
					'fr': {
						pagination: {
							page_size: 'Résultats par page',
							page_title: 'Aller à',
							first: '<i class="fas fa-backward-step" style="padding-top:3px;padding-bottom:2px"></i>',
							first_title: 'Première page',
							last: '<i class="fas fa-forward-step" style="padding-top:3px;padding-bottom:2px"></i>',
							last_title: 'Dernière page',
							prev: '<i class="fas fa-lg fa-caret-left" style="padding-top:10px;padding-bottom:9px;margin-top:1px"></i>',
							prev_title: 'Page précédente',
							next: '<i class="fas fa-lg fa-caret-right" style="padding-top:10px;padding-bottom:9px;margin-top:1px"></i>',
							next_title: 'Page suivante',
							all: 'Tous',
							counter: {
								showing: '',
								of: 'sur',
								rows: '',
								pages: 'pages'
							}
						},
						ajax: {
							loading: "Chargement",
							error: "Erreur de chargement",
						},
					}
				},
				...this.config.tabulator,
			})

		let tabulator = this.tabulator

		this.target.querySelectorAll('.add-authorizations.button').forEach(button => button.onclick = () => modal.open('/components/optimus_authorizations/editor.js', false,
			{
				tabulator: this.tabulator,
				blocks: this.config?.editor?.blocks || [store.user.admin == 1 ? 'owner' : null, 'resource-type', 'resource', 'user', 'rights'],
				owner: null,
				server: this.server,
				owner: this.config?.filters?.owner || null,
				user: this.config?.filters?.user || null,
				resourceType: (this.config?.filters?.resource?.split('/')[1] ? this.config?.filters?.resource?.split('/')[0] + '/id' : this.config?.filters?.resource?.split('/')[0]) || null,
				resourceId: this.config?.filters?.resource?.split('/')[1] || null,
				read: this.config?.editor?.read || false,
				write: this.config?.editor?.write || false,
				create: this.config?.editor?.create || false,
				delete: this.config?.editor?.delete || false,
				onCreated: this.config?.editor?.onCreated || null,
				onCancel: this.config?.editor?.onCancel || null
			}))

		this.tabulator.on('dataProcessed', () =>
		{
			if (store.innerWidth > 768)
				target.querySelector('.tabulator').style.height = target.querySelector('.tabulator-header')?.offsetHeight + (this.tabulator.getRows().length * 32) + (target.querySelector('.tabulator-footer')?.offsetHeight || 1) + (config.bordered ? 1 : -1) + (target.querySelector('.tabulator-tableholder').scrollWidth > target.querySelector('.tabulator-tableholder').offsetWidth ? 18 : 0) + 'px'
		})

		return new Promise(resolve => tabulator.on("tableBuilt", async () => 
		{
			let subtitle = new Array()
			if (config.filters.user)
			{
				let response = await rest(this.server + '/optimus-base/users/' + config.filters.user, 'GET', null)
				if (!response.data?.displayname)
				{
					loader(this.target, false)
					return optimusToast("L'utilisateur n°" + config.filters.user + " indiqué n'existe pas", 'is-danger')
				}
				else
					subtitle.push(' à ' + response.data.displayname)
			}
			if (config.filters.resource)
				if (config.filters.resource.includes('/'))
				{
					let response = await rest(this.server + store.resources.find(resource => resource.path == config.filters.resource.split('/')[0] + '/id').endpoint.replace('{owner}', config.filters.owner) + '/' + config.filters.resource.split('/')[1], 'GET', null)
					if (response.code == 404)
					{
						loader(this.target, false)
						return optimusToast("La ressource \"" + config.filters.resource + "\" indiquée n'existe pas", 'is-danger')
					}
					else
						subtitle.push(' sur ' + response.data.displayname)
				}
				else
					subtitle.push(' sur ' + config.filters.resource)
			if (subtitle.length > 0)
				target.querySelector('.optimus-authorizations-subtitle').innerHTML = subtitle.join('<br/>')

			//SET COMPONENT WIDTH
			let width = 0
			tabulator.columnManager.columns.map(column => width += column.visible ? column.maxWidth : 0)
			target.firstElementChild.style.setProperty('max-width', (config.bordered ? 2 : 0) + width + 'px', 'important')
			tabulator.width = width

			loader(this.target, false)
			resolve(this)
		}))
	}

	removeAuthorization(id)
	{
		rest(this.server + '/optimus-base/authorizations/' + id, 'DELETE', null, 'toast')
			.then(response => 
			{
				if (response.code == 200)
					this.tabulator.deleteRow(id)
						.then(() => this.tabulator.setData())
			})
	}

	updateAuthorization(object, item)
	{
		rest(this.server + '/optimus-base/authorizations/' + object.id, 'PATCH', { [item]: (object[item] == 1) ? false : true }, 'toast')
			.then(response => 
			{
				if (response.code == 200)
					this.tabulator.updateData([{ id: response.data.id, [item]: response.data[item] }])
			})
	}

	onUnload()
	{
		this.tabulator.destroy()
		delete this.tabulator
		return this
	}
}