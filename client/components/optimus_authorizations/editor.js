export default class OptimusAuthorizationsEditor
{
	constructor(target, params) 
	{
		this.target = target
		this.tabulator = params?.tabulator || null
		this.blocks = params?.blocks || ['server', 'owner', 'resource-type', 'resource', 'user', 'rights']
		this.server = params?.server || null
		this.owner = params?.owner || null
		this.user = params?.user || null
		this.resourceType = params?.resourceType || null
		this.resourceId = params?.resourceId || null
		this.read = params?.read || false
		this.write = params?.write || false
		this.create = params?.create || false
		this.delete = params?.delete || false
		this.onCreated = params?.onCreated || null
		this.onCancel = params?.onCreated || null
		return this
	}

	async init()
	{
		await load('/components/optimus_authorizations/editor.html', this.target)

		let component = this

		//SERVER SELECTOR ONCHANGE
		modal.querySelector('.server').onchange = async function ()
		{
			if (this.value == 0)
				step(1)
			else
			{
				await component.getOwners()
				step(2)
			}
		}

		//OWNER SELECTOR ONCHANGE
		modal.querySelector('.owner').onchange = function ()
		{
			if (this.value == 0)
				step(2)
			else
			{
				step(3)
				component.getResourcesTypes()
			}
		}

		//RESSOURCE TYPE SELECTOR ONCHANGE
		modal.querySelector('.resource-type').onchange = function ()
		{
			if (modal.querySelector('.resource-type').value.includes('/id'))
			{
				step(4)
				component.getResourcesList()
			}
			else if (modal.querySelector('.resource-type').value == 0)
				step(3)
			else
			{
				step(5)
				component.getUsers()
			}
		}

		//RESSOURCE SELECTOR ONCHANGE
		modal.querySelector('.resource').onchange = function ()
		{
			if (this.value == 0)
				step(4)
			else
			{
				step(5)
				component.getUsers()
			}
		}

		//USER SELECTOR ONCHANGE
		modal.querySelector('.user').onchange = function ()
		{
			if (this.value == 0)
				step(5)
			else
				step(6)
		}

		//SHOW HIDE BLOCKS ACCORDING TO STEP EVOLUTION OR RECEIVED CONFIGURATION
		function step(step)
		{
			modal.querySelector('.server-block').classList.toggle('is-hidden', step <= 0 || !component.blocks.includes('server'))

			modal.querySelector('.owner-block').classList.toggle('is-hidden', step <= 1 || !component.blocks.includes('owner'))
			if (step <= 1)
				clear(modal.querySelector('.owner'))

			modal.querySelector('.resource-type-block').classList.toggle('is-hidden', step <= 2 || !component.blocks.includes('resource-type'))

			modal.querySelector('.resource-block').classList.toggle('is-hidden', step <= 3 || (step > 3 && !modal.querySelector('.resource-type').value.includes('/id')) || !component.blocks.includes('resource'))
			if (step <= 3 || (step > 3 && !modal.querySelector('.resource-type').value.includes('/id')))
				clear(modal.querySelector('.resource'))

			modal.querySelector('.user-block').classList.toggle('is-hidden', step <= 4 || !component.blocks.includes('user'))
			if (step <= 4)
				modal.querySelector('.user').value = 0

			modal.querySelector('.rights-block').classList.toggle('is-hidden', step <= 5 || !component.blocks.includes('rights'))
			modal.querySelector('.add-button').classList.toggle('is-hidden', step <= 5 || !component.blocks.includes('rights'))
			if (step > 5)
				modal.querySelector('.create-block').classList.toggle('is-hidden', modal.querySelector('.resource-type').value.includes('/id'))
		}

		//ATTACH EVENTS TO BUTTONS
		modal.querySelector('.modal-card-close').onclick = () => this.onCancel ? this.onCancel() : modal.close()
		modal.querySelector('.cancel-button').onclick = () => this.onCancel ? this.onCancel() : modal.close()
		modal.querySelector('.add-button').onclick = () => this.createAuthorization(component.onCreated)

		//POPULATE SERVERS
		modal.querySelector('.server').add(new Option('', 0))
		modal.querySelector('.server').add(new Option(store.user.server, store.user.server))
		for (let server of store.user.secondary_servers)
			modal.querySelector('.server').add(new Option(server, server))

		//RESSOURCE TYPE SELECTOR POPULATE
		modal.querySelector('.resource-type').add(new Option('', 0))

		//INIT
		let currentstep = 1
		if (this.server)
		{
			modal.querySelector('.server').value = this.server
			await component.getOwners()
			currentstep = 2

			if (this.owner)
			{
				modal.querySelector('.owner').value = this.owner
				await component.getResourcesTypes()
				currentstep = 3

				if (this.resourceType)
				{
					if (!modal.querySelector('.resource-type option[value="' + this.resourceType + '"]'))
						return optimusToast("Le type de ressource \"" + this.resourceType + "\" indiqué n'existe pas", 'is-danger')
					modal.querySelector('.resource-type').value = this.resourceType
					if (this.resourceType.includes('/id'))
						await component.getResourcesList()
					currentstep = 4

					if (this.resourceType.includes('/id') && this.resourceId)
					{
						if (!modal.querySelector('.resource option[value="' + this.resourceId + '"]'))
							return optimusToast("La ressource n°" + this.resourceId + " indiquée n'existe pas", 'is-danger')
						modal.querySelector('.resource').value = this.resourceId
						currentstep = 5
					}

					if ((this.resourceType.includes('/id') && this.resourceId) || !this.resourceType.includes('/id'))
						await component.getUsers()

					if (this.user)
					{
						if (!modal.querySelector('.user option[value="' + this.user + '"]'))
							return optimusToast("L'utilisateur n°" + this.user + " indiqué n'existe pas", 'is-danger')
						modal.querySelector('.user').value = this.user
						modal.querySelector('.user').onchange()
						currentstep = 6
					}
				}
			}
		}
		step(currentstep)

		if (this.read)
			modal.querySelector('.read-checkbox').checked = (this.read == 1)

		if (this.write)
			modal.querySelector('.write-checkbox').checked = (this.write == 1)

		if (this.create && this.resourceType.includes('/id'))
			modal.querySelector('.create-checkbox').checked = (this.create == 1)

		if (this.delete)
			modal.querySelector('.delete-checkbox').checked = (this.delete == 1)

		return this
	}

	async getOwners()
	{
		clear(modal.querySelector('.owner'))
		modal.querySelector('.owner').add(new Option('', 0))
		if (store.user.admin == 0)
			modal.querySelector('.owner').add(new Option(store.user.displayname, store.user.id))
		else
		{
			let owners = await rest(modal.querySelector('.server').value + '/optimus-base/users', 'GET', null, 'toast')
			for (let owner of owners.data)
				modal.querySelector('.owner').add(new Option(owner.displayname, owner.id))
		}
	}


	async getUsers()
	{
		clear(modal.querySelector('.user'))
		modal.querySelector('.user').add(new Option('', 0))
		let users = await rest(modal.querySelector('.server').value + '/optimus-base/users', 'GET', null, 'toast')
		let existing_authorizations = await rest(modal.querySelector('.server').value + '/optimus-base/authorizations', 'GET',
			{
				owner: modal.querySelector('.owner').value,
				resource: modal.querySelector('.resource-type').value.includes('/id') ? modal.querySelector('.resource-type').value.replace('id', modal.querySelector('.resource').value) : modal.querySelector('.resource-type').value
			}, 'toast')
		existing_authorizations = existing_authorizations.data.map(authorization => authorization.user)
		for (let user of users.data)
			if (user.id != modal.querySelector('.owner').value && !existing_authorizations.includes(user.id))
				modal.querySelector('.user').add(new Option(user.displayname, user.id))
		if (modal.querySelector('.user').options.length == 1)
			optimusToast("Tous les utilisateurs existants disposent déjà d'un partage sur cette ressource", 'is-danger')
	}


	async getResourcesTypes()
	{
		let owner_services = (this.owner != store.user.id) ? await rest(store.user.server + '/optimus-base/users/' + modal.querySelector('.owner').value + '/services').then(response => response.data.map(service => service.name)) : store.services.map(service => service.name)
		clear(modal.querySelector('.resource-type'))
		modal.querySelector('.resource-type').add(new Option('', 0))
		modal.querySelector('.resource-type').add(new Option('Toutes les ressources', '*'))
		for (let service of store.services)
			if (owner_services.includes(service.name) && service.resources)
				for (let resource of service.resources)
					modal.querySelector('.resource-type').add(new Option(resource.description, resource.path))
	}


	async getResourcesList()
	{
		clear(modal.querySelector('.resource'))
		let resourceTypes = store.resources.find(item => item.path == modal.querySelector('.resource-type').value)
		modal.querySelector('.resource-block > label').innerText = resourceTypes.name[0].toUpperCase() + resourceTypes.name.slice(1) + ' à partager :'
		modal.querySelector('.resource').add(new Option('', 0))
		let resources = await rest(modal.querySelector('.server').value + resourceTypes.endpoint.replace('{owner}', modal.querySelector('.owner').value), 'GET', null, 'toast')
		for (let resource of resources.data)
			modal.querySelector('.resource').add(new Option(resource.displayname, resource.id))
		if (modal.querySelector('.resource').options.length == 1)
			optimusToast("Ce propriétaire ne dispose d'aucune ressource de ce type", 'is-danger')
	}


	async createAuthorization(onCreated)
	{
		let object =
		{
			owner: modal.querySelector('.owner').value,
			user: modal.querySelector('.user').value,
			resource: modal.querySelector('.resource-type').value.replace('/id', '/' + modal.querySelector('.resource').value),
			resourcename: modal.querySelector('.resource-type').value.replace('/id', '/' + modal.querySelector('.resource').value),
			username: modal.querySelector('.user').selectedOptions[0].text,
			read: modal.querySelector('.read-checkbox').checked ? 1 : 0,
			write: modal.querySelector('.write-checkbox').checked ? 1 : 0,
			create: modal.querySelector('.create-checkbox').checked ? 1 : 0,
			delete: modal.querySelector('.delete-checkbox').checked ? 1 : 0
		}

		let new_authorization = await rest(modal.querySelector('.server').value + '/optimus-base/authorizations', 'POST', object, 'toast')
		if (new_authorization.code == 201)
		{
			new_authorization.data.resourcename = modal.querySelector('.resource-type')?.value.replace('/id', '/' + modal.querySelector('.resource')?.value)
			new_authorization.data.username = modal.querySelector('.user')?.selectedOptions[0].text
			if (this.tabulator)
				this.tabulator.setData()
			if (onCreated)
				onCreated()
			else
				modal.close()
		}
	}
}