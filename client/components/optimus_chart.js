export default class OptimusChart
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		return this
	}

	init()
	{
		return import('/libs/chart.umd.js')
			.then(() => 
			{
				Chart.defaults.color = getComputedStyle(document.documentElement).getPropertyValue('--optimus-leftmenu-item-text-color')
				Chart.defaults.borderColor = getComputedStyle(document.documentElement).getPropertyValue('--optimus-border-color')

				let div = document.createElement('div')
				div.classList.add('optimus-charts')
				this.target.appendChild(div)

				let canvas = document.createElement('canvas')
				div.appendChild(canvas)

				this.chart = new Chart(canvas, this.params)
				return this
			})
	}

	onUnload()
	{
		delete this.chart
		return this
	}

	onThemeChange()
	{
		Chart.defaults.color = getComputedStyle(document.documentElement).getPropertyValue('--optimus-leftmenu-item-text-color')
		Chart.defaults.borderColor = getComputedStyle(document.documentElement).getPropertyValue('--optimus-border-color')

		if (this.chart.options.scales.x)
		{
			this.chart.options.scales.x.ticks.color = Chart.defaults.color
			this.chart.options.scales.x.border.color = Chart.defaults.borderColor
			this.chart.options.scales.x.grid.color = Chart.defaults.borderColor
		}
		if (this.chart.options.scales.y)
		{
			this.chart.options.scales.y.ticks.color = Chart.defaults.color
			this.chart.options.scales.y.border.color = Chart.defaults.borderColor
			this.chart.options.scales.y.grid.color = Chart.defaults.borderColor
		}
		this.chart.options.color = Chart.defaults.color
		this.chart.options.plugins.title.color = Chart.defaults.color

		this.chart._doResize()
	}
}