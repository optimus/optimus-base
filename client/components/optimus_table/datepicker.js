
export default class optimusDatePickerTabulator
{
	constructor()
	{
		return this
	}

	open = function (cell, onRendered, success, cancel, editorParams)
	{
		let editor = document.createElement('input')
		editor.classList.add('datepicker-keyboard-input')
		let allowKeyboardInput = editorParams?.optimusDatepicker?.allowKeyboardInput === false ? false : !store.touchscreen
		if (!allowKeyboardInput)
			editor.classList.add('is-hidden')

		if (editorParams?.optimusDatepicker?.type == 'datetime')
			editor.type = 'datetime-local'
		else if (editorParams?.optimusDatepicker?.type == 'time')
			editor.type = 'time'
		else
			editor.type = 'date'
		if (editorParams?.optimusDatepicker?.required)
			editor.required = true
		editor.value = cell.getValue() || luxon.DateTime.now().toFormat(cell.getColumn().getDefinition()?.formatterParams?.inputFormat)
		editor.initialValue = cell.getValue()

		onRendered(function ()
		{
			const dialog = document.createElement('div')
			dialog.id = 'datepicker-dialog'
			dialog.classList.add('modal', 'is-active')
			dialog.style.zIndex = 10002

			const background = document.createElement('div')
			background.classList.add('modal-background')
			background.style.opacity = 0.86
			background.onclick = () => { dialog.remove(); cancel() }
			dialog.appendChild(background)

			const container = document.createElement('div')
			container.classList.add('modal-container')
			container.style.zIndex = 10003
			dialog.appendChild(container)

			document.body.appendChild(dialog)

			let datepicker = document.createElement('input')
			container.appendChild(datepicker)

			bulmaCalendar.attach(datepicker,
				{
					lang: 'fr',
					displayMode: 'inline',
					showHeader: false,
					dateFormat: cell.getColumn().getDefinition()?.formatterParams?.inputFormat,
					color: 'link',
					validateLabel: 'Valider',
					todayLabel: editorParams?.optimusDatepicker?.type?.includes('time') ? 'Maintenant' : 'Aujourd\'hui',
					cancelLabel: 'Annuler',
					clearLabel: 'Effacer',
					minuteSteps: 1,
					weekStart: 1,
					type: editorParams.optimusDatepicker.type,
					startDate: cell.getValue() ? luxon.DateTime.fromFormat(cell.getValue(), cell.getColumn().getDefinition()?.formatterParams?.inputFormat).toFormat('yyyy-MM-dd') : new Date().toLocaleString('lt-LT').slice(0, 10),
					startTime: cell.getValue() ? luxon.DateTime.fromFormat(cell.getValue(), cell.getColumn().getDefinition()?.formatterParams?.inputFormat).toFormat('HH:mm') : new Date().toLocaleString('lt-LT').slice(11, 16),
					showClearButton: editorParams?.optimusDatepicker?.required === false,
				})

			if (editorParams.optimusDatepicker.type == 'date')
				datepicker.bulmaCalendar.on('select', datepicker => successFunc(datepicker.data.value()))
			else
				datepicker.bulmaCalendar.on('select', () => editor.value = datepicker.bulmaCalendar.value().replace(' ', 'T').slice(0, 16))

			container.querySelector('.datetimepicker-footer-validate').onclick = () => successFunc(document.querySelector('.datepicker-keyboard-input').value)
			container.querySelector('.datetimepicker-footer-today').onclick = () => successFunc(luxon.DateTime.now().toFormat(cell.getColumn().getDefinition()?.formatterParams?.inputFormat))
			container.querySelector('.datetimepicker-footer-clear').onclick = () => successFunc()
			container.querySelector('.datetimepicker-footer-cancel').onclick = () => { dialog.remove(); cancel() }

			let newchild = container.appendChild(cell.getElement().firstElementChild)
			if (!allowKeyboardInput)
				newchild.classList.add('is-hidden')

			if (cell.getValue())
				cell.getElement().innerText = luxon.DateTime.fromFormat(cell.getValue(), cell.getColumn().getDefinition()?.formatterParams?.inputFormat || "yyyy-MM-dd").toFormat(cell.getColumn().getDefinition()?.formatterParams?.outputFormat || "dd/MM/yyyy")

			cell.getElement().classList.remove('tabulator-editing')

			editor.onkeydown = event => 
			{
				if (event.key == 'Escape')
					successFunc(editor.initialValue)

				if (event.key == 'Enter' || event.key == 'Tab')
				{
					if (event.target.validity.badInput)
						return setTimeout(() => report('Date invalide'), 10)
					if (editorParams?.optimusDatepicker?.required && !editor.value)
						return setTimeout(() => report('Une date doit impérativement être renseignée'), 10)
					if (event.key == 'Tab')
						if (event.shiftKey)
							setTimeout(() => cell.navigateLeft(), 10)
						else
							setTimeout(() => cell.navigateRight(), 10)
					successFunc(editor.value)
				}
			}
			editor.focus()
		})

		function report(message)
		{
			document.querySelector('.datepicker-keyboard-input').setCustomValidity(message)
			document.querySelector('.datepicker-keyboard-input').reportValidity()
		}

		function successFunc(value)
		{
			document.getElementById('datepicker-dialog').remove()
			success(value?.replace('T', ' '))
		}

		return editor
	}
}