export default class ExportModal
{
	constructor(target, params)
	{
		this.component = params
		this.target = target
		this.config = params.config
		this.tabulator = params.tabulator
	}

	async init()
	{
		await load('/components/optimus_table/manage_filters_modal.html', this.target)

		modal.querySelector('.modal-card-close').onclick = () => 
		{
			this.component.update_saved_filters_list()
			modal.close()
		}

		this.saved_filters = localStorage.getItem('tabulator-' + this.config.id + (this.config.version ? '-' + this.config.version : '') + '-saved-filters')
		this.saved_filters = this.saved_filters ? JSON.parse(this.saved_filters) : new Array

		for (let filter of this.saved_filters)
		{
			let row = modal.querySelector('.saved-filters-row').content.firstElementChild.cloneNode(true)
			row.id = filter.title
			row.config = this.config
			row.querySelector('input').value = filter.title

			row.querySelector('.trash').onclick = () =>
			{
				this.saved_filters = localStorage.getItem('tabulator-' + this.config.id + (this.config.version ? '-' + this.config.version : '') + '-saved-filters')
				this.saved_filters = this.saved_filters ? JSON.parse(this.saved_filters) : new Array
				localStorage.setItem('tabulator-' + this.config.id + (this.config.version ? '-' + this.config.version : '') + '-saved-filters', JSON.stringify(this.saved_filters.filter(filter => filter.title != row.id)))
				row.remove()
				if (this.saved_filters.length == 1)
				{
					this.component.update_saved_filters_list()
					modal.close()
				}
			}

			row.querySelector('input').onfocus = event =>
			{
				event.target.classList.add('is-danger')
			}

			row.querySelector('input').onkeyup = event =>
			{
				event.target.setCustomValidity('')
				event.target.reportValidity()
			}

			row.querySelector('input').onchange = event =>
			{
				event.target.blur()
			}

			row.querySelector('input').onblur = event =>
			{
				this.saved_filters = localStorage.getItem('tabulator-' + this.config.id + (this.config.version ? '-' + this.config.version : '') + '-saved-filters')
				this.saved_filters = this.saved_filters ? JSON.parse(this.saved_filters) : new Array

				if (event.target.value == '')
				{
					event.target.setCustomValidity('Le champ ne peut pas être vide')
					event.target.reportValidity()
					return false
				}

				if (this.saved_filters.filter(filter => filter.title == event.target.value).length > 0 && row.id != event.target.value)
				{
					event.target.setCustomValidity('Une recherche portant ce nom existe déjà')
					event.target.reportValidity()
					return false
				}

				this.saved_filters = this.saved_filters.map(filter => filter.title == event.target.parentNode.id ? { ...filter, title: event.target.value } : filter)
				localStorage.setItem('tabulator-' + this.config.id + (this.config.version ? '-' + this.config.version : '') + '-saved-filters', JSON.stringify(this.saved_filters))

				event.target.classList.remove('is-danger')
				if (row.id != this.value)
				{
					event.target.classList.add('is-success')
					setTimeout(() => event.target.classList.remove('is-success'), 500)
				}
				row.id = event.target.value
			}

			modal.querySelector('.saved-filters-container').appendChild(row)
		}

		import('/libs/sortable.min.js')
			.then(() =>
			{
				Sortable.create(document.querySelector('.saved-filters-container'),
					{
						animation: 200,
						handle: '.grip',
						onEnd: event => 
						{
							this.saved_filters = JSON.parse(localStorage.getItem('tabulator-' + this.config.id + (this.config.version ? '-' + this.config.version : '') + '-saved-filters'))

							let new_filters = new Array
							for (let input of modal.querySelectorAll('input'))
								new_filters.push(this.saved_filters.filter(filter => filter.title == input.value)[0])

							localStorage.setItem('tabulator-' + this.config.id + (this.config.version ? '-' + this.config.version : '') + '-saved-filters', JSON.stringify(new_filters))
						}
					})
			})
	}
}

