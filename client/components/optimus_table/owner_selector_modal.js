export default class OwnerSelectorModal
{
	constructor(target, params)
	{
		this.target = target
		this.tabulator = params.tabulator
		this.config = params.config
		this.parent = params
		return this
	}

	async init()
	{
		await load('/components/optimus_table/owner_selector_modal.html', this.target)

		modal.querySelector('.modal-card-close').onclick = () => modal.close()

		return rest(store.user.server + '/optimus-base/authorizations', 'GET',
			{
				resource: this.config.resource,
				user: store.user.id,
			})
			.then(authorizations => authorizations.data)
			.then(authorizations => 
			{
				//AJOUTE L'UTILISATEUR CONNECTE
				if (!this.config.owner_type || this.config.owner_type == store.user.type)
					authorizations.push({ owner_displayname: store.user.displayname, owner: store.user.id, resource: '*' })

				//EXTRAIT LES OWNERS QUI PARTAGENT LA RESSOURCE CONCERNEE OU *
				authorizations = authorizations.filter(authorization => (authorization.resource == '*' || authorization.resource.startsWith(this.config.resource)))

				//SUPPRIME LE OWNER ACTUELLEMENT SELECTIONNE
				authorizations = authorizations.filter(authorization => authorization.owner != (store.queryParams.owner || store.user.id))

				//SUPPRIME LES DOUBLONS
				authorizations = authorizations.filter((v, i, a) => a.findIndex(v2 => (v2.owner === v.owner)) === i)

				//TRI PAR ORDRE ALPHABETIQUE
				authorizations.sort((a, b) => a.owner_displayname.localeCompare(b.owner_displayname))

				for (let authorization of authorizations)
				{
					let row = modal.querySelector('.optimus_owner_row').content.firstElementChild.cloneNode(true)
					row.owner = authorization.owner
					row.owner_displayname = authorization.owner_displayname
					row.tabulator = this.tabulator
					row.config = this.config
					row.parent = this.parent
					row.querySelector('span').innerText = authorization.owner_displayname
					row.onclick = function ()
					{
						const url = new URL(window.location)
						url.searchParams.set('owner', this.owner)
						window.history.pushState({ previous: window.location.href }, '', url)
						this.tabulator.setData(this.config.url.replace('owner', this.owner))
						store.queryParams.owner = this.owner
						this.parent.getAuthorizations()
						modal.close()
					}

					modal.querySelector('.modal-card-body').appendChild(row)
				}
			})
	}
}








