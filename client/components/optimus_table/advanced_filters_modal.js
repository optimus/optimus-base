export default class AdvancedSearchModal
{
	constructor(target, params) 
	{
		this.component = params
		this.tabulator = params.tabulator
		this.config = params.config
		this.target = target
		this.params = params
		return this
	}

	async init()
	{
		await load('/components/optimus_table/advanced_filters_modal.html', this.target)

		//USER CLICKS ON APPLY
		modal.querySelector('.apply_button').onclick = () => 
		{
			for (let row of modal.querySelectorAll('.modal-card-body:first-of-type > div'))
				if (row.querySelector('.filter_value').value == '')
				{
					row.querySelector('.filter_value').setCustomValidity('La valeur recherchée ne peut pas être vide')
					row.querySelector('.filter_value').reportValidity()
					return true
				}
			this.update_adv_filters()
			modal.close()
		}


		//USER CLICKS ON REMOVE ALL FILTERS
		modal.querySelector('.delete_filters_button').onclick = () => 
		{
			this.tabulator.clearFilter()
			this.component.update_filters_count()
			modal.close()
		}


		//USER CLICKS ON SAVE FILTERS
		modal.querySelector('.save_filters_button').onclick = () => 
		{
			this.saved_filters = localStorage.getItem('tabulator-' + this.config.id + (this.config.version ? '-' + this.config.version : '') + '-saved-filters')
			this.saved_filters = this.saved_filters ? JSON.parse(this.saved_filters) : new Array()
			this.new_filters =
			{
				title: '',
				filters: this.update_adv_filters()
			}
			for (let filters of this.saved_filters)
				if (JSON.stringify(filters.filters) == JSON.stringify(this.new_filters.filters))
					return optimusToast('Des filtres identiques ont déjà été sauvegardés sous le nom ' + filters.title, 'is-warning')

			modal.close()
			modal.open('/components/optimus_table/save_filters_modal.js', false, this)
		}


		//CLOSE MODAL
		modal.querySelectorAll('.modal-card-close').forEach(element => element.onclick = () => modal.close())


		//ADD SINGLE FILTER
		modal.querySelector('.add_filter_button').onclick = () => 
		{
			for (let row of modal.querySelectorAll('.modal-card-body:first-of-type > div'))
				if (row.querySelector('.filter_value').value == '')
				{
					row.querySelector('.filter_value').setCustomValidity('La valeur recherchée ne peut pas être vide')
					row.querySelector('.filter_value').reportValidity()
					return true
				}
			this.add_adv_filter()
		}

		//INIT
		let filters = this.tabulator.getFilters()

		for (let filter of filters)
			if (Array.isArray(filter))
			{
				for (let or_filter of filter)
					if (or_filter.field != '*')
						this.add_adv_filter(or_filter.field, or_filter.type, or_filter.value, 'or')
			}
			else if (filter.field != '*')
				this.add_adv_filter(filter.field, filter.type, filter.value, 'and')

		if (!modal.querySelector('.columns'))
			this.add_adv_filter()

		if (!store.touchscreen)
			setTimeout(() => modal.querySelector('.columns:last-child .filter_value').focus(), 50)

	}

	//ADD ONE FILTER ROW
	add_adv_filter = (field, type = '=', value = null, operator = 'and') =>
	{
		//POPULATE FIELD SELECT
		let row = modal.querySelector('.filter_row').content.firstElementChild.cloneNode(true)
		let columns = this.tabulator.getColumns()
		for (let column of columns)
			row.querySelector('.filter_field').add(new Option(column.getDefinition().title, column.getDefinition().field))

		//USER CHANGES FIELD
		row.querySelector('.filter_field').onchange = event =>
		{
			if (columns.filter(column => column.getDefinition().field == event.currentTarget.value)[0].getDefinition()?.sorterParams?.type == 'date')
				event.currentTarget.parentNode.parentNode.parentNode.querySelector('.filter_value').type = 'date'
			else if (columns.filter(column => column.getDefinition().field == event.currentTarget.value)[0].getDefinition()?.sorterParams?.type == 'integer')
			{
				event.currentTarget.parentNode.parentNode.parentNode.querySelector('.filter_value').type = 'number'
				event.currentTarget.parentNode.parentNode.parentNode.querySelector('.filter_value').step = 1
				event.currentTarget.parentNode.parentNode.parentNode.querySelector('.filter_value').pattern = '[0-9]'
			}
			else
			{
				event.currentTarget.parentNode.parentNode.parentNode.querySelector('.filter_value').type = 'text'
				event.currentTarget.parentNode.parentNode.parentNode.querySelector('.filter_value').removeAttribute('pattern')
			}
		}

		//POPULATE VALUES
		if (field)
			row.querySelector('.filter_field').value = field
		else if ([...row.querySelector('.filter_field').options].map(option => option.value).includes("displayname"))
			row.querySelector('.filter_field').value = 'displayname'
		else
			row.querySelector('.filter_field').value = row.querySelector('.filter_field').options[0].value
		row.querySelector('.filter_field').dispatchEvent(new Event('change'))

		row.querySelector('.filter_type').value = type
		row.querySelector('.filter_value').value = value
		row.querySelector('.filter_operator').value = operator

		row.querySelector('.filter_value').addEventListener("keypress", event => 
		{

			//USER PRESSES SHIFT + ENTER
			if (event.shiftKey && event.key === "Enter")
				modal.querySelector('.add_filter_button').click()
			//USER PRESSES ENTER
			else if (event.key === "Enter")
				modal.querySelector('.apply_button').click()
		})

		//USER CHANGES FIELD VALUE
		row.querySelector('.filter_field').onchange = event => 
		{
			if (columns.filter(column => column.getDefinition().field == event.currentTarget.value)[0].getDefinition()?.sorterParams?.type == 'date')
				event.currentTarget.parentNode.parentNode.parentNode.querySelector('.filter_value').type = 'date'
			else if (columns.filter(column => column.getDefinition().field == event.currentTarget.value)[0].getDefinition()?.sorterParams?.type == 'integer')
			{
				event.currentTarget.parentNode.parentNode.parentNode.querySelector('.filter_value').type = 'number'
				event.currentTarget.parentNode.parentNode.parentNode.querySelector('.filter_value').step = 1
				event.currentTarget.parentNode.parentNode.parentNode.querySelector('.filter_value').pattern = '[0-9]'
			}
			else
			{
				event.currentTarget.parentNode.parentNode.parentNode.querySelector('.filter_value').type = 'text'
				event.currentTarget.parentNode.parentNode.parentNode.querySelector('.filter_value').removeAttribute('pattern')
			}
		}

		//USER CLICKS ON THE TRASH BIN
		row.querySelector('.filter_delete').addEventListener("click", event => this.remove_adv_filter(event))

		//ADD ROW TO DOM
		modal.querySelector('.modal-card-body').appendChild(row)

		//CHANGE FIRST OPERATOR
		if (modal.querySelectorAll('.filter_operator').length == 2)
			modal.querySelector('.columns:nth-child(2) .filter_operator').value = modal.querySelector('.columns:first-child .filter_operator').value

		//FOCUS ON LAST INPUT
		if (!store.touchscreen)
			modal.querySelector('.columns:last-child .filter_value').focus()

		//HIDE FIRST OPERATOR
		modal.querySelector('.columns:first-child .filter_operator').parentNode.classList.add('is-hidden')

		//HIDE FIRST TRASH BIN IF ONLY ONE FILTER PRESENT
		modal.querySelector('.columns:first-child .filter_delete').parentNode.classList.toggle('is-hidden', modal.querySelectorAll('.filter_delete').length == 1)
	}


	//REMOVE ONE SPECIFIC FILTER
	remove_adv_filter = function (event)
	{
		if (modal.querySelectorAll('.filter_value').length > 1)
			event.currentTarget.parentElement.parentElement.remove()
		else
			event.currentTarget.parentElement.parentElement.querySelector('.filter_value').value = ''
		if (!store.touchscreen)
			modal.querySelector('.columns:last-child .filter_value').focus()
		modal.querySelector('.columns:first-child .filter_operator').parentNode.classList.add('is-hidden')
		//HIDE FIRST TRASH BIN IF ONLY ONE FILTER PRESENT
		modal.querySelector('.columns:first-child .filter_delete').parentNode.classList.toggle('is-hidden', modal.querySelectorAll('.filter_delete').length == 1)
	}


	//UPDATE FILTERS
	update_adv_filters = function ()
	{
		if (modal.querySelectorAll('.filter_operator').length > 1)
			modal.querySelector('.columns:first-child .filter_operator').value = modal.querySelector('.columns:nth-child(2) .filter_operator').value

		let and_filters = new Array()
		for (let row of modal.querySelectorAll('.modal-card-body:first-of-type > div'))
			if (row.querySelector('.filter_type').value != '' && row.querySelector('.filter_value').value != '')
				if (row.querySelector('.filter_operator').value == 'and')
					and_filters.push({
						field: row.querySelector('.filter_field').value,
						type: row.querySelector('.filter_type').value,
						value: row.querySelector('.filter_value').value
					})

		let or_filters = new Array()
		for (let row of modal.querySelectorAll('.modal-card-body:first-of-type > div'))
			if (row.querySelector('.filter_type').value != '' && row.querySelector('.filter_value').value != '')
				if (row.querySelector('.filter_operator').value == 'or')
					or_filters.push({
						field: row.querySelector('.filter_field').value,
						type: row.querySelector('.filter_type').value,
						value: row.querySelector('.filter_value').value
					})

		let filters = and_filters
		if (or_filters.length > 0)
			filters.push([...or_filters])

		if (filters.length > 0)
			this.tabulator.setFilter(filters)
		else
			this.tabulator.clearFilter()

		this.component.update_filters_count()

		return filters
	}
}