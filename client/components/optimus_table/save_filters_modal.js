export default class SaveFiltersModal
{
	constructor(target, params)
	{
		this.component = params.component || params
		this.target = target
		this.tabulator = params.tabulator
		this.config = params.config
		this.saved_filters = params.saved_filters
		this.new_filters = params.new_filters
		return this
	}

	async init()
	{
		await load('/components/optimus_table/save_filters_modal.html', this.target)

		setTimeout(() => modal.querySelector('input').focus(), 50)

		modal.querySelector('input').onkeydown = event => 
		{
			event.target.setCustomValidity('')
			event.target.reportValidity()
			if (event.key === "Enter")
				modal.querySelector('.save-button').click()
		}

		modal.querySelector('.save-button').onclick = () =>
		{
			if (modal.querySelector('input').value == '')
			{
				modal.querySelector('input').setCustomValidity('Merci de renseigner un nom')
				modal.querySelector('input').reportValidity()
				return false
			}

			if (this.saved_filters.filter(filter => filter.title == modal.querySelector('input').value).length > 0)
			{
				modal.querySelector('input').setCustomValidity('Une recherche portant ce nom existe déjà')
				modal.querySelector('input').reportValidity()
				return false
			}

			this.new_filters.title = modal.querySelector('input').value

			this.saved_filters.push(this.new_filters)

			localStorage.setItem('tabulator-' + this.config.id + (this.config.version ? '-' + this.config.version : '') + '-saved-filters', JSON.stringify(this.saved_filters))

			this.component.update_filters_count()
			this.component.update_saved_filters_list()
			modal.close()
			return true
		}
	}
}


