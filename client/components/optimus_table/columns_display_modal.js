export default class ColumnsDisplayModal
{
	constructor(target, params)
	{
		this.target = target
		this.tabulator = params.tabulator
		this.config = params.config
	}

	async init()
	{
		await load('/components/optimus_table/columns_display_modal.html', this.target)

		modal.querySelector('.modal-card-close').onclick = () => 
		{
			if (store.innerWidth < 768)
				router(store.route)
			modal.close()
		}

		for (let column of this.tabulator.getColumns())
		{
			let row = modal.querySelector('#optimus_table_column_row').content.firstElementChild.cloneNode(true)
			row.id = column.getField()

			if (column.isVisible())
				row.querySelector('input').checked = true

			row.querySelector('.switch').onclick = () => 
			{
				column.toggle()
			}

			row.querySelector('label').onclick = () => 
			{
				column.toggle()
				row.querySelector('input').checked = column.isVisible()
			}

			row.querySelector('label').innerHTML = column.getDefinition().title

			modal.querySelector('.columns-container').appendChild(row)
		}

		import('/libs/sortable.min.js')
			.then(() =>
			{
				Sortable.create(document.querySelector('.columns-container'),
					{
						animation: 200,
						handle: '.fa-grip',
						onEnd: event => 
						{
							if (document.getElementById(event.item.id).previousSibling?.id)
								this.tabulator.moveColumn(event.item.id, document.getElementById(event.item.id).previousSibling.id, true)
							else
								this.tabulator.moveColumn(event.item.id, document.getElementById(event.item.id).nextSibling.id)
						}
					})
			})
	}
}

