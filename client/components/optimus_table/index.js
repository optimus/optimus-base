load_CSS('/libs/tabulator/css/tabulator_bulma.min.css')
import { Tabulator, ClipboardModule, ColumnCalcsModule, ExportModule, DownloadModule, FrozenColumnsModule, HtmlTableImportModule, FormatModule, InteractionModule, MoveColumnsModule, MutatorModule, PageModule, PersistenceModule, PrintModule, ResizeColumnsModule, SortModule, FilterModule, AjaxModule } from '/libs/tabulator/js/tabulator_esm.min.js'
Tabulator.registerModule([ClipboardModule, ColumnCalcsModule, ExportModule, DownloadModule, FrozenColumnsModule, HtmlTableImportModule, FormatModule, InteractionModule, MoveColumnsModule, MutatorModule, PageModule, PersistenceModule, PrintModule, ResizeColumnsModule, SortModule, FilterModule, AjaxModule])

export default class optimusTable
{
	constructor(target, params)
	{
		this.target = target
		this.config = params
		return this
	}

	async init()
	{
		await load('/components/optimus_table/index.html', this.target).then(() => 
		{
			this.target.querySelector('.optimus-table').classList.toggle('is-striped', this.config.striped)
			this.target.querySelector('.optimus-table').classList.toggle('is-bordered', this.config.bordered)
			this.target.querySelector('.optimus-table').classList.toggle('is-bordered-vertical', this.config.columnSeparator)
		})

		let config = this.config

		//ADAPTATION SMARTPHONE
		if (store.innerWidth < 768)
		{

			this.config.mode = 'smartphone'

			this.config.tabulator.headerVisible = false
			if (localStorage.getItem('tabulator-' + this.config.containerId + '-smartphone-columns'))
				if (JSON.parse(localStorage.getItem('tabulator-' + this.config.containerId + '-smartphone-columns')).filter(column => column.visible == true).length > 1)
					this.config.tabulator.headerVisible = true

			if (this.config.tabulator.progressiveLoad == 'auto')
				this.config.tabulator.progressiveLoad = 'scroll'

			for (let i = 0; i < this.config.tabulator.columns.length; i++)
				if (this.config.tabulator.columns[i].field == 'displayname')
				{
					this.config.tabulator.columns[i].visible = true
					this.config.tabulator.columns[i].width = store.innerWidth - 10
				}
				else
					this.config.tabulator.columns[i].visible = false
		}
		else
		{
			this.config.mode = 'view'
			if (this.config.tabulator.progressiveLoad == 'auto')
				this.config.tabulator.progressiveLoad = false
			this.config.tabulator.columns.map(column => column.visible = column.visible === false ? false : true)
		}

		//CALCUL DE LA TAILLE INITIALE DU TABLEAU
		this.target.querySelector('.optimus-table-container').style.height = main.getBoundingClientRect().bottom - this.target.querySelector('.optimus-table-container').getBoundingClientRect().top - parseInt(window.getComputedStyle(main).getPropertyValue('padding-bottom')) + 'px'
		this.config.initialWidth = store.innerWidth

		//CONFIGURATION DU BOUTON DE SELECTION DES PROPRIETAIRES
		await this.getAuthorizations()

		//CHARGEMENT DE TABULATOR
		this.tabulator = new Tabulator(this.target.querySelector('.optimus-table-container'), {
			...this.config.tabulator,
			ajaxURL: this.config.url ? this.config.url.replace('owner', (store.queryParams.owner || store.user.id)) : null,
			ajaxRequestFunc: (url, ajaxConfig, params) => new Promise(function (resolve, reject)
			{
				if (!store.user.server)
					modal.open('/services/optimus-base/login/index.js')
				else
					rest(url, ajaxConfig.method, params, 'toast')
						.then(response =>
						{
							if (response.code >= 400)
								reject(response.message)
							else
								resolve(response)
						})
			}),
			ajaxConfig: {
				method: 'GET',
				credentials: 'include'
			},
			dataLoader: false,
			progressiveLoadScrollMargin: 1,
			pagination: this.config.tabulator.progressiveLoad ? false : true,
			paginationSizeSelector: this.config.tabulator.progressiveLoad ? false : [25, 50, 100, 500, true],
			paginationCounter: "rows",
			ajaxContentType: 'json',
			movableColumns: true,
			layout: 'fitDataFill',
			layoutColumnsOnNewData: false,
			sortOrderReverse: true,
			headerSortElement: "<i class='fas fa-arrow-up'></i>",
			persistence: true,
			persistenceMode: 'local',
			persistenceID: this.config.id + (this.config.version ? '-' + this.config.version : ''),
			persistence: {
				columns: true,
				sort: true,
				filter: true,
				page: this.config.tabulator.progressiveLoad ? false : { size: false, page: true },
			},
			persistenceWriterFunc: function (id, type, data)
			{
				id = id + '-' + this.table.mode + '-' + type
				localStorage.setItem(id, JSON.stringify(data))
			},
			persistenceReaderFunc: function (id, type)
			{
				id = id + '-' + this.table.mode + '-' + type
				var data = localStorage.getItem(id)
				return data ? JSON.parse(data) : false
			},
			clipboard: 'copy',
			clipboardCopyStyled: false,
			sortMode: 'remote',
			filterMode: "remote",
			paginationMode: 'remote',
			locale: 'fr',
			placeholder: 'Aucun résultat',
			langs: {
				'fr': {
					pagination: {
						page_size: 'Résultats par page',
						page_title: 'Aller à',
						first: '<i class="fas fa-backward-step" style="padding-top:3px;padding-bottom:2px"></i>',
						first_title: 'Première page',
						last: '<i class="fas fa-forward-step" style="padding-top:3px;padding-bottom:2px"></i>',
						last_title: 'Dernière page',
						prev: '<i class="fas fa-lg fa-caret-left" style="padding-top:10px;padding-bottom:9px;margin-top:1px"></i>',
						prev_title: 'Page précédente',
						next: '<i class="fas fa-lg fa-caret-right" style="padding-top:10px;padding-bottom:9px;margin-top:1px"></i>',
						next_title: 'Page suivante',
						all: 'Tous',
						counter: {
							showing: '',
							of: 'sur',
							rows: '',
							pages: 'pages'
						}
					},
					ajax: {
						loading: "Chargement",
						error: "Erreur de chargement",
					},
				}
			},
		})

		this.tabulator.mode = this.config.mode
		this.tabulator.optimusTable = this

		//TITRE
		this.target.querySelector('.title').innerText = this.config.title

		//BOUTON AJOUTER
		if (this.config.showAddButton === false)
			this.target.querySelectorAll('.add-button').forEach(button => button.classList.add('is-hidden'))
		if (this.config.addIcon)
			this.target.querySelector('.add-button-icon').classList.replace('fa-plus', this.config.addIcon)

		//MOUSE ENTER SUR UNE LIGNE
		if (this.config.rowMouseOver)
			this.tabulator.on("rowMouseEnter", (event, row) => this.config.rowMouseOver(event, row))
		else
			this.tabulator.on("rowMouseEnter", (event, row) => 
			{
				if (event.target.parentNode.classList.contains('tabulator-calcs'))
					return false
				if (this.config.rowClick)
					row.getElement().classList.add('is-clickable')
				row.getElement().classList.add('has-text-link')
			})

		//MOUSE LEAVE SUR UNE LIGNE
		if (this.config.rowMouseLeave)
			this.tabulator.on("rowMouseLeave", (event, row) => this.config.rowMouseLeave(event, row))
		else
			this.tabulator.on("rowMouseLeave", (event, row) => !event.target.parentNode.classList.contains('tabulator-calcs') && row.getElement().classList?.remove('has-text-link'))

		//CLICK SUR UNE LIGNE
		if (this.config.rowClick)
			this.tabulator.on("rowClick", (event, row) => !event.target.parentNode.classList.contains('tabulator-calcs') && this.config.rowClick(event, row))

		//CLIC SUR AJOUTER
		this.target.querySelectorAll('.add-button').forEach(el => el.addEventListener('click', () => this.config.addClick()))

		//IMPRIMER
		this.target.querySelectorAll('.print-button').forEach(button => button.onmousedown = () => modal.open('/components/optimus_table/print_modal.js', false, this))

		//EXPORTER
		this.target.querySelectorAll('.export-button').forEach(button => button.onmousedown = () => modal.open('/components/optimus_table/export_modal.js', false, this))

		//AFFICHAGE
		this.target.querySelectorAll('.columns-display-button').forEach(button => button.onmousedown = () => modal.open('/components/optimus_table/columns_display_modal.js', false, this))

		//RECHERCHE AVANCEE
		this.target.querySelectorAll('.advanced-filters-button').forEach(button => button.onmousedown = () => modal.open('/components/optimus_table/advanced_filters_modal.js', false, this))
		this.target.querySelectorAll('.advanced-filters-mobile-button').forEach(button => button.onmousedown = () => modal.open('/components/optimus_table/advanced_filters_modal.js', false, this))

		//CREER UNE NOUVELLE RECHERCHE AVANCEE
		this.target.querySelectorAll('.new-advanced-filters-button').forEach(button => button.onmousedown = () => 
		{
			this.tabulator.clearFilter()
			this.update_filters_count()
			modal.open('/components/optimus_table/advanced_filters_modal.js', false, this)
		})

		//MODIFIER LA RECHERCHE EN COURS
		this.target.querySelectorAll('.change-advanced-filters-button').forEach(button => button.onmousedown = () => modal.open('/components/optimus_table/advanced_filters_modal.js', false, this))

		//SAUVEGARDER LA RECHERCHE EN COURS
		this.target.querySelectorAll('.save-advanced-filters-button').forEach(button => button.onmousedown = () => 
		{
			this.saved_filters = localStorage.getItem('tabulator-' + this.config.id + (this.config.version ? '-' + this.config.version : '') + '-saved-filters')
			this.saved_filters = this.saved_filters ? JSON.parse(this.saved_filters) : new Array()
			this.new_filters =
			{
				title: '',
				filters: this.tabulator.getFilters()
			}
			for (let filters of this.saved_filters)
				if (JSON.stringify(filters.filters) == JSON.stringify(this.new_filters.filters))
					return optimusToast('Une recherche identique a déjà été sauvegardée sous le nom ' + filters.title, 'is-warning')
			modal.open('/components/optimus_table/save_filters_modal.js', false, this)
		})

		//SUPPRIMER TOUS LES FILTRES
		this.target.querySelectorAll('.delete-all-filters-button').forEach(button => button.onmousedown = () => 
		{
			this.tabulator.clearFilter()
			this.update_filters_count()
		})

		//GERER LES FILTRES SAUVEGARDES
		this.target.querySelectorAll('.manage-filters-button').forEach(button => button.onmousedown = () => modal.open('/components/optimus_table/manage_filters_modal.js', false, this))

		//CHANGEMENT DE BASE DE DONNEES
		this.target.querySelector('.owner-button').onmousedown = () => modal.open('/components/optimus_table/owner_selector_modal.js', false, this)
		this.target.querySelector('.owner-button-mobile').onmousedown = () => modal.open('/components/optimus_table/owner_selector_modal.js', false, this)

		//RECHERCHE GLOBALE
		this.target.querySelector('.quick-filter-input').onsearch = () => 
		{
			clearTimeout(this.delayTimer)
			this.delayTimer = setTimeout(() =>
			{
				if (this.target.querySelector('.quick-filter-input').value != '')
				{
					this.tabulator.setFilter('*', 'like', this.target.querySelector('.quick-filter-input').value)
					this.target.querySelectorAll('.advanced-filters-button').forEach(button => button.classList.remove('is-warning', 'is-light'))
					this.target.querySelectorAll('.advanced-filters-button > span:nth-child(2)').forEach(button => button.innerText = 'Filtres avancés')
				}
				else
					this.tabulator.removeFilter(this.tabulator.getFilters().filter(filter => filter.field == '*'))
			}, 300)
		}
		this.target.querySelector('.quick-filter-input').onkeyup = () => this.target.querySelector('.quick-filter-input').onsearch()

		//GESTION DES ERREURS DE CHARGEMENT
		this.tabulator.on("dataLoadError", () => this.tabulator.clearData())

		//ADAPTATION DE LA HAUTEUR DU TABLEAU
		this.tabulator.refreshHeight = () =>
		{
			if (store.innerWidth > 768)
				this.target.querySelector('.tabulator').style.height = this.target.querySelector('.tabulator-header')?.offsetHeight + (this.tabulator.getRows().length * 32) + (this.target.querySelector('.tabulator-footer')?.offsetHeight || 1) + (config.bordered ? 1 : -1) + (this.target.querySelector('.tabulator-tableholder').scrollWidth > this.target.querySelector('.tabulator-tableholder').offsetWidth ? 18 : 0) + 'px'
			this.tabulator.redraw()
		}

		//FIN DE L'INITIALISATION
		return new Promise(resolve =>
		{
			this.tabulator.on('tableBuilt', function ()
			{
				let filters = this.getFilters()
				this.optimusTable.update_filters_count()
				this.optimusTable.update_saved_filters_list()
				this.refreshHeight()

				this.on('dataProcessed', () => this.refreshHeight())

				if (filters.filter(filter => filter.field == '*')[0]?.value)
					this.optimusTable.target.querySelector('.quick-filter-input').value = filters.filter(filter => filter.field == '*')[0]?.value

				if (store.innerWidth < 768)
					this.getColumn('displayname').setWidth(store.innerWidth - 10)

				if (!store.touchscreen)
					this.optimusTable.target.querySelector('.quick-filter-input').focus()

				this.optimusTable.target.querySelector('.optimus-table').style.opacity = 1
				resolve(this)
			})
		})
	}

	async getAuthorizations()
	{
		await rest(store.user.server + '/optimus-base/authorizations', 'GET',
			{
				filter:
					[
						{ field: 'user', type: '=', value: store.user.id },
						[
							{ field: 'resource', type: '=', value: '*' },
							{ field: 'resource', type: 'starts', value: this.config.resource },
						]
					]
			})
			.then(authorizations =>
			{
				console.log(authorizations)
				if (this.config.showAddButton != false)
					if (store.queryParams.owner && store.queryParams.owner != store.user.id)
					{
						let allowedToCreate = authorizations.data.filter(item => (item.resource == this.config.resource || item.resource == '*') && item.owner == store.queryParams.owner && item.create == 1)
						this.target.querySelectorAll('.add-button').forEach(button => button.classList.toggle('is-hidden', allowedToCreate.length == 0))
					}
					else
						this.target.querySelectorAll('.add-button').forEach(button => button.classList.remove('is-hidden'))

				let usersWhoSharesWithMe = authorizations.data.filter(item => item.read == 1)
				if (!this.config.owner_type || this.config.owner_type == store.user.type)
					usersWhoSharesWithMe.push({ owner_displayname: store.user.displayname, owner: store.user.id, resource: '*' })
				this.target.querySelectorAll('.owner-button').forEach(button => button.classList.toggle('is-hidden', usersWhoSharesWithMe.length < 2))
				this.target.querySelector('.owner-button > span:last-child').innerText = authorizations.data.filter(item => item.owner === store.queryParams.owner)[0]?.owner_displayname || store.user.displayname
			})
	}

	update_filters_count()
	{
		let filters = this.tabulator.getFilters()
		let nb_filter = 0
		for (let filter of filters)
			if (Array.isArray(filter))
			{
				for (let or_filter of filter)
					if (or_filter.field != '*')
						nb_filter++
			}
			else if (filter.field != '*')
				nb_filter++

		if (nb_filter > 0)
		{
			this.target.querySelector('.quick-filter-input').value = null
			this.target.querySelectorAll('.advanced-filters-button').forEach(button => button.classList.add('is-warning'))
			this.target.querySelector('.mobile-menu button .icon').classList.add('has-text-warning')
			this.target.querySelectorAll('.advanced-filters-button > span:nth-of-type(2)').forEach(button => button.innerText = nb_filter + ' filtre(s) actif(s)')
			this.target.querySelector('.advanced-filters-mobile-button > span:nth-of-type(2)').classList.add('has-text-warning')
			this.target.querySelectorAll('.change-advanced-filters-button').forEach(link => link.classList.remove('is-hidden'))
			this.target.querySelectorAll('.save-advanced-filters-button').forEach(link => link.classList.remove('is-hidden'))
			this.target.querySelectorAll('.delete-all-filters-button').forEach(link => link.classList.remove('is-hidden'))
		}
		else
		{
			this.target.querySelector('.quick-filter-input').value = null
			this.target.querySelectorAll('.advanced-filters-button').forEach(button => button.classList.remove('is-warning'))
			this.target.querySelector('.mobile-menu button .icon').classList.remove('has-text-warning')
			this.target.querySelectorAll('.advanced-filters-button > span:nth-of-type(2)').forEach(button => button.innerText = 'Filtres avancés')
			this.target.querySelector('.advanced-filters-mobile-button > span:nth-of-type(2)').classList.remove('has-text-warning')
			this.target.querySelectorAll('.change-advanced-filters-button').forEach(link => link.classList.add('is-hidden'))
			this.target.querySelectorAll('.save-advanced-filters-button').forEach(link => link.classList.add('is-hidden'))
			this.target.querySelectorAll('.delete-all-filters-button').forEach(link => link.classList.add('is-hidden'))
		}
	}

	update_saved_filters_list()
	{
		let saved_filters = localStorage.getItem('tabulator-' + this.config.id + (this.config.version ? '-' + this.config.version : '') + '-saved-filters')

		if (!saved_filters)
			return false

		saved_filters = JSON.parse(saved_filters)

		this.target.querySelectorAll('.saved-filters-header').forEach(element => element.classList.toggle('is-hidden', saved_filters.length == 0))

		this.target.querySelectorAll('.saved-filters').forEach(element => clear(element))

		for (let filters of saved_filters)
			this.target.querySelectorAll('.saved-filters').forEach(element => 
			{
				let a = document.createElement('a')
				a.classList.add('dropdown-item')
				a.innerText = filters.title
				a.onmousedown = () => 
				{
					this.tabulator.setFilter(filters.filters)
					this.update_filters_count()
				}
				element.appendChild(a)
			})
	}

	set_columns()
	{
		let SavedColumnLayout = JSON.parse(localStorage.getItem('tabulator-' + this.config.id + (this.config.version ? '-' + this.config.version : '') + '-' + this.tabulator.mode + '-columns'))
		let newColumnLayout = []
		for (let i = 0; i < SavedColumnLayout.length; i++)
			newColumnLayout[i] = { ...this.config.tabulator.columns.find(column => column.field == SavedColumnLayout[i].field), ...SavedColumnLayout[i] }
		this.tabulator.setColumns(newColumnLayout)
	}

	onUnload()
	{
		this.tabulator.destroy()
		delete this.tabulator
		return this
	}

	onWindowResize()
	{
		if ((this.config.initialWidth < 768 && store.innerWidth >= 768) || (this.config.initialWidth >= 768 && store.innerWidth < 768))
			router(store.route + (store.hashParams ? '#' + store.hashParams.join('_') : null))
		else if (store.innerWidth < 768 && JSON.parse(localStorage.getItem('tabulator-' + this.config.id + (this.config.version ? '-' + this.config.version : '') + '-smartphone-columns')).filter(column => column.visible == true).length == 1)
			this.tabulator.getColumn('displayname').setWidth(store.innerWidth - 10)
	}
}

