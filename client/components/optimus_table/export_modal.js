export default class ExportModal
{
	constructor(target, params)
	{
		this.optimusTable = params
		this.target = target
		this.tabulator = params.tabulator
	}

	async init()
	{
		await load('/components/optimus_table/export_modal.html', this.target)

		if (!localStorage.getItem('tabulator-' + this.optimusTable.config.id + (this.optimusTable.config.version ? '-' + this.optimusTable.config.version : '') + '-export-columns'))
			localStorage.setItem('tabulator-' + this.optimusTable.config.id + (this.optimusTable.config.version ? '-' + this.optimusTable.config.version : '') + '-export-columns', localStorage.getItem('tabulator-' + this.optimusTable.config.id + (this.optimusTable.config.version ? '-' + this.optimusTable.config.version : '') + '-' + this.tabulator.mode + '-columns'))

		modal.querySelector('.modal-card-close').onclick = () => modal.close()

		modal.querySelector('.title-input').value = this.optimusTable.config.resource.toUpperCase()

		modal.querySelector('.data-end').value = this.tabulator.modules.page.remoteRowCountEstimate
		modal.querySelector('.data-end').max = this.tabulator.modules.page.remoteRowCountEstimate

		modal.querySelector('.type-select').onchange = () =>
		{
			modal.querySelector('.title-block').classList.add('is-hidden')
			if (modal.querySelector('.type-select').value == 'pdf' || modal.querySelector('.type-select').value == 'xlsx')
				modal.querySelector('.title-block').classList.remove('is-hidden')
		}

		modal.querySelector('.data-select').onchange = () =>
		{
			modal.querySelector('.data-selector').classList.add('is-hidden')
			if (modal.querySelector('.data-select').value == 'selected')
				modal.querySelector('.data-selector').classList.remove('is-hidden')
		}

		modal.querySelector('.columns-select').onchange = () =>
		{
			modal.querySelector('.columns-container').classList.add('is-hidden')
			if (modal.querySelector('.columns-select').value == 'selected')
			{
				modal.querySelector('.columns-container').classList.remove('is-hidden')
				this.tabulator.mode = 'export'
				this.optimusTable.set_columns()
				this.populate_columns()
			}
			else
			{
				this.tabulator.mode = store.innerWidth < 768 ? 'smartphone' : 'view'
				this.optimusTable.set_columns()
			}
		}

		modal.querySelector('.data-start').onchange = () => modal.querySelector('form').validateRange()
		modal.querySelector('.data-end').onchange = () => modal.querySelector('form').validateRange()

		modal.querySelector('form').validateRange = () =>
		{
			if (modal.querySelector('.data-end').value < modal.querySelector('.data-start').value)
				modal.querySelector('.data-end').setCustomValidity('La valeur de fin doit être supérieure à la valeur de début')
		}

		modal.querySelector('.export-button').onclick = () => modal.querySelector('form').onsubmit()
		modal.querySelector('form').onsubmit = () => 
		{
			let columns = this.tabulator.getColumns()

			if (modal.querySelector('.columns-select').value == 'all')
				for (let column of columns)
					column.getDefinition().download = this.optimusTable.config.tabulator.columns.find(source_column => source_column.field == column.getField()).download === false ? false : true
			else
				for (let column of columns)
					column.getDefinition().download = column.isVisible()

			if (modal.querySelector('.data-select').value == 'all')
			{
				let pageSize = this.tabulator.getPageSize()
				this.tabulator.options.downloadRowRange = 'all'
				this.tabulator.options.clipboardCopyRowRange = 'all'
				this.tabulator.setPageSize(true)
					.then(() => 
					{
						this.start_download()
							.then(() => this.tabulator.setPageSize(pageSize))
					})
			}
			else if (modal.querySelector('.data-select').value == 'selected')
			{
				let pageSize = this.tabulator.getPageSize()
				this.tabulator.options.downloadRowRange = () => this.tabulator.getRows().slice(modal.querySelector('.data-start').value - 1, modal.querySelector('.data-end').value)
				this.tabulator.options.clipboardCopyRowRange = () => this.tabulator.getRows().slice(modal.querySelector('.data-start').value - 1, modal.querySelector('.data-end').value)
				this.tabulator.setPageSize(true)
					.then(() => 
					{
						this.start_download()
							.then(() => this.tabulator.setPageSize(pageSize))
					})
			}
			else
				this.start_download()

			return false
		}
	}

	populate_columns()
	{
		clear(modal.querySelector('.columns-container'))

		modal.querySelector('.modal-card-close').onclick = () => 
		{
			this.tabulator.mode = store.innerWidth < 768 ? 'smartphone' : 'view'
			this.optimusTable.set_columns()
			modal.close()
		}

		let downloadColumnLayout = JSON.parse(localStorage.getItem('tabulator-' + this.optimusTable.config.id + (this.optimusTable.config.version ? '-' + this.optimusTable.config.version : '') + '-export-columns'))
		for (let column of downloadColumnLayout)
		{
			if (this.optimusTable.config.tabulator.columns.find(source_column => source_column.field == column.field).download === false)
			{
				this.tabulator.getColumn(column.field).hide()
				continue
			}

			let row = modal.querySelector('#optimus_table_column_row').content.firstElementChild.cloneNode(true)
			row.id = column.field

			if (column.visible == true)
				row.querySelector('input').checked = true

			row.querySelector('.switch').onclick = () => 
			{
				this.tabulator.getColumn(column.field).toggle()
			}

			row.querySelector('label').onclick = () => 
			{
				this.tabulator.getColumn(column.field).toggle()
				row.querySelector('input').checked = this.tabulator.getColumn(column.field).isVisible()
			}

			row.querySelector('label').innerText = column.title

			modal.querySelector('.columns-container').appendChild(row)
		}

		import('/libs/sortable.min.js')
			.then(() => 
			{
				Sortable.create(document.querySelector('.columns-container'),
					{
						animation: 200,
						handle: '.fa-grip',
						onEnd: event => 
						{
							if (document.getElementById(event.item.id).previousSibling?.id)
								this.tabulator.moveColumn(event.item.id, document.getElementById(event.item.id).previousSibling.id, true)
							else
								this.tabulator.moveColumn(event.item.id, document.getElementById(event.item.id).nextSibling.id)
						}
					})
			})


	}

	start_download()
	{
		return new Promise((resolve, reject) =>
		{
			if (modal.querySelector('.type-select').value == 'csv')
				resolve(this.tabulator.download('csv', this.optimusTable.config.resource + '.csv', { bom: true }))
			else if (modal.querySelector('.type-select').value == 'json')
				resolve(this.tabulator.download('json', this.optimusTable.config.resource + '.json'))
			else if (modal.querySelector('.type-select').value == 'html')
				resolve(this.tabulator.download('html', this.optimusTable.config.resource + '.html'))
			else if (modal.querySelector('.type-select').value == 'xlsx')
				import('/libs/xlsx.core.min.js')
					.then(() => resolve(this.tabulator.download('xlsx', this.optimusTable.config.resource + '.xlsx',
						{
							sheetName: this.optimusTable.config.resource,
							documentProcessing: function (workbook) 
							{
								workbook.Props =
								{
									Title: modal.querySelector('.title-input').value,
									Subject: modal.querySelector('.title-input').value,
									CreatedDate: new Date()
								}
								return workbook
							}
						})))
			else if (modal.querySelector('.type-select').value == 'pdf')
				import('/libs/jspdf.umd.min.js')
					.then(() => import('/libs/jspdf.plugin.autotable.min.js'))
					.then(() => resolve(this.tabulator.download('pdf', this.optimusTable.config.resource + '.pdf',
						{
							orientation: 'landscape',
							title: modal.querySelector('.title-input').value,
							autoTable:
							{
								columns: [...this.tabulator.getColumns().filter(column => column.getDefinition().download == true).map(column => { let x = new Object; x.header = { halign: 'center' }; x.dataKey = column.getField(); x.halign = column.getDefinition().headerHozAlign; return x })],
								headStyles:
								{
									fillColor: '#485fc7',
									textColor: '#ffffff',
									lineColor: '#ffffff',
									lineWidth: 1,
								},
								styles:
								{
									fillColor: '#ffffff',
									textColor: '#404040',
								},
								alternateRowStyles:
								{
									fillColor: '#eeeeee',
									textColor: '#000000',
								},
								columnStyles: [...this.tabulator.getColumns().filter(column => column.getDefinition().download == true).map(column => { let x = new Object; x.halign = column.getDefinition().hozAlign; return x })],
								didParseCell: (HookData) => 
								{
									if (HookData.cell.text == 'true')
										HookData.cell.text = 'X'
									else if (HookData.cell.text == 'false')
										HookData.cell.text = ''
								}
							},
						})))
			else if (modal.querySelector('.type-select').value == 'clipboard')
			{
				let columns = this.tabulator.getColumns()
				for (let column of columns)
					column.getDefinition().clipboard = column.getDefinition().download
				optimusToast('Les données ont été copiées dans le presse papier')
				resolve(this.tabulator.copyToClipboard())
			}
		})
	}
}