export default class PrintModal
{
	constructor(target, params)
	{
		this.optimusTable = params
		this.target = target
		this.tabulator = params.tabulator
	}

	async init()
	{
		await load('/components/optimus_table/print_modal.html', this.target)

		if (!localStorage.getItem('tabulator-' + this.optimusTable.config.id + (this.optimusTable.config.version ? '-' + this.optimusTable.config.version : '') + '-print-columns'))
			localStorage.setItem('tabulator-' + this.optimusTable.config.id + (this.optimusTable.config.version ? '-' + this.optimusTable.config.version : '') + '-print-columns', localStorage.getItem('tabulator-' + this.optimusTable.config.id + (this.optimusTable.config.version ? '-' + this.optimusTable.config.version : '') + '-' + this.tabulator.mode + '-columns'))

		modal.querySelector('.modal-card-close').onclick = () => modal.close()

		modal.querySelector('.title-input').value = this.optimusTable.config.resource.toUpperCase()

		modal.querySelector('.data-end').value = this.tabulator.modules.page.remoteRowCountEstimate
		modal.querySelector('.data-end').max = this.tabulator.modules.page.remoteRowCountEstimate

		modal.querySelector('.data-select').onchange = () =>
		{
			modal.querySelector('.data-selector').classList.add('is-hidden')
			if (modal.querySelector('.data-select').value == 'selected')
				modal.querySelector('.data-selector').classList.remove('is-hidden')
		}

		modal.querySelector('.columns-select').onchange = () =>
		{
			modal.querySelector('.columns-container').classList.add('is-hidden')
			if (modal.querySelector('.columns-select').value == 'selected')
			{
				modal.querySelector('.columns-container').classList.remove('is-hidden')
				this.tabulator.mode = 'print'
				this.optimusTable.set_columns()
				this.populate_columns()
			}
			else
			{
				this.tabulator.mode = store.innerWidth < 768 ? 'smartphone' : 'view'
				this.optimusTable.set_columns()
			}
		}

		modal.querySelector('.data-start').onchange = () => modal.querySelector('form').validateRange()
		modal.querySelector('.data-end').onchange = () => modal.querySelector('form').validateRange()

		modal.querySelector('form').validateRange = () =>
		{
			if (modal.querySelector('.data-end').value < modal.querySelector('.data-start').value)
				modal.querySelector('.data-end').setCustomValidity('La valeur de fin doit être supérieure à la valeur de début').checkValidity()
		}

		modal.querySelector('.print-button').onclick = () => modal.querySelector('form').onsubmit()
		modal.querySelector('form').onsubmit = () => 
		{
			this.tabulator.options.printAsHtml = false
			this.tabulator.options.printStyled = false
			this.tabulator.options.printHeader = modal.querySelector('.title-input').value

			let columns = this.tabulator.getColumns()
			let columnLayout = this.tabulator.getColumnLayout()

			if (modal.querySelector('.columns-select').value == 'all')
				for (let column of columns)
					column.getDefinition().print = this.optimusTable.config.tabulator.columns.find(source_column => source_column.field == column.getField()).print === false ? false : true
			else if (modal.querySelector('.columns-select').value == 'visible')
				for (let column of columnLayout)
					this.tabulator.getColumn(column.field).getDefinition().print = this.optimusTable.config.tabulator.columns.find(source_column => source_column.field == column.field).print === false ? false : column.visible
			else if (modal.querySelector('.columns-select').value == 'selected')
				for (let column of columns)
					column.getDefinition().print = column.isVisible()

			if (modal.querySelector('.data-select').value == 'all')
			{
				let pageSize = this.tabulator.getPageSize()
				this.tabulator.options.printRowRange = 'all'
				this.tabulator.setPageSize(true)
					.then(() => 
					{
						this.tabulator.print()
						this.tabulator.setPageSize(pageSize)
					})
			}
			else if (modal.querySelector('.data-select').value == 'selected')
			{
				let pageSize = this.tabulator.getPageSize()
				this.tabulator.options.printRowRange = () => this.tabulator.getRows().slice(modal.querySelector('.data-start').value - 1, modal.querySelector('.data-end').value)
				this.tabulator.setPageSize(true)
					.then(() => 
					{
						this.tabulator.print()
						this.tabulator.setPageSize(pageSize)
					})
			}
			else
			{
				this.tabulator.print()
			}

			return false
		}
	}

	populate_columns()
	{
		clear(modal.querySelector('.columns-container'))

		modal.querySelector('.modal-card-close').onclick = () => 
		{
			this.tabulator.mode = store.innerWidth < 768 ? 'smartphone' : 'view'
			this.optimusTable.set_columns()
			modal.close()
		}

		let printColumnLayout = JSON.parse(localStorage.getItem('tabulator-' + this.optimusTable.config.id + (this.optimusTable.config.version ? '-' + this.optimusTable.config.version : '') + '-print-columns'))
		for (let column of printColumnLayout)
		{
			if (this.optimusTable.config.tabulator.columns.find(source_column => source_column.field == column.field).print === false)
			{
				this.tabulator.getColumn(column.field).hide()
				continue
			}

			let row = modal.querySelector('#optimus_table_column_row').content.firstElementChild.cloneNode(true)
			row.id = column.field

			if (column.visible === false)
				this.tabulator.getColumn(column.field).hide()
			else
			{
				row.querySelector('input').checked = true
				this.tabulator.getColumn(column.field).show()
			}

			row.querySelector('.switch').onclick = () => 
			{
				this.tabulator.getColumn(column.field).toggle()
			}

			row.querySelector('label').onclick = () => 
			{
				this.tabulator.getColumn(column.field).toggle()
				row.querySelector('input').checked = this.tabulator.getColumn(column.field).isVisible()
			}

			row.querySelector('label').innerText = column.title

			modal.querySelector('.columns-container').appendChild(row)
		}

		import('/libs/sortable.min.js')
			.then(() => 
			{
				Sortable.create(document.querySelector('.columns-container'),
					{
						animation: 200,
						handle: '.fa-grip',
						onEnd: event => 
						{
							if (document.getElementById(event.item.id).previousSibling?.id)
								this.tabulator.moveColumn(event.item.id, document.getElementById(event.item.id).previousSibling.id, true)
							else
								this.tabulator.moveColumn(event.item.id, document.getElementById(event.item.id).nextSibling.id)
						}
					})
			})
	}
}