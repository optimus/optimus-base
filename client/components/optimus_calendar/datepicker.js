export default class datepicker
{
	constructor(target, params)
	{
		this.target = target
		this.fullcalendar = params
		return this
	}

	async init()
	{
		await load('/components/optimus_calendar/datepicker.html', this.target)

		await load_script('/libs/fullcalendar/index.global.min.js')
			.then(() => load('/libs/fullcalendar/locales/fr.global.min.js')
				.then(() => load('/libs/rrule/rrule.min.js')
					.then(() => load('/libs/rrule/index.global.min.js')
					)))

		const fullcalendar = this.fullcalendar

		let datepicker = document.createElement('input')
		datepicker.type = 'date'
		datepicker.style.display = 'none'

		document.querySelector('.modal-container').appendChild(datepicker)

		datepicker.parentNode.style.maxWidth = '320px'
		var calendars = bulmaCalendar.attach(datepicker, {
			lang: 'fr',
			type: 'date',
			color: 'link',
			displayMode: 'inline',
			startDate: fullcalendar.getDate(),
			showHeader: false,
			dateFormat: 'yyyy-MM-dd',
			todayLabel: 'Aujourd\'hui',
			showClearButton: false
		})

		document.getElementById(calendars[0]._id).style.position = 'fixed'

		datepicker.bulmaCalendar.on('select', function (datepicker)
		{
			fullcalendar.gotoDate(datepicker.data.value())
			modal.close()
		})
		document.getElementById(calendars[0]._id).querySelector('.datetimepicker-footer-today').onclick = () => 
		{
			fullcalendar.today()
			modal.close()
		}
	}

}