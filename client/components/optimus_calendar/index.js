export default class OptimusCalendar
{
	constructor(target, params)
	{
		this.target = target
		this.config = params
		return this
	}

	async init()
	{
		await load('/components/optimus_calendar/index.html', this.target)

		await load_script('/libs/fullcalendar/index.global.min.js')
			.then(() => load('/libs/fullcalendar/locales/fr.global.min.js')
				.then(() => load('/libs/rrule/rrule.min.js')
					.then(() => load('/libs/rrule/index.global.min.js')
					)))

		let container = this.target

		let fullcalendar = new FullCalendar.Calendar(this.target.querySelector('.optimus-calendar-container'), {
			themeSystem: 'custom',
			editable: true,
			selectable: true,
			selectMirror: true,
			selectMinDistance: 1.5,
			dayMaxEvents: true,
			weekNumbers: true,
			weekText: 'S',
			nowIndicator: true,
			initialDate: store.queryParams.date || null,
			firstDay: 1,
			scrollTime: '06:00:00',
			weekends: true,
			longPressDelay: 750,
			locale: 'fr',
			headerToolbar: { left: null, center: null, right: null },
			allDayText: '',
			defaultTimedEventDuration: '01:30',
			forceEventDuration: true,
			datesSet: function (arg)
			{
				container.querySelector('.optimus-calendar-datepicker-button > span').innerText = arg.view.title
			}
		})

		container.querySelectorAll('.optimus-calendar-weekends-button').forEach((item) =>
		{
			item.addEventListener('mousedown', () =>
			{
				fullcalendar.setOption('weekends', !fullcalendar.getOption('weekends'))
				item.classList.toggle('has-text-danger')
				fullcalendar.refetchEvents()
			})
		})

		container.querySelector('.optimus-calendar-dropdown-weekends-trigger').onclick = container.querySelector('.optimus-calendar-weekends-button').onclick

		container.setActiveView = function (value)
		{
			container.querySelectorAll('.optimus-calendar-changeview-buttons .button').forEach((button) =>
			{
				button.classList.remove('is-link')
			})
			container.querySelector('.optimus-calendar-' + value + '-button').classList.add('is-link')
			fullcalendar.changeView(value)
			store.calendarView = value
		}

		container.querySelector('.optimus-calendar-timeGridDay-button').onclick = () => container.setActiveView('timeGridDay')
		container.querySelector('.optimus-calendar-dropdown-timeGridDay-trigger').onmousedown = () => container.setActiveView('timeGridDay')
		container.querySelector('.optimus-calendar-timeGridWeek-button').onclick = () => container.setActiveView('timeGridWeek')
		container.querySelector('.optimus-calendar-dropdown-timeGridWeek-trigger').onmousedown = () => container.setActiveView('timeGridWeek')
		container.querySelector('.optimus-calendar-dayGridMonth-button').onclick = () => container.setActiveView('dayGridMonth')
		container.querySelector('.optimus-calendar-dropdown-dayGridMonth-trigger').onmousedown = () => container.setActiveView('dayGridMonth')
		container.querySelector('.optimus-calendar-listYear-button').onclick = () => container.setActiveView('listYear')
		container.querySelector('.optimus-calendar-dropdown-listYear-trigger').onmousedown = () => container.setActiveView('listYear')
		container.querySelector('.optimus-calendar-next-button').onclick = () => fullcalendar.next()
		container.querySelector('.optimus-calendar-previous-button').onclick = () => fullcalendar.prev()
		container.querySelector('.optimus-calendar-today-button').onclick = () => fullcalendar.today()
		container.querySelector('.optimus-calendar-today-touch-button').onclick = () => fullcalendar.today()

		fullcalendar.container = container

		container.querySelector('.optimus-calendar-datepicker-button').onclick = function ()
		{
			modal.open('/components/optimus_calendar/datepicker.js', true, fullcalendar)
		}

		//CHANGE DEFAULT MODE ACCORDING TO SCREEN WIDTH
		if (window.innerWidth > 768) container.setActiveView('timeGridWeek')
		else container.setActiveView('timeGridDay')

		//HORIZONTAL SWIPE DETECTION WITHOUT SCROLL INTERFERENCE
		// container.touchstartX = 0
		// container.touchendX = 0
		// container.touchstartY = 0
		// container.touchendY = 0
		// container.thresholdX = 70
		// container.thresholdY = 20

		// container.addEventListener(
		// 	'touchstart',
		// 	function (e)
		// 	{
		// 		this.touchstartX = e.changedTouches[0].pageX
		// 		this.touchstartY = e.changedTouches[0].pageY
		// 	},
		// 	false
		// )

		// container.addEventListener(
		// 	'touchend',
		// 	function (e)
		// 	{
		// 		this.touchendX = e.changedTouches[0].pageX
		// 		this.touchendY = e.changedTouches[0].pageY
		// 		this.handleGesture()
		// 	},
		// 	false
		// )

		// container.handleGesture = function ()
		// {
		// 	if (this.touchendY > this.touchstartY - this.thresholdY && this.touchstartY < this.touchstartY + this.thresholdY)
		// 		if (this.touchendX < this.touchstartX - this.thresholdX) fullcalendar.next()
		// 		else if (this.touchendX > this.touchstartX + this.thresholdX) fullcalendar.prev()
		// }



		fullcalendar.element = container
		container.fullcalendar = fullcalendar
		this.fullcalendar = fullcalendar

		fullcalendar.render()

		return this
	}

	onWindowResize() 
	{
		setTimeout(() => this.fullcalendar.render(), 250)
	}

	onUnload()
	{
		this.fullcalendar.destroy()
		delete this.fullcalendar
		return this
	}
}
