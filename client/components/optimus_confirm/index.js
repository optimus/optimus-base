export default class OptimusConfirm
{
	constructor(target, params) 
	{
		this.target = target
		this.message = params.message
		this.color = params.color
		this.server = store.user.server
		return this
	}

	async init()
	{
		modal.open('/components/optimus_confirm/index.html')
	}
}
