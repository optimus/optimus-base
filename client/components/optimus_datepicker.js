export default class optimusDatepicker
{
	constructor(target, params)
	{
		this.target = target
		this.config = params
		return this
	}

	init()
	{
		const dialog = document.createElement('div')
		dialog.style.animationDuration = '.1s'
		dialog.classList.add('modal', 'is-active')
		dialog.style.zIndex = 10002

		const background = document.createElement('div')
		background.classList.add('modal-background')
		background.style.opacity = 0.86
		background.onmousedown = () => dialog.remove()
		dialog.appendChild(background)

		const container = document.createElement('div')
		container.style.animationDuration = '.2s'
		container.classList.add('modal-container', 'animate__animated', 'animate__zoomIn')
		container.style.zIndex = 10003
		dialog.appendChild(container)

		document.body.appendChild(dialog)

		let input = this.config.input

		if (input.type == 'date')
			input.format = 'yyyy-MM-dd'
		else if (input.type == 'datetime-local')
			input.format = 'yyyy-MM-dd HH:mm'
		else if (input.type == 'time')
			input.format = 'HH:mm'

		let datepicker = document.createElement('input')
		datepicker.type = input.type

		container.appendChild(datepicker)

		bulmaCalendar.attach(datepicker,
			{
				lang: 'fr',
				type: input.type.replace('-local', ''),
				color: 'link',
				displayMode: 'inline',
				startDate: input.value ? input.value.slice(0, 10) : new Date().toLocaleString('lt-LT').slice(0, 10),
				startTime: input.value ? (input.type == 'time' ? input.value : input.value.slice(11, 16)) : new Date().toLocaleString('lt-LT').slice(11, 16),
				showHeader: false,
				dateFormat: input.format,
				validateLabel: 'Valider',
				todayLabel: input.type.includes('time') ? 'Maintenant' : 'Aujourd\'hui',
				cancelLabel: 'Annuler',
				clearLabel: 'Effacer',
				minuteSteps: 1,
				weekStart: 1,
				showClearButton: input.required == false,
			})

		datepicker.bulmaCalendar.on('save', datepicker =>
		{
			dialog.remove()
			input.value = datepicker.data.value().slice(0, 16)
			input.focus()
			input.blur()
		})

		dialog.querySelector('.datetimepicker-footer-today').onclick = () => 
		{
			dialog.remove()
			input.value = luxon.DateTime.now().toFormat(input.format)
			input.focus()
			input.blur()
		}

		dialog.querySelector('.datetimepicker-footer-clear').onclick = () => 
		{
			dialog.remove()
			input.value = null
			input.focus()
			input.blur()
		}
	}
}