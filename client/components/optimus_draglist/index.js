await load_CSS('/libs/tabulator/css/tabulator_bulma.min.css')
import { Tabulator, ColumnCalcsModule, FormatModule, InteractionModule, MoveRowsModule, SelectRowModule, SortModule } from '/libs/tabulator/js/tabulator_esm.min.js'
Tabulator.registerModule([FormatModule, ColumnCalcsModule, InteractionModule, MoveRowsModule, SelectRowModule, SortModule])

export default class OptimusDragList
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		return this
	}

	async init()
	{
		return fetch('/components/optimus_draglist/index.html').then(response => response.text())
			.then(response =>
			{
				this.target.innerHTML = response

				this.tabulator1 = new Tabulator(this.target.querySelector('.sender'),
					{
						data: this.params.senderData,
						initialSort: [{ column: 'displayname', dir: 'asc' }],
						height: 300,
						rowHeight: 30,
						layout: "fitColumns",
						movableRows: store.touchscreen === true ? false : true,
						movableRowsConnectedTables: this.target.querySelector('.receiver'),
						movableRowsReceiver: "add",
						movableRowsSender: "delete",
						columnHeaderVertAlign: "middle",
						selectable: true,
						columns:
							[
								{ formatter: "rowSelection", titleFormatter: "rowSelection", headerSort: false, width: 42 },
								{
									title: this.params.senderTitle,
									field: "displayname",
									vertAlign: "middle",
									headerHozAlign: "center",
									headerSort: false,
									cellDblClick: function (e, cell)
									{
										tabulator2.addRow(cell._cell.row.data)
										senderDrop(cell._cell.row)
										tabulator1.deleteRow(cell._cell.row.data.id)
										tabulator2.setSort('displayname', 'asc')
									},
									cellTapHold: (e) => { e.preventDefault(); return false },
									cellContext: (e) => { e.preventDefault(); return false }
								}
							],
					})

				this.tabulator2 = new Tabulator(this.target.querySelector('.receiver'),
					{
						data: this.params.receiverData,
						initialSort: [{ column: 'displayname', dir: 'asc' }],
						height: 300,
						rowHeight: 30,
						layout: "fitColumns",
						movableRows: store.touchscreen === true ? false : true,
						movableRowsConnectedTables: this.target.querySelector('.sender'),
						movableRowsReceiver: "add",
						movableRowsSender: "delete",
						columnHeaderVertAlign: "middle",
						selectable: true,
						columns:
							[
								{ formatter: "rowSelection", titleFormatter: "rowSelection", headerSort: false, width: 42 },
								{
									title: this.params.receiverTitle,
									field: "displayname",
									vertAlign: "middle",
									headerHozAlign: "center",
									headerSort: false,
									cellDblClick: function (e, cell)
									{
										tabulator1.addRow(cell._cell.row.data)
										receiverDrop(cell._cell.row)
										tabulator2.deleteRow(cell._cell.row.data.id)
										tabulator1.setSort('displayname', 'asc')
									},
									cellTapHold: (e) => { e.preventDefault(); return false },
									cellContext: (e) => { e.preventDefault(); return false }
								}
							],
					})

				const senderDrop = this.params.senderDrop
				const receiverDrop = this.params.receiverDrop
				const tabulator1 = this.tabulator1
				const tabulator2 = this.tabulator2

				tabulator1.add_single = function ()
				{
					tabulator1.getSelectedRows().forEach(item => 
					{
						tabulator2.addRow(item._row.data)
						senderDrop(item._row)
						tabulator1.deleteRow(item._row.data.id)
					})
					tabulator2.setSort('displayname', 'asc')

				}

				tabulator1.add_all = function ()
				{
					tabulator1.rowManager.rows.forEach(item => 
					{
						tabulator2.addRow(item.data)
						senderDrop(item)
					})
					tabulator1.clearData()
					tabulator2.setSort('displayname', 'asc')
				}

				tabulator1.remove_single = function ()
				{
					tabulator2.getSelectedRows().forEach(item => 
					{
						tabulator1.addRow(item._row.data)
						receiverDrop(item._row)
						tabulator2.deleteRow(item._row.data.id)
					})
					tabulator1.setSort('displayname', 'asc')

				}

				tabulator1.remove_all = function ()
				{
					tabulator2.rowManager.rows.forEach(item => 
					{
						tabulator1.addRow(item.data)
						receiverDrop(item)
					})
					tabulator2.clearData()
					tabulator1.setSort('displayname', 'asc')
				}

				tabulator1.on("movableRowsSent", function (fromRow, toRow, toTable)
				{
					senderDrop(fromRow._row)
					tabulator2.setSort('displayname', 'asc')
				})

				tabulator1.on("movableRowsReceived", function (fromRow, toRow, fromTable)
				{
					receiverDrop(fromRow._row)
					tabulator1.setSort('displayname', 'asc')
				})

				this.target.querySelector('#add_button').addEventListener('click', tabulator1.add_single)
				this.target.querySelector('#add_button_mobile').addEventListener('click', tabulator1.add_single)
				this.target.querySelector('#add_all_button').addEventListener('click', tabulator1.add_all)
				this.target.querySelector('#add_all_button_mobile').addEventListener('click', tabulator1.add_all)
				this.target.querySelector('#remove_button').addEventListener('click', tabulator1.remove_single)
				this.target.querySelector('#remove_button_mobile').addEventListener('click', tabulator1.remove_single)
				this.target.querySelector('#remove_all_button').addEventListener('click', tabulator1.remove_all)
				this.target.querySelector('#remove_all_button_mobile').addEventListener('click', tabulator1.remove_all)

				return Promise.all
					(
						[
							new Promise(resolve => tabulator1.on("tableBuilt", () => { tabulator1.element.querySelector(".tabulator-col").style.borderRight = 0; return resolve(tabulator1) })),
							new Promise(resolve => tabulator2.on("tableBuilt", () => { tabulator2.element.querySelector(".tabulator-col").style.borderRight = 0; return resolve(tabulator2) }))
						]
					)
			})
	}

	onUnload()
	{
		this.tabulator1.destroy()
		this.tabulator1 = null
		this.tabulator2.destroy()
		this.tabulator2 = null
		return this
	}

	onWindowResize()
	{
		for (let table of this.target.querySelectorAll('.optimus-table'))
			setTimeout(() => { table.style.width = window.innerWidth <= 768 ? '100%' : (table.parentNode.parentNode.offsetWidth - 120) / 2 + 'px'; table.tabulator.redraw(true) }, 250)
	}
}