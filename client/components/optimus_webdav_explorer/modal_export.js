export default class ExportModal
{
	constructor(target, params)
	{
		this.component = this
		this.target = target
		this.tabulator = params.tabulator
		this.config = params.config
		this.columns = params.columns
	}

	async init()
	{
		await load('/components/optimus_webdav_explorer/modal_export.html', this.target)

		this.tabulator.getColumn('checkboxes').hide()

		modal.querySelector('.modal-card-close').onclick = () => 
		{
			this.tabulator.getColumn('checkboxes').show()
			this.tabulator.getColumn('displayname').show()
			if (store.innerWidth < 768)
			{
				this.tabulator.getColumn('size').hide()
				this.tabulator.getColumn('lastmodified').hide()
			}
			else
			{
				this.tabulator.getColumn('size').show()
				this.tabulator.getColumn('lastmodified').show()
			}
			modal.close()
		}

		modal.querySelector('.title-input').value = 'Fichiers'

		modal.querySelector('.type-select').onchange = () =>
		{
			modal.querySelector('.title-block').classList.add('is-hidden')
			if (modal.querySelector('.type-select').value == 'pdf' || target.querySelector('.type-select').value == 'xlsx')
				modal.querySelector('.title-block').classList.remove('is-hidden')
		}

		modal.querySelector('.columns-select').onchange = () =>
		{
			modal.querySelector('.columns-container').classList.add('is-hidden')
			if (modal.querySelector('.columns-select').value == 'selected')
			{
				modal.querySelector('.columns-container').classList.remove('is-hidden')
				this.populate_columns()
			}
		}

		modal.querySelector('.export-button').onclick = () => modal.querySelector('form').onsubmit()
		modal.querySelector('form').onsubmit = () => 
		{
			let columns = this.tabulator.getColumns()

			if (modal.querySelector('.columns-select').value == 'all')
			{
				for (let column of columns)
					if (column.getField() != 'checkboxes')
						column.getDefinition().download = true
			}
			else if (modal.querySelector('.columns-select').value == 'selected')
			{
				for (let column of columns)
					if (column.getField() != 'checkboxes')
						column.getDefinition().download = column.isVisible()
			}

			this.start_download()
			return false
		}
	}

	populate_columns()
	{
		clear(modal.querySelector('.columns-container'))

		for (let column of this.columns)
			if (column.field != 'checkboxes')
			{
				let row = modal.querySelector('#optimus_table_column_row').content.firstElementChild.cloneNode(true)
				row.id = column.field

				if (column.visible == true)
					row.querySelector('input').checked = true

				row.querySelector('.switch').onclick = () => this.tabulator.getColumn(column.field).toggle()

				row.querySelector('label').onclick = () => 
				{
					this.tabulator.getColumn(column.field).toggle()
					row.querySelector('input').checked = this.tabulator.getColumn(column.field).isVisible()
				}

				row.querySelector('label').innerText = column.title

				modal.querySelector('.columns-container').appendChild(row)
			}

		import('/libs/sortable.min.js')
			.then(() => 
			{
				Sortable.create(document.querySelector('.columns-container'),
					{
						animation: 200,
						handle: '.fa-grip',
						onEnd: event => 
						{
							if (document.getElementById(event.item.id).previousSibling?.id)
								this.tabulator.moveColumn(event.item.id, document.getElementById(event.item.id).previousSibling.id, true)
							else
								this.tabulator.moveColumn(event.item.id, document.getElementById(event.item.id).nextSibling.id)
						}
					})
			})
	}

	start_download()
	{
		return new Promise((resolve, reject) =>
		{
			if (modal.querySelector('.type-select').value == 'csv')
				this.tabulator.download('csv', this.config.resource + '.csv', { bom: true })
			else if (modal.querySelector('.type-select').value == 'json')
				this.tabulator.download('json', this.config.resource + '.json')
			else if (modal.querySelector('.type-select').value == 'html')
				this.tabulator.download('html', this.config.resource + '.html')
			else if (modal.querySelector('.type-select').value == 'xlsx')
				import('/libs/xlsx.core.min.js')
					.then(() => this.tabulator.download('xlsx', this.config.resource + '.xlsx',
						{
							sheetName: this.config.resource,
							documentProcessing: function (workbook) 
							{
								workbook.Props =
								{
									Title: modal.querySelector('.title-input').value,
									Subject: modal.querySelector('.title-input').value,
									CreatedDate: new Date()
								}
								return workbook
							}
						}))
			else if (modal.querySelector('.type-select').value == 'pdf')
				import('/libs/jspdf.umd.min.js')
					.then(() => import('/libs/jspdf.plugin.autotable.min.js'))
					.then(() => this.tabulator.download('pdf', this.config.resource + '.pdf',
						{
							orientation: 'landscape',
							title: modal.querySelector('.title-input').value,
							autoTable:
							{
								columns: [...this.tabulator.getColumns().filter(column => column.getDefinition().download == true).map(column => { let x = new Object; x.header = { halign: 'center' }; x.dataKey = column.getField(); x.halign = column.getDefinition().headerHozAlign; return x })],
								headStyles:
								{
									fillColor: '#485fc7',
									textColor: '#ffffff',
									lineColor: '#ffffff',
									lineWidth: 1,
								},
								styles:
								{
									fillColor: '#ffffff',
									textColor: '#404040',
								},
								alternateRowStyles:
								{
									fillColor: '#eeeeee',
									textColor: '#000000',
								},
								columnStyles: [...this.tabulator.getColumns().filter(column => column.getDefinition().download == true).map(column => { let x = new Object; x.halign = column.getDefinition().hozAlign; return x })],
								didParseCell: (HookData) => 
								{
									if (HookData.cell.text == 'true')
										HookData.cell.text = 'X'
									else if (HookData.cell.text == 'false')
										HookData.cell.text = ''
								}
							},
						}))
			else if (modal.querySelector('.type-select').value == 'clipboard')
			{
				let columns = this.tabulator.getColumns()
				for (let column of columns)
					column.getDefinition().clipboard = column.getDefinition().download
				this.tabulator.copyToClipboard()
				optimusToast('Les données ont été copiées dans le presse papier')
			}
		})
	}
}