export default class OptimusWebdavExplorerModalShowPicture
{
	constructor(target, params) 
	{
		this.target = target
		this.tabulator = params.tabulator
		this.server = params.server
		this.root = params.root
		this.path = this.tabulator.getSelectedData()[0].path
		this.displayname = this.tabulator.getSelectedData()[0].displayname
	}

	async init()
	{
		let img = document.createElement('img')
		img.style.maxWidth = main.offsetWidth * 0.9 + 'px'
		img.style.maxHeight = main.offsetHeight * 0.9 + 'px'
		img.src = this.server + '/' + encodeURIComponent(this.path) + '/' + encodeURIComponent(this.displayname)
		this.target.appendChild(img)
	}
}
