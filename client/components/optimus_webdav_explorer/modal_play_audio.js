export default class OptimusWebdavExplorerModalPlayAudio
{
	constructor(target, params) 
	{
		this.target = target
		this.tabulator = params.tabulator
		this.server = params.server
		this.root = params.root
		this.path = this.tabulator.getSelectedData()[0].path
		this.displayname = this.tabulator.getSelectedData()[0].displayname
	}

	async init()
	{
		let audio = document.createElement('audio')
		audio.toggleAttribute('controls')
		audio.toggleAttribute('autoplay')

		let source = document.createElement('source')
		source.src = this.server + '/' + encodeURIComponent(this.path) + '/' + encodeURIComponent(this.displayname)

		audio.appendChild(source)
		this.target.appendChild(audio)
		return this
	}
}
