load_CSS('/libs/tabulator/css/tabulator_bulma.min.css')
import { Tabulator, ClipboardModule, ExportModule, DownloadModule, FormatModule, GroupRowsModule, InteractionModule, MenuModule, PrintModule, SelectRowModule, SortModule, FilterModule } from '/libs/tabulator/js/tabulator_esm.min.js'
Tabulator.registerModule([ClipboardModule, ExportModule, DownloadModule, FormatModule, GroupRowsModule, InteractionModule, MenuModule, PrintModule, SelectRowModule, SortModule, FilterModule])

export default class optimusWebdavExplorer
{
	constructor(target, params)
	{
		this.target = target
		this.config = params
		this.server = params.server
		this.root = params.root
		this.path = store.queryParams.path || params.path || ''
		this.cannotGetHigherThan = params.cannotGetHigherThan || null
		this.clipboard = null
		return this
	}

	async init()
	{
		await load('/components/optimus_webdav_explorer/index.html', this.target)

		//HOME BUTTON
		this.target.querySelector('.home-button').onclick = () => this.changePath(this.cannotGetHigherThan || '')

		//GLOBAL SEARCH
		if (this.config.showSearchBox == false)
			this.target.querySelector('.global-search-box').classList.add('is-hidden')
		else if (!store.touchscreen)
			this.target.querySelector('.global-search').focus()
		this.search = event =>
		{
			if (event.type == 'keyup' && event.key == 'Enter')
				return this.recursiveSearch(this.root + '/' + this.path)
			if (this.target.querySelector('.global-search').value)
				this.tabulator.setFilter('displayname', 'like', this.target.querySelector('.global-search').value)
			else
				this.changePath(this.path)
			this.tabulator.dispatchEvent('dataProcessed')
		}
		this.target.querySelector('.global-search').addEventListener('search', this.search)
		this.target.querySelector('.global-search').addEventListener('keyup', this.search)

		if (this.config.showRecursiveSearchButton == false)
			this.target.querySelector('.recursive-search-button').classList.add('is-hidden')
		this.target.querySelector('.recursive-search-button').addEventListener('click', () => 
		{
			if (this.target.querySelector('.global-search').value)
				this.recursiveSearch(this.root + '/' + this.path)
			else
				this.target.querySelector('.global-search').focus()
		})

		//BREADCRUMB
		if (this.config.showBreadcrumb == false)
			this.target.querySelector('.breadcrumb').classList.add('is-hidden')

		//CONTEXTMENU
		if (store.innerWidth < 768)
			this.target.querySelector('.contextmenu').style.minHeight = '100px'

		//ACTIONS
		this.actions =
			[
				{
					id: 'back',
					title: 'Remonter d\'un niveau',
					icon: 'fas fa-share fa-flip-horizontal',
					visibility: ['noselection', 'singleselection', 'multiselection'],
					action: async () =>
					{
						this.changePath(this.path.split('/').slice(0, -1).join('/'))
						this.redrawContextMenu()
					}
				},
				{
					id: 'refresh',
					title: 'Rafraîchir',
					icon: 'fas fa-rotate',
					visibility: ['noselection', 'singleselection', 'multiselection'],
					action: async () => this.changePath(this.path),
				},
				{
					id: 'print',
					title: 'Imprimer',
					icon: 'fas fa-print',
					visibility: ['noselection', 'singleselection', 'multiselection'],
					action: () => modal.open('/components/optimus_webdav_explorer/modal_print.js', false, this)
				},
				{
					id: 'export',
					title: 'Exporter',
					icon: 'fas fa-file-export',
					visibility: ['noselection', 'singleselection', 'multiselection'],
					action: () => modal.open('/components/optimus_webdav_explorer/modal_export.js', false, this)
				},
				{
					id: 'create_folder',
					title: 'Créer un dossier',
					icon: 'fas fa-folder-plus',
					visibility: ['noselection', 'singleselection', 'multiselection'],
					action: () => modal.open('/components/optimus_webdav_explorer/modal_create_folder.js', false, this)
				},
				{
					id: 'upload',
					title: 'Envoyer des fichiers ou dossiers',
					icon: 'fas fa-upload',
					visibility: ['noselection', 'singleselection', 'multiselection'],
					action: async () =>
					{
						let fileUploadElement = document.createElement('input')
						fileUploadElement.type = 'file'
						fileUploadElement.toggleAttribute('multiple')
						fileUploadElement.click()
						fileUploadElement.addEventListener("change", () =>
						{
							this.fileHandles = fileUploadElement.files
							modal.open('/components/optimus_webdav_explorer/modal_upload.js', false, this)
						})
					}
				},
				{
					id: 'cut',
					title: 'Couper',
					icon: 'fas fa-cut',
					visibility: ['singleselection', 'multiselection'],
					action: () =>
					{
						for (let row of this.tabulator.getFilteredAndSelectedRows())
							row.getCells()[1]._cell.element.querySelector('span').className = row.isSelected() ? 'has-text-danger' : null
						this.clipboard =
						{
							method: 'MOVE',
							data: this.tabulator.getFilteredAndSelectedData()
						}
						this.redrawContextMenu()
					}
				},
				{
					id: 'copy',
					title: 'Copier',
					icon: 'fas fa-copy',
					visibility: ['singleselection', 'multiselection'],
					action: () =>
					{
						for (let row of this.tabulator.getFilteredAndSelectedRows())
							row.getCells()[1]._cell.element.querySelector('span').className = row.isSelected() ? 'has-text-success' : null
						this.clipboard =
						{
							method: 'COPY',
							data: this.tabulator.getFilteredAndSelectedData()
						}
						this.redrawContextMenu()
					}
				},
				{
					id: 'paste',
					title: 'Coller',
					icon: 'fas fa-paste',
					visibility: ['noselection', 'singleselection', 'multiselection', 'clipboard_has_data'],
					action: () => modal.open('/components/optimus_webdav_explorer/modal_paste.js', false, this)
				},
				{
					id: 'rename',
					title: 'Renommer',
					icon: 'fas fa-i-cursor',
					visibility: ['singleselection'],
					shortcut: 'F2',
					action: () => modal.open('/components/optimus_webdav_explorer/modal_rename.js', false, this)
				},
				{
					id: 'zip',
					title: 'Compresser',
					icon: 'fas fa-minimize',
					visibility: ['singleselection', 'multiselection'],
					action: () => modal.open('/components/optimus_webdav_explorer/modal_compress.js', false, this)
				},
				{
					id: 'unzip',
					title: 'Décompresser',
					icon: 'fas fa-maximize',
					visibility: ['singleselection'],
					fileTypes: ['zip'],
					action: () => 
					{
						this.target.querySelector('.button-unzip > span > i').classList.remove('fa-maximize')
						this.target.querySelector('.button-unzip').classList.add('is-loading')
						rest(store.user.server + '/optimus-drive/unzip', 'GET', { zip: encodeURIComponent(this.root + '/' + this.path + '/' + this.tabulator.getSelectedData()[0].displayname) })
							.then(response => 
							{
								if (response.code == 200)
									this.tabulator.addData(
										{
											id: Math.max(...this.tabulator.getData().map(node => node.id)) + 1,
											type: 'folder',
											path: this.root + '/' + this.path,
											displayname: this.tabulator.getSelectedData()[0].displayname.slice(0, -4),
											lastmodified: new Date(),
											size: null,
										})
								this.target.querySelector('.button-unzip > span > i').classList.add('fa-maximize')
								this.target.querySelector('.button-unzip').classList.remove('is-loading')
							})
					}
				},
				{
					id: 'play_audio',
					title: 'Jouer le fichier audio',
					icon: 'fas fa-circle-play',
					visibility: ['singleselection'],
					fileTypes: ['mp3', 'wav', 'ogg'],
					defaultAction: ['mp3', 'wav', 'ogg'],
					action: () => modal.open('/components/optimus_webdav_explorer/modal_play_audio.js', true, this)
				},
				{
					id: 'play_video',
					title: 'Jouer le fichier vidéo',
					icon: 'fas fa-circle-play',
					visibility: ['singleselection'],
					fileTypes: ['mp4', 'webm'],
					defaultAction: ['mp4', 'webm'],
					action: () => modal.open('/components/optimus_webdav_explorer/modal_play_video.js', true, this)
				},
				{
					id: 'show_picture',
					title: 'Afficher l\'image',
					icon: 'fas fa-image',
					visibility: ['singleselection'],
					fileTypes: ['jpg', 'png', 'bmp', 'gif'],
					defaultAction: ['jpg', 'png', 'bmp', 'gif'],
					action: () => modal.open('/components/optimus_webdav_explorer/modal_show_picture.js', true, this)
				},
				{
					id: 'show_svg',
					title: 'Afficher l\'image',
					icon: 'fas fa-image',
					visibility: ['singleselection'],
					fileTypes: ['svg'],
					defaultAction: ['svg'],
					action: () => modal.open('/components/optimus_webdav_explorer/modal_show_svg.js', true, this)
				},
				{
					id: 'show_code',
					title: 'Afficher le code',
					icon: 'fas fa-code',
					visibility: ['singleselection'],
					fileTypes: ['php', 'js', 'c', 'html', 'css', 'json'],
					defaultAction: ['php', 'js', 'css', 'html', 'json'],
					action: () => modal.open('/components/optimus_webdav_explorer/modal_show_code.js', false, this)
				},
				{
					id: 'edit_txt',
					title: 'Editer le fichier TXT',
					icon: 'fas fa-edit',
					visibility: ['singleselection'],
					fileTypes: ['txt'],
					defaultAction: ['txt'],
					action: (e, cell, path) => modal.open('/components/optimus_webdav_explorer/modal_edit_txt.js', false, this)
				},
				{
					id: 'edit_word',
					title: 'Editer le fichier',
					icon: 'fab fa-microsoft',
					visibility: ['singleselection'],
					fileTypes: ['doc', 'docx'],
					defaultAction: ['doc', 'docx'],
					action: () => window.open('ms-word:ofe|u|' + this.server + '/' + encodeURIComponent(this.tabulator.getSelectedData()[0].path) + '/' + encodeURIComponent(this.tabulator.getSelectedData()[0].displayname))
				},
				{
					id: 'edit_excel',
					title: 'Editer le fichier',
					icon: 'fab fa-microsoft',
					visibility: ['singleselection'],
					fileTypes: ['xls', 'xlsx'],
					defaultAction: ['xls', 'xlsx'],
					action: () => window.open('ms-excel:ofe|u|' + this.server + '/' + encodeURIComponent(this.tabulator.getSelectedData()[0].path) + '/' + encodeURIComponent(this.tabulator.getSelectedData()[0].displayname))
				},
				{
					id: 'edit_powerpoint',
					title: 'Editer le fichier',
					icon: 'fab fa-microsoft',
					visibility: ['singleselection'],
					fileTypes: ['pptx', 'ppt'],
					defaultAction: ['pptx', 'ppt'],
					action: () => window.open('ms-powerpoint:ofe|u|' + this.server + '/' + encodeURIComponent(this.tabulator.getSelectedData()[0].path) + '/' + encodeURIComponent(this.tabulator.getSelectedData()[0].displayname))
				},
				{
					id: 'edit_libreoffice',
					title: 'Editer le fichier avec Libreoffice',
					icon: 'fas fa-file-pen',
					visibility: ['singleselection'],
					fileTypes: ['odt', 'ods', 'odg', 'csv'],
					defaultAction: ['odt', 'ods', 'odg', 'csv'],
					action: () => window.open('vnd.libreoffice.command:ofe|u|' + this.server + '/' + encodeURIComponent(this.tabulator.getSelectedData()[0].path) + '/' + encodeURIComponent(this.tabulator.getSelectedData()[0].displayname)),
				},
				{
					id: 'open_pdf',
					title: 'Ouvrir le fichier PDF',
					icon: 'fas fa-eye',
					visibility: ['singleselection'],
					fileTypes: ['pdf'],
					defaultAction: ['pdf'],
					action: () => window.open('https://' + store.user.server + '/optimus-drive/inline?file=' + encodeURIComponent(this.tabulator.getSelectedData()[0].path) + '/' + encodeURIComponent(this.tabulator.getSelectedData()[0].displayname))
				},
				{
					id: 'convert_to_pdf',
					title: 'Convertir en PDF',
					icon: 'fas fa-file-pdf',
					visibility: ['singleselection'],
					fileTypes: ['doc', 'docx', 'odt', 'xls', 'xlsx', 'ods', 'csv', 'ppt', 'pptx', 'odg', 'bmp', 'gif', 'jpg', 'png', 'txt', 'xml'],
					action: () => 
					{
						this.target.querySelector('.button-convert_to_pdf > span > i').classList.remove('fa-file-pdf')
						this.target.querySelector('.button-convert_to_pdf').classList.add('is-loading')
						rest(store.user.server + '/optimus-drive/convert-pdf', 'GET', { file: encodeURIComponent(this.tabulator.getSelectedData()[0].path) + '/' + encodeURIComponent(this.tabulator.getSelectedData()[0].displayname) })
							.then(response => 
							{
								if (response.code == 200)
									this.tabulator.addData(
										{
											id: Math.max(...this.tabulator.getData().map(node => node.id)) + 1,
											type: 'file',
											path: this.root + '/' + this.path,
											displayname: response.data.filename,
											lastmodified: response.data.lastmodified,
											size: response.data.size,
										})
								this.target.querySelector('.button-convert_to_pdf > span > i').classList.add('fa-file-pdf')
								this.target.querySelector('.button-convert_to_pdf').classList.remove('is-loading')
							})
					}
				},
				{
					id: 'download',
					title: 'Télécharger',
					icon: 'fas fa-download',
					visibility: ['fileonly', 'singleselection'],
					action: () => this.tabulator.getSelectedData()[0].type == 'file' && window.open(this.server + '/' + encodeURIComponent(this.tabulator.getSelectedData()[0].path) + '/' + encodeURIComponent(this.tabulator.getSelectedData()[0].displayname))
				},
				{
					id: 'delete',
					title: 'Supprimer',
					icon: 'fas fa-trash',
					visibility: ['singleselection', 'multiselection'],
					shortcut: 'Delete',
					action: () => 
					{
						for (let row of this.tabulator.getFilteredAndSelectedRows())
							fetch(this.server + '/' + encodeURIComponent(row.getData().path) + '/' + encodeURIComponent(row.getData().displayname),
								{
									method: 'DELETE',
									credentials: 'include'
								})
								.then(response => 
								{
									if (response.status == 204)
										row.delete() && this.tabulator.dispatchEvent('dataProcessed')
									else if (response.status == 423)
										optimusToast('Erreur : Le fichier est bloqué par un autre processus', 'is-danger')
									else
										optimusToast('Erreur ' + response.status + '<br/>' + response.statusText, 'is-danger')
									this.redrawContextMenu()
								})
					}
				},
			]

		//COLUMNS DEFINITIONS
		this.columns =
			[
				{
					field: 'checkboxes',
					width: 40,
					minWidth: 40,
					maxWidth: 40,
					headerSort: false,
					formatter: 'rowSelection',
					titleFormatter: 'rowSelection',
					cellClick: (e, cell) => cell.getRow().toggleSelect()
				},
				{
					field: 'displayname',
					title: 'Nom',
					visible: true,
					formatter: cell => 
					{
						if (cell.getData().type == 'folder')
							return '<a><i class="fas fa-folder fa-lg fa-fw mr-1 has-text-warning"></i><span class="is-unselectable">' + cell.getValue() + '</span></a>'
						else
						{
							let icon
							let extension = cell.getValue().split('.').pop().toLowerCase()
							if (['mp3', 'wav', 'ogg'].includes(extension))
								icon = 'far fa-file-audio'
							else if (['xls', 'xlsx', 'csv', 'ods'].includes(extension))
								icon = 'far fa-file-excel'
							else if (['doc', 'docx', 'rtf', 'odt'].includes(extension))
								icon = 'far fa-file-word'
							else if (['pdf'].includes(extension))
								icon = 'far fa-file-pdf'
							else if (['zip', 'rar', 'tar', '7z', 'gz'].includes(extension))
								icon = 'far fa-file-zipper'
							else if (['mp4', 'mkv', 'avi', 'mov', 'webm', 'wmv'].includes(extension))
								icon = 'far fa-file-video'
							else if (['ppt', 'pptx', 'odg'].includes(extension))
								icon = 'far fa-file-powerpoint'
							else if (['txt'].includes(extension))
								icon = 'far fa-file-lines'
							else if (['jpg', 'png', 'bmp', 'svg', 'tif', 'gif'].includes(extension))
								icon = 'far fa-file-image'
							else if (['php', 'js', 'c', 'css', 'html', 'json'].includes(extension))
								icon = 'far fa-file-code'
							else if (['kdbx'].includes(extension))
								icon = 'fas fa-key'
							else
								icon = 'far fa-file'
							return '<i class="' + icon + ' fa-lg fa-fw mr-1 has-text-grey"></i><span class="is-unselectable">' + cell.getValue() + '</span>'
						}
					},
					sorter: (a, b, aRow, bRow, column, dir, sorterParams) =>
					{
						if (aRow.getData().type == 'folder' && bRow.getData().type == 'file')
							return 1
						else if (aRow.getData().type == 'file' && bRow.getData().type == 'folder')
							return -1
						else if (a > b)
							return -1
						else
							return 1
					},
					cellDblClick: async (e, cell) => 
					{
						if (cell.getData().type == 'file')
						{
							//this.tabulator.getRows().map(row => row.deselect())
							cell.getRow().select()
							for (let action of this.actions)
								if (action.action && action.defaultAction && action.defaultAction.includes(cell.getData().displayname?.split('.')?.pop().toLowerCase()))
									return action.action(null, null, this.server + '/' + this.root + '/' + this.path + '/' + cell.getData().displayname)
										.then(() => cell.getRow().deselect())
							this.actions.find(action => action.id == 'download').action()
						}
					},
					cellTap: this.cellClick,
					cellClick: async (e, cell) => 
					{
						if (cell.getData().type == 'file')
						{
							this.tabulator.getRows().map(row => row.deselect())
							cell.getRow().select()
							//for (let action of this.actions)
							//if (action.action && action.defaultAction && action.defaultAction.includes(cell.getData().displayname?.split('.')?.pop().toLowerCase()))
							//return action.action(null, null, this.server + '/' + this.root + '/' + this.path + '/' + cell.getData().displayname)
							//.then(() => cell.getRow().deselect())
							//this.actions.find(action => action.id == 'download').action()
						}
						else
							this.changePath(cell.getData().path + '/' + cell.getValue())
					},
					cellDblTap: this.cellDblClick,
					cellContext: (e, cell) => 
					{
						if (!cell.getRow().isSelected())
							setTimeout(() => cell.getRow().getElement().dispatchEvent(e), 100)
						this.tabulator.getRows().map(row => row.deselect())
						cell.getRow().select()
					},
					cellTapHold: this.cellContext,
					widthGrow: 1
				},
				{
					field: 'size',
					title: 'Taille',
					formatter: cell => cell.getValue() ? new Intl.NumberFormat('fr-FR',).format(Math.ceil(cell.getValue() / 1024)).replaceAll(' ', '&nbsp;') + ' KB' : null,
					hozAlign: 'right',
					width: 150,
					minWidth: 150,
					maxWidth: 150,
					visible: store.innerWidth >= 768
				},
				{
					field: 'lastmodified',
					formatter: cell => 
					{
						let date = new Date(cell.getValue()).toLocaleString('fr-FR')
						return date.slice(6, 10) + '-' + date.slice(3, 5) + '-' + date.slice(0, 2) + date.slice(10, 16)
					},
					title: 'Modifié le',
					width: 150,
					minWidth: 150,
					maxWidth: 150,
					visible: store.innerWidth >= 768
				},
			]

		//CHARGEMENT DE TABULATOR
		let maxHeight = this.target.querySelector('.optimus-table-container').offsetHeight - (store.innerWidth < 768 ? 0 : 48)
		this.target.querySelector('.optimus-table-container').classList.remove('is-flex', 'is-flex-direction-column', 'is-flex-grow-1')
		this.tabulator = new Tabulator(this.target.querySelector('.optimus-table-container'),
			{
				maxHeight: maxHeight,
				height: maxHeight,
				columns: this.columns,
				initialSort: [{ column: "displayname", dir: "desc" }],
				dataLoader: false,
				movableColumns: false,
				layout: 'fitColumns',
				columnHeaderSortMulti: false,
				sortOrderReverse: true,
				headerSortElement: "<i class='fas fa-arrow-up'></i>",
				clipboard: 'copy',
				clipboardCopyStyled: false,
				rowContextMenu: () => this.rowContextMenu(),
				locale: 'fr',
				placeholder: () => this.tabulator.initialized && this.tabulator.getFilters().length > 0 ? "Aucun résultat" : "Ce dossier est vide",
				groupHeader: (value, count, data, group) => '<i class="fas fa-folder fa-lg fa-fw has-text-warning mr-1"></i>' + value.slice(this.root.length + 1),
				groupToggleElement: 'header',
				langs: {
					'fr': {
						ajax: {
							loading: "Chargement",
							error: "Erreur de chargement",
						},
					}
				},
			})

		this.tabulator.optimusTable = this

		this.tabulator.on("rowSelectionChanged", () => this.redrawContextMenu())

		this.tabulator.on("dataFiltered", (filters, rows) => this.tabulator.filteredRows = rows)
		this.tabulator.getFilteredAndSelectedRows = () => this.tabulator.filteredRows.filter(row => row.isSelected())
		this.tabulator.getFilteredAndSelectedData = () => this.tabulator.searchData('displayname', 'like', this.target.querySelector('.global-search').value).filter(item => this.tabulator.getRow(item.id).isSelected())


		//GESTION DES ERREURS DE CHARGEMENT
		this.tabulator.on("dataLoadError", () => this.tabulator.clearData())

		//ADAPTATION DE LA HAUTEUR DU TABLEAU
		this.tabulator.on('dataChanged', () => this.resizeTable())
		this.tabulator.on('dataProcessed', () => this.resizeTable())

		//RACCOURCIS CLAVIERS
		window.addEventListener('cut', this.actions.filter(action => action.id == 'cut')[0].action)
		window.addEventListener('copy', this.actions.filter(action => action.id == 'copy')[0].action)
		window.addEventListener('paste', this.actions.filter(action => action.id == 'paste')[0].action)

		//FILE UPLOAD
		this.tabulator.element.ondragenter = event => !event.currentTarget.contains(event.relatedTarget) && this.tabulator.alert('Déposer vos fichiers ici')
		this.tabulator.element.ondragover = event => event.preventDefault()
		this.tabulator.element.ondragleave = event => !event.currentTarget.contains(event.relatedTarget) && this.tabulator.clearAlert()
		this.tabulator.element.ondrop = async event => 
		{
			this.tabulator.clearAlert()
			event.preventDefault()

			this.fileHandles = []
			for (let fileHandle of event.dataTransfer.items)
				if (fileHandle.kind === "file")
					this.fileHandles.push(fileHandle.webkitGetAsEntry())

			modal.open('/components/optimus_webdav_explorer/modal_upload.js', false, this)
		}

		//FIN DE L'INITIALISATION
		return new Promise(resolve =>
		{
			this.tabulator.on('tableBuilt', () =>
			{
				if (store.innerWidth < 768)
					this.tabulator.getColumn('displayname').setWidth(store.innerWidth - 8)

				this.target.querySelector('.optimus-table').style.opacity = 1
				this.target.querySelector('.tabulator-tableholder').style.overflowX = 'hidden'

				this.changePath(this.path)
				this.redrawContextMenu()

				resolve(this)
			})
		})
	}

	resizeTable()
	{
		if (this.tabulator.initialized && this.tabulator.getFilters().length > 0)
			this.displayedRows = this.tabulator.searchData('displayname', 'like', this.target.querySelector('.global-search').value)
		else
			this.displayedRows = this.tabulator.getData()

		if (this.tabulator.initialized)
			this.tabulator.setSort(this.tabulator.getSorters()[0].field, this.tabulator.getSorters()[0].dir)
	}

	redrawContextMenu()
	{
		clear(this.target.querySelector('.contextmenu'))

		for (let icon of this.actions)
		{
			if ((this.tabulator.getSelectedData().length == 0 && !icon.visibility.includes('noselection')) || (this.tabulator.getSelectedData().length == 1 && !icon.visibility.includes('singleselection')) || (this.tabulator.getSelectedData().length > 1 && !icon.visibility.includes('multiselection')) || (icon.visibility.includes('clipboard_has_data') && this.clipboard == null) || (icon.fileTypes && this.tabulator.getSelectedData().length == 1 && !icon.fileTypes.includes(this.tabulator.getSelectedData()[0].displayname?.split('.')?.pop())) || (icon.visibility && this.tabulator.getSelectedData().length == 1 && icon.visibility.includes('fileonly') && this.tabulator.getSelectedData()[0].type == 'folder'))
				continue

			let template = this.target.querySelector('#button_template').content.cloneNode(true)
			template.querySelector('button').classList.add('button-' + icon.id)
			template.querySelector('button').title = icon.title
			template.querySelector('button').onclick = icon.action

			template.querySelector('span:first-of-type').classList.add('icon')
			template.querySelector('i').classList.add(...icon.icon.split(' '))

			this.target.querySelector('.contextmenu').appendChild(template)

			if (icon.shortcut)
				main.addEventListener('keyup', event => event.key == icon.shortcut && icon.action())
		}

		if (this.clipboard?.data)
			this.target.querySelector('.contextmenu').querySelector('.button-paste').nextElementSibling.innerText = this.clipboard.data.length > 0 ? this.clipboard.data.length : null

		if (this.path == '' || this.path == '/' || this.path == this.cannotGetHigherThan)
			this.target.querySelector('i.fa-share').parentNode.parentNode.disabled = true
		else
			this.target.querySelector('i.fa-share').parentNode.parentNode.disabled = false
	}

	rowContextMenu()
	{
		var menu = []
		for (let icon of this.actions.filter(action => (action.visibility.includes('singleselection') || action.visibility.includes('multiselection')) && !action.visibility.includes('noselection')))
			if ((this.tabulator.getSelectedData().length == 0 && !icon.visibility.includes('noselection')) || (this.tabulator.getSelectedData().length == 1 && !icon.visibility.includes('singleselection')) || (this.tabulator.getSelectedData().length > 1 && !icon.visibility.includes('multiselection')) || (icon.visibility.includes('clipboard_has_data') && this.clipboard == null) || (icon.fileTypes && this.tabulator.getSelectedData().length == 1 && !icon.fileTypes.includes(this.tabulator.getSelectedData()[0].displayname?.split('.')?.pop())) || (icon.visibility && this.tabulator.getSelectedData().length == 1 && icon.visibility.includes('fileonly') && this.tabulator.getSelectedData()[0].type == 'folder'))
				continue
			else
				menu.push(
					{
						label: '<i class="fa-fw ' + icon.icon + ' mr-2 has-text-grey-dark"></i><span class="has-text-grey-dark">' + icon.title + '</span>',
						action: icon.action
					})
		return menu
	}

	async changePath(path, selectedNodesArray = [])
	{
		if (path.slice(0, this.root.length) == this.root)
			path = path.slice(this.root.length + 1)

		this.tabulator.setGroupBy()

		this.target.querySelector('.global-search').value = ''
		if (!store.query.includes('path=') && path != '')
		{
			store.query = store.query + (store.query == '' ? '' : '&') + 'path=' + encodeURIComponent(path)
			window.history.replaceState({ previous: window.location.href }, '', store.origin + '/' + store.route.split('?')[0] + '?' + store.query + (store.hashParams.length > 0 ? '#' + store.hashParams.join('/') : ''))
			store.queryParams.path = path
		}
		else if (store.queryParams.path != path)
		{
			window.history.pushState({ previous: window.location.href }, '', window.location.href.replace('path=' + encodeURIComponent(store.queryParams.path), 'path=' + encodeURIComponent(path)))
			store.queryParams.path = path
		}

		this.path = path

		if (this.target.querySelector('i.fa-share'))
			if (this.path == '' || this.path == '/' || this.path == this.cannotGetHigherThan)
				this.target.querySelector('i.fa-share').parentNode.parentNode.disabled = true
			else
				this.target.querySelector('i.fa-share').parentNode.parentNode.disabled = false

		this.tabulator.setData(await this.list(path)).then(() => this.path.includes('/modeles') && optimusToast('Les modèles doivent être impérativement modifiés avec <a href="https://fr.libreoffice.org/download/telecharger-libreoffice/" target="_blank">LibreOffice Writer</a> (logiciel libre gratuit)', 'is-warning', { duration: 5000, single: false, dismissible: false, appendTo: main.querySelector('.tabulator-table') }))

		while (this.target.querySelectorAll('.breadcrumb-list > li').length > 1)
			this.target.querySelectorAll('.breadcrumb-list > li')[1].remove()

		let li_path = ['']
		for (let node of path.split('/').filter(Boolean))
		{
			let li = document.createElement('li')
			let a = document.createElement('a')
			a.innerText = node
			li_path.push(node)
			a.dataset.path = li_path.join('/')
			a.onclick = event => this.changePath(event.target.dataset.path)
			li.appendChild(a)
			this.target.querySelector('.breadcrumb-list').appendChild(li)
		}

		this.target.querySelector('.breadcrumb-list > li:last-of-type').classList.add('is-active')

		for (let node of selectedNodesArray)
			if (this.tabulator.searchRows("displayname", "=", node).length > 0)
				this.tabulator.searchRows("displayname", "=", node)[0].select()
	}

	async list(path)
	{
		let folders = []
		let files = []

		return fetch(this.server + '/' + this.root + '/' + encodeURIComponent(path),
			{
				method: 'PROPFIND',
				credentials: 'include'
			})
			.then(response => response.text())
			.then(response => new DOMParser().parseFromString(response, 'text/xml'))
			.then(nodes => nodes.getElementsByTagName('d:response'))
			.then(nodes =>
			{
				for (let i = 1; i < nodes.length; i++)
					if (nodes[i].getElementsByTagName('d:collection').length > 0)
						folders.push(
							{
								id: i,
								path: decodeURIComponent(nodes[0].getElementsByTagName('d:href')[0].childNodes[0].nodeValue.slice(1, -1)),
								type: 'folder',
								displayname: decodeURIComponent(nodes[i].getElementsByTagName('d:href')[0].childNodes[0].nodeValue.split('/').at(-2)),
								lastmodified: nodes[i].getElementsByTagName('d:getlastmodified')[0].childNodes[0].nodeValue,
								size: null
							}
						)
					else
						files.push(
							{
								id: i,
								path: decodeURIComponent(nodes[0].getElementsByTagName('d:href')[0].childNodes[0].nodeValue.slice(1, -1)),
								type: 'file',
								displayname: decodeURIComponent(nodes[i].getElementsByTagName('d:href')[0].childNodes[0].nodeValue.split('/').at(-1)),
								lastmodified: nodes[i].getElementsByTagName('d:getlastmodified')[0].childNodes[0].nodeValue,
								size: nodes[i].getElementsByTagName('d:getcontentlength')[0].childNodes[0].nodeValue
							}
						)

				folders.sort((a, b) => a.displayname.toLowerCase() > b.displayname.toLowerCase() ? 1 : -1)
				files.sort((a, b) => a.displayname.toLowerCase() > b.displayname.toLowerCase() ? 1 : -1)
				nodes = folders.concat(files)

				return nodes
			})
	}

	recursiveSearch(path)
	{
		this.tabulator.clearFilter()

		rest(store.user.server + '/optimus-drive/search', 'GET', { path: path, search: this.target.querySelector('.global-search').value })
			.then(response =>
			{
				if (response.code == 200)
				{
					this.tabulator.setData(response.data)
					this.tabulator.setGroupBy('path')
				}
			})
	}

	onUnload()
	{
		window.removeEventListener('cut', this.actions.filter(action => action.id == 'cut')[0].action)
		window.removeEventListener('copy', this.actions.filter(action => action.id == 'copy')[0].action)
		window.removeEventListener('paste', this.actions.filter(action => action.id == 'paste')[0].action)
		this.tabulator.destroy()
		delete this.tabulator
		return this
	}
}