export default class OptimusWebdavExplorerModalShowSVG
{
	constructor(target, params) 
	{
		this.target = target
		this.tabulator = params.tabulator
		this.server = params.server
		this.root = params.root
		this.path = this.tabulator.getSelectedData()[0].path
		this.displayname = this.tabulator.getSelectedData()[0].displayname
	}

	async init()
	{
		let svg = await fetch(this.server + '/' + encodeURIComponent(this.path) + '/' + encodeURIComponent(this.displayname),
			{
				method: 'GET',
				credentials: 'include'
			})
			.then(response => response.text())

		this.target.insertAdjacentHTML("afterbegin", svg)

		this.target.firstElementChild.style.maxWidth = main.offsetWidth * 0.9 + 'px'
		this.target.firstElementChild.style.maxHeight = main.offsetHeight * 0.9 + 'px'
	}
}
