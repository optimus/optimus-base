export default class OptimusWebdavExplorerModalShowCode
{
	constructor(target, params) 
	{
		this.target = target
		this.tabulator = params.tabulator
		this.server = params.server
		this.root = params.root
		this.id = this.tabulator.getSelectedData()[0].id
		this.path = this.tabulator.getSelectedData()[0].path
		this.displayname = this.tabulator.getSelectedData()[0].displayname
	}

	async init()
	{
		await load('/components/optimus_webdav_explorer/modal_rename.html', this.target)

		modal.querySelector('input').value = this.tabulator.getSelectedData()[0].displayname

		if (!store.touchscreen)
			setTimeout(() => 
			{
				modal.querySelector('input').focus()
				modal.querySelector('input').setSelectionRange(0, this.tabulator.getSelectedData()[0].displayname.lastIndexOf('.'))
			}, 100)

		modal.querySelector('input').onkeyup = event => 
		{
			if (!this.validateFilename())
				return false
			if (event.key == 'Enter')
				modal.querySelector('.rename-button').click()
		}
		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.cancel-button').onclick = () => modal.close()


		modal.querySelector('.rename-button').onclick = () => 
		{
			if (!this.validateFilename())
				return false

			fetch(this.server + '/' + encodeURIComponent(this.path) + '/' + encodeURIComponent(this.displayname),
				{
					method: 'MOVE',
					credentials: 'include',
					headers:
					{
						destination: this.server + '/' + encodeURIComponent(this.path) + '/' + encodeURIComponent(modal.querySelector('input').value)
					}
				})
				.then(async response => 
				{
					let error_message = new DOMParser().parseFromString(await response.text(), 'text/xml')?.getElementsByTagName('s:message')[0]?.childNodes[0]?.nodeValue
					if (response.status == 201)
					{
						this.tabulator.getSelectedRows()[0].getCell('displayname').setValue(modal.querySelector('input').value)
						modal.close()
					}
					else if (response.status == 403)
					{
						if (error_message == 'Source and destination uri are identical.')
							modal.querySelector('input').setCustomValidity('Renommage impossible : un dossier ou fichier portant le même nom existe déjà')
						else
							modal.querySelector('input').setCustomValidity('Renommage impossible : vous ne disposez pas des autorisations nécessaires pour modifier cette ressource')
						modal.querySelector('input').reportValidity()
					}
					else if (response.status == 423)
					{
						modal.querySelector('input').setCustomValidity('Renommage impossible : cette ressource est actuellement utilisée par un autre utilisateur')
						modal.querySelector('input').reportValidity()
					}
					else
						optimusToast(error_message, 'is-danger')
				})
		}
	}

	validateFilename()
	{
		if (!modal.querySelector('input').value.match(/^[a-zA-Z0-9 ._@€\-àâäéèêëïîôöùûüÿç()\']+$/))
		{
			modal.querySelector('input').setCustomValidity('La valeur attendue est un nom de fichier ou dossier.\n Ne sont autorisés que les lettres, chiffres et les caractères spéciaux suivants :\n. _ @ à â ä é è ê ë ï î ô ö ù û ü ÿ ç ( ) \'')
			modal.querySelector('input').reportValidity()
			return false
		}
		else
		{
			modal.querySelector('input').setCustomValidity('')
			modal.querySelector('input').reportValidity()
			return true
		}
	}
}
