export default class OptimusWebdavExplorerModalShowCode
{
	constructor(target, params) 
	{
		this.target = target
		this.server = params.server
		this.root = params.root
		this.path = params.path
		this.tabulator = params.tabulator
	}

	async init()
	{
		await load('/components/optimus_webdav_explorer/modal_create_folder.html', this.target)

		if (!store.touchscreen)
			setTimeout(() => modal.querySelector('input').focus(), 100)

		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.cancel-button').onclick = () => modal.close()

		modal.querySelector('input').onkeyup = event => 
		{
			if (!this.validateFilename())
				return false
			if (event.key == 'Enter')
				modal.querySelector('.create-button').click()
		}

		modal.querySelector('.create-button').onclick = () => 
		{
			if (!this.validateFilename())
				return false

			fetch(this.server + '/' + encodeURIComponent(this.root) + '/' + encodeURIComponent(this.path) + '/' + encodeURIComponent(modal.querySelector('input').value),
				{
					method: 'MKCOL',
					credentials: 'include'
				})
				.then(response => 
				{
					if (response.status == 201)
					{
						this.tabulator.addRow(
							{
								id: Math.max(...this.tabulator.getData().map(data => data.id)) + 1,
								type: 'folder',
								path: this.path,
								displayname: modal.querySelector('input').value,
								lastmodified: new Date().toLocaleString('fr-FR'), size: null
							})
						this.tabulator.dispatchEvent('dataProcessed')
						modal.close()
					}
					else if (response.status == 405)
					{
						modal.querySelector('input').setCustomValidity('Création impossible : un dossier ou fichier portant le même nom existe déjà')
						modal.querySelector('input').reportValidity()
					}
					else if (response.status == 403)
						optimusToast("Vous n'avez pas les autorisations suffisantes pour créer un dossier à cet emplacement", 'is-danger')
					else if (response.status == 507)
						optimusToast("Espace disque insuffisant", 'is-danger')
				})
		}
	}

	validateFilename()
	{
		if (!modal.querySelector('input').value.match(/^[a-zA-Z0-9 ._@€\-àâäéèêëïîôöùûüÿç()\']+$/))
		{
			modal.querySelector('input').setCustomValidity('La valeur attendue est un nom de fichier ou dossier.\n Ne sont autorisés que les lettres, chiffres et les caractères spéciaux suivants :\n. _ @ à â ä é è ê ë ï î ô ö ù û ü ÿ ç ( ) \'')
			modal.querySelector('input').reportValidity()
			return false
		}
		else
		{
			modal.querySelector('input').setCustomValidity('')
			modal.querySelector('input').reportValidity()
			return true
		}
	}
}
