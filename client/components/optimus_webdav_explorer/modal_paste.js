export default class OptimusWebdavExplorerModalShowCode
{
	constructor(target, params) 
	{
		this.target = target
		this.clipboard = params.clipboard
		this.server = params.server
		this.root = params.root
		this.path = params.path
		this.tabulator = params.tabulator
		this.component = params
	}

	async init()
	{
		await load('/components/optimus_webdav_explorer/modal_paste.html', this.target)

		modal.querySelector('.modal-card-title').innerText = this.clipboard.method == 'COPY' ? 'Copie' : 'Déplacement'
		modal.querySelector('.file-progress').max = this.clipboard.data.length
		modal.querySelector('.modal-card-close').onclick = () => modal.close()

		setTimeout(async () => 
		{
			for (let node of this.clipboard.data)
				await this.transferNode(this.clipboard.method, this.server + '/' + node.path + '/' + node.displayname, this.server + '/' + this.root + '/' + this.path + '/' + node.displayname, false)
			this.component.redrawContextMenu()
			this.component.changePath(this.path)
			modal.close()
		}, 200)
	}

	async transferNode(method, source, destination, overwrite = false)
	{
		modal.querySelector('.subtitle').innerText = source.split('/').pop()
		modal.querySelector('.file-progress').value += 1

		return fetch(source.split('/').slice(0, 1) + '/' + source.split('/').slice(1).map(value => encodeURIComponent(value)).join('/'),
			{
				method: method,
				credentials: 'include',
				headers:
				{
					destination: destination.split('/').slice(0, 1) + '/' + destination.split('/').slice(1).map(value => encodeURIComponent(value)).join('/'),
					overwrite: overwrite ? 'T' : 'F'
				}
			})
			.then(async response => 
			{
				if (response.status == 201 || response.status == 204)
				{
					if (this.clipboard.data)
						this.clipboard.data = this.clipboard.data.filter(node => node.displayname != source.split('/').pop())
					return true
				}
				else if (response.status == 403)
				{
					let error_message = new DOMParser().parseFromString(await response.text(), 'text/xml')?.getElementsByTagName('s:message')[0]?.childNodes[0]?.nodeValue
					if (error_message == 'Source and destination uri are identical.')
						optimusToast('Un dossier ou fichier portant le même nom existe déjà', 'is-danger')
					else
						optimusToast('Vous ne disposez pas des autorisations nécessaires pour effectuer cette opération', 'is-danger')
					return false
				}
				else if (response.status == 409)
				{
					optimusToast('Un ou plusieurs sous-dossiers spécifiés en destination n\'existent pas', 'is-danger')
					return false
				}
				else if (response.status == 412)
				{
					let filename = destination.split('/').pop()
					let radical = filename
					let extension = null
					if (filename.includes('.'))
					{
						extension = filename.split('.').pop()
						radical = filename.split('.').slice(0, -1)
					}
					let new_destination = destination.split('/').slice(0, -1).join('/') + '/' + radical + ' (créé le ' + new Date().toISOString().slice(0, 23).replace(/\D/g, '') + ')' + (extension ? '.' + extension : '')
					return this.transferNode(method, source, new_destination, overwrite)
				}
				else if (response.status == 423)
				{
					optimusToast('La source ou la destination sont bloqués par un autre utilisateur', 'is-danger')
					return false
				}
				else
				{
					let error_message = new DOMParser().parseFromString(await response.text(), 'text/xml')?.getElementsByTagName('s:message')[0]?.childNodes[0]?.nodeValue
					optimusToast(error_message, 'is-danger')
					return false
				}
			})
	}
}
