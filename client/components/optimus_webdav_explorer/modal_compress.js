export default class OptimusWebdavExplorerModalShowCode
{
	constructor(target, params) 
	{
		this.target = target
		this.server = params.server
		this.root = params.root
		this.path = params.path
		this.tabulator = params.tabulator
	}

	async init()
	{
		await load('/components/optimus_webdav_explorer/modal_compress.html', this.target)

		if (!store.touchscreen)
			setTimeout(() => modal.querySelector('input').focus(), 100)

		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.cancel-button').onclick = () => modal.close()

		modal.querySelector('input').value = this.tabulator.getFilteredAndSelectedData()[0].displayname + '.zip'
		modal.querySelector('input').select()

		modal.querySelector('input').onkeyup = event => 
		{
			if (!this.validateFilename())
				return false
			if (event.key == 'Enter')
				modal.querySelector('.create-button').click()
		}

		modal.querySelector('.create-button').onclick = () => 
		{
			if (!this.validateFilename())
				return false

			modal.querySelector('.create-button').classList.add('is-loading')

			let nodes = []
			for (let node of this.tabulator.getFilteredAndSelectedData())
				nodes.push(encodeURIComponent(node.path) + '/' + encodeURIComponent(node.displayname))

			rest(store.user.server + '/optimus-drive/zip', 'GET',
				{
					file: encodeURIComponent(this.root + '/' + this.path) + '/' + encodeURIComponent(modal.querySelector('input').value),
					content: nodes
				})
				.then(response => 
				{
					if (response.code == 200)
						this.tabulator.addData(
							{
								id: Math.max(...this.tabulator.getData().map(node => node.id)) + 1,
								type: 'file',
								path: this.root + '/' + this.path,
								displayname: response.data.filename,
								lastmodified: response.data.lastmodified,
								size: response.data.size,
							})
					modal.querySelector('.create-button').classList.remove('is-loading')
					modal.close()
				})
				.catch(() =>
				{
					modal.querySelector('.create-button').classList.remove('is-loading')
				})
		}
	}

	validateFilename()
	{
		let zipname = modal.querySelector('input').value.toLowerCase().slice(-4) == '.zip' ? modal.querySelector('input').value : modal.querySelector('input').value + '.zip'
		if (!modal.querySelector('input').value.match(/^[a-zA-Z0-9 ._@€\-àâäéèêëïîôöùûüÿç()\']+$/))
		{
			modal.querySelector('input').setCustomValidity('La valeur attendue est un nom de fichier ou dossier.\n Ne sont autorisés que les lettres, chiffres et les caractères spéciaux suivants :\n. _ @ € à â ä é è ê ë ï î ô ö ù û ü ÿ ç ( ) \'')
			modal.querySelector('input').reportValidity()
			return false
		}
		if (this.tabulator.getData().filter(node => node.displayname == zipname).length > 0)
		{
			modal.querySelector('input').setCustomValidity('Un fichier portant ce nom existe déjà')
			modal.querySelector('input').reportValidity()
			return false
		}
		else
		{
			modal.querySelector('input').setCustomValidity('')
			modal.querySelector('input').reportValidity()
			return true
		}
	}
}
