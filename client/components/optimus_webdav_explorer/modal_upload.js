export default class OptimusWebdavExplorerUploadModal
{
	constructor(target, params) 
	{
		this.target = target
		this.server = params.server
		this.root = params.root
		this.path = params.path
		this.tabulator = params.tabulator
		this.fileHandles = params.fileHandles
		this.component = params
		this.overwrite_instruction = 'ask'
		this.counter = 0
	}

	async init()
	{
		await load('/components/optimus_webdav_explorer/modal_upload.html', this.target)

		//CLOSE BUTTON
		modal.querySelector('.modal-card-close').onclick = () => modal.close()

		//CONFLICT MANAGER : OVERWRITE BUTTON
		modal.querySelector('.overwrite').onclick = async () => 
		{
			modal.querySelector('.instruction-block').classList.add('is-hidden')
			modal.querySelector('.fa-spinner').classList.add('fa-spin')
			if (modal.querySelector('.save-instruction').checked)
				this.overwrite_instruction = 'overwrite'
			await this.upload('overwrite')
		}

		//CONFLICT MANAGER : RENAME BUTTON
		modal.querySelector('.rename').onclick = async () => 
		{
			modal.querySelector('.instruction-block').classList.add('is-hidden')
			modal.querySelector('.fa-spinner').classList.add('fa-spin')
			if (modal.querySelector('.save-instruction').checked)
				this.overwrite_instruction = 'rename'
			await this.upload('rename')
		}

		//CONFLICT MANAGER : IGNORE BUTTON
		modal.querySelector('.ignore').onclick = async () => 
		{
			modal.querySelector('.instruction-block').classList.add('is-hidden')
			modal.querySelector('.fa-spinner').classList.add('fa-spin')
			if (modal.querySelector('.save-instruction').checked)
				this.overwrite_instruction = 'ignore'
			await this.upload('ignore')
		}

		//COUNT FILES RECURSIVELY (USER CAN DROP FOLDERS WHICH CONTAINS FILES THAT CAN ONLY BE COUNTED AFTER THE FOLDER IS READ)
		for (let entry of this.fileHandles)
			this.count_files(entry)

		//MAX STARTS AT 1. CANNOT BE SET TO ZERO
		modal.querySelector('.file-progress').max -= 1

		//START THE UPLOAD SEQUENCE
		setTimeout(async () => await this.upload(), 250)
	}

	//FUNCTION CALLED TO COUNT FILES RECURSIVELY
	async count_files(entry)
	{
		if (entry.isDirectory)
		{
			var dirReader = entry.createReader()
			dirReader.readEntries(async entries => 
			{
				for (let entry of entries)
					await this.count_files(entry)
			})
		}
		modal.querySelector('.file-progress').max++
	}

	//MAIN UPLOAD FUNCTION
	async upload(overwrite_instruction)
	{
		if (!overwrite_instruction)
			overwrite_instruction = this.overwrite_instruction

		//COUNTER IS NECESSARY WHEN UPLOADING THROUGH INPUT FILE UPLOAD BUTTON
		if (this.fileHandles.length == 0 || this.counter == this.fileHandles.length)
		{
			this.component.changePath(this.path)
			modal.close()
			return true
		}

		//PROCESS THE NEXT FILE OR FOLDER
		let fileHandle = this.fileHandles[this.counter]

		//FILE PATH IS SET DIFFERENTLY. DROP USES FileSystemFileEntry (with property called fullPath). BUTTON USES fileUploadElement.files (with property called path)
		fileHandle.path = fileHandle.fullPath || ('/' + fileHandle.name)

		//UPDATES THE FILE NAME IN THE MODAL
		modal.querySelector('.filename').innerText = fileHandle.path.slice(1)

		//CONFLICT MANAGER
		if (overwrite_instruction != 'overwrite')
		{
			let already_exists = await fetch(this.server + '/' + encodeURIComponent(this.root) + '/' + encodeURIComponent(this.path) + fileHandle.path.split('/').map(value => encodeURIComponent(value)).join('/'),
				{
					method: 'HEAD',
					credentials: 'include',
				})
				.then(response => response.status == 200)
			if (already_exists)
			{
				//IGNORE = SKIP CURRENT FILE AND MOVE TO THE NEXT ONE
				if (overwrite_instruction == 'ignore')
				{
					if (typeof this.fileHandles.slice == 'function')
						this.fileHandles = this.fileHandles.slice(1)
					else
						this.counter++
					this.upload()
					return true
				}
				//RENAME = ADD SUFFIX TO CURRENT FILE NAME OR FOLDER NAME
				else if (overwrite_instruction == 'rename')
				{
					let filename = fileHandle.path.split('/').pop()
					let radical = filename
					let extension = null
					if (filename.includes('.'))
					{
						extension = filename.split('.').pop()
						radical = filename.split('.').slice(0, -1)
					}
					let new_filename = radical + ' (créé le ' + new Date().toISOString().slice(0, 23).replace(/\D/g, '') + ')' + (extension ? '.' + extension : '')
					fileHandle.path = fileHandle.path.split('/').slice(0, -1).join('/') + '/' + new_filename
					modal.querySelector('.filename').innerText = fileHandle.path.slice(1)
				}
				//ASK = STOPS THE SEQUENCE AND WAIT FOR USER TO PRESS A BUTTON
				else
				{
					modal.querySelector('.instruction-block').classList.remove('is-hidden')
					modal.querySelector('.fa-spinner').classList.remove('fa-spin')
					return true
				}
			}
		}

		//PROCESS DIRECTORIES (CAN ONLY COME FROM DROP)
		if (fileHandle.isDirectory)
		{
			if (overwrite_instruction == 'overwrite')
				await fetch(this.server + '/' + encodeURIComponent(this.root) + '/' + encodeURIComponent(this.path) + fileHandle.path.split('/').map(value => encodeURIComponent(value)).join('/'),
					{
						method: 'DELETE',
						credentials: 'include',
					})
			return fetch(this.server + '/' + encodeURIComponent(this.root) + '/' + encodeURIComponent(this.path) + fileHandle.path.split('/').map(value => encodeURIComponent(value)).join('/'),
				{
					method: 'MKCOL',
					credentials: 'include'
				})
				.then(async () =>
				{
					this.fileHandles = this.fileHandles.filter(file => file != fileHandle)
					var dirReader = fileHandle.createReader()
					await dirReader.readEntries(async entries => 
					{
						for (let entry of entries)
							this.fileHandles.push(entry)
						this.upload()
					})
				})
		}
		//PROCESS FILES (COMING FROM DROP OR BUTTON)
		else
		{
			modal.querySelector('.file-counter').innerText = (modal.querySelector('.file-progress').value + 1) + '/' + modal.querySelector('.file-progress').max

			if (fileHandle.file)
				fileHandle.file(async file =>
				{
					return fetch(this.server + '/' + encodeURIComponent(this.root) + '/' + encodeURIComponent(this.path) + fileHandle.path.split('/').map(value => encodeURIComponent(value)).join('/'),
						{
							method: 'PUT',
							credentials: 'include',
							body: file,
						})
						.then(async response =>
						{
							if (response.status == 201 || response.status == 204)
							{
								this.fileHandles = this.fileHandles.filter(file => file != fileHandle)
								modal.querySelector('.file-progress').value++
								await this.upload()
								return true
							}
							else if (response.status == 403 || response.status == 409)
							{
								optimusToast('Vous n\'avez par les droits suffisants pour ajouter un fichier dans ce dossier', 'is-danger')
								modal.close()
							}
						})
				})
			else
			{
				return fetch(this.server + '/' + encodeURIComponent(this.root) + '/' + encodeURIComponent(this.path) + fileHandle.path.split('/').map(value => encodeURIComponent(value)).join('/'),
					{
						method: 'PUT',
						credentials: 'include',
						body: fileHandle
					})
					.then(async response =>
					{
						if (response.status == 201 || response.status == 204)
						{
							modal.querySelector('.file-progress').value++
							this.counter++
							await this.upload()
							return true
						}
					})
			}
		}
	}
}