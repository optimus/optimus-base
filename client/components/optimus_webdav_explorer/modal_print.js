export default class PrintModal
{
	constructor(target, params)
	{
		this.component = this
		this.target = target
		this.tabulator = params.tabulator
		this.config = params.config
		this.columns = params.columns
	}

	async init()
	{
		await load('/components/optimus_webdav_explorer/modal_print.html', this.target)

		this.tabulator.getColumn('checkboxes').hide()

		modal.querySelector('.modal-card-close').onclick = () => 
		{
			this.tabulator.getColumn('checkboxes').show()
			this.tabulator.getColumn('displayname').show()
			if (store.innerWidth < 768)
			{
				this.tabulator.getColumn('size').hide()
				this.tabulator.getColumn('lastmodified').hide()
			}
			else
			{
				this.tabulator.getColumn('size').show()
				this.tabulator.getColumn('lastmodified').show()
			}
			modal.close()
		}

		modal.querySelector('.title-input').value = 'Fichiers'

		modal.querySelector('.columns-select').onchange = () =>
		{
			modal.querySelector('.columns-container').classList.add('is-hidden')
			if (modal.querySelector('.columns-select').value == 'selected')
			{
				modal.querySelector('.columns-container').classList.remove('is-hidden')
				this.populate_columns()
			}
		}

		modal.querySelector('.print-button').onclick = () => modal.querySelector('form').onsubmit()
		modal.querySelector('form').onsubmit = () => 
		{
			this.tabulator.options.printAsHtml = false
			this.tabulator.options.printStyled = false
			this.tabulator.options.printHeader = modal.querySelector('.title-input').value

			let columns = this.tabulator.getColumns()

			if (modal.querySelector('.columns-select').value == 'all')
			{
				for (let column of columns)
					if (column.getField() != 'checkboxes')
						column.getDefinition().print = true
			}
			else if (modal.querySelector('.columns-select').value == 'selected')
				for (let column of columns)
					if (column.getField() != 'checkboxes')
						column.getDefinition().print = column.isVisible()

			this.tabulator.print()

			return false
		}
	}

	populate_columns()
	{
		clear(modal.querySelector('.columns-container'))

		for (let column of this.columns)
			if (column.field != 'checkboxes')
			{
				let row = modal.querySelector('#optimus_table_column_row').content.firstElementChild.cloneNode(true)
				row.id = column.field

				if (column.visible == true)
					row.querySelector('input').checked = true

				row.querySelector('.switch').onclick = () => 
				{
					this.tabulator.getColumn(column.field).toggle()
				}

				row.querySelector('label').onclick = () => 
				{
					this.tabulator.getColumn(column.field).toggle()
					row.querySelector('input').checked = this.tabulator.getColumn(column.field).isVisible()
				}

				row.querySelector('label').innerText = column.title

				modal.querySelector('.columns-container').appendChild(row)
			}

		import('/libs/sortable.min.js')
			.then(() => 
			{
				Sortable.create(document.querySelector('.columns-container'),
					{
						animation: 200,
						handle: '.fa-grip',
						onEnd: event => 
						{
							if (document.getElementById(event.item.id).previousSibling?.id)
								this.tabulator.moveColumn(event.item.id, document.getElementById(event.item.id).previousSibling.id, true)
							else
								this.tabulator.moveColumn(event.item.id, document.getElementById(event.item.id).nextSibling.id)
						}
					})
			})
	}
}