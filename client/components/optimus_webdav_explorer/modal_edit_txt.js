export default class OptimusWebdavExplorerModalShowCode
{
	constructor(target, params) 
	{
		this.target = target
		this.tabulator = params.tabulator
		this.server = params.server
		this.root = params.root
		this.path = this.tabulator.getSelectedData()[0].path
		this.displayname = this.tabulator.getSelectedData()[0].displayname
	}

	async init()
	{
		await load('/components/optimus_webdav_explorer/modal_edit_txt.html', this.target)

		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.cancel-button').onclick = () => modal.close()

		modal.querySelector('.modal-card-title').innerText = this.path.split('/').pop()

		modal.querySelector('.save-button').onclick = () =>
			fetch(this.server + '/' + encodeURIComponent(this.path) + '/' + encodeURIComponent(this.displayname),
				{
					method: 'PUT',
					credentials: 'include',
					body: modal.querySelector('textarea').value
				})
				.then(response => response.status == 204 && modal.close())

		modal.querySelector('textarea').value = await fetch(this.server + '/' + encodeURIComponent(this.path) + '/' + encodeURIComponent(this.displayname),
			{
				credentials: 'include'
			})
			.then(response => response.text())
	}
}
