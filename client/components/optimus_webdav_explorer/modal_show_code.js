export default class OptimusWebdavExplorerModalShowCode
{
	constructor(target, params) 
	{
		this.target = target
		this.tabulator = params.tabulator
		this.server = params.server
		this.root = params.root
		this.path = this.tabulator.getSelectedData()[0].path
		this.displayname = this.tabulator.getSelectedData()[0].displayname
	}

	async init()
	{
		await load('/components/optimus_webdav_explorer/modal_show_code.html', this.target)

		modal.querySelector('.modal-card-close').onclick = () => modal.close()

		modal.querySelector('.modal-card-title').innerText = this.path.split('/').pop()

		modal.querySelector('pre').style.borderRadius = 0

		if (this.displayname.split('.').pop() == 'css')
			modal.querySelector('code').classList.add('language-css')
		else if (this.displayname.split('.').pop() == 'html')
			modal.querySelector('code').classList.add('language-html')
		else if (this.displayname.split('.').pop() == 'js' || this.displayname.split('.').pop() == 'json')
			modal.querySelector('code').classList.add('language-javascript')
		else if (this.displayname.split('.').pop() == 'php')
			modal.querySelector('code').classList.add('language-php')

		let src = await fetch(this.server + '/' + encodeURIComponent(this.path) + '/' + encodeURIComponent(this.displayname),
			{
				credentials: 'include'
			}
		).then(response => response.text())

		if (this.displayname.split('.').pop() == 'html')
			modal.querySelector('code').innerHTML = src.replaceAll('<', '&lt;')
		else if (this.displayname.split('.').pop() == 'php')
			modal.querySelector('code').innerHTML = src.replaceAll('<?php', '&lt;?php')
		else
			modal.querySelector('code').innerHTML = src

		highlight_code(modal)

		setTimeout(() => modal.querySelector('.modal-card').style.width = modal.querySelector('code').offsetWidth + 40 + 'px', 100)
	}
}
