export default class OptimusWebdavExplorerModalPlayVideo
{
	constructor(target, params) 
	{
		this.target = target
		this.tabulator = params.tabulator
		this.server = params.server
		this.root = params.root
		this.path = this.tabulator.getSelectedData()[0].path
		this.displayname = this.tabulator.getSelectedData()[0].displayname
	}

	async init()
	{
		let video = document.createElement('video')
		video.toggleAttribute('controls')
		video.toggleAttribute('autoplay')
		video.style.maxWidth = main.offsetWidth * 0.9 + 'px'
		video.style.maxHeight = main.offsetHeight * 0.9 + 'px'

		let source = document.createElement('source')
		source.src = this.server + '/' + encodeURIComponent(this.path) + '/' + encodeURIComponent(this.displayname)

		video.appendChild(source)
		this.target.appendChild(video)
		return this
	}
}
