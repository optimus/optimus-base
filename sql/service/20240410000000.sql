USE `server`;

ALTER TABLE `server`.`users` ADD COLUMN IF NOT EXISTS `type` tinyint unsigned NOT NULL DEFAULT 1 AFTER `id`;


USE `common`;

CREATE TABLE IF NOT EXISTS `users_types` (
  `id` tinyint unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

INSERT IGNORE INTO `users_types` VALUES (1, "user");
INSERT IGNORE INTO `users_types` VALUES (2, "business");