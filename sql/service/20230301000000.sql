USE `server`;

CREATE TABLE `allowed_origins` (
  `id` tinyint(4) unsigned NOT NULL AUTO_INCREMENT,
  `origin` varchar(256) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `authorizations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner` int(10) unsigned NOT NULL,
  `user` int(10) unsigned NOT NULL,
  `resource` varchar(128) DEFAULT NULL,
  `read` bit(1) NOT NULL DEFAULT b'0',
  `write` bit(1) NOT NULL DEFAULT b'0',
  `create` bit(1) NOT NULL DEFAULT b'0',
  `delete` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `domains` (
  `id` tinyint(4) unsigned NOT NULL AUTO_INCREMENT,
  `status` bit(1) NOT NULL DEFAULT b'0',
  `domain` varchar(256) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `preferences` (
  `id` varchar(128) NOT NULL,
  `value` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `preferences` VALUES ('branch','stable');

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server` varchar(128) DEFAULT NULL,
  `status` bit(1) NOT NULL DEFAULT b'0',
  `admin` bit(1) NOT NULL DEFAULT b'0',
  `email` varchar(128) NOT NULL,
  `firstname` varchar(64) DEFAULT NULL,
  `lastname` varchar(64) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `displayname` varchar(128) GENERATED ALWAYS AS (ifnull(nullif(trim(concat(ifnull(`firstname`,''),' ',ifnull(`lastname`,''))),''),`email`)) VIRTUAL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `users_servers` (
  `user` int(10) unsigned NOT NULL,
  `server` varchar(128) NOT NULL,
  PRIMARY KEY (`user`,`server`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `users_services` (
  `user` int(10) unsigned NOT NULL,
  `service` varchar(32) NOT NULL,
  PRIMARY KEY (`user`,`service`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE DATABASE IF NOT EXISTS `common` CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

USE `common`;
CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

INSERT INTO `languages` VALUES (1,'anglais');
INSERT INTO `languages` VALUES (2,'français');